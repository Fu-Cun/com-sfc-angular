import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { AppComponent } from './app.component';
import { AppRoutingModule }        from './app-routing.module';
import { LoginModule } from './login/login.module'
//公用文件导入
import { HttpInterceptorService } from "./common/http-interceptor.service";
import { PromptPageModule }    from './common/not-found.component';
import { NotificationModule } from './components/notification/module';





@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    PromptPageModule,BrowserModule,LoginModule,BrowserAnimationsModule,AppRoutingModule,NotificationModule.forRoot()
  ],
  providers: [HttpInterceptorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
