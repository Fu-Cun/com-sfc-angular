import { NgModule }             from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { NotFoundComponent }    from './common/not-found.component';
import { LoginComponent }    from './login/login.component';

/* import { CanDeactivateGuard }       from './can-deactivate-guard.service';
import { AuthGuard }                from './auth-guard.service'; */
import { SelectivePreloadingStrategy } from './common/selective-preloading-strategy';

const appRoutes: Routes = [
  { path: '',   redirectTo: '/saas', pathMatch: 'full' },
  {path: 'login',component: LoginComponent,data:{title:'SAAS登录'}},
  {path: 'saas',loadChildren: './saas/saas.module#SaasModule'},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { preloadingStrategy: SelectivePreloadingStrategy}
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
   /*  CanDeactivateGuard, */
    SelectivePreloadingStrategy
  ]
})
export class AppRoutingModule { }