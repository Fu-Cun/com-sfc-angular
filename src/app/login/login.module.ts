import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';

import {CheckboxModule} from '../components/checkbox/checkbox';
import {ButtonModule} from '../components/button/button';
import {InputTextModule} from '../components/inputtext/inputtext';
import {DialogModule} from '../components/dialog/dialog';
import {GrowlModule} from '../components/growl/growl';


import { ErrorBarComponent } from "../common/error-bar.component";
import { LoginService } from './login.service';
import { LoginComponent } from "./login.component";
import { ForgetpwComponent } from "./forgetpw.component";
import { LoginRoutingModule } from './login-routing.module';
// import { ServiceProviderApplicationModule } from '../service/serviceProviderApplication/serviceProviderApplication.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    LoginRoutingModule,
    CheckboxModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    GrowlModule,
    // ServiceProviderApplicationModule
  ],
  declarations: [
   LoginComponent,ErrorBarComponent,ForgetpwComponent,
  ],
  providers: [ LoginService]
})
export class LoginModule {}