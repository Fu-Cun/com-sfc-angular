import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./login.component";
import { ForgetpwComponent } from "./forgetpw.component";
import { LoginService } from "./login.service";
import { StorageService } from "../common/storage.service";
// import { ServiceProviderRegistrationComponent } from '../service/serviceProviderApplication/serviceProviderRegistration/serviceProviderRegistration.component';
const loginRoutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent , data: { title: '登录' }},
  { path: 'login/forgetpw', component: ForgetpwComponent,data:{title:'忘记密码'}},
  // { path: 'serviceRegistration', component: ServiceProviderRegistrationComponent,data:{title:'服务商注册'}},
];

@NgModule({
  imports: [
    RouterModule.forChild(loginRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers:[LoginService, StorageService]
})
export class LoginRoutingModule { }