import { Injectable }    from '@angular/core';
import { HttpInterceptorService } from '../common/http-interceptor.service';

@Injectable()
export class LoginService {
  constructor(private loginService: HttpInterceptorService) { }
 
  private heroesUrl = "sysSecurity/login/api";
  private loginBaseUrl = 'http://172.18.8.76:80/' // 登录
  private allBaseUrl = 'http://172.18.8.76:80/' // 服务
  private employeeIsExistsByMobileBaseUrl = 'http://172.18.8.79/'
  isLogin = false;
  redirectUrl:string;

  getComDep(params?: any){
    return this.loginService.post('com-dyrs-mtsp-organizationservice/organizationservice/findOrgTreeList/api', params, this.allBaseUrl)
  };//登录
  
  //登录
  login(params: any){
    return this.loginService.post("sysSecurity/login/api", params)
  };
  
  // 登出
  logout(){
    return this.loginService.post('com-dyrs-mtsp-loginservice/signout', {}, this.allBaseUrl)
  }

  getCheckCode(params: any){
    // return this.loginService.post('images/imagecode', params, this.loginBaseUrl)
    return (this.loginService.baseUrl  + "sysSecurity/getImageCode/api?randomStr=" + params)+"&r="+new Date().getTime(); 
  };//获取验证码

  smVerifyCodeSend(params: any){
    return this.loginService.post('com-dyrs-mtsp-loginservice/phone/smVerifyCodeSend/api', params, this.allBaseUrl)
  };//手机登录-发送短信验证码

  employeeIsExistsByMobile(params: any){
    return this.loginService.post('com-dyrs-mtsp-authorityservice/user/isExistsByMobile/api', params, this.allBaseUrl)
  };//验证手机号是否存在

  dic(params?: any){
    return this.loginService.post('com-dyrs-mtsp-dictionaryservice/dictionaryservice/findDictData/api', params, this.allBaseUrl)
  };//获取字典信息

  resetPwd(params: any){
    return this.loginService.post('com-dyrs-mtsp-authorityservice/account/resetPwd/api', params, this.allBaseUrl)
  };//重置密码
  
  //忘记密码 发送短信
  sendMsg(params:any) {
    return this.loginService.post('com-dyrs-mtsp-loginservice/phone/sendSms/api', params, this.allBaseUrl)
  }
  validSms(params:any) {
    return this.loginService.post('com-dyrs-mtsp-loginservice/phone/validSms/api', params, this.allBaseUrl)
  }

  forgetPwd(params: any){
    return this.loginService.post('com-dyrs-mtsp-authorityservice/rest/forgetPwd/api', params, this.allBaseUrl)
  };//重置密码

  followUpLoginRemind(){
    return this.loginService.post('com-dyrs-mtsp-opportunityservice/opportunityservice/clue/timeTaskRemindFollowUpByCode/api', this.allBaseUrl)
  };//线索登录跟进弹窗提醒
}

