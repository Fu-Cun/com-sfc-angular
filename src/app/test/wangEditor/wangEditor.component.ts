import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TimeConfigClass } from '../../common/common-config';
import { StorageService } from "../../common/storage.service";
import { WangEditorService } from './wangEditor.service';

@Component({
  selector: 'wangEditor',
  templateUrl: './wangEditor.component.html',
  // styleUrls: ['./articleList.component.css'],
  providers: [WangEditorService]
})
export class WangEditorComponent implements OnInit {
  //列表
  timeConfig: any = new TimeConfigClass(); // 时间空间初始化

  constructor(private storage: StorageService, private route: ActivatedRoute, private router: Router, private server: WangEditorService) {  }

  title: string = '新建';
  objToSubmit: any = {}; // 提交集合
  objToPost: any = {}; 

  templateArr: any = []; // 模板
  tree: any[] = []; //目录
  selectedFiles: any = [];;

  uploadRrl: String = 'files/upload/api'; // 上传图片地址
  imageArr = [];  //标题照片

  weParam: String = ''; //文本编辑器参数
  

  ngOnInit() {
    this.uploadRrl = this.server.httpService.baseUrl + this.uploadRrl;
    this.initIframeObj(null)
  }
  
  /*
   * 初始化iframe页面
   */
  initIframeObj(iframeObj){
    this.weParam += "&tP=cms_article0content01";
    this.weParam = this.weParam + '&reqUrl=' + this.server.httpService.baseUrl;
    iframeObj = document.getElementById('myrame');
    iframeObj.src += this.weParam;
  }

	//返回
  returnBtn(iframeObj) {
    //this.router.navigate(['../../article'], { relativeTo: this.route });
    
  }
    




}
