import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

// 模块引入
import { WangEditorComponent } from './wangEditor/wangEditor.component';


/**
 * 客户自助APP后台路由参数配置
 */
const mainRoutes: Routes = [
  {path: 'we', component: WangEditorComponent, data:{title:'wangEditor'} },

];
/**
 * 用户管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CmsRoutingModule { }
