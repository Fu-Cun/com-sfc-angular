export class User {
    isCheckCode: boolean | string;
    phoneVerifyCode: boolean | string;
    accountPassword: string;
    accountType: number;
    randomStr: any;
    code: string;
    constructor(str?: string) {
        if (str) {
            this.phoneVerifyCode = false;
            this.isCheckCode = false;
        }
    }
}
export class ErrorMsg {
    code: number;
    msg: string
}

export class Phone {
    isCheckCode: boolean | string; // 数字验证码
    phoneVerifyCode: boolean | string; // 手机登录短信验证码
    accountType: number; // 登录类型
    code: string; // 手机号
    randomStr: any;
    constructor(str?: string) {

    }
}
export class PhoneErrorMsg {
    code: number;
    msg: string
}

export interface AdvanceItem {
    keyword: string; //后台需要的 key 值;
    title?: string;// 标题
    type: string;
    dataKey?:string;//下拉框 datakey
    optionLabel?:string //下拉框显示关键字
    data?:any;
}

export interface SaveLabel {
    name: string; //标签名称
    current: boolean;// 选中状态
    data:AdvanceItem[]; // 绑定的数组
}