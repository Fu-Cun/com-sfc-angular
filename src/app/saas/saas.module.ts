import { NgModule }       from '@angular/core';

import { CommonModule }   from '@angular/common';

import { PanelMenuModule } from "../components/panelmenu/panelmenu";

import { DialogMessageModule } from '../components/dialogmessage/dialogmessage';

import { GrowlModule } from '../components/growl/growl';

import { OverlayPanelModule } from '../components/overlaypanel/overlaypanel';

import { ConfirmDialogModule } from '../components/confirmdialog/confirmdialog'

import { SaasRoutingModule } from "./saas-routing.module";

import { SaasComponent } from "./saas.component";

import { ConfirmationService } from '../components/common/confirmationservice';

import { MessageService } from '../components/common/messageservice';

import { StorageService } from "../common/storage.service";

import { SocketmessageService } from "../common/socketmessage.service";

import { SaasService } from './saas.service';
import { PromptPageModule }    from '../common/not-found.component';

@NgModule({
  imports: [
    CommonModule,
    GrowlModule,
    DialogMessageModule,
    PanelMenuModule,
    OverlayPanelModule,
    SaasRoutingModule,ConfirmDialogModule,PromptPageModule
  ],
  declarations: [
    SaasComponent,
  ],
  providers: [ StorageService,MessageService,ConfirmationService,SocketmessageService,SaasService]
})
export class SaasModule {}
