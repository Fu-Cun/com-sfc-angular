import { SaasComponent } from './saas.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router, Route } from '@angular/router';
import { StorageService } from "../common/storage.service";
import { NotFoundComponent, HomePageComponent } from '../common/not-found.component';
const SaasRoutes: Routes = [
  {
    path: '',
    component: SaasComponent,
    canActivate: [StorageService],
    canActivateChild: [StorageService],
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full', },
      { path: 'home',component: HomePageComponent, data: {title: '主页'}},
      // { path: 'sys', loadChildren: '../sys/sys.module#SysModule', data: { title: '系统管理' } },
      // { path: 'sysset', loadChildren: '../sysset/sysset.module#SysSetModule', data: { title: '系统设置' } },
      // { path: 'clue', loadChildren: '../clue/clue.module#ClueModule', data: { title: '线索管理' } },
      // { path: 'house', loadChildren: '../house/house.module#HouseModule', data: { title: '楼盘管理' } },
      // { path: 'reactive', loadChildren: '../reactive/reactive.module#ReactiveModule', data: { title: '楼盘管理' } },
      // { path: 'business', loadChildren: '../business/business.module#BusinessModule', data: { title: '商机管理' } },
      // { path: 'finance', loadChildren: '../finance/finance.module#FinanceModule', data: { title: '发票管理' } },
      // { path: 'contract', loadChildren: '../contract/contract.module#ContractModule', data: { title: '合同管理' } },
      // { path: 'amount', loadChildren: '../amount/amount.module#AmountModule', data: { title: '量房管理' } },
      // { path: 'customer', loadChildren: '../customer/labelmanagement/customermanage.module#CustomermanageModule', data: { title: '客户' } },
      // { path: 'orders', loadChildren: '../orders/orders.module#OrdersModule' },
      //{ path: 'engineer', loadChildren: 'app/engineer/engineer.module#EngineerModule', data: { title: '工程管理' } },
      // { path: 'risk', loadChildren: 'app/risk/risk.module#RiskModule', data: { title: '风控管理' } },
      // { path: 'basicservice', loadChildren: '../basicservice/basicservice.module#BasicserviceModule' },
      // { path: 'account', loadChildren: '../account/account/account.module#AccountModule', data: { title: '账号设置' } },
      // { path: 'service', loadChildren: '../service/service.module#ServiceModule', data: { title: '服务商信息管理' } },
      // { path: 'customerSystem', loadChildren: '../customerSystem/customerSystem.module#CustomerSystemModule', data: { title: '客服系统' } },
      // { path: 'quotation', loadChildren: '../quotation/quotation.module#QuotationModule', data: { title: '报价管理' } },
      // { path: 'preferential', loadChildren: '../preferential/preferential.module#PreferentialModule', data: { title: '优惠管理' } },
      // { path: 'moor', loadChildren: '../moor/moor.module#MoorModule', data: { title: '线索管理' } },
      { path: 'cms', loadChildren: 'app/cms/main.module#CmsModule', data: { title: '工程管理' } },
      { path: 'test', loadChildren: 'app/test/main.module#CmsModule', data: { title: 'test' } },
      { path: '**', component: NotFoundComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(SaasRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [StorageService]
})
export class SaasRoutingModule {
  constructor(private router: Router) {
    // console.log(router.config.filter(val=>val.path == 'saas')[0]['_loadedConfig'])


  }
}

