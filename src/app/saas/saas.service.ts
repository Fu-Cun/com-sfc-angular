import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../common/http-interceptor.service';

@Injectable()
export class SaasService {

  constructor(private service: HttpInterceptorService) { }

  expireCluei(data) {
    let param = {
      clueCode: data.data.data.clueCode,
      type: "expire"
    }
    this.service.post("com-dyrs-mtsp-opportunityservice/opportunityservice/clue/addoperationrecord/api", param);
    return;
  }

  refuseClue(data) {
    let param = {
      clueCode: data.data.data.clueCode,
      type: "refuse"
    }
    return this.service.post("com-dyrs-mtsp-opportunityservice/opportunityservice/clue/addoperationrecord/api", param);
  }

  acceptClue(data) {
    let param = {
      clueCode: data.data.data.clueCode
    }
    return this.service.post("com-dyrs-mtsp-opportunityservice/opportunityservice/clue/acceptclue/api", param);
  }
  //resetRoleByUser
  resetRoleByUser(param) {
    return this.service.post("com-dyrs-mtsp-authorityservice/account/resetRoleByUser/api", param);
  }
  //查看我的线索
  seeClues(params: any) {
    return this.service.post("com-dyrs-mtsp-opportunityservice/opportunityservice/clue/findClueByCode/api", params);
  }
  //拨打电话
  dialingClueKf(params: any) {
    return this.service.post("com-dyrs-mtsp-opportunityservice/opportunityservice/external/dialingPhone/api", params);
  }
  //我的商机列表查看页面
  myBusinessDetail(params: any) {
    return this.service.post("com-dyrs-mtsp-businessopportunityservice/businessopportunityservice/myBusiness/findByCode/api", params);
  }
  //拨打电话
  dialingBusKf(params: any) {
    return this.service.post("com-dyrs-mtsp-businessopportunityservice/businessopportunityservice/myBusiness/dialingPhone/api", params);
  }
}
