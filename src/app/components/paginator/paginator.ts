import {NgModule,Component,ElementRef,Input,Output,SimpleChange,EventEmitter} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from '../dropdown/dropdown';
import {SelectItem} from '../common/selectitem';
import {InputTextModule} from '../inputtext/inputtext';

@Component({
    selector: 'p-paginator',
    template: `
        <div [class]="styleClass" style="text-align: right;padding: 10px 20px;" [ngStyle]="style" [ngClass]="'bg-none border-none ui-paginator ui-widget ui-widget-header ui-unselectable-text'"
            *ngIf="alwaysShow ? true : (pageLinks && pageLinks.length > 1)">
            <span href="#" class=" ui-paginator-element ui-corner-all dialog-hide">
                <span >共 {{getPageCount()}} 页</span>
            </span>
            <span class="m-table ui-paginator-element ui-corner-all dialog-hide">
                <span >每页显示</span>
            </span>
            <p-dropdown [options]="rowsPerPageItems" styleClass="text-center dialog-hide" [(ngModel)]="rows" *ngIf="rowsPerPageOptions" 
            (onChange)="onRppChange($event)" [lazy]="false" [autoWidth]="false"></p-dropdown>
            <span class="m-table ui-paginator-element ui-corner-all dialog-hide">
             <span >行</span>
            </span>
            <a href="#" title="首页" class="ui-paginator-first ui-paginator-element ui-state-default bg-d4d1d1 ui-corner-all"
                    (click)="changePageToFirst($event)" [ngClass]="{'ui-state-disabled':isFirstPage()}" [tabindex]="isFirstPage() ? -1 : null">
                <span class="fa fa-step-backward"></span>
            </a>
            <a href="#" class="bg-eaeaea ui-paginator-prev ui-paginator-element bg-d4d1d1 ui-state-default ui-corner-all"
                    (click)="changePageToPrev($event)" [ngClass]="{'ui-state-disabled':isFirstPage()}" [tabindex]="isFirstPage() ? -1 : null">
                <span class="fa fa-play fa-rotate-180"></span>
            </a>
            <span class="ui-paginator-pages">
                <a href="#" *ngFor="let pageLink of pageLinks" class="ui-paginator-page ui-paginator-element ui-state-default ui-corner-all"
                    (click)="onPageLinkClick($event, pageLink - 1)" [ngClass]="{'ui-state-active': (pageLink-1 == getPage())}">{{pageLink}}</a>
            </span>
            <a href="#" class="bg-eaeaea ui-paginator-next ui-paginator-element bg-d4d1d1 ui-state-default ui-corner-all"
                    (click)="changePageToNext($event)" [ngClass]="{'ui-state-disabled':isLastPage()}" [tabindex]="isLastPage() ? -1 : null">
                <span class="fa fa-play"></span>
            </a>
            <a href="#" title="尾页" class="ui-paginator-last ui-paginator-element bg-d4d1d1 ui-state-default ui-corner-all"
                    (click)="changePageToLast($event)" [ngClass]="{'ui-state-disabled':isLastPage()}" [tabindex]="isLastPage() ? -1 : null">
                <span class="fa fa-step-forward"></span>
            </a>
            <span class="m-table ui-paginator-element ui-corner-all">
             <span >跳转到</span>
            </span>
            <input id="input" type="text"  class="p-input" style="width: 50px" size="30" pInputText (keyUp.enter)="keyupEnter()" [(ngModel)]="pageNo">
        <span class="m-table ui-paginator-element ui-corner-all">
         <span >页</span>
        </span>
            
        </div>
    `,
    styles:[`.m-table{margin:0 15px} a.ui-paginator-element{outline: none;padding: .375em .625em !important;border:none !important} .ui-paginator-pages a{margin:0 5px !important}`]
})
export class Paginator {

    @Input() pageLinkSize: number = 5;

    @Output() onPageChange: EventEmitter<any> = new EventEmitter();

    @Input() style: any;

    @Input() styleClass: string;

    @Input() alwaysShow: boolean = true;

    pageLinks: number[];

    pageNo:any = 1;

    _totalRecords: number = 0;

    _first: number = 0;

    _rows: number = 0;
    
    _rowsPerPageOptions: number[];
    
    rowsPerPageItems: SelectItem[];

    @Input() get totalRecords(): number {
        return this._totalRecords;
    }

    set totalRecords(val:number) {
        this._totalRecords = val;
        this.updatePageLinks();
    }

    @Input() get first(): number {
        return this._first;
    }

    set first(val:number) {
        this._first = val;
        this.updatePageLinks();
        this.pageNo = this.getPage()?this.getPage()+1:1;
    }

    @Input() get rows(): number {
        return this._rows;
    }

    set rows(val:number) {
        this._rows = val;
        this.updatePageLinks();
    }
    
    @Input() get rowsPerPageOptions(): number[] {
        return this._rowsPerPageOptions;
    }

    set rowsPerPageOptions(val:number[]) {
        this._rowsPerPageOptions = val;
        if(this._rowsPerPageOptions) {
            this.rowsPerPageItems = [];
            for(let opt of this._rowsPerPageOptions) {
                this.rowsPerPageItems.push({label: String(opt), value: opt});
            }
        }
    }

    isFirstPage() {
        return this.getPage() === 0;
    }

    isLastPage() {
        return this.getPage() === this.getPageCount() - 1;
    }

    getPageCount() {
        return Math.ceil(this.totalRecords/this.rows)||1;
    }

    calculatePageLinkBoundaries() {
        let numberOfPages = this.getPageCount(),
        visiblePages = Math.min(this.pageLinkSize, numberOfPages);

        //calculate range, keep current in middle if necessary
        let start = Math.max(0, Math.ceil(this.getPage() - ((visiblePages) / 2))),
        end = Math.min(numberOfPages - 1, start + visiblePages - 1);

        //check when approaching to last page
        var delta = this.pageLinkSize - (end - start + 1);
        start = Math.max(0, start - delta);

        return [start, end];
    }

    updatePageLinks() {
        this.pageLinks = [];
        let boundaries = this.calculatePageLinkBoundaries(),
        start = boundaries[0],
        end = boundaries[1];

        for(let i = start; i <= end; i++) {
            this.pageLinks.push(i + 1);
        }
    }

    changePage(p :number) {
        var pc = this.getPageCount();
        if(p >= 0 && p < pc) {
            this.first = this.rows * p;
            var state = {
                page: p,
                first: this.first,
                rows: this.rows,
                pageCount: pc
            };
            this.updatePageLinks();


            this.onPageChange.emit(state);
        }
    }

    getPage(): number {
        return Math.floor(this.first / this.rows);
    }

    keyupEnter(){
        let pageNo = Math.trunc(this.pageNo);
        if(!pageNo || pageNo<0){
            this.pageNo = 1;
        }else if(pageNo>=this.getPageCount()){
            this.pageNo = this.getPageCount();
        }
        this.changePage(this.pageNo-1)
    }

    changePageToFirst(event) {
      if(!this.isFirstPage()){
          this.changePage(0);
      }

      event.preventDefault();
    }

    changePageToPrev(event) {
        this.changePage(this.getPage() - 1);
        event.preventDefault();
    }

    changePageToNext(event) {
        this.changePage(this.getPage()  + 1);
        event.preventDefault();
    }

    changePageToLast(event) {
      if(!this.isLastPage()){
          this.changePage(this.getPageCount() - 1);
      }

      event.preventDefault();
    }

    onPageLinkClick(event, page) {
        this.changePage(page);
        event.preventDefault();
    }

    onRppChange(event) {
        this.changePage(this.getPage());
    }
}

@NgModule({
    imports: [CommonModule,DropdownModule,FormsModule,InputTextModule],
    exports: [Paginator,DropdownModule,FormsModule],
    declarations: [Paginator]
})
export class PaginatorModule { }
