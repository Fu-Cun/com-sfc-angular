import {NgModule,Component,Input,Output,EventEmitter,ElementRef,ContentChild,AfterViewChecked,ViewChild} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule,Footer} from '../common/shared';
import {BlockableUI} from '../common/blockableui';
import {trigger,state,style,transition,animate} from '@angular/animations';
import {DomHandler} from '../dom/domhandler';
import { BlockUIModule } from '../blockui/blockui'
import {InputTextModule} from '../inputtext/inputtext';

let idx: number = 0;

@Component({
    selector: 'p-panel',
    template: `
        <div #panelEle [attr.id]="id" [ngClass]="'ui-panel ui-widget ui-widget-content ui-corner-all border-eaeaea relative'" [ngStyle]="style" [class]="styleClass">
            <div class="ui-panel-titlebar ui-widget-header ui-helper-clearfix ui-corner-all" [ngClass]="headerClass" *ngIf="showHeader">
                <span class="ui-panel-title border-l-1a8fe8 p-l-8 line-height-30" *ngIf="header">{{header}}</span>
                <ng-content select="p-header"></ng-content>
                <div class="inline-block width-320 pull-right" *ngIf="search">
                    <div class="relative">
                    <input maxlength='20' (change)="searchTextChange.emit(inpuEle.value.trim())"  [value]="searchText || ''" type="text" #inpuEle pInputText class="width-full p-input height-30" (keyUp.enter) ="onSearch.emit(inpuEle.value)" [placeholder]="search">     
                        <span class="icon-search-right" (click)="onSearch.emit(inpuEle.value)"></span>
                    </div>
                </div>
                <a *ngIf="toggleable" [attr.id]="id + '-label'" class="ui-panel-titlebar-icon ui-panel-titlebar-toggler ui-corner-all ui-state-default" href="#"
                    (click)="toggle($event)" [attr.aria-controls]="id + '-content'" role="tab" [attr.aria-expanded]="!collapsed">
                    <span [class]="collapsed ? 'fa fa-fw ' + expandIcon : 'fa fa-fw ' + collapseIcon"></span>
                </a>
            </div>
            <div [attr.id]="id + '-content'" class="ui-panel-content-wrapper" [@panelContent]="collapsed ? 'hidden' : 'visible'" (@panelContent.done)="onToggleDone($event)"
                [ngClass]="{'ui-panel-content-wrapper-overflown': collapsed||animating}"
                role="region" [attr.aria-hidden]="collapsed" [attr.aria-labelledby]="id + '-label'">
                <div class="ui-panel-content p-20 ui-content-scrool ui-widget-content" [ngClass]="contentClass" #content>
                    <ng-content></ng-content>
                </div>
                
                <div class="ui-panel-footer ui-widget-content"  *ngIf="footerFacet">
                    <ng-content select="p-footer"></ng-content>
                   
                </div>
            </div>
        </div>
        <p-blockUI [target]="panelEle" [blocked]="blocked">
        </p-blockUI>
    `,
    animations: [
        trigger('panelContent', [
            state('hidden', style({
                height: '0'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible <=> hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ],
    providers: [DomHandler]
})
export class Panel implements BlockableUI,AfterViewChecked {

    @Input() showScroll: boolean;

    @Input() blocked: boolean;

    @Input() toggleable: boolean;

    @Input() searchText: string;

    @Input() header: string;

    @Input() search: string;

    @Input() collapsed: boolean = false;
    
    @Input() style: any;
    
    @Input() styleClass: string;

    @Input() headerClass: string;

    @Input() contentClass: string;
    
    @Input() expandIcon: string = 'fa-plus';
    
    @Input() collapseIcon: string = 'fa-minus';

    @Input() marginHeight: number = 0;
  
    @Input() showHeader: boolean = true;
    
    @Output() collapsedChange: EventEmitter<any> = new EventEmitter();

    @Output() searchTextChange: EventEmitter<any> = new EventEmitter();

    @Output() onSearch: EventEmitter<any> = new EventEmitter();

    @Output() onBeforeToggle: EventEmitter<any> = new EventEmitter();

    @Output() onAfterToggle: EventEmitter<any> = new EventEmitter();
    
    @ContentChild(Footer) footerFacet;

    @ViewChild('content') contentEle;
    
    animating: boolean;
    
    id: string = `ui-panel-${idx++}`;
    
    constructor(private el: ElementRef,public domHandler: DomHandler) {}

    ngAfterViewChecked() {
        if(!this.showScroll){return}
        let maxHeight = this.domHandler.getViewport().height;
        let height = this.contentEle.nativeElement.offsetTop;
        this.contentEle.nativeElement.style.maxHeight = maxHeight - height - 20 - this.marginHeight  + 'px';
        this.contentEle.nativeElement.style.overflowY = 'auto';
    }
    
    toggle(event) {
        if(this.animating) {
            return false;
        }
        
        this.animating = true;
        this.onBeforeToggle.emit({originalEvent: event, collapsed: this.collapsed});
        
        if(this.toggleable) {
            if(this.collapsed)
                this.expand(event);
            else
                this.collapse(event);
        }
        
        event.preventDefault();
    }
    
    expand(event) {
        this.collapsed = false;
        this.collapsedChange.emit(this.collapsed);
    }
    
    collapse(event) {
        this.collapsed = true;
        this.collapsedChange.emit(this.collapsed);
    }
    
    getBlockableElement(): HTMLElement {
        return this.el.nativeElement.children[0];
    }
    
    onToggleDone(event: Event) {
        this.animating = false;
        this.onAfterToggle.emit({originalEvent: event, collapsed: this.collapsed});
    }

}

@NgModule({
    imports: [CommonModule,InputTextModule,BlockUIModule],
    exports: [Panel,SharedModule],
    declarations: [Panel]
})
export class PanelModule { }
