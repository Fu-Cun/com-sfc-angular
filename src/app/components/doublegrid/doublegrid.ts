import {NgModule,Component,Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Header,Footer,PrimeTemplate,SharedModule} from '../common/shared';


@Component({
    selector: 'p-doubleTable',
    styles:[`
    .td-class{border-left:1px solid #d7d8da;border-top:1px solid #d7d8da;height:55px;color:#858585;padding:.5em 1em !important}
    .tb-class{border-bottom:1px solid #d7d8da;border-right:1px solid #d7d8da;text-align:left}
    .bg-title{background:#f7f8fa}
    `],
    template: `
<div [ngClass]="{'tb-class ui-g ui-datagrid ui-widget':true,'td-class text-center flex-td':!value}" [ngStyle]="style" [class]="styleClass">
    <div *ngIf="value" class="width-full ui-g">

        <div *ngFor="let item of rowClos;index as index;even as even;odd as odd" class="ui-g-6 p-0 clearfix ">
            <div [style.width.px]="titleWidth" class="flex-td td-class pull-left bg-title">
                <span class="ellipsis-td-2">
                {{item.title}}  
                </span>
            </div>
            <div [style.width]="'calc(100% - '+ (titleWidth + 2) +'px)'" [title]="value[item.field]" class="td-class pull-left flex-td " [ngStyle]="item.style" [ngClass]="item.class">
                <span class="ellipsis-td-2" (click)="item.click?item.click(item.field,value):''">
                   {{item.fun?item.fun(item.field,value):value[item.field]}}  
                </span>

            </div>
        </div>

    </div>
    <div *ngIf="!value" class="width-full text-center">{{emptyMessage}}</div>

</div>
    `
})
export class DoubleTabel {

    @Input() emptyMessage: string = '没有搜到您想要的数据';

    @Input() style:any;

    @Input() styleClass:string;

    @Input() titleWidth:number = 200;

    @Input() rowClos:any = [];

    _value: any;


    @Input() get value(): any {
        return this._value;
    }

    set value(val:any) {
        this._value = val;
    }

}

@NgModule({
    imports: [CommonModule,SharedModule],
    exports: [DoubleTabel,SharedModule],
    declarations: [DoubleTabel]
})
export class DoubleTableModule { }
