import {NgModule,Component,Input,Output,EventEmitter} from '@angular/core';
import {CommonModule} from '@angular/common';

@Component({
    selector: 'p-label',
    template: `
    <div class="ui-chips-token p-label ui-corner-all {{styleClass}}" [ngStyle]="{'border-color':styleColor,'color':styleColor,'background-color':active?styleColor:colorObj[styleColor]}" [ngClass]="{'active':active}" (click)="select($event)">
        <span class="p-label-icon fa " [ngClass]="icon" [ngStyle]="{'background':active?colorObj[styleColor]:styleColor,'color':active?styleColor:'#fff'}" *ngIf="showClose" (click)="remove($event)" ></span>
        <span class="ui-chips-token-label" [style.margin-right.px]="showClose?20:0">{{label || ''}}</span>
    </div>
    `,
    styles:[`
    .p-label{cursor: pointer;padding: 3px 10px;border-radius: 16px !important;border:1px solid #1a91eb ;font-size: 12px !important;color: #1a91eb}
    .p-label.active{color: #fff !important}
    .p-label-icon{cursor: pointer;position: absolute;width: 18px;height: 18px;border-radius: 50%;text-align: center;line-height: 18px !important;right: 3px !important;top: 50% !important;transform: translateY(-50%);}
    `]
})
export class UILabel {

    @Input() active: boolean;
    
    @Input() label: string;

    @Input() styleClass: string;

    @Input() icon: string = 'fa-close';

    @Input() styleColor: string = '#1a91eb';

    @Input() showClose: boolean = true;

    @Input() controlSelect: boolean;

    @Output() onClose: EventEmitter<any> = new EventEmitter();

    @Output() onSelect: EventEmitter<any> = new EventEmitter();

    colorObj:any = {'#1a91eb':'#def1ff','#02bb5f':'#dcffee','#ff7676':'#ffe4e4','#ffb54a':'#fff3e3',}
    
    remove(event){
        this.onClose.emit();
        event.stopPropagation();
    }

    select(event){
        if(this.active && this.controlSelect) return
        this.onSelect.emit();
        event.stopPropagation();
    }
    
}



@NgModule({
    imports: [CommonModule],
    exports: [UILabel],
    declarations: [UILabel]
})
export class LabelModule { }