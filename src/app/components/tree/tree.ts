import {NgModule,Component,Input,AfterContentInit,OnDestroy,Output,EventEmitter,OnInit,ElementRef,EmbeddedViewRef,ViewContainerRef,ViewChild,
    ContentChildren,QueryList,TemplateRef,Inject,forwardRef,Host} from '@angular/core';
import {Optional} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NG_VALUE_ACCESSOR,ControlValueAccessor} from '@angular/forms';

import {TreeNode} from '../common/treenode';
import {SharedModule} from '../common/shared';
import {PrimeTemplate} from '../common/shared';
import {TreeDragDropService} from '../common/treedragdropservice';
import {Subscription}   from 'rxjs/Subscription';
import { OverlayPanelModule } from "../overlaypanel/overlaypanel";

export const INPUTSWITCH_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TreeSelect),
    multi: true
  };

@Component({
    selector: 'p-treeNodeTemplateLoader',
    template: ``
})
export class TreeNodeTemplateLoader implements OnInit, OnDestroy {

    @Input() node: any;

    @Input() template: TemplateRef<any>;

    view: EmbeddedViewRef<any>;

    constructor(public viewContainer: ViewContainerRef) {}

    ngOnInit() {
        this.view = this.viewContainer.createEmbeddedView(this.template, {
            '\$implicit': this.node
        });
    }

    ngOnDestroy() {
        this.view.destroy();
    }
}

@Component({
    selector: 'p-treeNode',
    template: `
        <ng-template [ngIf]="node">
            <li *ngIf="tree.droppableNodes" class="ui-treenode-droppoint" [ngClass]="{'ui-treenode-droppoint-active ui-state-highlight':draghoverPrev}"
            (drop)="onDropPoint($event,-1)" (dragover)="onDropPointDragOver($event)" (dragenter)="onDropPointDragEnter($event,-1)" (dragleave)="onDropPointDragLeave($event)"></li>
            <li *ngIf="!tree.horizontal" [ngClass]="['ui-treenode',node.styleClass||'', isLeaf() ? 'ui-treenode-leaf': '']">
                <div class="ui-treenode-content ellipsis-td" (click)="onNodeClick($event)" (contextmenu)="onNodeRightClick($event)" (touchend)="onNodeTouchEnd()"
                    (drop)="onDropNode($event)" (dragover)="onDropNodeDragOver($event)" (dragenter)="onDropNodeDragEnter($event)" (dragleave)="onDropNodeDragLeave($event)"
                    [ngClass]="{'ui-treenode-selectable':tree.selectionMode && node.selectable !== false,'ui-treenode-dragover':draghoverNode, 'ui-treenode-content-selected':isSelected()}" [draggable]="tree.draggableNodes" (dragstart)="onDragStart($event)" (dragend)="onDragStop($event)">
                    <span class="ui-tree-toggler  fa fa-fw" [ngClass]="{'fa-caret-right':!node.expanded,'fa-caret-down':node.expanded}"
                            (click)="toggle($event)"></span
                    ><div class="ui-chkbox" *ngIf="tree.selectionMode == 'checkbox'"><div class="ui-chkbox-box ui-widget ui-corner-all ui-state-default">
                        <span class="ui-chkbox-icon ui-clickable fa" 
                            [ngClass]="{'fa-check':isSelected(),'fa-minus':node.partialSelected}"></span></div></div
                    ><span [class]="getIcon()" *ngIf="node.icon||node.expandedIcon||node.collapsedIcon"></span
                    ><span class="ui-treenode-label ui-corner-all" 
                        [ngClass]="{'ui-state-highlight':isSelected()}">
                            <span *ngIf="!tree.getTemplateForNode(node)" [ngClass]="node?.searchClass" [title]="node.label">{{node.label}}</span>
                            <span *ngIf="tree.getTemplateForNode(node)">
                                <p-treeNodeTemplateLoader [node]="node" [template]="tree.getTemplateForNode(node)"></p-treeNodeTemplateLoader>
                            </span>
                    </span>
                </div>
                <ul class="ui-treenode-children" style="display: none;" *ngIf="node.children && node.expanded" [style.display]="node.expanded ? 'block' : 'none'">
                    <p-treeNode *ngFor="let childNode of node.children;let firstChild=first;let lastChild=last; let index=index" [node]="childNode" [parentNode]="node"
                        [firstChild]="firstChild" [lastChild]="lastChild" [index]="index" ></p-treeNode>
                </ul>
            </li>
            <li *ngIf="tree.droppableNodes&&lastChild" class="ui-treenode-droppoint" [ngClass]="{'ui-treenode-droppoint-active ui-state-highlight':draghoverNext}"
            (drop)="onDropPoint($event,1)" (dragover)="onDropPointDragOver($event)" (dragenter)="onDropPointDragEnter($event,1)" (dragleave)="onDropPointDragLeave($event)"></li>
            <table *ngIf="tree.horizontal" [class]="node.styleClass">
                <tbody>
                    <tr>
                        <td class="ui-treenode-connector" *ngIf="!root">
                            <table class="ui-treenode-connector-table">
                                <tbody>
                                    <tr>
                                        <td [ngClass]="{'ui-treenode-connector-line':!firstChild}"></td>
                                    </tr>
                                    <tr>
                                        <td [ngClass]="{'ui-treenode-connector-line':!lastChild}"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td class="ui-treenode" [ngClass]="{'ui-treenode-collapsed':!node.expanded}">
                            <div class="ui-treenode-content ui-state-default ui-corner-all ellipsis-td" 
                                [ngClass]="{'ui-treenode-selectable':tree.selectionMode,'ui-state-highlight':isSelected()}" (click)="onNodeClick($event)" (contextmenu)="onNodeRightClick($event)"
                                (touchend)="onNodeTouchEnd()">
                                <span class="ui-tree-toggler fa fa-fw" [ngClass]="{'fa-plus':!node.expanded,'fa-minus':node.expanded}" *ngIf="!isLeaf()"
                                        (click)="toggle($event)"></span
                                ><span [class]="getIcon()" *ngIf="node.icon||node.expandedIcon||node.collapsedIcon"></span
                                ><span class="ui-treenode-label ui-corner-all">
                                        <span *ngIf="!tree.getTemplateForNode(node)" [title]="node.label">{{node.label}}</span>
                                        <span *ngIf="tree.getTemplateForNode(node)">
                                            <p-treeNodeTemplateLoader [node]="node" [template]="tree.getTemplateForNode(node)"></p-treeNodeTemplateLoader>
                                        </span>
                                </span>
                            </div>
                        </td>
                        <td class="ui-treenode-children-container" *ngIf="node.children && node.expanded" [style.display]="node.expanded ? 'table-cell' : 'none'">
                            <div class="ui-treenode-children">
                                <p-treeNode *ngFor="let childNode of node.children;let firstChild=first;let lastChild=last;" [node]="childNode" 
                                        [firstChild]="firstChild" [lastChild]="lastChild"></p-treeNode>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </ng-template>
    `
})
export class UITreeNode implements OnInit {

    static ICON_CLASS: string = 'ui-treenode-icon fa fa-fw';

    @Input() node: TreeNode;

    @Input() parentNode: TreeNode;

    @Input() root: boolean;

    @Input() index: number;

    @Input() firstChild: boolean;

    @Input() lastChild: boolean;

    constructor(@Inject(forwardRef(() => Tree)) public tree:Tree) {}

    draghoverPrev: boolean;

    draghoverNext: boolean;

    draghoverNode: boolean

    ngOnInit() {
        this.node.parent = this.parentNode;
    }

    getIcon() {
        let icon: string;

        if(this.node.icon)
            icon = this.node.icon;
        else
            icon = this.node.expanded && this.node.children && this.node.children.length ? this.node.expandedIcon : this.node.collapsedIcon;

        return UITreeNode.ICON_CLASS + ' ' + icon;
    }

    isLeaf() {
        return this.node.leaf == false ? false : !(this.node.children&&this.node.children.length);
    }

    toggle(event: Event) {
        if(this.node.expanded)
            this.tree.onNodeCollapse.emit({originalEvent: event, node: this.node});
        else
            this.tree.onNodeExpand.emit({originalEvent: event, node: this.node});

        this.node.expanded = !this.node.expanded
    }

    onNodeClick(event: MouseEvent) {
        this.tree.onNodeClick(event, this.node);
    }

    onNodeTouchEnd() {
        this.tree.onNodeTouchEnd();
    }

    onNodeRightClick(event: MouseEvent) {
        this.tree.onNodeRightClick(event, this.node);
    }

    isSelected() {
        return this.tree.isSelected(this.node);
    }

    onDropPoint(event: Event, position: number) {
        event.preventDefault();
        let dragNode = this.tree.dragNode;
        let dragNodeIndex = this.tree.dragNodeIndex;
        let dragNodeScope = this.tree.dragNodeScope;
        let isValidDropPointIndex = this.tree.dragNodeTree === this.tree ? (position === 1 || dragNodeIndex !== this.index - 1) : true;

        if(this.tree.allowDrop(dragNode, this.node, dragNodeScope) && isValidDropPointIndex) {
            let newNodeList = this.node.parent ? this.node.parent.children : this.tree.value;
            this.tree.dragNodeSubNodes.splice(dragNodeIndex, 1);
            let dropIndex = this.index;

            if(position < 0) {
                dropIndex = (this.tree.dragNodeSubNodes === newNodeList) ? ((this.tree.dragNodeIndex > this.index) ? this.index : this.index - 1) : this.index;
                newNodeList.splice(dropIndex, 0, dragNode);
            }
            else {
				dropIndex = newNodeList.length;
                newNodeList.push(dragNode);
            }

            this.tree.dragDropService.stopDrag({
                node: dragNode,
                subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
                index: dragNodeIndex
            });

            this.tree.onNodeDrop.emit({
                originalEvent: event,
                dragNode: dragNode,
                dropNode: this.node,
                dropIndex: dropIndex
            });
        }

        this.draghoverPrev = false;
        this.draghoverNext = false;
    }

    onDropPointDragOver(event) {
        event.dataTransfer.dropEffect = 'move';
        event.preventDefault();
    }

    onDropPointDragEnter(event: Event, position: number) {
        if(this.tree.allowDrop(this.tree.dragNode, this.node, this.tree.dragNodeScope)) {
            if(position < 0)
                this.draghoverPrev = true;
            else
                this.draghoverNext = true;
        }
    }

    onDropPointDragLeave(event: Event) {
        this.draghoverPrev = false;
        this.draghoverNext = false;
    }

    onDragStart(event) {
        if(this.tree.draggableNodes && this.node.draggable !== false) {
            event.dataTransfer.setData("text", "data");

            this.tree.dragDropService.startDrag({
                tree: this,
                node: this.node,
                subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
                index: this.index,
                scope: this.tree.draggableScope
            });
        }
        else {
            event.preventDefault();
        }
    }

    onDragStop(event) {
        this.tree.dragDropService.stopDrag({
            node: this.node,
            subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
            index: this.index
        });
    }

    onDropNodeDragOver(event) {
        event.dataTransfer.dropEffect = 'move';
        if(this.tree.droppableNodes) {
            event.preventDefault();
            event.stopPropagation();
        }
    }

    onDropNode(event) {
        if(this.tree.droppableNodes && this.node.droppable !== false) {
            event.preventDefault();
            event.stopPropagation();
            let dragNode = this.tree.dragNode;
            if(this.tree.allowDrop(dragNode, this.node, this.tree.dragNodeScope)) {
                let dragNodeIndex = this.tree.dragNodeIndex;
                this.tree.dragNodeSubNodes.splice(dragNodeIndex, 1);

                if(this.node.children)
                    this.node.children.push(dragNode);
                else
                    this.node.children = [dragNode];

                this.tree.dragDropService.stopDrag({
                    node: dragNode,
                    subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
                    index: this.tree.dragNodeIndex
                });

                this.tree.onNodeDrop.emit({
                    originalEvent: event,
                    dragNode: dragNode,
                    dropNode: this.node,
                    index: this.index
                });
            }
        }

        this.draghoverNode = false;
    }

    onDropNodeDragEnter(event) {
        if(this.tree.droppableNodes && this.node.droppable !== false && this.tree.allowDrop(this.tree.dragNode, this.node, this.tree.dragNodeScope)) {
            this.draghoverNode = true;
        }
    }

    onDropNodeDragLeave(event) {
        if(this.tree.droppableNodes) {
            let rect = event.currentTarget.getBoundingClientRect();
            if(event.x > rect.left + rect.width || event.x < rect.left || event.y >= Math.floor(rect.top + rect.height) || event.y < rect.top) {
               this.draghoverNode = false;
            }
        }
    }
}

@Component({
    selector: 'p-tree',
    template: `
        <div [ngClass]="{'ui-tree ui-widget ui-widget-content ui-corner-all':true,'ui-tree-selectable':selectionMode,'ui-treenode-dragover':dragHover,'ui-tree-loading': loading}" [ngStyle]="style" [class]="styleClass" *ngIf="!horizontal"
            (drop)="onDrop($event)" (dragover)="onDragOver($event)" (dragenter)="onDragEnter($event)" (dragleave)="onDragLeave($event)">
            <div class="ui-tree-loading-mask ui-widget-overlay" *ngIf="loading"></div>
            <div class="ui-tree-loading-content" *ngIf="loading">
                <i [class]="'fa fa-spin fa-2x ' + loadingIcon"></i>
            </div>
            <ul class="ui-tree-container relative" style="padding-right:40px">
                <p-treeNode *ngFor="let node of value;let firstChild=first;let lastChild=last; let index=index" [node]="node" 
                [firstChild]="firstChild" [lastChild]="lastChild" [index]="index"></p-treeNode>
                <span *ngIf="value && value.length && showExpanded" style="position: absolute;right: 10px;top:5px" (click)="expandedAll($event)" class="a-color user-select-none">{{expanded?'收起':'展开'}}</span>
                <div *ngIf="emptyMessage && value && !value.length" style="padding:5px">{{emptyMessage}}</div>  
            
            </ul>
        </div>
        <div [ngClass]="{'ui-tree ui-tree-horizontal ui-widget ui-widget-content ui-corner-all':true,'ui-tree-selectable':selectionMode}"  [ngStyle]="style" [class]="styleClass" *ngIf="horizontal">
            <div class="ui-tree-loading ui-widget-overlay" *ngIf="loading"></div>
            <div class="ui-tree-loading-content" *ngIf="loading">
                <i [class]="'fa fa-spin fa-2x ' + loadingIcon"></i>
            </div>
            <table *ngIf="value&&value[0]">
                <p-treeNode [node]="value[0]" [root]="true"></p-treeNode>
            </table>
        </div>
    `
})
export class Tree implements OnInit,AfterContentInit,OnDestroy {

    @Input() value: TreeNode[];

    @Input() selectionMode: string;

    @Input() selection: any;

    @Input() filter: string;

    @Input() emptyMessage: string = "没有搜到您想要的数据";

    @Input() canDelete: boolean;

    @Input() showExpanded: boolean = true;

    @Input() dataKey: string;

    @Input() keyWord: string = 'label';

    @Output() selectionChange: EventEmitter<any> = new EventEmitter();

    @Output() onNodeSelect: EventEmitter<any> = new EventEmitter();

    @Output() onNodeUnselect: EventEmitter<any> = new EventEmitter();

    @Output() onNodeExpand: EventEmitter<any> = new EventEmitter();

    @Output() onNodeCollapse: EventEmitter<any> = new EventEmitter();

    @Output() onNodeContextMenuSelect: EventEmitter<any> = new EventEmitter();

    @Output() onNodeDrop: EventEmitter<any> = new EventEmitter();

    @Input() style: any;

    @Input() styleClass: string;

    @Input() contextMenu: any;

    @Input() layout: string = 'vertical';

    @Input() draggableScope: any;

    @Input() droppableScope: any;

    @Input() draggableNodes: boolean;

    @Input() childrenAll: boolean = false;

    @Input() droppableNodes: boolean;

    @Input() metaKeySelection: boolean = true;

    @Input() propagateSelectionUp: boolean = true;

    @Input() propagateSelectionDown: boolean = true;

    @Input() loading: boolean;

    @Input() loadingIcon: string = 'fa-circle-o-notch';

    @ContentChildren(PrimeTemplate) templates: QueryList<any>;

    public _expanded:boolean;

    public templateMap: any;

    public nodeList: any = [];

    public nodeTouched: boolean;

    public dragNodeTree: Tree;

    public dragNode: TreeNode;

    public dragNodeSubNodes: TreeNode[];

    public dragNodeIndex: number;

    public dragNodeScope: any;

    public dragHover: boolean;

    public dragStartSubscription: Subscription;

    public dragStopSubscription: Subscription;

    constructor(@Optional() public dragDropService: TreeDragDropService) {}

    ngOnInit() {
        if(this.droppableNodes) {
            this.dragStartSubscription = this.dragDropService.dragStart$.subscribe(
              event => {
                this.dragNodeTree = event.tree;
                this.dragNode = event.node;
                this.dragNodeSubNodes = event.subNodes;
                this.dragNodeIndex = event.index;
                this.dragNodeScope = event.scope;
            });

            this.dragStopSubscription = this.dragDropService.dragStop$.subscribe(
              event => {
                this.dragNodeTree = null;
                this.dragNode = null;
                this.dragNodeSubNodes = null;
                this.dragNodeIndex = null;
                this.dragNodeScope = null;
                this.dragHover = false;
            });
        }
    }

    get horizontal(): boolean {
        return this.layout == 'horizontal';
    }

    ngAfterContentInit() {
        if(this.templates.length) {
            this.templateMap = {};
        }

        this.templates.forEach((item) => {
            this.templateMap[item.name] = item.template;
        });
        this.expandedAll()
    }

    onNodeClick(event: MouseEvent, node: TreeNode) {
        let eventTarget = (<Element> event.target);

        if(eventTarget.className && eventTarget.className.indexOf('ui-tree-toggler') === 0) {
            return;
        }
        else if(this.selectionMode) {
            if(node.selectable === false) {
                return;
            }

            let index = this.findIndexInSelection(node);
            let selected = (index >= 0);

            if(this.isCheckboxSelectionMode()) {
                if(selected) {
                    if(this.propagateSelectionDown)
                        this.propagateDown(node, false);
                    else
                        this.selection = this.selection.filter((val,i) => i!=index);

                    if(this.propagateSelectionUp && node.parent && !this.childrenAll) {
                        this.propagateUp(node.parent, false);
                    }

                    this.selectionChange.emit(this.selection);
                    this.onNodeUnselect.emit({originalEvent: event, node: node});
                }
                else {
                    if(this.propagateSelectionDown)
                        this.propagateDown(node, true);
                    else
                        this.selection = [...this.selection||[],node];

                    if(this.propagateSelectionUp && node.parent && !this.childrenAll) {
                        this.propagateUp(node.parent, true);
                    }

                    this.selectionChange.emit(this.selection);
                    this.onNodeSelect.emit({originalEvent: event, node: node});
                }
            }
            else {
                let metaSelection = this.nodeTouched ? false : this.metaKeySelection;

                if(metaSelection) {
                    let metaKey = (event.metaKey||event.ctrlKey || this.canDelete);
                    if(selected && metaKey) {
                        if(this.isSingleSelectionMode()) {
                            this.selectionChange.emit(null);
                        }
                        else {
                            this.selection = this.selection.filter((val,i) => i!=index);
                            this.selectionChange.emit(this.selection);
                        }

                        this.onNodeUnselect.emit({originalEvent: event, node: node});
                    }
                    else {
                        if(this.isSingleSelectionMode()) {
                            this.selectionChange.emit(node);
                        }
                        else if(this.isMultipleSelectionMode()) {
                            this.selection = (!metaKey) ? [] : this.selection||[];
                            this.selection = [...this.selection,node];
                            this.selectionChange.emit(this.selection);
                        }

                        this.onNodeSelect.emit({originalEvent: event, node: node});
                    }
                }
                else {
                    if(this.isSingleSelectionMode()) {
                        if(selected) {
                            this.selection = null;
                            this.onNodeUnselect.emit({originalEvent: event, node: node});
                        }
                        else {
                            this.selection = node;
                            this.onNodeSelect.emit({originalEvent: event, node: node});
                        }
                    }
                    else {
                        if(selected) {
                            this.selection = this.selection.filter((val,i) => i!=index);
                            this.onNodeUnselect.emit({originalEvent: event, node: node});
                        }
                        else {
                            this.selection = [...this.selection||[],node];
                            this.onNodeSelect.emit({originalEvent: event, node: node});
                        }
                    }

                    this.selectionChange.emit(this.selection);
                }
            }
        }

        this.nodeTouched = false;
    }

    onNodeTouchEnd() {
        this.nodeTouched = true;
    }

    @Input() get expanded():boolean{
        return this._expanded
    }

    set expanded(exp:boolean){
        this._expanded = exp
    }

    expandedAll(){
        this._expanded = !this._expanded;
        this.value.forEach( node => {
            this.expandRecursive(node, this._expanded);
        } )
    }

    expandRecursive(node:TreeNode, isExpand:boolean){
        node.expanded = isExpand;
        if(node.children){
            node.children.forEach( childNode => {
                this.expandRecursive(childNode, isExpand);
            } );
        }
    }

    onNodeRightClick(event: MouseEvent, node: TreeNode) {
        if(this.contextMenu) {
            let eventTarget = (<Element> event.target);

            if(eventTarget.className && eventTarget.className.indexOf('ui-tree-toggler') === 0) {
                return;
            }
            else {
                let index = this.findIndexInSelection(node);
                let selected = (index >= 0);

                if(!selected) {
                    if(this.isSingleSelectionMode())
                        this.selectionChange.emit(node);
                    else
                        this.selectionChange.emit([node]);
                }

                this.contextMenu.show(event);
                this.onNodeContextMenuSelect.emit({originalEvent: event, node: node});
            }
        }
    }

    findIndexInSelection(node: TreeNode) {
        let index: number = -1;

        if(this.selectionMode && this.selection) {
            if(this.isSingleSelectionMode()) {
                index = (this.selection == node || this.dataKey?this.selection[this.dataKey] == node[this.dataKey] : false) ? 0 : - 1;
            }
            else {
                for(let i = 0; i  < this.selection.length; i++) {
                    if(this.selection[i] == node || this.dataKey?this.selection[this.dataKey] == node[this.dataKey] : false) {
                        index = i;
                        break;
                    }
                }
            }
        }

        return index;
    }

    propagateUp(node: TreeNode, select: boolean) {
        if(node.children && node.children.length) {
            let selectedCount: number = 0;
            let childPartialSelected: boolean = false;
            let index = this.findIndexInSelection(node);
            for(let child of node.children) {
                if(this.isSelected(child)) {
                    selectedCount++;
                }
                else if(child.partialSelected) {
                    childPartialSelected = true;
                }
            }

            if(select && selectedCount >0 && index<0) {
                this.selection = [...this.selection||[],node];
                node.partialSelected = false;
            }
            else {
                if(!select && selectedCount == 0 && index >= 0) {
                    // let index = this.findIndexInSelection(node);
                    this.selection = this.selection.filter((val,i) => i!=index);
                }

                // if(childPartialSelected || selectedCount > 0 && selectedCount != node.children.length)
                //     node.partialSelected = true;
                // else
                //     node.partialSelected = false;
            }
        }

        let parent = node.parent;
        if(parent) {
            this.propagateUp(parent, select);
        }
    }

    propagateDown(node: TreeNode, select: boolean) {
        let index = this.findIndexInSelection(node);

        if(select && index == -1) {
            this.selection = [...this.selection||[],node];
        }
        else if(!select && index > -1) {
            this.selection = this.selection.filter((val,i) => i!=index);
        }

        node.partialSelected = false;

        if(node.children && node.children.length) {
            for(let child of node.children) {
                this.propagateDown(child, select);
            }
        }
    }

    isSelected(node: TreeNode) {
        return this.findIndexInSelection(node) != -1;
    }

    isSingleSelectionMode() {
        return this.selectionMode && this.selectionMode == 'single';
    }

    isMultipleSelectionMode() {
        return this.selectionMode && this.selectionMode == 'multiple';
    }

    isCheckboxSelectionMode() {
        return this.selectionMode && this.selectionMode == 'checkbox';
    }

    getTemplateForNode(node: TreeNode): TemplateRef<any> {
        if(this.templateMap)
            return node.type ? this.templateMap[node.type] : this.templateMap['default'];
        else
            return null;
    }

    onDragOver(event) {
        if(this.droppableNodes && (!this.value || this.value.length === 0)) {
            event.dataTransfer.dropEffect = 'move';
            event.preventDefault();
        }
    }

    onDrop(event) {
        if(this.droppableNodes && (!this.value || this.value.length === 0)) {
            event.preventDefault();
            let dragNode = this.dragNode;
            if(this.allowDrop(dragNode, null, this.dragNodeScope)) {
                let dragNodeIndex = this.dragNodeIndex;
                this.dragNodeSubNodes.splice(dragNodeIndex, 1);
                //this.value = this.value||[];
                this.value.push(dragNode);

                this.dragDropService.stopDrag({
                    node: dragNode
                });
            }
        }
    }

    onDragEnter(event) {
        if(this.droppableNodes && this.allowDrop(this.dragNode, null, this.dragNodeScope)) {
            this.dragHover = true;
        }
    }

    onDragLeave(event) {
        if(this.droppableNodes) {
            let rect = event.currentTarget.getBoundingClientRect();
            if(event.x > rect.left + rect.width || event.x < rect.left || event.y > rect.top + rect.height || event.y < rect.top) {
               this.dragHover = false;
            }
        }
    }

    allowDrop(dragNode: TreeNode, dropNode: TreeNode, dragNodeScope: any): boolean {
        if(this.isValidDragScope(dragNodeScope)) {
            let allow: boolean = true;
            if(dropNode) {
                if(dragNode === dropNode) {
                    allow = false;
                }
                else {
                    let parent = dropNode.parent;
                    while(parent != null) {
                        if(parent === dragNode) {
                            allow = false;
                            break;
                        }
                        parent = parent.parent;
                    }
                }
            }

            return allow;
        }
        else {
            return false;
        }
    }

    isValidDragScope(dragScope: any): boolean {
        let dropScope = this.droppableScope;

        if(dropScope) {
            if(typeof dropScope === 'string') {
                if(typeof dragScope === 'string')
                    return dropScope === dragScope;
                else if(dragScope instanceof Array)
                    return (<Array<any>>dragScope).indexOf(dropScope) != -1;
            }
            else if(dropScope instanceof Array) {
                if(typeof dragScope === 'string') {
                    return (<Array<any>>dropScope).indexOf(dragScope) != -1;
                }
                else if(dragScope instanceof Array) {
                    for(let s of dropScope) {
                        for(let ds of dragScope) {
                            if(s === ds) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        else {
            return true;
        }
    }

    ngOnDestroy() {
        if(this.dragStartSubscription) {
            this.dragStartSubscription.unsubscribe();
        }

        if(this.dragStopSubscription) {
            this.dragStopSubscription.unsubscribe();
        }
    }
}

@Component({
    selector: 'p-treeselect',
    template: `
<div class="relative" >
    <div [ngClass]="styleClass" [class.ui-state-focus]="focus" [class.opacity06]="disabled" style="width:100%" #selectEle [ngStyle]="style" class="line-height-27 ui-dropdown  ui-widget ui-state-default ui-corner-all ui-helper-clearfix ui-dropdown-open" (click)="mouseClick($event)">
        <label class="ui-dropdown-label ui-inputtext ui-corner-all p-l-10 ellipsis" [ngClass]="{'opacity06':getLabel == placeholder}" [title]="getLabel">{{ getLabel || placeholder}}</label>
        <div class="ui-dropdown-trigger ui-state-default ui-corner-right border-l-none">
            <span class="ui-clickable fa fa-angle-down" style="line-height: 25px;"></span>
        </div>
        <div class="ui-helper-hidden-accessible">
            <input #in type="text" [name]="name" (focus)="onInputFocus($event)" (blur)="onInputBlur($event)" [disabled]="disabled" [attr.tabindex]="tabindex">
        </div>
    </div>

    <p-overlayPanel #op1 styleClass="left-0 top-36 width-full border-none relavte">
        <div class="p-5 relative" *ngIf='filter'>
            <input type="text" #input class="width-full p-input ui-state-default" (keyup)="search(input.value)" pInputText style="padding: .45em 1.5em .45em .25em;">
            <span class="icon-search-right"></span>
        </div>
        <p-tree [expanded]='expanded' [emptyMessage]=[emptyMessage] [dataKey]="dataKey" [style]="{'overflow':'auto','height':'400px','width':'100%'}" styleClass="border-none" [value]="nodeList" (onNodeSelect)="onModelChange(selection);selectionChange.emit(selection);onNodeSelect.emit($event);op1.hide();"
            (onNodeUnselect)="onModelChange(selection);selectionChange.emit(selection);onNodeUnselect.emit($event);" [selectionMode]="selectionMode" [(selection)]="selection"></p-tree>
    </p-overlayPanel>
</div>
    `,
    providers: [INPUTSWITCH_VALUE_ACCESSOR]
})
export class TreeSelect implements OnInit,OnDestroy,ControlValueAccessor {

    @Input() styleClass:string;

    @Input() name:string;

    @Input() tabindex:any;

    @Input() emptyMessage:string = '没有搜到您想要的数据';

    @Input() style:any;

    @Input() dataKey:string;

    @Input() filter:boolean = true;

    @Input() canDelete:boolean;

    @Input() disabled:boolean;

    @Input() expanded:boolean;

    @Input() placeholder: string = '请选择';

    @Input() selectionMode: string;

    @Input() selection: any;

    @Output() selectionChange: EventEmitter<any> = new EventEmitter();

    @Output() onNodeSelect: EventEmitter<any> = new EventEmitter();

    @Output() onNodeUnselect: EventEmitter<any> = new EventEmitter();

    @Output() onFocus: EventEmitter<any> = new EventEmitter();

    @Output() onBlur: EventEmitter<any> = new EventEmitter();

    @ViewChild('panel') panel;

    @ViewChild('op1') op1;

    @ViewChild('in') focusViewChild: ElementRef;

    onModelChange: Function = () => {};

    onModelTouched: Function = () => {};

    focus:boolean;

    nodeList:any = [];

    _value:any = [];

    ngOnInit() {
        this.nodeList = this.value;
    }

    mouseClick(event){
        if(this.disabled){return}
        this.op1.toggle(event);
        this.focusViewChild.nativeElement.focus();
    }

    get getLabel():string{
        if(!this.selection){return this.placeholder}
        let len = this.selection.length,str = [];
        if(this.selectionMode == 'single'){return this.selection.label}
        this.selection.map(val=>str.push(val.label))
        return len?str.join(','):this.placeholder
    }

    // 搜索框回车事件响应
    search(keyword) {
        keyword = keyword.trim();
        this.expanded = keyword;
        if (!keyword) {
            this.nodeList = this.value;
        } else if (this.value && this.value.length > 0) {
            let nodelist = [];
            this.value.forEach((n, i, a) => {
                let node = this.searchEach(n, keyword);
                if (node) {
                    nodelist.push(node)
                }
            })
            this.nodeList = nodelist;
        }
    }
      // 对子节点进行搜索。
      searchEach(node, value) {
          let flag = node.label.includes(value)
          if (node.children && node.children.length) {
              let nodeChild = []
              node.children.forEach(val => {
                  let newChild = this.searchEach(val, value);
                  if (newChild) {
                      nodeChild.push(newChild)
                  }
              })
              if (nodeChild.length > 0) {
                  let newNode = Object.assign({}, node);
                  newNode.children = nodeChild;
                  newNode.expanded = true;
                  return newNode
              }
              if (flag) {
                  let newNode = Object.assign({}, node);
                  newNode.children = undefined;
                  newNode.expanded = true;
                  return newNode
              }
          } else {
              if (flag) {
                  return node
              } else {
                  return undefined
              }
          }
      }

    @Input() get value(): any[] {
        return this._value;
    }

    set value(val: any[]) {
        let opts = val;
        this._value = opts;
        this.nodeList = this._value;
    }

    onInputFocus(event) {
        this.focus = true;
        this.onFocus.emit(event);
    }

    onInputBlur(event) {
        this.focus = false;
        this.onModelTouched();
        this.onBlur.emit(event);
    }

    writeValue(value: any) : void {
        this.selection = value;
    }

    registerOnChange(fn: Function): void {
        this.onModelChange = fn;
    }

    registerOnTouched(fn: Function): void {
        this.onModelTouched = fn;
    }
    
    setDisabledState(val: boolean): void {
        this.disabled = val;
    }

    ngOnDestroy(){
        if(this.panel){
            this.panel.toggle()
        }
    }


}

@Component({
    selector: 'p-treesearch',
    template: `
        <p-tree [expanded]='expanded' [showExpanded]="showExpanded"  [emptyMessage]=[emptyMessage] [dataKey]="dataKey" [styleClass]="styleClass" [value]="nodeList" 
          (onNodeSelect)="selectionChange.emit(selection);onNodeSelect.emit($event)" 
          (onNodeUnselect)="selectionChange.emit(selection);onNodeUnselect.emit($event)"
          [selectionMode]="selectionMode" [(selection)]="selection"></p-tree> 
    `
})

export class TreeSearch extends TreeSelect {

  @Input() showExpanded: boolean = true;

}



@NgModule({
    imports: [CommonModule,OverlayPanelModule],
    exports: [Tree,SharedModule,TreeSelect,TreeSearch],
    declarations: [Tree,UITreeNode,TreeNodeTemplateLoader,TreeSelect,TreeSearch]
})
export class TreeModule { }
