import {NgModule,Component,Input,Output,EventEmitter} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuItem} from '../common/menuitem';
import {RouterModule} from '@angular/router';

@Component({
    selector: 'p-steps',
    styles:[`
     .step-icon .ui-steps-number{font-size:20px;border:3px #ececec solid;border-radius:50%;text-align:center;display:inline-block}
     .step-icon .actived{color:#1a91eb !important;background:#f7f7f7}
     .step-icon .color-1a91eb{color:#1a91eb !important}
     .step-icon .text{margin-top:25px}
     .step-icon .line{margin-right:120px}
     .step-icon .line-light{display:block;position: absolute;height:4px;left:100%;border-radius:2px}
     .step-icon .active{background:#1a91eb !important;border-color:#1a91eb !important}
     .step-icon li{background:none !important;border:none !important}
     .step-icon .default{opacity:0.7}
     .tool-tip{transition:.3s;padding:7px 10px;background:#1a91eb;color:#fff;border-radius:2px;left:calc(50% - 75px);position: absolute;width: 150px;}
     li:hover .tool-tip{display:block !important}
     .tool-tip::after{width:0;height:0;border-width:0 5px 5px;border-style:solid;border-color:transparent transparent #1a91eb;content:'';display:block;position: absolute;top: -5px;left: 45%;}
    `],
    template: `
        <div [ngClass]="{'ui-steps ui-widget ui-helper-clearfix step-icon':true,'ui-steps-readonly':readonly}" [ngStyle]="style" [class]="styleClass">
            <ul role="tablist">
                <li *ngFor="let item of model; let i = index;let last = last" class="ui-steps-item" #menuitem [style.margin-right.px]="last?0:widthStep"
                    [ngClass]="{'ui-state-highlight':(i === activeIndex),'ui-state-default':(i !== activeIndex),'relative':true}">
                    <a *ngIf="!item.routerLink" [href]="item.url||'#'" class="ui-menuitem-link p-20" (click)="itemClick($event, item, i)" [attr.target]="item.target" [attr.id]="item.id">
                        <span class="ui-steps-number fa {{item.icon}}" [style.width.px]="styleTheme=='step'?50:45" [style.height.px]="styleTheme=='step'?50:45"
                        [style.line-height.px]="styleTheme=='step'?45:40"
                        [ngClass]="{'actived':activeIndex>i,'color-#fff active':activeIndex==i,'default':activeIndex<i}">
                            {{item.icon?'':(item.number || (i + 1))}}
                        </span>
                        <span class="ui-steps-title text" [ngClass]="{'color-1a91eb':(styleTheme=='step'?activeIndex>=i:activeIndex==i),'default':activeIndex<i}">{{item.label}}</span>
                    </a>
                    <div [ngClass]="{'line-light':true}"
                    [style.background]="(styleTheme=='step'&& activeIndex>i)?'#1a91eb':'#ececec'"
                    [style.width.px]="last?0:widthStep" [style.top.px]="styleTheme=='step'?43:98" >
                    
                    </div>
                    <a *ngIf="item.routerLink" [routerLink]="item.routerLink" [queryParams]="item.queryParams" [routerLinkActive]="'ui-state-active'" [routerLinkActiveOptions]="item.routerLinkActiveOptions||{exact:false}" class="ui-menuitem-link" (click)="itemClick($event, item, i)" [attr.target]="item.target" [attr.id]="item.id">
                        <span class="ui-steps-number" *ngIf="!item.icon">{{i + 1}}</span>
                        <span class="ui-steps-number fa {{item.icon}}" *ngIf="item.icon"></span>
                        <span class="ui-steps-title">{{item.label}}</span>
                    </a>
                    <div class="tool-tip relative" *ngIf="styleTheme=='flow' && activeIndex>=i" [style.display]="activeIndex==i?'block':'none'">
                    {{item.toolTip}}
                    </div>
                </li>
            </ul>
        </div>
    `
})
export class Steps {
    
    @Input() activeIndex: number = 0;

    @Input() styleTheme:string = 'step';
    
    @Input() model: any[];
    
    @Input() readonly: boolean =  true;
    
    @Input() style: any;
        
    @Input() styleClass: string;
    
    @Output() activeIndexChange: EventEmitter<any> = new EventEmitter();

    get widthStep(){
        let width = 120;
        if(this.styleTheme=='step'){
            return width
        }
        if(this.styleTheme=='flow'){
           return 90
        }
        return width
    }
    
    itemClick(event: Event, item: any, i: number) {
        if(this.readonly || item.disabled) {
            event.preventDefault();
            return;
        }
        
        this.activeIndexChange.emit(i);
                
        if(!item.url) {
            event.preventDefault();
        }
        
        if(item.command) {            
            item.command({
                originalEvent: event,
                item: item,
                index: i
            });
        }
    }
    
}

@Component({
    selector: 'p-timelinex',
    styles:[`
    .cricl{width:15px;height:15px;background:#1a91eb;border-radius:50%;margin-top:30px}
    li{width:100px}
    li:hover{background:none !important}
    .tool-tip{width:106px;padding:7px 10px;background:#1a91eb;color:#fff;border-radius:2px;left:50%;position: absolute;white-space: nowrap;transform: translateX(-50%);}
    .line::after{width:100px;height:2px;content:'';display:block;position: absolute;top: 7px;left: -43px;background:#1a91eb}   
    .m-t-10{margin-top:10px}
    .tool-tip::after{width:0;height:0;border-width:0 5px 5px;border-style:solid;border-color:transparent transparent #1a91eb;content:'';display:block;position: absolute;top: -5px;left: 45%;} 
    `],
    template: `
        <div [ngClass]="{'ui-steps ui-widget ui-helper-clearfix ':true,'ui-steps-readonly':readonly}" [ngStyle]="style" [class]="styleClass">
            <div class="pull-left" style="margin-top:70px;padding:10px 20px;max-width:200px;" *ngIf="titles">
                <div class="m-t-10" *ngFor="let title of titles">
                    <div class="font-16"><strong>{{title.main}}</strong></div>
                    <div class="opacity06 ellipsis" [title]="title.sub">{{title.sub}}&nbsp;</div>
                </div>
            </div>
            <ul role="tablist">
                <li *ngFor="let item of model; let i = index;let last = last" class="ui-steps-item text-center relative" #menuitem >
                   <div>{{item.label}}</div>
                   <div class="cricl inline-block relative line" ></div>
                   
                       <ng-container  *ngFor="let tip of item.toolTip;let ti = index;last as last" [style.opacity]="">
                       <div class="tool-tip relative" *ngIf="!tip?.hidden"  [style.bottom.px]="-50*(ti+1)">
                        {{tip?.text}} &nbsp;
                        </div>
                       </ng-container >
                  
                </li>
            </ul>
        </div>
    `
})

export class TimeLineX extends Steps{
    @Input() titles:any[]
}

@Component({
    selector: 'p-timeliney',
    styles:[`
    .m-t-90{margin-top:90px}
    .check{width: 21px;height: 21px;background: #1a91eb;color: #fff;border-radius: 50%;line-height: 21px;}
    .color-d6d6d6{color:#d6d6d6}
    .color-1a91eb{color:#1a91eb}
    .p-0-20{padding:0 20px}
    .font-25{font-size:25px}
    .icon span{position: absolute;z-index: 10;left: 50%;transform: translateX(-50%);}
    .icon{height:100px;width:60px;text-align:center}
    .line::after{width:2px;height:100%;content:'';display:block;position: relative;top: 4px;left:calc(50% - 1px);background:#d6d6d6;z-index:5}
    `],
    template: `
        <div>
            <div *ngFor="let item of model; let i = index;let first = first;let last = last" [title]="item.title|| ''" class="clearfix table-row" [class.color-1a91eb]="first">
                <div class="time-date text-right table-cell p-0 v-top">
                    <div class="date font-16" *ngIf="item.date">{{item.date}}</div>
                    <div class="time" *ngIf="item.time">{{item.time}}</div>
                </div>
                <div class="icon relative p-0 table-cell" [class.line]="!last">
                    <span class="fa  bg-fff" [ngClass]="{'color-d6d6d6':!first,'fa-check check font-25':item.complete,'font-25 fa-clock-o':item.date&&!item.complete,'fa-circle':!item.date&&!first}" ></span>
                </div>
                
                <div class="content table-cell p-0 v-top">
                    <div class="font-16">{{item.status}}</div>
                    <div class="opacity06">{{item.content}}</div>
                </div>
            </div>
        </div>
    `
  

})
export class TimeLineY {

    @Input() styleClass: string;

    _model:any[];

    @Input() get model(){
        return this._model || []
    }

    set model(val){
        this._model = val
    }
}

@NgModule({
    imports: [CommonModule,RouterModule],
    exports: [Steps,RouterModule,TimeLineX,TimeLineY],
    declarations: [Steps,TimeLineX,TimeLineY]
})
export class StepsModule { }