import {AfterViewInit, Component, EventEmitter, Input, NgModule, OnDestroy, Output} from '@angular/core';
import {ButtonModule} from '../button/button'
import {CommonModule} from '@angular/common';

@Component({
    selector: 'p-dialogmsg',
    styles:[`
        .new-clue{position: fixed;right: 17px;bottom:17px;width: 320px;height: 200px;padding: 20px; color: #fff;background: rgba(72, 167, 239, 0.9);box-sizing: border-box;}
        .m-r-40{margin-right: 40px !important}
        .new-clue .btn-clue{background: none !important;border: 1px solid #fff !important}
        .new-clue .btn-clue:hover{background: #fff !important;border: 1px solid #fff !important;color: #48a7ef !important}
        .clue-code{width: 165px;}
        .clue-name{margin:14px auto ;width: 165px;}
        .detail-title{display: inline-block;width: 100px;text-align: right;}
        .ellipsis{width: 235px}
        .only-content{    width:280px;height: 100px;text-align: center;display: table-cell;vertical-align: middle;padding: 0 20px;word-break: break-word;}
    `],
    template: `
            <div class="new-clue font-14 clearfix" *ngIf="isShow">
                <div class="m-t-10">
                    <span class="fa fa-volume-up"></span>
                    <span class="font-16">{{title}}</span>
                    <span class="pull-right" *ngIf="!showClose">{{newTime}}S</span>
                    <span class="pull-right" *ngIf="showClose"><i class="fa fa-times h-pointer" (click)="close($event)"></i></span>
                </div>
                <div *ngIf="!onlyContent">
                    <div class="clue-code ellipsis" [style.margin]="value.data.planDate?'12px auto':'22px auto'"  >
                        <span class="detail-title">线索编号：</span><span [title]="value.data.clueCode">{{value.data.clueCode}}</span>
                    </div>
                    <div class="clue-code ellipsis" [style.margin]="value.data.planDate?'12px auto':'22px auto'"  >
                        <span class="detail-title">线索名称：</span><span [title]="value.data.clueName">{{value.data.clueName}}</span>
                    </div>
                    <div class="clue-code ellipsis" *ngIf="value.data.planDate" [style.margin]="value.data.planDate?'12px auto':'22px auto'"  >
                        <span class="detail-title">计划跟进时间：</span><span [title]="value.data.planDate">{{value.data.planDate}}</span>
                    </div>
                </div>
                <div *ngIf="onlyContent" class="only-content">
                    {{content}}
                </div>    
                
                <div class="text-center">
                    <button *ngIf="showAccept" pButton (click)="onAccept.emit(value);showClose?'':close()" [label]="showClose?'详情':acceptLabel" class="btn-clue m-r-40"></button>
                    <button *ngIf="rejectLabel" pButton (click)="onReject.emit(value);showClose?'':close()" [label]="showClose?'拨打电话':rejectLabel" class="btn-clue"></button>
                </div>
            </div>
    `
})
export class DialogMessage implements AfterViewInit,OnDestroy {

    @Input() clueCode:string;

    @Input() clueName:string;

    @Input() content:string;

    @Input() title:string = "新的线索";

    @Input() value:any;

    @Input() acceptLabel:string = "接收";

    @Input() showAccept:boolean = true;

    @Input() rejectLabel:string = "拒绝";

    @Input() showReject:boolean = true;

    @Output() onAccept: EventEmitter<any> = new EventEmitter();

    @Output() onReject: EventEmitter<any> = new EventEmitter();

    @Output() onClose: EventEmitter<any> = new EventEmitter();

    @Input() showClose:boolean;

    @Input() onlyContent:boolean;
    
    @Input() zIndex:number;

    @Input() get time():number{
        return this.newTime || 45
    }

    set time(val:number){
        this.newTime = val;
    }

    newTime:number;

    isShow:boolean = true;

    interval:any;

    ngAfterViewInit() {
        if(!this.showClose){
            this.interval = setInterval(_=>{
                if(!this.newTime){
                    clearInterval(this.interval);
                    this.close(true);
                    return
                }
                this.newTime--
            },1000)
        }
    }

    close(event?){
        if(this.interval){clearInterval(this.interval);}
        this.isShow = false;
        if(event){this.onClose.emit(this.value);}
        
    }

    ngOnDestroy() {
        if(this.interval){
            clearInterval(this.interval)
        }
    }
}


@Component({
    selector: 'p-msgdialog',
    styles:[`
        .new-clue{position: fixed;right: 17px;bottom:17px;width: 320px;height: 200px;padding: 20px; color: #fff;background: rgba(72, 167, 239, 0.9);box-sizing: border-box;}
        .m-l-40{margin-left: 40px !important}
        .new-clue .btn-clue{background: none !important;border: 1px solid #fff !important}
        .new-clue .btn-clue:hover{background: #fff !important;border: 1px solid #fff !important;color: #48a7ef !important}
        .detail{padding:7px 0;}
        .clue-name{margin:14px auto ;width: 165px;}
        .detail-title{display: inline-block;width: 100px;text-align: right;}
        .content{height:100px}
        .ellipsis{width: 235px}
        .m-5{margin:5px 0}
        .contnt-warp{position: absolute;left: 50%;transform: translate(-50%,-50%);top: 50%;}
        .only-content{    width:280px;height: 100px;text-align: center;display: table-cell;vertical-align: middle;padding: 0 20px;word-break: break-word;}
    `],
    template: `
            <div class="new-clue font-14 clearfix" *ngIf="isShow">
                <div class="m-t-10">
                    <span class="fa fa-volume-up"></span>
                    <span class="font-16">{{value.title}}</span>
                    <span class="pull-right" *ngIf="!value.showClose">{{newTime}}S</span>
                    <span class="pull-right" *ngIf="value.showClose"><i class="fa fa-times h-pointer" (click)="close($event)"></i></span>
                </div>
                <div class="content relative" [ngClass]="{'m-5':value.content?.length>2}">
                   <div class="inline-block contnt-warp">
                        <div *ngFor="let item of value.content" class="table-row" >
                            <div class="table-cell detail ellipsis-td text-right" *ngIf="item.title">{{item.title}}：</div>
                            <div class="table-cell detail ellipsis-td" [title]="item.content">{{item.content}}</div>
                        </div>
                   </div>
                </div>  
                
                <div class="text-center">
                    <button *ngIf="value.leftBtnLabel" pButton (click)="left()" [label]="value.leftBtnLabel" class="btn-clue "></button>
                    <button *ngIf="value.rightBtnLabel" pButton (click)="right()" [label]="value.rightBtnLabel" class="btn-clue m-l-40"></button>
                </div>
            </div>
    `
})

export class MessageDialog implements AfterViewInit,OnDestroy {

    @Input() value:any;
 
    @Input() zIndex:number;

    @Output() onLeftBtnClick: EventEmitter<any> = new EventEmitter();

    @Output() onRightBtnClick: EventEmitter<any> = new EventEmitter();

    @Output() onClose: EventEmitter<any> = new EventEmitter();

    @Input() get time():number{
        return this.newTime || 45
    }

    set time(val:number){
        this.newTime = val;
    }

    newTime:number;

    isShow:boolean = true;

    interval:any;

    ngAfterViewInit() {
        if(!this.value.showClose){
            this.interval = setInterval(_=>{
                if(!this.newTime){
                    clearInterval(this.interval);
                    this.close(true);
                    return
                }
                this.newTime--
            },1000)
        }
    }

    left(){
        this.onLeftBtnClick.emit(this.value);
        if(this.value.closeWhenLeftClick){this.close()}
    }

    right(){
        this.onRightBtnClick.emit(this.value);
        if(this.value.closeWhenRightClick){this.close()}
    }

    close(event?){
        if(this.interval){clearInterval(this.interval);}
        this.isShow = false;
        if(event){this.onClose.emit(this.value);}
        
    }

    ngOnDestroy() {
        if(this.interval){
            clearInterval(this.interval)
        }
    }
}

@NgModule({
    imports: [CommonModule,ButtonModule],
    exports: [DialogMessage,MessageDialog],
    declarations: [DialogMessage,MessageDialog]
})
export class DialogMessageModule { }
