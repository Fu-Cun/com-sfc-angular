import {NgModule,Directive,ElementRef,HostListener,Input,Output,OnInit,DoCheck,EventEmitter,AfterViewInit} from '@angular/core';
import {CommonModule} from '@angular/common';

import { NgModel,FormControl } from '@angular/forms';

@Directive({
    selector: '[pInputTextarea]',
    providers: [NgModel],
    host: {
        '[class.ui-inputtext]': 'true',
        '[class.ui-corner-all]': 'true',
        '[class.ui-state-default]': 'true',
        '[class.ui-widget]': 'true',
        '[class.ui-state-filled]': 'filled',
        '[attr.rows]': 'rows',
        '[attr.cols]': 'cols'
    }
})
export class InputTextarea implements OnInit,DoCheck,AfterViewInit {

    @Input() maxlength:string;
    
    @Input() autoResize: boolean;
    
    @Input() rows: number;
    
    @Input() cols: number;
    
    @Output() onResize: EventEmitter<any> = new EventEmitter();

    @Input() formControl: FormControl;
    
    rowsDefault: number;
    
    colsDefault: number;
    
    filled: boolean;
        
    constructor(public el: ElementRef,private control:NgModel) {}
    
    ngOnInit() {
        this.rowsDefault = this.rows;
        this.colsDefault = this.cols;
    }
    
    ngDoCheck() {
        this.updateFilledState();
    }
    
    //To trigger change detection to manage ui-state-filled for material labels when there is no value binding
    @HostListener('input', ['$event']) 
    onInput(e) {
        this.updateFilledState();
    }
    
    
    updateFilledState() {
        this.filled = this.el.nativeElement.value && this.el.nativeElement.value.length;
    }
    
    @HostListener('focus', ['$event']) 
    onFocus(e) {        
        if(this.autoResize) {
            this.resize(e);
        }
    }
    
    @HostListener('blur', ['$event']) 
    onBlur(e) {
        this.el.nativeElement.value = this.el.nativeElement.value.trim();
        this.updataValue()        
        if(this.autoResize) {
            this.resize(e);
        }
    }
    
    @HostListener('keyup', ['$event']) 
    onKeyup(e) {
        if(this.autoResize) {
            this.resize(e);
        }
    }
    
    resize(event?: Event) {
        let linesCount = 0,
        lines = this.el.nativeElement.value.split('\n');

        for(let i = lines.length-1; i >= 0 ; --i) {
            linesCount += Math.floor((lines[i].length / this.colsDefault) + 1);
        }

        this.rows = (linesCount >= this.rowsDefault) ? (linesCount + 1) : this.rowsDefault;
        this.onResize.emit(event||{});
    }

    updataValue(){
        this.control.control.setValue(this.el.nativeElement.value)
        //this.control.viewToModelUpdate(this.el.nativeElement.value);
        if(this.formControl) {
            this.formControl.setValue(this.el.nativeElement.value);
        }
    }

    ngAfterViewInit() {
        this.el.nativeElement.setAttribute('maxlength',this.maxlength || '999')
    }
}

@NgModule({
    imports: [CommonModule],
    exports: [InputTextarea],
    declarations: [InputTextarea]
})
export class InputTextareaModule { }