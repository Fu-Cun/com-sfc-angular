import {NgModule,Component,ElementRef,OnDestroy,Input} from '@angular/core';
import {trigger,state,style,transition,animate} from '@angular/animations';
import {CommonModule} from '@angular/common';
import {MenuItem} from '../common/menuitem';
import {RouterModule} from '@angular/router';

export class BasePanelMenuItem {
        
    handleClick(event, item) {
        if(item.disabled) {
            event.preventDefault();
            return;
        }
        item.expanded = !item.expanded;
        
        if(!item.url) {
            event.preventDefault();
        }
                   
        if(item.command) {
            item.command({
                originalEvent: event,
                item: item
            });
        }
    }

    routerLinkparse(linkparam){
        if(!linkparam){return linkparam}
        if(linkparam.startsWith('[')&&linkparam.endsWith(']')){
            return JSON.parse(linkparam)
        }
        return linkparam
    }

    
}

@Component({
    selector: 'p-panelMenuSub',
    template: `
        <ul class="ui-menu-list m-l-0  ui-helper-reset" [@submenu]="expanded ? 'visible' : 'hidden'">
            <ng-template ngFor let-child [ngForOf]="item.items">
                <li *ngIf="child.separator" class="ui-menu-separator ui-widget-content">
                <li *ngIf="!child.separator" class="ui-menuitem ui-corner-all" [ngClass]="{'ui-menu-parent':child.items}" [class]="child.styleClass" [ngStyle]="child.style">
                    <a *ngIf="!child.routerLink" [style.width.px]="width" [href]="child.url||'#'" [ngStyle]="{'padding':('16px 0 16px ' + (20*child.level + 40) + 'px')}" class="ui-menuitem-link ui-corner-all" [attr.tabindex]="item.expanded ? null : '-1'" [attr.id]="child.id"
                        [ngClass]="{'ui-menuitem-link-hasicon':child.icon&&child.items,'ui-state-disabled':child.disabled}" 
                        (click)="handleClick($event,child)" [attr.target]="child.target" [attr.title]="child.title">
                        <span class="ui-panelmenu-icon fa fa-fw fa-angle-right" [class]="expandedClass" [ngClass]="{'rotate-0':!child.expanded,'rotate-90':child.expanded}" *ngIf="child.items && child.items.length"></span
                        ><span class="ui-menuitem-icon fa fa-fw " [ngClass]="child.icon" *ngIf="child.icon"></span
                        ><span class="ui-menuitem-text ">{{child.label}}</span>
                    </a>
                    <a *ngIf="child.routerLink" [style.width.px]="width" [routerLink]="routerLinkparse(child.routerLink)" [ngStyle]="{'padding':('16px 0 16px ' + (20*child.level + 40) + 'px')}" [queryParams]="child.queryParams" [routerLinkActive]="activeSubClass" [routerLinkActiveOptions]="child.routerLinkActiveOptions||{exact:false}" class="ui-menuitem-link ui-corner-all" 
                        [ngClass]="{'ui-menuitem-link-hasicon':child.icon&&child.items,'ui-state-disabled':child.disabled}" [attr.tabindex]="item.expanded ? null : '-1'" [attr.id]="child.id"
                        (click)="handleClick($event,child)" [attr.target]="child.target" [attr.title]="child.title">
                        <span class="ui-panelmenu-icon fa fa-fw fa-angle-right" [class]="expandedClass" [ngClass]="{'rotate-0':!child.expanded,'rotate-90':child.expanded}" *ngIf="child.items && child.items.length"></span
                        ><span class="ui-menuitem-icon fa fa-fw" [ngClass]="child.icon" *ngIf="child.icon"></span
                        ><span class="ui-menuitem-text ">{{child.label}}</span>
                    </a>
                    <p-panelMenuSub [item]="child" [activeSubClass]="activeSubClass" [expandedClass]="expandedClass" [expanded]="child.expanded" *ngIf="child.items"></p-panelMenuSub>
                </li>
            </ng-template>
        </ul>
    `,
    styles:[`
    .rotate-90{
        transition: transform 0.3s;
        transform: rotate(90deg);
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
    }
    .rotate-0{
        transition: transform 0.3s;
        transform: rotate(0);
        -webkit-transform: rotate(0);
        -moz-transform: rotate(0);
        -o-transform: rotate(0);
        -ms-transform: rotate(0);
    `],
    animations: [
        trigger('submenu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class PanelMenuSub extends BasePanelMenuItem {

    @Input() expandedClass:string;

    @Input() activeSubClass:string = '';
    
    @Input() item: MenuItem;
    
    @Input() expanded: boolean;

    @Input() multiple: boolean = true;

    @Input() width:any = 210;

    handleClick(event, item) {
    	if(!this.multiple) {
            for(let modelItem of this.item.items) {
        		if(item !== modelItem && modelItem['expanded']) {
        			modelItem['expanded'] = false;
        		}
        	}
    	}
        super.handleClick(event, item);
    }
}

@Component({
    selector: 'p-panelMenu',
    template: `
        <div [class]="styleClass" [ngStyle]="style" [ngClass]="'ui-panelmenu ui-widget'">
            <div *ngFor="let item of model;let f=first;let l=last;" class="ui-panelmenu-panel">
                <div tooltipStyleClass="font-14" [ngClass]="{'ui-widget ui-panelmenu-header ui-state-default':true,'ui-corner-top':f,'ui-corner-bottom':l&&!item.expanded,
                    'ui-state-active':item.expanded,'ui-state-disabled':item.disabled}" [class]="item.styleClass" [ngStyle]="item.style">
                    <a *ngIf="!item.routerLink" [style.width.px]="width" class="p-16-0" [href]="item.url||'#'" [ngClass]="{'ui-panelmenu-headerlink-hasicon':item.icon}" [class]="item.expanded?activeClass:''" (click)="handleClick($event,item)"
                        [attr.target]="item.target" [attr.title]="item.title">
                        <span *ngIf="item.items && showLabel" class="ui-panelmenu-icon fa fa-angle-right" [class]="expandedClass" [ngClass]="{'rotate-0':!item.expanded,'rotate-90':item.expanded}"></span
                        ><span class="ui-menuitem-icon fa m-0-21" [ngClass]="item.icon || 'fa-circle-o'" ></span
                        ><span class="ui-menuitem-text " *ngIf="showLabel">{{item.label}}</span>
                    </a>
                    <a *ngIf="item.routerLink" [style.width.px]="width" class="p-16-0" [routerLink]="routerLinkparse(item.routerLink)" [queryParams]="item.queryParams" [routerLinkActive]="activeSubClass" [routerLinkActiveOptions]="item.routerLinkActiveOptions||{exact:false}" [ngClass]="{'ui-panelmenu-headerlink-hasicon':item.icon}" (click)="handleClick($event,item)"
                        [attr.target]="item.target" [attr.title]="item.title">
                        <span *ngIf="item.items && showLabel" class="ui-panelmenu-icon fa fa-angle-right" [class]="expandedClass" [ngClass]="{'rotate-0':!item.expanded,'rotate-90':item.expanded}"></span
                        ><span class="ui-menuitem-icon fa m-0-21" [ngClass]="item.icon || 'fa-circle-o'" ></span
                        ><span class="ui-menuitem-text " *ngIf="showLabel">{{item.label}}</span>
                    </a>
                </div>
                <div *ngIf="item.items" class="ui-panelmenu-content-wrapper" [@rootItem]="item.expanded ? 'visible' : 'hidden'"  (@rootItem.done)="onToggleDone($event)"
                    [ngClass]="{'ui-panelmenu-content-wrapper-overflown': !item.expanded||animating}">
                    <div [ngClass]="'ui-panelmenu-content ui-widget-content'" [class]="subClass">
                        <p-panelMenuSub [item]="item" [multiple]="multiple" [expandedClass]="expandedClass" [activeSubClass]="activeSubClass" [expanded]="true"></p-panelMenuSub>
                    </div>
                </div>
            </div>
        </div>
    `,
    styles:[`
    .rotate-90{
        transition: transform 0.3s;
        transform: rotate(90deg);
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
    }
    .rotate-0{
        transition: transform 0.3s;
        transform: rotate(0);
        -webkit-transform: rotate(0);
        -moz-transform: rotate(0);
        -o-transform: rotate(0);
        -ms-transform: rotate(0);
    `],
    animations: [
        trigger('rootItem', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class PanelMenu extends BasePanelMenuItem {

    @Input() showLabel:boolean = true;

    @Input() subClass:string;

    @Input() expandedClass:string;

    @Input() width:any = 210;

    @Input() activeSubClass:string = 'ui-state-active';

    @Input() activeClass:string;
    
    @Input() model: MenuItem[];

    @Input() style: any;

    @Input() styleClass: string;

    @Input() multiple: boolean = true;
    
    public animating: boolean;
                
    collapseAll() {
    	for(let item of this.model) {
    		if(item.expanded) {
    			item.expanded = false;
    		}
    	}
    }

    handleClick(event, item) {
        if(!this.showLabel){event.preventDefault();return}
    	if(!this.multiple) {
            for(let modelItem of this.model) {
        		if(item !== modelItem && modelItem.expanded) {
        			modelItem.expanded = false;
        		}
        	}
    	}
        
        this.animating = true;
        super.handleClick(event, item);
    }
    
    onToggleDone() {
        this.animating = false;
    }

}

@NgModule({
    imports: [CommonModule,RouterModule],
    exports: [PanelMenu,RouterModule],
    declarations: [PanelMenu,PanelMenuSub]
})
export class PanelMenuModule { }
