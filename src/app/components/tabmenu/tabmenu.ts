import {NgModule,Component,ElementRef,Input,Output} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DomHandler} from '../dom/domhandler';
import {MenuItem} from '../common/menuitem';
import {Location} from '@angular/common';
import {RouterModule} from '@angular/router';

@Component({
    selector: 'p-tabMenu',
    styles:[`a{width:115px;color:#999;line-height:90px;padding:0 30px !important;text-align:center} .active a{color:#1a91eb} a:hover{color:#1a91eb !important}
     .bottom-bar{display: block;position: absolute;bottom: 0;height: 2px;background: #1a91eb;-webkit-transition: ease-out 0.5s;transition: ease-out 0.5s;width:71px}
    `],
    template: `
        <div [ngClass]="'ui-tabmenu ui-widget ui-widget-content ui-corner-all'" [ngStyle]="style" [class]="styleClass">
            <ul class="ui-tabmenu-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all p-0 relative" role="tablist">
                <li *ngFor="let item of model" 
                    [ngClass]="{'ui-tabmenuitem ui-state-default ui-corner-top border-none bg-none m-0':true,'ui-state-disabled':item.disabled,
                        'ui-tabmenuitem-hasicon':item.icon,'active':activeItem==item,'ui-helper-hidden': item.visible === false}"
                        [routerLinkActive]="'ui-state-active'" [routerLinkActiveOptions]="item.routerLinkActiveOptions||{exact:false}">
                    <a *ngIf="!item.routerLink" [href]="item.url||'#'" class="ui-menuitem-link ui-corner-all" (click)="itemClick($event,item)"
                        [attr.target]="item.target" [attr.title]="item.title" [attr.id]="item.id">
                        <span class="ui-menuitem-icon fa" [ngClass]="item.icon" *ngIf="item.icon"></span>
                        <span class="ui-menuitem-text">{{item.label}}</span>
                    </a>
                    <a *ngIf="item.routerLink" [routerLink]="item.routerLink" [queryParams]="item.queryParams" class="ui-menuitem-link ui-corner-all" (click)="itemClick($event,item)"
                        [attr.target]="item.target" [attr.title]="item.title">
                        <span class="ui-menuitem-icon fa" [ngClass]="item.icon" *ngIf="item.icon"></span>
                        <span class="ui-menuitem-text">{{item.label}}</span>
                    </a>
                </li>
                <li class="bottom-bar"  [style.left.px]="22 + activeIndex*115"></li>
            </ul>
        </div>
    `,
    providers: [DomHandler]
})
export class TabMenu {

    @Input() model: MenuItem[];

    @Input() activeItem: MenuItem;

    @Input() popup: boolean;

    @Input() style: any;

    @Input() styleClass: string;

    get activeIndex(){
        if(!this.activeItem || !this.model){return undefined}
        let index = this.model.findIndex(val=>val==this.activeItem)
        return index
        
    }

    itemClick(event: Event, item: MenuItem) {
        
        if(item.disabled) {
            event.preventDefault();
            return;
        }

        if(!item.url) {
            event.preventDefault();
        }

        if(item.command) {
            item.command({
                originalEvent: event,
                item: item
            });
        }

        this.activeItem = item;
    }
}

@NgModule({
    imports: [CommonModule,RouterModule],
    exports: [TabMenu,RouterModule],
    declarations: [TabMenu]
})
export class TabMenuModule { }
