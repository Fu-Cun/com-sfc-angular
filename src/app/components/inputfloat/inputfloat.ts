import {NgModule,Directive,ElementRef,HostListener,Input,DoCheck,AfterViewInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import { NgModel } from '@angular/forms'

@Directive({
    selector: '[pInputFloat]',
    providers: [NgModel],
    host: {
        '[class.ui-inputtext]': 'true',
        '[class.ui-corner-all]': 'true',
        '[class.ui-state-default]': 'true',
        '[class.ui-widget]': 'true',
        '[class.ui-state-filled]': 'filled',
        
    }
})
export class InputFloat implements DoCheck,AfterViewInit {

    @Input() maxlength:string = '20';

    @Input() numberType:string;
    
    filled: boolean;

    constructor(public el: ElementRef,private control:NgModel) {}
        
    ngDoCheck() {
        this.updateFilledState();
    }
    
    //To trigger change detection to manage ui-state-filled for material labels when there is no value binding
    @HostListener('input', ['$event']) 
    onInput(e) {
        this.valueChange()
        this.updateFilledState();
    }

    ngAfterViewInit() {
        this.el.nativeElement.setAttribute('maxlength',this.maxlength);
        this.el.nativeElement.className += ' p-input';
        
    }
    
    updateFilledState() {
        this.filled = this.el.nativeElement.value && this.el.nativeElement.value.length;
    }

    valueChange(){
        switch(this.numberType){
            case 'number':this.onlyNumber(); break;   
            case 'decimal':this.decimal(); break;   
            case 'decimal2':this.decimal(true); break;   
            case 'initAndZero':this.initAndZero(); break;   
            case 'intNotZero':this.intNotZero(); break;   
        }
    }

    initAndZero(){
        this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^\d]/g,'');
        if(this.el.nativeElement.value){this.el.nativeElement.value = parseFloat(this.el.nativeElement.value);}
        this.control.viewToModelUpdate(this.el.nativeElement.value);
    }

    intNotZero(){
        this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^\d]/g,'');
        if(this.el.nativeElement.value.indexOf("0")==0){
            this.el.nativeElement.value = this.el.nativeElement.value.substring(0, this.el.nativeElement.value.length - 1)
        }
        this.control.viewToModelUpdate(this.el.nativeElement.value);
    }

    decimal(floadNum?){
        this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
        this.el.nativeElement.value = this.el.nativeElement.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的
        if(this.el.nativeElement.value.indexOf(".")==0){
            this.el.nativeElement.value = this.el.nativeElement.value.substring(0, this.el.nativeElement.value.length - 1)
        }
        this.el.nativeElement.value = this.el.nativeElement.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        if(floadNum){this.el.nativeElement.value = this.el.nativeElement.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3')};//只能输入两个小数
        if(this.el.nativeElement.value.indexOf(".")< 0 && this.el.nativeElement.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            this.el.nativeElement.value = parseFloat(this.el.nativeElement.value);
        }
        this.control.viewToModelUpdate(this.el.nativeElement.value);
    }

    onlyNumber(){
        this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^\d]/g,'');
        this.control.viewToModelUpdate(this.el.nativeElement.value);
    }
}

@Directive({
    selector: '[pUnit]'
})
export class Unit implements AfterViewInit {

    constructor(public el: ElementRef) {}
    
    @Input() unit:string;

    ngAfterViewInit() {
        if(this.unit){
            let iconElement = document.createElement("span");
        }
        
    }

}

@NgModule({
    imports: [CommonModule],
    exports: [InputFloat,Unit],
    declarations: [InputFloat,Unit]
})
export class InputFloatModule { }