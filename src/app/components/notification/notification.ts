import { Component, ElementRef, OnInit } from '@angular/core'
import { notifyAnimation } from '../../common/animate.common'

export const typeMap: any = {
  success: 'fa-check-circle',
  info: 'fa-info-circle',
  warning: 'fa-warning',
  error: 'fa-times-circle',
}

@Component({
  selector: 'notification-container',
  styles:[`
      .notification{position:fixed;min-width:250px;padding:20px;right:5px;}
      .notification-close{position: absolute;right: 10px;top: 10px;}
      .notification-title{font-size: 16px;padding: 5px 0;font-weight: 600;}
      .notification-content{opacity:0.8}
      .notification-group{min-width: 200px;max-width: 300px;}
      .success{background:#b4f0b6;color:#2c832f}
      .info{background:#bfe0fa;color:#1765a3}
      .warning{background:#ffe9b5;color:#8a6714}
      .error{background:#ffcbc8;color:#ab1a0f}
      .type-icon{vertical-align: middle;font-size: 24px;margin-right: 10px;}
  `],
  template: `
    <div [class]="'notification ' + horizontalDirection + ' ' + type + ' ' + customClass"
      [@notifyAnimation]="showBox" [style.top.px]="offset" [style.z-index]="zIndex" style="visibility: hidden;"
      (mouseenter)="clearTimer()" (mouseleave)="startTimer()">
      <i [class]="'fa type-icon ' + makeClass() + ' ' + iconClass" *ngIf="type || iconClass"></i>
      <div class="notification-group v-middle inline-block" [class.is-with-icon]="makeClass() || iconClass">
        <div class="notification-title ellipsis" [title]="title" *ngIf="title">{{title}}</div>
        <div class="notification-content font-14">
          <span class="ellipsis" [title]="message">{{message}}</span>
        </div>
        <div class="notification-close h-pointer fa fa-times" (click)="close()"></div>
      </div>
    </div>
  `,
  animations: [notifyAnimation]
})
export class NotificationContainer implements OnInit {
  
  // element id, for destroy com
  id: string
  height: number = 0
  
  // user setting
  offset: number = 55
  type: string = ''
  duration: number = 2000
  iconClass: string = ''
  customClass: string = ''
  zIndex: number = 1099
  position: string = 'top-right'
  
  title: string = ''
  message: string = ''
  showBox: boolean = false
  timer: any
  horizontalDirection: string = 'right'
  
  onClose: Function = () => {}
  onDestroy: Function = () => {}
  
  constructor(
    private el: ElementRef,
  ) {
  }
  
  makeClass(): string {
    return typeMap[this.type]
  }
  
  setContent(message: string, title: string = ''): void {
    this.message = message
    this.title = title
    setTimeout(() => {
      this.height = this.el.nativeElement.children[0].offsetHeight
    }, 0)
  }
  
  show(): void {
    this.showBox = true
    this.timer = setTimeout(() => {
      this.close()
    }, this.duration)
  }
  
  close(): void {
    this.timer && clearTimeout(this.timer)
    this.showBox = false
    this.onClose()
    this.onDestroy()
  }
  
  startTimer(): void {
    if (!this.showBox) return
    this.timer = setTimeout(() => {
      this.close()
    }, this.duration)
  }
  
  clearTimer(): void {
    this.timer && clearTimeout(this.timer)
  }
  
  ngOnInit(): void {
    this.horizontalDirection = this.position.includes('right') ? 'right' : 'left'
  }
  
}
