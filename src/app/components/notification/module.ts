import { NgModule, ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NotificationContainer } from './notification'
import { ExDynamicService } from '../common/dynamic'
import { NotificationService } from './notification.service'

@NgModule({
  declarations: [NotificationContainer],
  exports: [NotificationContainer],
  imports: [CommonModule],
  entryComponents: [NotificationContainer],
})
export class NotificationModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: NotificationModule, providers: [
      NotificationService,ExDynamicService
    ]}
  }
}
