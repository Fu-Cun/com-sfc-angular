import {NgModule,Component,Input,Output,EventEmitter,Optional} from '@angular/core';
import {CommonModule} from '@angular/common';

@Component({
    selector: 'p-message',
    styles:[`.ui-messages-error{color:#ff0000 !important;border:none !important;background:none !important}
    .ui-messages-info {color: #fff;background: none;border: none;}`],
    template: `
        <div aria-live="polite" class="ui-message ui-widget ui-corner-all {{styleClass}}" *ngIf="severity && message"
        [ngClass]="{'ui-messages-info': (severity === 'info'),
                'ui-messages-warn': (severity === 'warn'),
                'ui-messages-error': (severity === 'error'),
                'ui-messages-success': (severity === 'success')}">
            <span class="ui-message-icon m-r-5" *ngIf="icon" [ngClass]="icon"></span>
            <span class="ui-message-text" [ngClass]="{'color-333 opacity06':(severity === 'info')}">{{message}}</span>
        </div>
    `
  

})
export class UIMessage {

    @Input() styleClass: string;

    @Input() severity: string;

    @Input() isValue: boolean;

    _text:any;

    _timeout:any;

    @Input() get text(): any {
        return this._text;
    }

    set text(val: any) {
        this._text = val
        
        // if(this._timeout){
        //     clearTimeout(this._timeout)
        // }
        // this._timeout = setTimeout(()=>this._text = val,100)
    }

    get message():string{
        let message:any;
        if(typeof this.text == 'string'){return this.text}
        if(typeof this.text == 'object'){
            let ms = Object.keys(this.text);
            if(!this.isValue){
                message = ms.find(v=>this.text[v])
            }else{
                message = ms?(ms[0]?this.text[ms[0]]:''):'' 
            }
            return message;
        }
        
    }
        
    get icon(): string {
        let icon: string = null;
        
        if(this.severity) {
            switch(this.severity) {
                case 'success':
                    icon = 'fa fa-check';
                break;
                
                case 'info':
                    icon = 'fa fa-times-circle color-fff';
                break;
                
                case 'error':
                    icon = 'fa fa-times-circle';
                break;
                
                case 'warn':
                    icon = 'fa fa-exclamation-circle';
                break;
                
                case 'success':
                    icon = 'fa fa-check';
                break;
                
                default:
                    icon = 'fa fa-info-circle';
                break;
            }
        }
        
        return icon;
    }
}



@NgModule({
    imports: [CommonModule],
    exports: [UIMessage],
    declarations: [UIMessage]
})
export class MessageModule { }