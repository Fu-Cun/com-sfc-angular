import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TimeConfigClass } from '../../common/common-config';
import { StorageService } from "../../common/storage.service";
import { LeavemessageService } from './leavemessage.service';

@Component({
  selector: 'app-leavemessage',
  templateUrl: './leavemessageList.component.html',
  styleUrls: ['./leavemessageList.component.css'],
  providers: [LeavemessageService],
  styles: [':host /deep/ .ui-datatable{margin-top:29px} :host /deep/ .staff .org .relative{display:inline-block} :host /deep/ .staff .org .relative .ui-tree-container{display:block} .m-l-26{margin-left:26px}']
})
export class LeavemessageListComponent implements OnInit {
  //列表
  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  colsSelected: any;
  cols: any = [];
  selectedItem: any = []; //选中记录
  pageSize: number; // 每页条数
  pageNo: number; // 当前页
  listData: any = []; // 数据
  totalRecords: number; // 数据条数
  searchObj: any = {}; // 搜索条件
  searcSortObj: any = {}; // 排序信息
  submited: boolean ;

  tree: any[] = []; //目录
  selectedFiles: any = [];;

  constructor(private storage: StorageService, private route: ActivatedRoute, private router: Router, private server: LeavemessageService) {

    this.cols = [
      { field: 'name', header: '名称', frozen: false, sortable: true, width: '8%' },
      { field: 'phone', header: '电话', sortable: true, width: '8%' },
      { field: 'email', header: '邮箱', sortable: true, width: '8%' },
      { field: 'content', header: '内容', sortable: true, width: '18%' },
      { field: 'dateCreate', header: '创建时间', sortable: true, width: '8%' }
    ];

    this.colsSelected = this.cols;

  }

  ngOnInit() {

      //this.findTree();

  }


  /**
   * 获取数据
   */
  getData(params) {
    //let params = this.searchObj;
    this.storage.loading();
    this.server.list(params, this.pageSize, this.pageNo).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );
  }
  /**
   * 改变模糊搜索框数据值
   */
  searchTextChange(event) {
    if (event == undefined)
      return false
    if (event.constructor == String && event != '') {
      this.searchObj['searchValue'] = event;
    } else {
      this.searchObj['searchValue'] = undefined;
    };
  }
  /**
   * 搜索方法
   * @param event
   */
  search(event?) {
    //筛选查询
    this.searchTextChange(event);
    // this.pageNo = 1;
    const params = Object.assign(this.searchObj);
    params.catalogCode = this.selectedFiles ? this.selectedFiles.code  : '';
    this.getData(params);
  }
  /**
   * 分页查询
   * @param event
   */
  loadCarsLazy(event) {
    this.pageSize = + event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.searcSortObj['sortF'] = event.sortField;
    this.searcSortObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    const params = Object.assign(this.searchObj, this.searcSortObj);
    this.getData(params);
  }
  

  /**
   * 
   * @param data 树
   */
  expandAll(data){
    data.forEach( node => {
      this.expandRecursive(node, true);
    } );
  }

  private expandRecursive(node, isExpand:boolean){
    node.expanded = isExpand;
    if(node.children){
      node.children.forEach( childNode => {
        this.expandRecursive(childNode, isExpand);
      } );
    }
  }

}
