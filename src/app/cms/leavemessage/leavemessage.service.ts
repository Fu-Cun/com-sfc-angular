import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../common/http-interceptor.service';

@Injectable()
export class LeavemessageService {

    // 客户自助服务名称
    private serveName = '';
  
    constructor(public httpService: HttpInterceptorService) {}

    /**
     * 获取列表 分页方法
     * @param pageSize 分页大小 默认值10
     * @param pageNo  当前页 默认值是1
     * @param search  搜索条件
     */
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
        return  this.httpService.post(this.serveName + 'cmsLeaveMessage/pageList/api', { pageSize: pageSize, pageNo: pageNo, ...search } );
    }


    /**
     * 获取详情
     */
    get(search: any = {}) {
        return  this.httpService.post(this.serveName + 'cmsLeavemessage/get/api', { ...search } );
    }

    
}
