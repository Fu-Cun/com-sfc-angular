import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

// 模块引入
import { ArticleListComponent } from './article/articleList.component';
import { ArticleAddComponent } from './article/articleAdd.component';
import { CatalogListComponent } from './catalog/catalogList.component';
import { CatalogAddComponent } from './catalog/catalogAdd.component';
import { SiteinfoListComponent } from './siteinfo/siteinfoList.component';
import { SiteinfoAddComponent } from './siteinfo/siteinfoAdd.component';
import { LeavemessageListComponent } from './leavemessage/leavemessageList.component';

/**
 * 客户自助APP后台路由参数配置
 */
const mainRoutes: Routes = [
  {path: 'article', component: ArticleListComponent, data:{title:'文章管理'} },
  {path: 'articleAdd/:id', component: ArticleAddComponent, data:{title:'文章编辑'} },
  {path: 'catalog', component: CatalogListComponent, data:{title:'栏目管理'} },
  {path: 'catalogAdd/:id', component: CatalogAddComponent, data:{title:'栏目编辑'} },
  {path: 'siteinfo', component: SiteinfoListComponent, data:{title:'网站管理'} },
  {path: 'siteinfoAdd/:id', component: SiteinfoAddComponent, data:{title:'网站编辑'} },
  {path: 'Leavemessage', component: LeavemessageListComponent, data:{title:'留言管理'} }
];
/**
 * 用户管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CmsRoutingModule { }
