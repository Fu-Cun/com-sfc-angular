import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../common/http-interceptor.service';

@Injectable()
export class ArticleService {

    // 客户自助服务名称
    private serveName = '';
  
    constructor(public httpService: HttpInterceptorService) {}

    /**
     * 获取列表 分页方法
     * @param pageSize 分页大小 默认值10
     * @param pageNo  当前页 默认值是1
     * @param search  搜索条件
     */
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
        return  this.httpService.post(this.serveName + 'cmsArticle/pageList/api', { pageSize: pageSize, pageNo: pageNo, ...search } );
    }

    /**
     * 保存
     * @param params 
     */
    save(params: any){
        return  this.httpService.post(this.serveName + 'cmsArticle/save/api', {...params} );
    }

    /**
     * 获取详情
     */
    get(search: any = {}) {
        return  this.httpService.post(this.serveName + 'cmsArticle/get/api', { ...search } );
    }

    /**
     * 删除
     */
    delete(search: any = {}) {
        return  this.httpService.post(this.serveName + 'cmsArticle/delete/api', { ...search } );
    }

    /**
     * 获取模板列表
     */
    getTemplateArr(search: any = {}) {
        return  this.httpService.post(this.serveName + 'cmsTemplate/list/api', { ...search } );
    }

    /**
     * 查询目录树
     */
    findTree(search: any = {}) {
        return  this.httpService.post(this.serveName + 'cmsCatalog/findTree/api', { ...search } );
    }

    /**
     * 查询网站
     */
    findSite(search: any = {}) {
        return  this.httpService.post(this.serveName + 'cmsSiteinfo/list/api', { ...search } );
    }


}
