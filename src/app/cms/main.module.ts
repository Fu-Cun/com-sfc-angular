import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CmsRoutingModule } from './main-routing.module';

// primeNG
import { CheckboxModule } from '../components/checkbox/checkbox';
import { PanelModule } from '../components/panel/panel';
import { InputTextModule } from '../components/inputtext/inputtext';
import { DataTableModule } from '../components/datatable/datatable';
import { MultiSelectModule } from '../components/multiselect/multiselect';
import { InputMaskModule } from '../components/inputmask/inputmask';
import { DropdownModule } from '../components/dropdown/dropdown';
import { InputSwitchModule } from '../components/inputswitch/inputswitch';
import { ButtonModule } from '../components/button/button';
import { RatingModule } from '../components/rating/rating';
import { CalendarModule } from '../components/calendar/calendar';
import { RadioButtonModule } from '../components/radiobutton/radiobutton';
import { InputTextareaModule } from '../components/inputtextarea/inputtextarea';
import { FileUploadModule } from '../components/fileupload/fileupload';
import { DialogModule } from '../components/dialog/dialog';
import { LightboxModule } from '../components/lightbox/lightbox';
import { FieldsetModule } from '../components/fieldset/fieldset';
import { SpinnerModule } from '../components/spinner/spinner';
import { MessageModule } from '../components/message/message';
import { OverlayPanelModule } from '../components/overlaypanel/overlaypanel';
import {LabelModule} from '../components/label/label';
import { from } from 'rxjs/observable/from';
import { TreeModule } from '../components/tree/tree';
/*
 * 模块引入
 */
//文章
import { ArticleListComponent } from './article/articleList.component';
import { ArticleAddComponent } from './article/articleAdd.component';
//栏目
import { CatalogListComponent } from './catalog/catalogList.component';
import { CatalogAddComponent } from './catalog/catalogAdd.component';
//网站
import { SiteinfoListComponent } from './siteinfo/siteinfoList.component';
import { SiteinfoAddComponent } from './siteinfo/siteinfoAdd.component';
//留言
import { LeavemessageListComponent } from './leavemessage/leavemessageList.component';


/**
 * 施工队模块
 */
@NgModule({
  imports: [
    CmsRoutingModule,
    CommonModule,
    FormsModule,
    PanelModule,
    InputTextModule,
    DataTableModule,
    MultiSelectModule,
    ButtonModule,
    InputMaskModule,
    DropdownModule,
    InputSwitchModule,
    CalendarModule,
    RatingModule,
    CheckboxModule,
    RadioButtonModule,
    InputTextareaModule,
    FileUploadModule,
    DialogModule,
    ReactiveFormsModule,
    LightboxModule,
    FieldsetModule,
    SpinnerModule,
    MessageModule,
    SpinnerModule,
    OverlayPanelModule,
    LabelModule,
    TreeModule
  ],
  declarations: [
    ArticleListComponent,
    ArticleAddComponent,
    CatalogListComponent,
    CatalogAddComponent,
    SiteinfoListComponent,
    SiteinfoAddComponent,
    LeavemessageListComponent
  ],
  exports: [],
  providers: []
})
export class CmsModule {
}
