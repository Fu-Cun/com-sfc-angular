import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TimeConfigClass } from '../../common/common-config';
import { StorageService } from "../../common/storage.service";
import { SiteinfoService } from './siteinfo.service';

@Component({
  selector: 'app-siteinfo',
  templateUrl: './siteinfoList.component.html',
  styleUrls: ['./siteinfoList.component.css'],
  providers: [SiteinfoService],
  styles: [':host /deep/ .ui-datatable{margin-top:29px} :host /deep/ .staff .org .relative{display:inline-block} :host /deep/ .staff .org .relative .ui-tree-container{display:block} .m-l-26{margin-left:26px}']
})
export class SiteinfoListComponent implements OnInit {
  //列表
  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  colsSelected: any;
  cols: any = [];
  selectedItem: any = []; //选中记录
  pageSize: number; // 每页条数
  pageNo: number; // 当前页
  listData: any = []; // 数据
  totalRecords: number; // 数据条数
  searchObj: any = {}; // 搜索条件
  searcSortObj: any = {}; // 排序信息
  submited: boolean ;

  tree: any[] = []; //目录
  selectedFiles: any = [];;

  constructor(private storage: StorageService, private route: ActivatedRoute, private router: Router, private server: SiteinfoService) {

    this.cols = [
      { field: 'name', header: '名称', frozen: false, sortable: true, tem: true, width: '8%' },
      { field: 'url', header: '网址', sortable: true, width: '8%' },
      { field: 'phone', header: '电话', sortable: true, width: '8%' },
      { field: 'description', header: '描述', sortable: true, width: '18%' },
      { field: 'sortNum', header: '序号', sortable: true, width: '8%' },
      { field: 'dateCreate', header: '创建时间', sortable: true, width: '8%' }
    ];

    this.colsSelected = this.cols;

  }

  ngOnInit() {

      //this.findTree();

  }

  //新建
  add() {
    //debugger
    this.router.navigate(['../siteinfoAdd', '0'], { relativeTo: this.route }); // 相对于当前路由跳转
    // this.router.navigate(['/saas/sys/system/staffManagement/add']); // 相对于绝对路径跳转
  }

  /**
   * 编辑
   */
  edit(code) {
    if(code==undefined){
      this.storage.messageService.emit({ severity: 'error', detail:'失败' })
      return false;
    };
    this.router.navigate(['../siteinfoAdd', code], { relativeTo: this.route }); // 相对于当前路由跳转
  }

   /**
   * 生成页面
   */
  toHtml() {
    let list = this.selectedItem;
    if (list.length != 1) {
      this.storage.messageService.emit({severity:'warn', detail:'请选择一个网站'})
      return false;
    }
    let code = list[0].code
    let params = {parentCode:code};
    this.server.toHtml(params).then(data => {
      if (data.result) {
        this.storage.messageService.emit({severity:'success', detail:"操作成功！"})
        //this.storage.messageService.emit({severity:'warn', detail:'已超出最大商机量'})
        console.log('生成页面成功')
      } else {
        this.storage.messageService.emit({ severity: 'error', detail:'生成页面失败' })
        console.log('生成页面失败')
      }
    });
  
  }


  /**
   * 获取数据
   */
  getData(params) {
    //let params = this.searchObj;
    this.storage.loading();
    this.server.list(params, this.pageSize, this.pageNo).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );
  }
  /**
   * 改变模糊搜索框数据值
   */
  searchTextChange(event) {
    if (event == undefined)
      return false
    if (event.constructor == String && event != '') {
      this.searchObj['searchValue'] = event;
    } else {
      this.searchObj['searchValue'] = undefined;
    };
  }
  /**
   * 搜索方法
   * @param event
   */
  search(event?) {
    //筛选查询
    this.searchTextChange(event);
    // this.pageNo = 1;
    const params = Object.assign(this.searchObj);
    params.catalogCode = this.selectedFiles ? this.selectedFiles.code  : '';
    this.getData(params);
  }
  /**
   * 分页查询
   * @param event
   */
  loadCarsLazy(event) {
    this.pageSize = + event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.searcSortObj['sortF'] = event.sortField;
    this.searcSortObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    const params = Object.assign(this.searchObj, this.searcSortObj);
    this.getData(params);
  }


  /**
   *删除数据
   */
  delete() {
    let list = this.selectedItem;
    if (list.length != 1) {
      this.storage.messageService.emit({severity:'warn', detail:'请选择一个网站'})
      return false;
    };
    let id = list[0].id; 
    let params = {id:id};
    this.server.delete(params).then(data => {
      if (data.result) {
        this.storage.messageService.emit({severity:'success', detail:data.msg})
        this.search();
        console.log(data.msg)
      } else {
        this.storage.messageService.emit({ severity: 'error', detail:data.msg})
        console.log(data.msg)
      }
    });
  }

}
