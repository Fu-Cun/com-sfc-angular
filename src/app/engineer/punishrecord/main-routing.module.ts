import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

// 处罚管理- 处罚详情
import { InfoPunishrecordComponent } from './punishrecordmanage/punishrecord/info.component';
// 处罚管理- 处罚列表
import { ListPunishrecordComponent } from './punishrecordmanage/punishrecord/list.component';

/**
 * 客户自助APP后台路由参数配置
 */
const mainRoutes: Routes = [
  {
    path: 'punishrecord',
    component: ListPunishrecordComponent,
    data: { title: '处罚记录' }
  },
  {
    path: 'punishrecord/:id',
    component: InfoPunishrecordComponent,
    data: { title: '处罚详情', 'hasSidebar': false, 'hasHeader': false }
  },
];
/**
 * 用户管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PunishrecordRoutingModule { }
