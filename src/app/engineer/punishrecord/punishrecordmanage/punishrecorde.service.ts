import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class PunishrecordService {

  // 工程管理服务名称
  private serveName = 'com-dyrs-mtsp-constructionprocessservice';

  constructor(private httpService: HttpInterceptorService) {}

   /**
   * 获取列表 分页方法
   * @param pageSize 分页大小 默认值10
   * @param pageNo  当前页 默认值是1
   * @param search  搜索条件
   */
  list(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
    return  this.httpService.post(this.serveName + '/constructionInfoPunishrecord/list/api', { pageSize: pageSize, pageNo: pageNo, ...search } );
  }

  /**
   * 获取单条记录
   * @param code 
   */
  get(code: string){
    return  this.httpService.post(this.serveName + '/constructionInfoPunishrecord/get/api', {code} );
  }

  /**
   * 获取惩罚操作记录
   * @param code 
   */
  getRecordData(code: string){
    return  this.httpService.post(this.serveName + '/constructionInfoPunishrecordOperation/list/api', {code} );
  }

  /**
   * 申诉审核
   * @param code 
   */
  audit(params: any){
    return  this.httpService.post(this.serveName + '/constructionInfoPunishrecord/audit/api', {...params} );
  }

}
