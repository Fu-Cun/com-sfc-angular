import { Component, OnInit } from '@angular/core';
import { PunishrecordService } from '../punishrecorde.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from '../../../../common/storage.service';

@Component({
  selector: 'app-punishrecord-manage-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [PunishrecordService],
})
export class InfoPunishrecordComponent implements OnInit {


  isShow: boolean = false; //弹框是否显示
  dataKey: string = '';
  baseObj: any = {};//原始数据
  formObj: any = {};//表单数据
  formOptObj: any = {};//表单选中数据
  errorObj: any = {};//错误信息
  submited: boolean = false;

  punishRecordHead: any[] = []; // 处罚标题头
  punishRecordData: any = {}; // 处罚数据
  freeAmountDisplay: boolean = true; // 面额能否编辑

  //操作记录
  recordData: any[] = []; //
  cars: any[] = []; //

  constructor(private router: Router, private punishrecordService: PunishrecordService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {

  }

  ngOnInit() {

    this.punishRecordHead = [
      { title: '罚单流水号', field: 'code' },
      { title: '处罚类型', field: 'punishType', fun: (field) => this.storage.dicFilter('punishtype', this.punishRecordData[field]) },
      { title: '项目名称', field: 'businessOpportunityName' },
      { title: '罚款原因', field: 'punishReason' },
      { title: '项目编号', field: 'businessOpportunityCode' },
      { title: '备注说明', field: 'note' },
      { title: '所属组织', field: 'organizationCode', fun: (field) => this.storage.orgFilter(this.punishRecordData[field]) },
      { title: '罚款金额/元', field: 'pubishAmout' },
      { title: '责任人', field: 'responsibilitierName', fun: (field) => this.punishRecordData[field] + ' ' + this.storage.dicFilter('user_type', this.punishRecordData['responsibilitierType'])},
      { title: '免额/元', field: 'freeAmount' },
      { title: '创建人', field: 'createName', fun: (field) => this.punishRecordData[field] + ' ' + this.storage.dicFilter('user_type', this.punishRecordData['userType']) },
      { title: '实际金额/元', field: 'factAmount' },
      { title: '创建时间', field: 'dateCreated' },
      { title: '申诉原因', field: 'applyReason' },
      { title: '审核结果', field: 'status', fun: (field) => this.storage.dicFilter('penaltyStatus', this.punishRecordData[field] == 'penaltyStatus_180125000063' ? '' : this.punishRecordData[field]) },
      { title: '申诉时间', field: 'appealDate' },
      { title: '审核原因/说明', field: 'approveReason' },
      { title: '', field: 's' }
    ];

    this.routeInfo.params.subscribe((params) => {
      this.dataKey = params.id;
      this.getData();
      this.getRecordData();
    });

  }

  /**
  * 获取数据
  */
  getData() {
    this.storage.loading();
    this.punishrecordService.get(this.dataKey).then(data => {
      this.storage.loadout();
      if (data.result) {
        this.punishRecordData = data.data;
      } else {
        this.error('请求失败!记录不存在或已被删除');
      };
    });
  }

  /**
  * 获取操作记录
  */
  getRecordData() {
    this.storage.loading();
    this.punishrecordService.getRecordData(this.dataKey).then(data => {
      this.storage.loadout();
      if (data.result) {
        this.recordData = data.data;
      } else {
        this.error('请求失败!记录不存在或已被删除');
      };
    });
  }

  /*****表单 */
  /**
   * 重置表单数据
   */
  resetFormDate() {
    this.isShow = false; // 弹窗a
    this.baseObj = {}; // 原始数据
    this.formObj = {};
    this.formOptObj = {};
    this.errorObj = {}; // 错误信息
    this.submited = false;
  }

  /**
   * 打开申诉审核页面
   */
  openDialogAudit() {
    this.resetFormDate();
    this.freeAmountDisplay = true;
    this.formObj.status = 'penaltyStatus_180125000065'; //默认通过
    this.isShow = true;
  }
  /**
   * 关闭申诉审核页面
   */
  closeDialogAudit() {
    this.resetFormDate();
  }


  /**
   * 保存数据
   */
  save() {
    // 自定义验证
    if (!this.verifyCustom()) {
      return false;
    }
    let params = this.formObj;
    params['code'] = this.dataKey;
    this.storage.loading();
    this.punishrecordService.audit(params).then(data => {
      this.storage.loadout();
      if (data.result) {
        this.getData();
        this.getRecordData();
        this.isShow = false; // 弹窗a
      } else {
        this.error(data.msg);
      }
    });
  }


  /**
   * 自定义验证
   */
  verifyCustom() {
    if (this.formObj.status == undefined) {
      this.errorObj['status'] = { show: true };
    }
    if (this.formObj.status == undefined || !this.formObj.approveReason
      ||(!this.freeAmountDisplay && this.formObj.freeAmount==undefined)
      ||(this.formObj.freeAmount > this.punishRecordData.pubishAmout)) {

      this.errorObj['e'] = true;
      return false;
    }
    if (this.errorObj['e']) {
      delete this.errorObj['e'];
    }
    return true;
  }

  /**
   * 审核结果 验证
   */
  changeStatus() {
    if (this.formObj.status === 'penaltyStatus_180125000066') {
      this.freeAmountDisplay = true;
      this.formObj.freeAmount = '';
    } else {
      this.freeAmountDisplay = false;
    }
    delete this.errorObj['status'];
  }

  /**
   * 验证字段信息显示
   */
  verifyFields(e, obj) {
    if (e == undefined || e['name'] == undefined || obj == undefined) {
      return false;
    };
    let err = { show: true };
    if (e['value'] == undefined || e.value.trim() == '') {
      err['blank'] = true;
    };
    this.errorObj[e.name] = err;
    obj[e.name] = e['value'] == undefined ? '' : e.value.trim();
  }
  /**
   * 清除验证信息提示框
   */
  clearVerify(e) {
    if (e == undefined) {
      return false;
    }
    this.errorObj[e.name] = { show: false };
  }

  error(msg?: string) {
    msg = msg != undefined ? msg : '请选择要操作的数据！';
    this.storage.messageService.emit({ severity: 'error', detail: msg });
  }



}
