import { Component, OnInit } from '@angular/core';
import { PunishrecordService } from '../punishrecorde.service';
import { Router } from '@angular/router';
import { StorageService } from '../../../../common/storage.service';

@Component({
  selector: 'app-punishrecord-manage-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [PunishrecordService]
})
export class ListPunishrecordComponent implements OnInit {

  colsSelected: any;
  cols: any = [];
  selectedItem: any = []; //选中记录
  pageSize: number; // 每页条数
  pageNo: number; // 当前页
  listData: any = []; // 数据
  totalRecords: number; // 数据条数
  searchObj: any = {}; // 搜索条件
  searcSortObj: any = {}; // 排序信息
  selectedObj: any = {}; //选中条件

  //检索区
  statusArr: any = [];
  cities4: any = [];
  organizationArr: any = [];
  punishTypeArr: any = [];

  constructor(private router: Router, private punishrecordService: PunishrecordService, private storage: StorageService) {
    this.cols = [
      { field: 'sortNum', header: '序号', width: '80px', tem: true, hidden: false },
      { field: 'code', header: '处罚单号', frozen: false, tem: true, hidden: false },
      { field: 'projectCode', header: '项目编号', hidden: false },
      { field: 'organizationCode', header: '所属组织', tem: true, hidden: false },
      { field: 'responsibilitierName', header: '责任人', hidden: false, dic: 'user_type', tem: true },
      { field: 'punishType', header: '处罚类型', tem: true, hidden: false },
      { field: 'punishReason', header: '罚款原因', hidden: false },
      { field: 'pubishAmout', header: '罚款金额/元', hidden: false },
      { field: 'freeAmount', header: '免额/元', hidden: false },
      { field: 'factAmount', header: '实际金额/元', hidden: false },
      { field: 'createName', header: '创建人', hidden: false, dic: 'user_type', tem: true },
      { field: 'applyReason', header: '申诉原因', hidden: false },
      { field: 'status', header: '状态', tem: true, hidden: false }
    ];
    this.colsSelected = this.cols;
  }

  ngOnInit() {
    this.statusArr = [...this.storage.getDic('penaltyStatus')];
    this.punishTypeArr = [...this.storage.getDic('punishtype')];
    this.punishTypeArr.unshift({ code: '', name: '全部' });
    //所属组织
    let arr = [...this.storage.orgData];
    this.storage.getRights().then(res => {
      let userDataOrg = res.rights.userDataOrg;
      userDataOrg = userDataOrg.join(',');
      this.organizationArr = arr.filter(item => {
        if (userDataOrg.indexOf(item.code) > -1) {
          return true;
        }
      });
      this.organizationArr.unshift({ code: '', name: '全部' });
    });

  }

  /**
  * 获取数据
  */
  getData(params) {
    this.storage.loading();
    params['status'] = this.selectedObj.status ? this.selectedObj.status.code : '';
    this.punishrecordService.list(params, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.storage.loadout();
        } else {
          this.listData = [];
          this.totalRecords = 0;
          this.storage.loadout();
        }
      }
    );
  }
  /**
   * 改变模糊搜索框数据值
   */
  searchTextChange(event) {
    if (event == undefined)
      return false
    if (event.constructor == String && event != '') {
      this.searchObj['searchValue'] = event;
    } else {
      this.searchObj['searchValue'] = undefined;
    };
  }

  /**
   * 搜索方法
   * @param event
   */
  search(event?) {
    //筛选查询
    this.searchTextChange(event);
    const params = Object.assign(this.searchObj);
    this.getData(params);
  }

  /**
   * 分页查询
   * @param event
   */
  loadCarsLazy(event) {
    this.pageSize = + event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.searcSortObj['sortF'] = event.sortField;
    this.searcSortObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    const params = Object.assign(this.searchObj, this.searcSortObj);
    this.getData(params);
  }

}
