import {Component, OnInit} from "@angular/core";
import {TimeConfigClass} from "../../../../common/common-config";
import {Router, ActivatedRoute} from "@angular/router";
import {WorkerService} from "../worker.service";
import {TeaminfoService} from "../../team/teaminfo.service";
import {StorageService} from "../../../../common/storage.service";
@Component({
  selector: 'app-add-worker',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [WorkerService, TeaminfoService]
})
export class AddWorkerComponent implements OnInit {
  objToPost: any = {};
  selectObj: any = {};
  baseObj: any = {};
  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  uploadedFiles: any[] = [];
  kCode: string = '';//id
  isLoadDic: Boolean = false;
  loadDicCount: number = 0;
  workTypeArr: any = [];//工种
  sexArr: any = [];//性别
  teamInfoArr: any = [];//工队
  levelArr: any = [];//级别
  addressCurrentProvinceArr: any = []; //现居地_省
  addressCurrentCityArr: any = [];     //现居地_市
  addressCurrentAreaArr: any = [];     //现居地_区
  addressBirthProvinceArr: any = [];   //籍贯所在地_省
  addressBirthCityArr: any = [];       //籍贯所在地_市
  addressBirthAreaArr: any = [];       //籍贯所在地_区

  fileUrl: string = 'zuul/com-dyrs-mtsp-fileservice/fileservice/fileDFSSave/api';//附件服务器
  files: any = [];
  workPhotoArr = [];  //手持身份证照片
  idcardPhotoFrontArr = [] //身份证正面
  idcardPhotoReverseArr = [] //身份证反面
  electricAttachArr = [] //电工证
  pipeCardAttachArr = [] //管道证
  maxWorkingStartTime: any; // 从业日期最大值

  electricAttachEndTimeMinDate;
  pipeCardStartTimeMinDate;
  electricAttachEndTimeMaxDate;
  pipeCardStartTimeMaxDate;



  //验证
  submitSta: Boolean = false;
  isWorkTypeIn_DianGong: Boolean = false;
  codeCase: number;
  idNumberCase: number;
  telephoneCase: number;
  isElectricAttach: string;
  isPipeCard: Boolean;
  isWorkType: Boolean;
  teamId;
  descriptionFlag = true;
  addressCurrentDetailFlag = true;
  nameFlag = true;
  telephoneFlag = true;
  idNumberFlag = true;
  codeflag = true;
  submited = false;
  proCityDisError;  //省市区错误提示
  errorFiel:any;  //上传文件大小超过指定值


  workPhotoError;  //手持身份证附件
  idcardPhotoFrontError; // 身份证正面错误提示
  idcardPhotoReverseError; // 身份证反面错误提示
  electricAttachError; // 电工证附件错误提示
  pipeCardAttachError; // 管道证附件错误提示
  currentProvinceError; //省市区验证错误提示

  constructor(private teaminfoService: TeaminfoService, private route: ActivatedRoute,
    private router: Router, private server: WorkerService, private storage: StorageService) { }

  ngOnInit() {
    this.initDate();
  }
  /**
   * 初始化数据
   */
  initDate() {

    this.route.params.subscribe(params => {
      this.kCode = params['code'];
      if (this.kCode) {
        this.getData(this.kCode);
      } else {
        this.resetdate();
      }
      if (params.teamId) {
        this.selectObj.teamInfo = { 'id': Number(params.teamId) };
        this.teamId = params.teamId;
        this.server.getTeamInfoById(params.teamId).then((data) => {
          this.selectObj.teamInfo = data.data.teamInfo;
        });
      }
    });
    this.fileUrl = (this.server.httpService.baseUrl || 'http://172.18.8.76/') + this.fileUrl;
    this.selectObj['level'] = { code: 3, name: '3分' };
    this.getDictionaries();
    this.teaminfoService.getCurrentTime().then((data) => {
      if (data.result) {
        this.maxWorkingStartTime = new Date(data.data);
      } else {
        this.storage.messageService.emit({ severity: 'error', detail: '获取服务器时间失败!' });
      }
    });
  }
  /**
   * 重置表单数据
   */
  resetdate() {
    this.kCode = "";
    this.objToPost = {};
    this.selectObj = {};
    this.workPhotoArr = [];  //手持身份证照片
    this.idcardPhotoFrontArr = [];//身份证正面
    this.idcardPhotoReverseArr = [];
    this.electricAttachArr = [];//电工证
    this.pipeCardAttachArr = []; //管道证
    this.objToPost.level = 3;
    this.objToPost.sex = "sex_man";
  }

  /**
   * 返回列表页面
   */
  back() {
    this.storage.loading();
    if (this.kCode) {
      this.router.navigate(['saas/engineer/constructionteam/worker', this.kCode]);
      this.storage.loadout();
    } else {
      this.router.navigate(['saas/engineer/constructionteam/teaminfo']);
      this.storage.loadout();
    }

  }

  /**
   * Description: 数据监测
   */
  checkData(){
      let flag: boolean = true;
      if (!this.objToPost['workPhoto']) {
        flag = false;
        this.workPhotoError = '请上传手持身份证照片';
      } else {
        this.workPhotoError = '';
      }
      if (!this.objToPost['idcardPhotoFront']) {
        flag = false;
        this.idcardPhotoFrontError = '请上传身份证正面照片';
      } else {
        this.idcardPhotoFrontError = '';
      }
      if (!this.objToPost['idcardPhotoReverse']) {
        flag = false;
        this.idcardPhotoReverseError = '请上传身份证反面照片';
      } else {
        this.idcardPhotoReverseError = '';
      }
      if(this.isWorkTypeIn_DianGong && !this.objToPost['electricAttach']){
        flag = false;
        this.electricAttachError = "请上传电工证附件"
      }else{
        this.electricAttachError = '';
      }
      return flag;
  }

  /**
   * 保存
   */
  save(code: string) {
    this.submitSta = true;
    //自定义验证
    let flag: boolean = this.checkData();
    if (this.codeCase == 0 || this.idNumberCase == 0 || this.telephoneCase == 0 || this.isElectricAttach == 'false'
      || this.isPipeCard == false || this.isWorkType == false
      || this.workPhotoArr.length == 0 || this.idcardPhotoFrontArr.length == 0 || this.idcardPhotoReverseArr.length == 0
      || (this.isWorkTypeIn_DianGong == true && (this.objToPost.electricAttachStartTime == undefined || this.objToPost.electricAttachEndTime == undefined))
      || this.isWorkTypeIn_DianGong == true && (this.objToPost.electricAttach == undefined || this.objToPost.electricAttach == '' || !flag)) {
      return false;
    }
    let params = this.objToPost;
    params.foreman = '';
    if (this.selectObj.sex) {
      params['sex'] = this.selectObj.sex.code;
    }
    if (this.selectObj.workType) {
      let workType = this.selectObj.workType;
      params['workType'] = workType.filter(item => item != '').toString();
    } else {
      return false;
    }
    if (this.selectObj.level) {
      params['level'] = this.selectObj.level.code;
    }
    if (this.selectObj.teamInfo) {
      params['teamInfoId'] = this.selectObj.teamInfo.id;
      params['organization'] = this.selectObj.teamInfo.organization;
      params['organizationCode'] = this.selectObj.teamInfo.organizationCode;
    }
    if (this.selectObj.addressCurrentProvince) {
      params['addressCurrentProvince'] = this.selectObj.addressCurrentProvince.area_code;
    }
    if (this.selectObj.addressCurrentCity) {
      params['addressCurrentCity'] = this.selectObj.addressCurrentCity.area_code;
    }
    if (this.selectObj.addressCurrentArea) {
      params['addressCurrentArea'] = this.selectObj.addressCurrentArea.area_code;
    }
    if (this.selectObj.addressBirthProvince) {
      params['addressBirthProvince'] = this.selectObj.addressBirthProvince.area_code;
    }
    if (this.selectObj.addressBirthCity) {
      params['addressBirthCity'] = this.selectObj.addressBirthCity.area_code;
    }
    if (this.selectObj.addressBirthArea) {
      params['addressBirthArea'] = this.selectObj.addressBirthArea.area_code;
    }

    this.storage.loading();
    this.server.save(params).then(data => {
      this.storage.loadout();
      if (data.result) {
        if (code == 'submit') {
          if (this.teamId) {
            this.router.navigate(['saas/engineer/constructionteam/teaminfo']);
          } else {
            this.router.navigate(['saas/engineer/constructionteam/worker/', this.kCode]);
          }
          this.storage.messageService.emit({ severity: 'success', detail: data.msg });
        } else {
          this.resetdate();
          this.router.navigate(['saas/engineer/constructionteam/worker/add']);
          this.storage.messageService.emit({ severity: 'success', detail: data.msg });
        }
      } else {
        this.storage.messageService.emit({ severity: 'error', detail: data.msg });
      }
    });
  }


  /**
 * 根据用户选中的开始时间。设置结束时间的mixdate
 * @param mixDate
 */
  updateCalendarEndTime(type) {
    if (type === 'electric') {
      this.electricAttachEndTimeMinDate = new Date(this.objToPost['electricAttachStartTime']);
    } else if (type === 'pipe') {
      this.pipeCardStartTimeMinDate = new Date(this.objToPost['pipeCardStartTime']);
    }
  }
  /**
  * 根据用户选中的开始时间。设置结束时间的mixdate
  * @param mixDate
  */
  updateCalendarStartTime(type) {
    if (type === 'electric') {
      this.electricAttachEndTimeMaxDate = new Date(this.objToPost['electricAttachEndTime']);
    } else if (type === 'pipe') {
      this.pipeCardStartTimeMaxDate = new Date(this.objToPost['pipeCardEndTime']);
    }
  }

  /**
   * 现居地 行政区
   * @param val
   */
  currentChange(val) {
    if (val == 'province') {
      this.addressCurrentCityArr = this.selectObj.addressCurrentProvince.city;
      this.addressCurrentAreaArr = [];
      this.selectObj.addressCurrentCity = undefined;
      this.selectObj.addressCurrentArea = undefined;
      this.currentProvinceError = '';
    } else if (val == 'city') {
      this.addressCurrentAreaArr = this.selectObj.addressCurrentCity.area;
      this.selectObj.addressCurrentArea = undefined;
      this.currentProvinceError = '';
    }
  }
  /**
   * 籍贯所在地 行政区
   * @param val
   */
  birthChange(val) {
    if (val == 'province') {
      this.addressBirthCityArr = this.selectObj.addressBirthProvince.city;
      this.addressBirthAreaArr = [];
    } else if (val == 'city') {
      this.addressBirthAreaArr = this.selectObj.addressBirthCity.area;
    }
  }
  /**
   * 检测账号是否存在
   */
  checkCode(obj) {
    this.objToPost.code = this.objToPost.code ? this.objToPost.code.trim() : '';
    if (obj.errors != null) {
      return false;
    }
    // if (!this.objToPost.code) {
    //   this.codeCase = 4;
    // }
    this.codeCase = 2;
    setTimeout(() => {
      if (this.codeCase == 2) { this.codeCase = 3; }
    }, 10000);
    let code = this.objToPost.code
    this.server.checkCode({ code: code }).then(data => {
      if (data.result && (this.codeCase == 2)) {
        if (data.data == "true") {
          this.codeCase = 1;
          setTimeout(() => { this.codeCase = undefined; }, 2000);
        } else {
          this.codeCase = 0;
        };
      }
    });
  }
  /**
   * 检测身份证号是否存在
   */
  checkIdNumber(obj) {
    this.objToPost.idNumber = this.objToPost.idNumber ? this.objToPost.idNumber.trim() : '';
    if (obj.errors != null) {
      return false;
    }
    let code = this.objToPost.idNumber;
    if (this.baseObj.idNumber == code) {
      this.idNumberCase = undefined;
      return false;
    }
    this.idNumberCase = 2;
    setTimeout(() => {
      if (this.idNumberCase == 2) { this.idNumberCase = 3; }
    }, 10000);
    this.server.checkIdNumber({ idNumber: code }).then(data => {
      if (data.result && (this.idNumberCase == 2)) {
        if (data.data == true) {
          this.idNumberCase = 1;
          setTimeout(() => { this.idNumberCase = undefined; }, 2000);
        } else {
          this.idNumberCase = 0;
        };
      }
    });
  }
  /**
   * 清楚自定义验证状态
   * @param obj
   */
  clearVerifySta(obj) {
    this[obj] = undefined
  }

  /**
   * 手机号唯一验证
   * @param data
   */
  uniqueFTelephone(obj) {
    this.objToPost.telephone = this.objToPost.telephone ? this.objToPost.telephone.trim() : '';
    if (obj.errors != null) {
      return false;
    }
    let telephone = this.objToPost.telephone;
    if (this.baseObj.telephone == telephone) {
      return false;
    }
    this.telephoneCase = 2;
    setTimeout(() => {
      if (this.telephoneCase == 2) { this.telephoneCase = 3; }
    }, 10000);
    this.server.uniqueFTelephone({ 'mobile': telephone }).then(data => {
      if (this.telephoneCase == 2) {
        if (data.result == true) {
          this.telephoneCase = 1;
          setTimeout(() => {
            if (this.telephoneCase == 1) { this.telephoneCase = undefined; }
          }, 1500);
        } else {
          this.telephoneCase = 0;
        };
      }
    });

  }

  /**
   * 姓名验证
   */
  nameCk() {
    this.objToPost.name = this.objToPost.name ? this.objToPost.name.trim() : '';
  }

  /**
   * 当前地址验证
   */
  addressCurrentDetailCk() {
    this.objToPost.addressCurrentDetail = this.objToPost.addressCurrentDetail ? this.objToPost.addressCurrentDetail.trim() : '';
  }

  /**
   * 个人简历验证
   */
  descriptionCk() {
    this.objToPost.description = this.objToPost.description ? this.objToPost.description.trim() : '';
  }


  /**
   * 手机号唯一验证
   * @param data
   */
  // uniqueFTelephone(obj) {
  //   if (obj.errors != null) {
  //     return false;
  //   }
  // let telephone = this.objToPost.telephone
  // if(this.baseObj.telephone==telephone){
  //   return false;
  // }
  // this.telephoneCase = 2;
  // setTimeout(() => {
  //       if(this.telephoneCase==2){this.telephoneCase = 3;}
  //   }, 10000);
  // this.server.uniqueFTelephone({ 'mobile': telephone }).then(data => {
  //   if(data.result&&(this.telephoneCase==2)){
  //     if(data.data.isExists==true){
  //       this.telephoneCase = 1;
  //     }else{
  //       this.telephoneCase = 0;
  //     };
  //   }
  // });

  // }
  /**
   * 计算工作年限
   * @param d
   */
  workingStartTimeChange(d) {
    let days: number = new Date().getTime() - d.getTime();
    days = parseInt((days / (1000 * 60 * 60 * 24 * 365)) + '');
    this.objToPost.workingYears = days;
    this.objToPost.workingYearsShow = days == 0 ? '不满一年' : days + '年';
  }
  /**
   * 验证是否选择电工
   */
  workTypeChange() {
    let arr = (this.selectObj.workType == undefined) ? [] : this.selectObj.workType;
    this.isWorkType = (arr.length > 0) ? true : false;
    arr = arr.filter(item => item == 'worktype_electrician');
    this.isWorkTypeIn_DianGong = (arr.length > 0) ? true : false;
    if(!this.isWorkTypeIn_DianGong){
      this.electricAttachError = '';
    }
  }
  /**
   * 电工证起止日期验证
   */
  electricAttachChange() {
    const start = this.objToPost.electricAttachStartTime;
    const end = this.objToPost.electricAttachEndTime;
    if ((this.isWorkTypeIn_DianGong) && (start == undefined || start == '')) {
      // || ((start==undefined||start=='')&&(end!=undefined&&end!=''))
      this.isElectricAttach = 'start';
      return false;
    };
    if ((this.isWorkTypeIn_DianGong) && (end == undefined || end == '')) {
      //|| ((end==undefined||end=='')&&(start!=undefined&&start!=''))
      this.isElectricAttach = 'end';
      return false;
    };
    //this.isElectricAttach = 'true';
    if (start == undefined || start == '' || end == undefined || end == '') {
      return false;
    }
    // const time = new Date(end.replace(/-/g, "/")).getTime() - new Date(start.replace(/-/g, "/")).getTime();
    // this.isElectricAttach = (time >= 0) ? 'true' : 'false';
  }
  /**
   * 管道证起止日期验证
   */
  pipeCardChange() {
    const start = this.objToPost.pipeCardStartTime;
    const end = this.objToPost.pipeCardEndTime;
    if (start == undefined || start == '' || end == undefined || end == '') {
      return false;
    }
    const time = new Date(end.replace(/-/g, "/")).getTime() - new Date(start.replace(/-/g, "/")).getTime();
    this.isPipeCard = (time >= 0) ? true : false;
  }
  /**
  * 获取数据
  */
  getData(id) {
    this.server.get(id).then(
      data => {
        if (data.result) {
          this.objToPost = data.data;
          //工作年限
          this.objToPost.workingYearsShow = this.objToPost.workingYears == 0 ? '不满一年' : this.objToPost.workingYears + '年';
          this.baseObj = Object.assign(this.baseObj, data.data);
          this.loadDicData();
          this.imgUrlData();
          this.initVerify();
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }
  /**
   * 初始化数据验证
   */
  initVerify() {
    this.pipeCardChange();
    this.electricAttachChange();
    let workType = this.objToPost.workType;
    this.isWorkTypeIn_DianGong = (workType.indexOf('worktype_electrician') > -1) ? true : false;
  }

  /**
  * 加载字典项
  */
  getDictionaries() {
    // 加载工种和工人状态
    this.server.getDictionaries().then(
      data => {
        if (data.result) {
          this.levelArr = [{ code: 1, name: '1分' }, { code: 2, name: '2分' }, { code: 3, name: '3分' }, { code: 4, name: '4分' }, { code: 5, name: '5分' }]

          this.workTypeArr = data.data.workType;
          this.sexArr = data.data.sex;
          this.sexArr = this.sexArr.filter((items, index) => {
            if (index === 2) {
              return false;
            }else{
              return true;
            }
          });
          this.selectObj.sex = this.sexArr[0];
          this.teamInfoArr = data.data.teamInfoList;
          this.addressCurrentProvinceArr = data.data.district.province;
          this.addressBirthProvinceArr = data.data.district.province;

          this.isLoadDic = true;
          this.loadDicData();
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '字典服务异常！' });
        }
      }
    );
  }

  /**
   * 解析字典项数据
   */
  loadDicData() {
    if (this.isLoadDic && this.objToPost.id) {
      this.selectObj.sex = (this.sexArr.filter(item => item.code == this.objToPost.sex))[0];
      this.selectObj.level = (this.levelArr.filter(item => item.code == this.objToPost.level))[0];
      this.selectObj.workType = this.objToPost.workType.split(',')
      this.selectObj.teamInfo = (this.teamInfoArr.filter(item => item.id == this.objToPost.foreman.teamInfo.id))[0];
      //现居地
      this.selectObj.addressCurrentProvince = (this.addressCurrentProvinceArr.filter(item => item.area_code == this.objToPost.addressCurrentProvince))[0];
      this.addressCurrentCityArr = this.selectObj.addressCurrentProvince.city;
      this.selectObj.addressCurrentCity = (this.addressCurrentCityArr.filter(item => item.area_code == this.objToPost.addressCurrentCity))[0];
      this.addressCurrentAreaArr = this.selectObj.addressCurrentCity.area;
      this.selectObj.addressCurrentArea = (this.addressCurrentAreaArr.filter(item => item.area_code == this.objToPost.addressCurrentArea))[0];
      //户籍所在地
      this.selectObj.addressBirthProvince = (this.addressBirthProvinceArr.filter(item => item.area_code == this.objToPost.addressBirthProvince))[0];
      this.addressBirthCityArr = this.selectObj.addressBirthProvince ? this.selectObj.addressBirthProvince.city : [];
      this.selectObj.addressBirthCity = (this.addressBirthCityArr.filter(item => item.area_code == this.objToPost.addressBirthCity))[0];
      this.addressBirthAreaArr = this.selectObj.addressBirthCity ? this.selectObj.addressBirthCity.area : [];
      this.selectObj.addressBirthArea = (this.addressBirthAreaArr.filter(item => item.area_code == this.objToPost.addressBirthArea))[0];
    }
  }


  /**
  * 上传你成功以后数据会显并更新对应图片的数据
  * @param event
  * @param f
  */
  onUpload(event, f) {
    const result = JSON.parse(event.xhr.responseText);
    if (result && result.result && result.data) {
      this.objToPost[f.cancelLabel] = this.objToPost[f.cancelLabel] ? this.objToPost[f.cancelLabel] + ',' + result.data.fileCode :
        result.data.fileCode;
      for (let items of result.data.filePath.split(',')) {
        this[f.cancelLabel + 'Arr'] = [...this[f.cancelLabel + 'Arr'], {
          source: items, thumbnail: items,
          width: '100px', maxWidth: '800px', title: ''
        }];
      }
    }
  }


  /**
   * 上传单张图片验证
   */
  onSelect(event, id) {
    if (event.files.length <= 1) {
      event.upload();
      // this[id.cancelLabel + 'Error'] = '';
    } else {
      id.clear();
      this[id.cancelLabel + 'Error'] = '图片最多上传1张';
    }
  }

  /**
   * Description: 图片大小超过指定值
   */
  onSelectError(event,id){
    this.errorFiel = event;
    let errorType:any = event[0].errorType;
    if(errorType == "typeError"){
      this[id.cancelLabel + 'Error'] = "上传的图片类型错误";
    }
    if(errorType == "maxError"){
      this[id.cancelLabel + 'Error'] = "图片大小超过限制";
    }
  }

  /**
   * 上传9张图片验证
   */
  onSelect9(event, id) {
    if(this.errorFiel && this.errorFiel.length!=0){
      id.clear();
      this.errorFiel = undefined;
      return
    }else{
      this.errorFiel = undefined;
    }
    if (event.files.length <= (20 - this[id.cancelLabel + 'Arr'].length)) {
      event.upload();
      this[id.cancelLabel + 'Error'] = '';
    } else {
      id.clear();
      this[id.cancelLabel + 'Error'] = '图片最多上传20张';
    }
  }


  /**
  *删除图片并更新对应的图片数据
  * @param event
  * @param f
  */
  removeItem(event, f) {
    let codeArray = this.objToPost[f].split(',');
    codeArray.splice(event.index, 1);
    this.objToPost[f] = codeArray.join(',');
    this[f + 'Arr'] = this[f + 'Arr'].filter((val, index) => index !== event.index);
  }


  /**
   * 上传图片的回显
   */
  imgUrlData() {

    // 手持身份证照片
    this.imgUrlData_show(this.objToPost['workPhoto' + 'Path'], 'workPhoto' + 'Arr');
    // for (let item of this.objToPost['workPhoto'].split(',')) {
    //   this.getFilePathByCode(item, 'workPhoto' + 'Arr');
    // }

    // 身份证正面
    this.imgUrlData_show(this.objToPost['idcardPhotoFront' + 'Path'], 'idcardPhotoFront' + 'Arr');
    // for (let item of this.objToPost['idcardPhotoFront'].split(',')) {
    //   this.getFilePathByCode(item, 'idcardPhotoFront' + 'Arr');
    // }

    // 身份证反面
    this.imgUrlData_show(this.objToPost['idcardPhotoReverse' + 'Path'], 'idcardPhotoReverse' + 'Arr');
    // this.imgUrlData_show(this.objToPost['workPhoto'], 'workPhotoArr');
    // for (let item of this.objToPost['idcardPhotoReverse'].split(',')) {
    //   this.getFilePathByCode(item, 'idcardPhotoReverse' + 'Arr');
    // }

    // 电工证附件
    this.imgUrlData_show(this.objToPost['electricAttach' + 'Path'], 'electricAttach' + 'Arr');
    // for (let item of this.objToPost['electricAttach'].split(',')) {
    //   this.getFilePathByCode(item, 'electricAttach' + 'Arr');
    // }

    // 管道证附件
    this.imgUrlData_show(this.objToPost['pipeCardAttach' + 'Path'], 'pipeCardAttach' + 'Arr');
    // for (let item of this.objToPost['pipeCardAttach'].split(',')) {
    //   this.getFilePathByCode(item, 'pipeCardAttach' + 'Arr');
    // }


  }

  /**
   * 加载图片路径
   */
  getFilePathByCode(code: string, field: string) {
    if (code == "" || field == "") {
      return false;
    }
    this.server.getFilePathByCode(code).then(
      data => {
        if (data.result && data.data[0]) {
          let item = data.data[0]
          this[field] = [...this[field], {
            source: item.filePath, thumbnail: item.filePath,
            width: '100px', maxWidth: '800px', title: ''
          }];
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '获取文件路径失败！' });
        }
      }
    );
  }

  /**
   * 展示图片数据
   */
  imgUrlData_show(fileUrl: string, fileArr: any, isClear?: string) {
    if (!fileUrl || !fileArr) {
      return false;
    };
    if (isClear == undefined || isClear == 'true') {
      this[fileArr] = [];
    };
    for (let item of fileUrl.split(',')) {
      this[fileArr] = [...this[fileArr], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    };
  }

  /**
   * Description: 省市区验证
   */
  checkProCityDis(){
    this.currentProvinceError = "";
    if(!this.selectObj.addressCurrentProvince || !this.selectObj.addressCurrentCity || !this.selectObj.addressCurrentArea){
      this.proCityDisError = "请选择省市区";
    }else{
      this.proCityDisError = "";
      if(!this.objToPost.addressCurrentDetail){
        this.currentProvinceError = '请输入详细地址';
      }
    }
  }

  /**
   * Description: 手持身份照片验证
   */
  checkWorkPhoto() {
    if (!this.objToPost['workPhoto']) {
      this.workPhotoError = '请上传手持身份证照片';
    } else {
      this.workPhotoError = '';
    }

  }

  /**
   * Description: 身份证正面附件验证
   */
  checkIdcardPhotoFront() {
    if (!this.objToPost['idcardPhotoFront']) {
      this.idcardPhotoFrontError = '请上传身份证正面';
    } else {
      this.idcardPhotoFrontError = '';
    }

  }

  /**
   * Description: 身份证反面附件验证
   */
  checkIdcardPhotoReverse(){
    if (!this.objToPost['idcardPhotoReverse']) {
      this.idcardPhotoReverseError = '请上传身份证反面';
    } else {
      this.idcardPhotoReverseError = '';
    }

  }

  /**
   * Description: 电工证附件验证
   */
  checkElectricAttach(){
    if (!this.objToPost['electricAttach']) {
      this.electricAttachError = '请上传电工证附件';
    } else {
      this.electricAttachError = '';
    }

  }

  /**
   * Description: 管道证附件验证
   */
  checkPipeCardAttach(){
    if (!this.objToPost['pipeCardAttach']) {
      this.pipeCardAttachError = '请上传管道证附件';
    } else {
      this.pipeCardAttachError = '';
    }

  }

  /**
   * Description: 校验省市区
   */
  currentAddressCk(){
    this.objToPost['addressCurrentDetail'] = this.objToPost['addressCurrentDetail'] ? this.objToPost['addressCurrentDetail'].trim() : '';
    if(!this.selectObj.addressCurrentProvince || !this.selectObj.addressCurrentCity || !this.selectObj.addressCurrentArea){
    this.proCityDisError = "请选择省市区";
    return;
  }
  if (!this.objToPost['addressCurrentDetail']) {
    this.currentProvinceError = '请输入详细地址';
  } else {
    if (this.objToPost['addressCurrentDetail'].length > 25) {
      this.currentProvinceError = '详细地址25个字内';
    } else {
      this.currentProvinceError = '';
    }
  }
}

}
