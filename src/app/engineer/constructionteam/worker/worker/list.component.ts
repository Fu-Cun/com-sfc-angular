import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WorkerService } from '../worker.service';
import { StorageService } from '../../../../common/storage.service';


@Component({
  selector: 'app-worker',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [WorkerService]
})
export class WorkerComponent implements OnInit {

  privatecars: any;
  colsSelected: any;
  cols: any = [];
  selectedItem: any = []; //选中记录
  pageSize: number; // 每页条数
  pageNo: number; // 当前页
  listData: any = []; // 数据
  totalRecords: number; // 数据条数
  searchObj: any = {}; // 搜索条件
  selectedObj: any = {}; //选中条件
  workTypeArr: any = []; // 工种
  workerStatusArr: any = []; // 工人状态
  searFlag = true;
  searResult = '您还未添加任何内容';
  paginator = false;
  constructor( private router: Router, private server: WorkerService, private storage: StorageService) {
    this.cols = [
      { field: 'code', header: '工人用户名', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'name', header: '姓名', frozen: false, sortable: true },
      { field: 'foreman.teamInfo.name', header: '所属项目工队', width: '150px', sortable: true },
      { field: 'foreman.teamInfo.companyName', header: '所属公司', sortable: true },
      { field: 'workType', header: '工种', sortable: true },
      { field: 'telephone', header: '手机号', width: '80px', sortable: true },
      { field: 'inConstruction', header: '在施数量', sortable: true },
      { field: 'level', header: '级别', width: '50px', sortable: true },
      { field: 'status', header: '状态', width: '200px', sortable: true }
    ];
    this.colsSelected = this.cols;
    this.getDictionaries();
  }

  ngOnInit() {

  }
  // onRowSelect(event) {
  //   if (event.type === 'row') {
  //     this.router.navigate(['saas/engineer/constructionteam/worker/',event.data.id]);
  //   }
  // }
  /**
  * 跳转详情页面
  * @param event
  */
  info(event) {
    this.router.navigate(['saas/engineer/constructionteam/worker/', event.id]);
  }
  /**
   * 跳转编辑页面
   */
  add() {
    this.router.navigate(['saas/engineer/constructionteam/worker/add']);
  }


  /**
   * 获取数据
   */
  getData() {
    if (this.selectedObj.workType) {
      this.searchObj['workType'] = this.selectedObj.workType.code;
    }
    if (this.selectedObj.status) {
      this.searchObj['status'] = this.selectedObj.status.code;
    }
    if (this.searchObj['searchValue']) {
      this.searchObj['searchValue'] = this.searchObj['searchValue'].trim();
    }

    this.storage.loading();
    this.server.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        this.storage.loadout();
        this.selectedItem = [];
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.searResult = '未查到想要的数据';
          this.listData.length > 0 ? this.paginator = true : this.paginator = false;
        } else {
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败' });
        }
      }
    );
  }


  //停止接单
  stopOrder() {
    if (this.selectedItem) {
      let list = this.selectedItem;
      let ids = [];
      for (let index in list) {
        ids.push(list[index].id)
      }

      if (ids.length === 0) {
        this.nodata();
        return;
      }
      this.storage.makesureService.emit({
        message: '是否确认停止' + ids.length + '个工人接单？',
        header: '提示',
        rejectLabel:'取消',
        acceptLabel:'确定',
        acceptVisible:true,
        rejectVisible:true,
        accept: () => {
          this.storage.loading();
          this.server.updateStatus({ id: ids}).then(
            data => {
              this.storage.loadout();
              if (data.result) {
                this.getData();
                this.storage.messageService.emit({ severity: 'success', detail: data.msg });
              } else {
                this.storage.messageService.emit({ severity: 'error', detail: data.msg });
              }
              this.selectedItem = [];
            }
          );
        },
        reject: () => {
          this.selectedItem = [];
        }
      });


    }

  }

  // 未选择
  nodata() {
    this.storage.makesureService.emit({
      message: '请选择要操作的数据！',
      header: '提示',
      rejectVisible: false,
      acceptVisible: false,
      accept: () => {
      }
    });
  }


  /**
  * 加载字典项
  */
  getDictionaries() {
    // 加载工种和工人状态
    this.server.getDictionaries().then(
      data => {
        if (data.result) {
          const selectAllStatus = { code: '', name: '全部' };
          this.workTypeArr = data.data.workType;
          this.workTypeArr.unshift(selectAllStatus);
          this.workerStatusArr = data.data.orderreceivingstatus;
          this.workerStatusArr.unshift(selectAllStatus)
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  //搜索方法
  search(event?) {
    // this.pageNo = 1;
    this.searchObj['sortF'] = "";
    this.getData()
  }
  /**
   * 分页查询
   * @param event
   */
  loadCarsLazy(event) {
    this.pageSize = + event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.searchObj['sortF'] = event.sortField;
    this.searchObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    this.getData();
  }

}

