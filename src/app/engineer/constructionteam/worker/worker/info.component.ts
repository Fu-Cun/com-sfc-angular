import { Component, OnInit } from '@angular/core';
import { WorkerService } from '../worker.service';
import { ActivatedRoute, Router } from '@angular/router';
import {StorageService} from "../../../../common/storage.service";



@Component({
  selector: 'app-info-worker',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [WorkerService]
})
export class InfoWorkerComponent implements OnInit {
  objToPost: any = {};
  selectObj: any = {};
  kCode: string = '';//id
  isLoadDic: Boolean = false;
  loadDicCount: number = 0;
  workTypeArr: any = [];//工种
  addressCurrentProvinceArr: any = []; //现居地_省
  addressCurrentCityArr: any = [];     //现居地_市
  addressCurrentAreaArr: any = [];     //现居地_区
  addressBirthProvinceArr: any = [];   //籍贯所在地_省
  addressBirthCityArr: any = [];       //籍贯所在地_市
  addressBirthAreaArr: any = [];       //籍贯所在地_区

  workPhotoArr = [];  //手持身份证照片
  idcardPhotoFrontArr = []; //身份证正面
  idcardPhotoReverseArr = []; //身份证反面
  electricAttachArr = []; //电工证
  pipeCardAttachArr = [] ;//管道证

  constructor(private routeInfo: ActivatedRoute, private server: WorkerService, private router: Router,private storage: StorageService) { }

  ngOnInit() {
    this.storage.loading()
    this.routeInfo.params.subscribe((params) => {
      this.server.getWorkerInfoById(params.id).then(data => {
        this.storage.loadout();
        this.objToPost = data.data;
        //工作年限
        this.objToPost.workingYears = this.objToPost.workingYears == 0 ? '不满一年' : this.objToPost.workingYears + '年';
        this.kCode = params.id;
        this.loadDicData();
        this.imgUrlData();
        if (this.objToPost.foreman != undefined && this.objToPost.foreman.teamInfo != undefined) {
          this.objToPost['teamInfoName'] = this.objToPost.foreman.teamInfo.name
        }
      });
    });
    this.getDictionaries();
    this.storage.loadout();
  }

  /**
   * 返回列表页面
   */
  back() {
    this.router.navigate(['saas/engineer/constructionteam/worker']);
  }
  /**
   * 跳转编辑页面
   */
  enit() {
    this.router.navigate(['saas/engineer/constructionteam/worker/add', { code: this.kCode }]);
  }


  /**
   * 加载字典项
   */
  getDictionaries() {
    // 加载工种和工人状态
    this.server.getDictionaries().then(
      data => {
        if (data.result) {
          this.workTypeArr = data.data.workType;
          this.addressCurrentProvinceArr = data.data.district.province;
          this.addressBirthProvinceArr = data.data.district.province;
          this.isLoadDic = true;
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '字典服务异常!' });
        }
      }
    );
  }

  /**
  * 解析字典项数据
  */
  loadDicData() {
    this.loadDicCount++
    if (this.loadDicCount > 14) {
      return false;
    }
    if (this.isLoadDic) {

      let workTypeStr = '';
      this.workTypeArr.forEach(item => {
        if (this.objToPost.workType.split(',').indexOf(item.code) > -1) {
          workTypeStr += item.name + ' ';
        }
      });
      this.objToPost.workTypeStr = workTypeStr;
      //现居地
      this.selectObj.addressCurrentProvince = (this.addressCurrentProvinceArr.filter(item => item.area_code == this.objToPost.addressCurrentProvince))[0];
      this.addressCurrentCityArr = this.selectObj.addressCurrentProvince.city;
      this.selectObj.addressCurrentCity = (this.addressCurrentCityArr.filter(item => item.area_code == this.objToPost.addressCurrentCity))[0];
      this.addressCurrentAreaArr = this.selectObj.addressCurrentCity.area;
      this.selectObj.addressCurrentArea = (this.addressCurrentAreaArr.filter(item => item.area_code == this.objToPost.addressCurrentArea))[0];

      this.objToPost.addressCurrent = this.selectObj.addressCurrentProvince.area_name + '  ' + this.selectObj.addressCurrentCity.area_name + '  ' + this.selectObj.addressCurrentArea.area_name + '  ' + this.objToPost.addressCurrentDetail
      //户籍所在地
      this.selectObj.addressBirthProvince = (this.addressBirthProvinceArr.filter(item => item.area_code == this.objToPost.addressBirthProvince))[0];
      this.addressBirthCityArr = this.selectObj.addressBirthProvince ? this.selectObj.addressBirthProvince.city : [];
      this.selectObj.addressBirthCity = (this.addressBirthCityArr.filter(item => item.area_code == this.objToPost.addressBirthCity))[0];
      this.addressBirthAreaArr = this.selectObj.addressBirthCity ? this.selectObj.addressBirthCity.area : [];
      this.selectObj.addressBirthArea = (this.addressBirthAreaArr.filter(item => item.area_code == this.objToPost.addressBirthArea))[0];
      let addressBirth = []
      if (this.selectObj.addressBirthProvince) {
        addressBirth.push(this.selectObj.addressBirthProvince.area_name)
      }
      if (this.selectObj.addressBirthCity) {
        addressBirth.push(this.selectObj.addressBirthCity.area_name)
      }
      if (this.selectObj.addressBirthArea) {
        addressBirth.push(this.selectObj.addressBirthArea.area_name)
      }
      if (this.objToPost.addressBirthDetail) {
        addressBirth.push(this.objToPost.addressBirthDetail)
      }
      addressBirth.toString().replace(',', ' ')
      this.objToPost.addressBirth = addressBirth

    } else {
      setTimeout(() => {
        this.loadDicData();
      }, 1000);
    }
  }


  /**
    * 上传图片的回显
    */
  imgUrlData() {
    // 手持身份证照片
    this.imgUrlData_show(this.objToPost['workPhoto'+'Path'], 'workPhoto' + 'Arr');
    // for (let item of this.objToPost['workPhoto'].split(',')) {
    //   this.getFilePathByCode(item, 'workPhoto' + 'Arr');
    // }

    // 身份证正面
    this.imgUrlData_show(this.objToPost['idcardPhotoFront'+'Path'], 'idcardPhotoFront' + 'Arr');
    // for (let item of this.objToPost['idcardPhotoFront'].split(',')) {
    //   this.getFilePathByCode(item, 'idcardPhotoFront' + 'Arr');
    // }

    // 身份证反面
    this.imgUrlData_show(this.objToPost['idcardPhotoReverse'+'Path'], 'idcardPhotoReverse' + 'Arr');
    // this.imgUrlData_show(this.objToPost['workPhoto'], 'workPhotoArr');
    // for (let item of this.objToPost['idcardPhotoReverse'].split(',')) {
    //   this.getFilePathByCode(item, 'idcardPhotoReverse' + 'Arr');
    // }

    // 电工证附件
    this.imgUrlData_show(this.objToPost['electricAttach'+'Path'], 'electricAttach' + 'Arr');
    // for (let item of this.objToPost['electricAttach'].split(',')) {
    //   this.getFilePathByCode(item, 'electricAttach' + 'Arr');
    // }

    // 管道证附件
    this.imgUrlData_show(this.objToPost['pipeCardAttach'+'Path'], 'pipeCardAttach' + 'Arr');
    // for (let item of this.objToPost['pipeCardAttach'].split(',')) {
    //   this.getFilePathByCode(item, 'pipeCardAttach' + 'Arr');
    // }
  }

  /**
   * 加载图片路径
   */
  getFilePathByCode(code: string, field: string) {
    if (code == "" || field == "") {
      return false;
    }
    this.server.getFilePathByCode(code).then(
      data => {
        if (data.result && data.data[0]) {
          let item = data.data[0]
          this[field] = [...this[field], {
            source: item.filePath, thumbnail: item.filePath,
            width: '100px', maxWidth: '800px', title: ''
          }];
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '获取文件路径失败!' });
        }
      }
    );
  }

  /**
   * 展示图片数据
   */
  imgUrlData_show(fileUrl: string,fileArr: any,isClear?: string) {
    if(!fileUrl || !fileArr){
      return false;
    };
    if(isClear==undefined||isClear=='true'){
      this[fileArr] = [];
    };
    for (let item of fileUrl.split(',')) {
      this[fileArr] = [...this[fileArr], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    };
  }


}
