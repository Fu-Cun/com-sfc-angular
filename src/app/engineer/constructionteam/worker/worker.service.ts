import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class WorkerService {

  private baseUrl = '';
  fileUrl = 'http://172.18.8.76/zuul/com-dyrs-mtsp-fileservice/';
  // 工程队服务名称
  private serveName = 'com-dyrs-mtsp-constructionteamservice';
  // 基础服务名称
  private authority = 'com-dyrs-mtsp-authorityservice';
  // 文件服务名称
  private files = '/zuul/com-dyrs-mtsp-fileservice/';

  constructor(public httpService: HttpInterceptorService) { }


  /**
   * 获取工人列表 分页方法
   * @param pageSize 分页大小 默认值10
   * @param pageNo  当前页 默认值是1
   * @param search  搜索条件
   */
  list(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/list',
      { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }

  /**
    * 通过id获取施工队信息
    * @param id 工队id
    */
  getTeamInfoById(id) {
    return this.httpService.post(this.serveName + '/constructionteamservice/teamInfo/teamInfoDetail', { teamId: id }, this.baseUrl);
  }

  /**
  * 获取单条记录
  * @param id
  */
  get(id: string) {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/get', { id }, this.baseUrl);
  }
  /**
   * 停止接单
   * @param search
   * @param pageSize
   * @param pageNo
   */
  updateStatus(search: any = {}) {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/updateStatus', { ...search }, this.baseUrl);
  }

  /**
   * 获取数据字典
   */
  getDictionaries() {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/getDictionaries', {}, this.baseUrl);
  }

  /**
   * 保存数据
   * @param params
   */
  save(params: any) {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/save', { ...params }, this.baseUrl);
  }

  /**
   *  通过工人id获取工人信息
   * @param id 工人id
   */
  getWorkerInfoById(id) {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/get', { id: id }, this.baseUrl);
  }

  /**
  * 检测账号是否存在
  * @param params
  */
  checkCode(params: any) {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/checkCode', { ...params }, this.baseUrl);
  }
  /**
   * 身份证一性验证
   * @param param
   */
  checkIdNumber(params: any) {
    return this.httpService.post(this.serveName + '/constructionteamservice/worker/checkIdNumber', { ...params }, this.baseUrl);
  }
  /**
   * 手机号唯一性验证
   * @param param
   */
  uniqueFTelephone(param: any = {}) {
    return this.httpService.post(this.authority + '/user/isExistsByMobile/api', param, '');
  }
  /**
   * 获取图片路径
   */
  getFilePathByCode(code: string) {
    return this.httpService.post(this.files + '/fileservice/fileAttachmentAll/api', { fileCode: code }, '');
  }

}
