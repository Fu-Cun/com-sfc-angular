import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class TeaminfoService {
    private baseUrl = '';
    // 工程队服务名称
    private serveName = 'com-dyrs-mtsp-constructionteamservice';
    // 基础服务名称
    private authority = 'com-dyrs-mtsp-authorityservice';
    // 所属公司
    private organization = 'com-dyrs-mtsp-organizationservice';

    constructor(public teaminfoService: HttpInterceptorService) { }

    /**
     * 获取施工队信息 分页方法
     * @param pageSize 分页大小 默认值10
     * @param pageNo  当前页 默认值是1
     * @param search  搜索条件
     */
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1, ) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/list',
            { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }
    /**
     * 添加方法
     * @param param
     */
    save(param: any = {}) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/save',
            param, this.baseUrl);
    }
    /**
     * 修改方法
     * @param param
     */
    updata(param: any = {}) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/update',
            param, this.baseUrl);
    }
    /**
     * 获取施工队状态和施工队押金状态
     */
    getStatus() {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/getSelection', {}, this.baseUrl);
    }

    /**
     * 通过id获取施工队信息
     * @param id 工队id
     */
    getTeamInfoById(id) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/teamInfoDetail', { teamId: id }, this.baseUrl);
    }


    /**
     * 用户唯一性验证
     * @param param
     */
    uniqueAccount(param: any = {}) {
        return this.teaminfoService.post(this.authority + '/user/checkUniqueName/api',
            param);
    }

    /**
     * 用户唯一性验证
     * @param param
     */
    uniqueFTelephone(param: any = {}) {
        return this.teaminfoService.post(this.authority + '/user/isExistsByMobile/api',
            param, '');
    }
    /**
    * 获取公司
    * @param param
    */
    getOrganization(param: any = {}) {
        return this.teaminfoService.post(this.organization + '/organizationservice/findOrgIsNotDept/api',
            {orgData:'1'}, '');
    }
    /**
   * Description: 停止接单
   */
    updateStatus(search) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/updateStatus', search, this.baseUrl);
    }


    /**
     * 根据公司查询团队信息
     * @param param
     */
    getTeamName(param: any = {}) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/getTeamName', param, this.baseUrl);
    }

    /**
    * 根据公司查询团队信息
    * @param param
    */
    getWorkingYears(param: any = {}) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/getWorkingYears', param, this.baseUrl);
    }


    /**
     * 用户省份证唯一性验证
     * @param param
     */
    uniqueUserIDCard(param: any = {}) {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/checkIdCardIsUsable',
            param, this.baseUrl);
    }

    /**
     * 用户省份证唯一性验证
     * @param param
     */
    // xxxxx(param: any = {}) {
    //     return this.teaminfoService.get(this.serveName + '/constructionteamservice/teamInfo/checkIdCardIsUsable',
    //         param, 'htpp://.123213');
    // }

    /**
     * 获取服务器当时间
     */
    getCurrentTime() {
        return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/getCurrentDate',
            {}, this.baseUrl);
    }

    /**
     * Description: 获取当前用户所在的组织code
     * @Note:
     */
    getCurrentUserOrgCode(){
      return this.teaminfoService.post(this.serveName + '/constructionteamservice/teamInfo/getCurrentUserOrgCode',
        {}, this.baseUrl);
    }
}

