import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TimeConfigClass } from '../../../../common/common-config';
import { TeaminfoService } from '../teaminfo.service';
import { WorkerService } from '../../worker/worker.service';
import { Check } from '../../../../common/check';
import { StorageService } from '../../../../common/storage.service';
declare const window;
@Component({
  selector: 'app-add-teaminfo',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [TeaminfoService, WorkerService, Check]
})


export class AddTeaminfoComponent implements OnInit {

  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  region: any[] = []; // 区域
  selectedRegion: any[] = []; // 选择区域
  teamInfo: any = {}; // 工队信息
  foreman: any = {}; // 项目经理信息
  selectSex: any = {}; // 性别数据
  cashDepositStatus = []; // 施工队保证金状态
  selectedCashDepositStatus; // 选中后的施工队保证金状态
  isRecommendErr: Boolean = true; // 是否推荐验证是否通过
  selectRecommend; // 是否推荐
  title: string = '创建项目工队';
  sexArr: any[] = []; // 性别
  addressCurrentProvinceArr: any[] = []; // 现居地_省
  addressCurrentCityArr: any[] = [];     // 现居地_市
  addressCurrentAreaArr: any[] = [];     // 现居地_区
  selectedCurrentProvince = {}; // 选中现居地_省
  selectedCurrentCity: any = {}; // 选中现居地_市
  selectedCurrentArea: any = {}; // 选中现居地_区
  addressBirthProvinceArr: any[] = [];   // 籍贯所在地_省
  addressBirthCityArr: any[] = [];       // 籍贯所在地_市
  addressBirthAreaArr: any = [];       // 籍贯所在地_区
  selectedBirthProvinceArr = {};   // 选中籍贯所在地_省
  selectedBirthCityArr: any = {};      // 选中籍贯所在地_市
  selectedBirthAreaArr: any = {};      // 选中籍贯所在地_区
  MobileRight: Boolean = false; // 手机验证
  teamId; // 编辑的id
  saveFlag = false;
  submitFlag = false;
  uploadRrl: String = 'zuul/com-dyrs-mtsp-fileservice/fileservice/fileDFSSave/api'; // 上传图片地址
  managerPhotoPcImages = []; // 手持身份证照片
  idcardPhotoFrontImages = []; // 身份证正面
  idcardPhotoReverseImages = []; // 身份证反面
  insuranceAttachImages = []; // 保险单附件
  appStandardImages = []; // APP使用规范
  engineerStandardImages = []; // 工程部处罚规范
  productManualImages = []; // 产品套餐手册
  constructionManualImages = []; // 施工手册
  pipeCardAttachImages = []; // 管道证附件
  electricAttachImages = []; // 电工证附件
  serviceContractAttachImages = []; // 合同附件
  organizationArr: any[] = []; // 组织机构
  selectedOrganizationArr: any = {}; // 选中的组织机构
  levelArr = []; // 级别数据
  selectLevel = {}; // 级别选中的数据
  maxWorkingStartTime: any; // 从业时间最大值

  organizationError; // 公司所属错误提示
  regionError; // 服务区域错误提示
  labourCompanyError; // 劳务公司错误提示
  workerNumError; // 工人数量人数错误提示
  tTelephoneError; // 联系电话工队联系电话错误提示
  abilityError; // 施工能力错误提示
  selectRecommendError; // 是否推荐选中值错误提示
  cashDepositStatusError; // 保证金状态错误提示
  cashAmountError; // 保证金额错误提示

  foremanAccountError;  // 账号错误提示
  workingStartTimeError; // 从业时间错误提示
  idNumberError; // 身份证错误提示
  workingYearsError; // 从业年限错误提示
  managerTelephoneError; // 手机号错误提示
  foremanNameError; // 项目经理错误提示
  sexError; // 性别错误提示
  birthPlaceError; // 籍贯错误提示
  levelError; // 级别错误提示
  CurrentProvinceError; // 现住地省错误提示
  CurrentCityError; // 现住地市错误提示
  CurrentAreaError; // 现住区域错误提示
  CurrentDetailError; // 现住详细地址错误提示
  BirthProvinceError; // 出生地省错误提示
  BirthCityError; // 出生地市错误提示
  BirthAreaError; // 出生区域错误提示
  BirthDetailError; // 出生详细地址错误提示
  descriptionError; // 个人简介错误提示

  managerPhotoPcError; // 照片错误提示
  idcardPhotoFrontError; // 身份证正面错误提示
  idcardPhotoReverseError; // 身份证反面错误提示
  insuranceAttachError; // 保险单附件错误提示
  serviceContractAttachError; // 劳务合同附件错误提示
  insuranceStartTimeError; // 保险开始时间错误提示
  insuranceEndTimeError; // 保险结束时间错误提示
  contractStartTimeError; // 合同起止日期错误提示
  contractEndTimeError; // 合同止日期错误提示
  electricAttachError; // 电工证附件错误提示
  electricAttachStartTimeError; // 电工证起止日期错误提示
  electricAttachEndTimeError; // 电工证止日期错误提示
  pipeCardAttachError; // 管道证附件错误提示
  pipeCardStartTimeError; // 管道证起止日期错误提示
  pipeCardEndTimeError; // 管道证止日期错误提示
  constructionManualError; // 施工手册错误提示
  productManualError; // 产品套餐手册错误提示
  engineerStandardError; // 工程部处罚规范错误提示
  appStandardError; // APP使用规范错误提示
  proCityDisError;//省市区错误提示

  insuranceEndTimeMinDate; // 保险结束日期最小值
  contractStartTimeMinDate; // 合同结束日期最小值
  electricAttachEndTimeMinDate; // 电证结束日期最小值
  pipeCardEndTimeMinDate; // 管道结束日期最小值

  insuranceEndTimeMaxDate; // 保险开始日期最大值
  contractStartTimeMaxDate; // 合同开始日期最大值
  electricAttachEndTimeMaxDate; // 电证开始日期最大值
  pipeCardEndTimeMaxDate; // 管道开始日期最大值


  saveBtnFlag: Boolean = false; // 提交按钮是否可用

  managerTelephoneOld = '';  // 手机号的旧值
  idNumberOld = ''; // 身份证的旧值

  errorFiel:any; //文件大小超标提示

  currentUserOrgCode: any;  //当前用户所属组织编号


  constructor(private teaminfoService: TeaminfoService, private workServer: WorkerService, private check: Check,
    private routeInfo: ActivatedRoute, private router: Router, private storage: StorageService, ) {
    this.teamInfo['workerNum'] = '0';
    this.teamInfo['cashAmount'] = '0';
    this.selectRecommend = '0';
    this.uploadRrl = (this.teaminfoService.teaminfoService.baseUrl || 'http://172.18.8.76/') + this.uploadRrl;
  }

  ngOnInit() {
    this.routeInfo.params.subscribe((params) => {
      this.teamId = params['id'];
      // 施工队押金状态并填充到对对应下拉框中
      this.teaminfoService.getStatus().then(
        data => {
          if (data.result) {
            this.cashDepositStatus = data.data.cashDepositStatus.filter((items, index) => {
              if (items['code'] === 'cashdepositstatus_havepaid') {
                this.selectedCashDepositStatus = items;
              }
              if (index === 0) {
                return false;
              } else {
                return true;
              }
            });
            this.sexArr = data.data.sex;
            this.sexArr = this.sexArr.filter((items, index) => {
              if (items['code'] === 'sex_man') {
                this.selectSex = items;
              }
              if (index === 2) {
                return false;
              }else{
                return true;
              }
            });
            // this.loadDicData();
            // 查询组织公司
            this.getOrganization();
          } else {
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
      this.teaminfoService.getCurrentTime().then((data) => {
        if (data.result) {
          this.maxWorkingStartTime = new Date(data.data);
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '获取服务器时间失败!' });
        }
      });
      // 省市数据字典加载
      this.getDictionaries();

      // 获取路由参数
      if (this.teamId) {
        // 获取施工对信息
        this.teaminfoService.getTeamInfoById(this.teamId).then(data => {
          this.teamInfo = data.data.teamInfo;
          this.foreman = data.data.foreman;
          this.idNumberOld = this.foreman['idNumber'];
          this.managerTelephoneOld = this.foreman['managerTelephone'];
          this.imgUrlData();
          if (this.teamInfo['billStatus'] === 'billstatus_noinapprove') {
            this.saveFlag = false;
            this.submitFlag = true;
          } else {
            this.saveFlag = true;
            this.submitFlag = false;
          }
          this.teaminfoService.getTeamName({
            companyName: this.teamInfo['companyName'],
            provinceCode: this.teamInfo['provinceCode'],
            cityCode: this.teamInfo['cityCode'],
            organizationCode: this.teamInfo['organizationCode']
          }).then(data1 => {
            if (data1.result) {
              this.region = data1.data.area;
              this.loadDicData();
            } else {
              this.storage.messageService.emit({ severity: 'error', detail: '获取工队名称失败！' });
            }
          });
        });
      } else {
        this.saveFlag = false;
        this.submitFlag = true;
      }
    });
    this.levelArr = [{ code: 1, name: '1分' },
    { code: 2, name: '2分' },
    { code: 3, name: '3分' },
    { code: 4, name: '4分' },
    { code: 5, name: '5分' }];
    this.selectLevel = { code: 3, name: '3分' };
  }


  /**
   * 上传图片的回显
   */
  imgUrlData() {
    // 手持身份证照片
    for (let item of this.foreman['managerPhotoPcUrls'].split(',')) {
      this['managerPhotoPcImages'] = [...this['managerPhotoPcImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 身份证正面
    for (let item of this.foreman['idcardPhotoFrontUrls'].split(',')) {
      this['idcardPhotoFrontImages'] = [...this['idcardPhotoFrontImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 身份证反面
    for (let item of this.foreman['idcardPhotoReverseUrls'].split(',')) {
      this['idcardPhotoReverseImages'] = [...this['idcardPhotoReverseImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 保险单附件
    for (let item of this.foreman['insuranceAttachUrls'].split(',')) {
      this['insuranceAttachImages'] = [...this['insuranceAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 合同附件
    for (let item of this.foreman['serviceContractAttachUrls'].split(',')) {
      this['serviceContractAttachImages'] = [...this['serviceContractAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 电工证附件
    for (let item of this.foreman['electricAttachUrls'].split(',')) {
      this['electricAttachImages'] = [...this['electricAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 管道证附件
    for (let item of this.foreman['pipeCardAttachUrls'].split(',')) {
      this['pipeCardAttachImages'] = [...this['pipeCardAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 施工手册
    for (let item of this.foreman['constructionManualUrls'].split(',')) {
      this['constructionManualImages'] = [...this['constructionManualImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 产品套餐手册
    for (let item of this.foreman['productManualUrls'].split(',')) {
      this['productManualImages'] = [...this['productManualImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 工程部处罚规范
    for (let item of this.foreman['engineerStandardUrl'].split(',')) {
      this['engineerStandardImages'] = [...this['engineerStandardImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // APP使用规范
    for (let item of this.foreman['appStandardUrls'].split(',')) {
      this['appStandardImages'] = [...this['appStandardImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
  }

  /**
   * 现居地 行政区
   * @param val
   */
  currentChange(val) {
    if (val === 'province') {
      this.addressCurrentCityArr = this.selectedCurrentProvince['city'];
      this.addressCurrentAreaArr = [];
      this.selectedCurrentCity = {};
      this.selectedCurrentArea = {};
      this.CurrentProvinceError = '';
    } else if (val === 'city') {
      this.addressCurrentAreaArr = this.selectedCurrentCity['area'];
      this.selectedCurrentArea = {};
      this.CurrentProvinceError = '';
    }
  }
  /**
   * 籍贯所在地 行政区
   * @param val
   */
  birthChange(val) {
    if (val === 'province') {
      this.addressBirthCityArr = this.selectedBirthProvinceArr['city'];
      this.addressBirthAreaArr = [];
    } else if (val === 'city') {
      this.addressBirthAreaArr = this.selectedBirthCityArr['area'];
    }
  }

  /**
   * 通过选则公司修改区域名称和团队名称
   */
  organizationChange() {
    const companyName = this.selectedOrganizationArr['name']; // 公司四名称
    const provinceCode = this.selectedOrganizationArr['provinceCode']; // 公司所属省
    const cityCode = this.selectedOrganizationArr['cityCode'];  // 公司所属市
    this.getTeamName({ companyName: companyName, provinceCode: provinceCode, cityCode: cityCode })
  }


  /**
   * Description: 获取项目名称
   */
  getTeamName(search:any = {}){
    this.teaminfoService.getTeamName(search).then(
      data => {
        if (data.result && data.data) {
          this.region = data.data.area;
          this.teamInfo['name'] = data.data.teamName;
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '获取施工队名称出错！' });
        }
      }
    );
  }

  /**
   * 获取公司数据
   */
  getOrganization() {
    this.teaminfoService.getOrganization().then(
      data => {
        if (data.result) {
          this.organizationArr = data.data;
          if(!this.teamId){
              this.teaminfoService.getCurrentUserOrgCode().then(
                data =>{
                  if(data.result){
                    this.currentUserOrgCode = data.data;
                    for(let index of this.organizationArr){
                        if(index.code == this.currentUserOrgCode){
                          this.selectedOrganizationArr = index;
                          const companyName = this.selectedOrganizationArr['name']; // 公司四名称
                          const provinceCode = this.selectedOrganizationArr['provinceCode']; // 公司所属省
                          const cityCode = this.selectedOrganizationArr['cityCode'];  // 公司所属市
                          this.getTeamName({ companyName: companyName, provinceCode: provinceCode, cityCode: cityCode })
                          break;
                        }
                    }
                  }else{
                    this.storage.messageService.emit({ severity: 'error', detail: '获取当前用户组织code出错！' });
                  }
                }
              )
          }
          this.loadDicData();
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '获取组织机构信息出错！' });
        }
      }
    );
  }

  /**
   * 加载性别参数 和 省市区数据
   */
  getDictionaries() {
    this.workServer.getDictionaries().then(
      data => {
        if (data.result) {
          this.sexArr = data.data.sex;
          this.sexArr = this.sexArr.filter((items, index) => {
            if (items['code'] === 'sex_man') {
              this.selectSex = items;
            }
            if (index === 2) {
              return false;
            }else{
              return true;
            }
          });
          this.addressCurrentProvinceArr = data.data.district.province;
          this.addressBirthProvinceArr = data.data.district.province;
          this.loadDicData();
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '字典服务异常！' });
        }
      }
    );
  }

  /**
   * 账号唯一验证
   * @param data
   */
  uniqueAccount(name) {
    this.foreman['account'] = this.foreman['account'] ? this.foreman['account'].trim() : '';
    if (!this.foreman['account']) {
      this.foremanAccountError = '请输入账号';
    } else {
      this.teaminfoService.uniqueAccount({ 'name': this.foreman['account'] }).then(data => {
        if (data.result) {
          this.saveBtnFlag = true;
          this.foremanAccountError = '';
        } else {
          this.saveBtnFlag = false;
          this.foremanAccountError = '此账号已被占用';
        }
      });
    }
  }

  /**
   * 项目经理手机号唯一验证
   * @param data
   */
  uniqueFTelephone(telephone) {
    this.foreman['managerTelephone'] = this.foreman['managerTelephone'] ? this.foreman['managerTelephone'].trim() : '';
    if (!this.foreman['managerTelephone']) {
      this.managerTelephoneError = '请输入手机号';
    } else {
      if (!this.check.phone(this.foreman['managerTelephone'])) {
        this.managerTelephoneError = '请输入正确的手机号';
      } else {
        if (this.managerTelephoneOld !== this.foreman['managerTelephone']) {
          this.teaminfoService.uniqueFTelephone({ 'mobile': telephone }).then(data => {
            if (data.result) {
              this.saveBtnFlag = true;
              this.managerTelephoneError = '';
            } else {
              this.saveBtnFlag = false;
              this.managerTelephoneError = data.msg;
            }
          });
        }

      }
    }
  }




  // saveAndNew() {
  //   if (!this.checkSaveData()) {
  //     return;
  //   }
  //
  //   let region = [];
  //   if (!this.selectedRegion) {
  //     return;
  //   }
  //   // 服务区域拼接成字符串
  //   for (const a of this.selectedRegion) {
  //     region.push(a.area_name);
  //   }
  //   this.teamInfo['serviceArea'] = region.join(',');
  //   if (this.selectedCashDepositStatus && this.selectedCashDepositStatus.code !== this.cashDepositStatus[0].code) {
  //     console.log('sss');
  //     this.teamInfo['cashDepositStatus'] = this.selectedCashDepositStatus.code;
  //   } else {
  //     delete this.teamInfo['cashDepositStatus'];
  //   }
  //   console.log('teamInfo', JSON.stringify(this.teamInfo, undefined, 1));
  //
  //   this.teamInfo['isRecommend'] = this.selectRecommend === '0' ? false : true;
  //   this.teamInfo['companyName'] = this.selectedOrganizationArr['companyName'];
  //   this.teamInfo['companyCode'] = this.selectedOrganizationArr['companyCode'];
  //   this.teamInfo['organizationCode'] = this.selectedOrganizationArr['code'];
  //
  //   this.teamInfo['cityCode'] = this.selectedOrganizationArr['cityCode'];
  //   this.teamInfo['provinceCode'] = this.selectedOrganizationArr['provinceCode'];
  //
  //   // 性别
  //   this.foreman['sex'] = this.selectSex['code'];
  //
  //   if (this.selectedOrganizationArr) {
  //     this.foreman['organization'] = this.selectedOrganizationArr['name'];
  //   }
  //
  //   if (this.selectedCurrentProvince) {
  //     this.foreman['addressCurrentProvince'] = this.selectedCurrentProvince['area_code'];
  //   }
  //   if (this.selectedCurrentCity) {
  //     this.foreman['addressCurrentCity'] = this.selectedCurrentCity['area_code'];
  //   }
  //   if (this.selectedCurrentArea) {
  //     this.foreman['addressCurrentArea'] = this.selectedCurrentArea['area_code'];
  //   }
  //
  //   if (this.selectedBirthProvinceArr) {
  //     this.foreman['addressBirthProvince'] = this.selectedBirthProvinceArr['area_code'];
  //   }
  //   if (this.selectedBirthCityArr) {
  //     this.foreman['addressBirthCity'] = this.selectedBirthCityArr['area_code'];
  //   }
  //   if (this.selectedBirthAreaArr) {
  //     this.foreman['addressBirthArea'] = this.selectedBirthAreaArr['area_code'];
  //   }
  //
  //   if (this.selectLevel) {
  //     this.foreman['level'] = this.selectLevel['code'];
  //   }
  //
  //   this.foreman['workingYears'] = this.foreman['workingYears'] === '不满一年' ? 0 : this.foreman['workingYears'];
  //
  //   console.log('foreman', JSON.stringify(this.foreman, undefined, 1));
  //
  //   if (this.teamId) {
  //     this.teaminfoService.updata({ foreman: this.foreman, teamInfo: this.teamInfo }).then(
  //       data => {
  //         if (data.result) {
  //           this.storage.messageService.emit({ severity: 'success', detail: data.msg });
  //           window.location.href = '/saas/engineer/constructionteam/teaminfo/add';
  //         } else {
  //           this.storage.messageService.emit({ severity: 'error', detail: data.msg });
  //         }
  //       }
  //     );
  //   } else {
  //     this.teaminfoService.save({ foreman: this.foreman, teamInfo: this.teamInfo }).then(
  //       data => {
  //         if (data.result) {
  //           this.storage.messageService.emit({ severity: 'success', detail: data.msg });
  //           window.location.reload();
  //
  //         } else {
  //           this.storage.messageService.emit({ severity: 'error', detail: data.msg });
  //         }
  //       }
  //     );
  //   }
  //
  // }

  /**
   * 保存数据
   */
  save() {
    if (!this.checkSaveData()) {
      return;
    }

    let region = [];
    if (!this.selectedRegion) {
      return;
    }
    // 服务区域拼接成字符串
    for (const a of this.selectedRegion) {
      region.push(a.area_code);
    }
    this.teamInfo['serviceArea'] = region.join(',');
    if (this.selectedCashDepositStatus && this.selectedCashDepositStatus.code) {
      this.teamInfo['cashDepositStatus'] = this.selectedCashDepositStatus.code;
    } else {
      delete this.teamInfo['cashDepositStatus'];
    }

    this.teamInfo['isRecommend'] = this.selectRecommend === '0' ? false : true;
    this.teamInfo['companyName'] = this.selectedOrganizationArr['name'];
    this.teamInfo['companyCode'] = this.selectedOrganizationArr['companyCode'];
    this.teamInfo['organizationCode'] = this.selectedOrganizationArr['code'];
    this.teamInfo['cityCode'] = this.selectedOrganizationArr['cityCode'];
    this.teamInfo['provinceCode'] = this.selectedOrganizationArr['provinceCode'];

    // 性别
    this.foreman['sex'] = this.selectSex['code'];

    this.foreman['level'] = this.selectLevel['code'];

    if (this.selectedOrganizationArr) {
      this.foreman['organization'] = this.selectedOrganizationArr['name'];
    }

    if (this.selectedCurrentProvince) {
      this.foreman['addressCurrentProvince'] = this.selectedCurrentProvince['area_code'];
    }
    if (this.selectedCurrentCity) {
      this.foreman['addressCurrentCity'] = this.selectedCurrentCity['area_code'];
    }
    if (this.selectedCurrentArea) {
      this.foreman['addressCurrentArea'] = this.selectedCurrentArea['area_code'];
    }

    if (this.selectedBirthProvinceArr) {
      this.foreman['addressBirthProvince'] = this.selectedBirthProvinceArr['area_code'];
    }
    if (this.selectedBirthCityArr) {
      this.foreman['addressBirthCity'] = this.selectedBirthCityArr['area_code'];
    }
    if (this.selectedBirthAreaArr) {
      this.foreman['addressBirthArea'] = this.selectedBirthAreaArr['area_code'];
    }
    this.foreman['workingYears'] = this.foreman['workingYears'] === '不满一年' ? 0 : this.foreman['workingYears'];

    if (this.teamId) {
      this.storage.loading();
      this.teaminfoService.updata({ foreman: this.foreman, teamInfo: this.teamInfo }).then(
        data => {
          this.storage.loadout();
          if (data.result) {
            this.storage.messageService.emit({ severity: 'success', detail: data.msg });
            this.router.navigate(['/saas/engineer/constructionteam/teaminfo/', this.teamId]);
          } else {
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
    } else {
      this.storage.loading();
      this.teaminfoService.save({ foreman: this.foreman, teamInfo: this.teamInfo }).then(
        data => {
          this.storage.loadout();
          if (data.result) {
            this.storage.messageService.emit({ severity: 'success', detail: data.msg });
            this.router.navigate(['/saas/engineer/constructionteam/teaminfo']);
          } else {
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
    }

  }

  /**
   *  所属公司
   */
  organizationCk() {
    if (!(this.selectedOrganizationArr && this.selectedOrganizationArr['name'])) {
      this.organizationError = '请选择所属公司';
    } else {
      this.organizationError = '';
    }
  }

  labourCompanyCk() {
    this.teamInfo['labourCompany'] = this.teamInfo['labourCompany'] ? this.teamInfo['labourCompany'].trim() : '';
    if (!this.teamInfo['labourCompany']) {
      this.labourCompanyError = '请输入劳务公司';
    } else {
      if (this.teamInfo['labourCompany'].length > 50) {
        this.labourCompanyError = '最多50个字符';
      } else {
        this.labourCompanyError = '';
      }
    }
  }

  selectedRegionCk() {
    if (!(this.selectedRegion && this.selectedRegion.length > 0)) {
      this.regionError = '请选择服务区域';
    } else {
      this.regionError = '';
    }
  }

  abilityCk() {
    this.teamInfo['ability'] = this.teamInfo['ability'] ? this.teamInfo['ability'].trim() : '';
    if (!this.teamInfo['ability']) {
      this.abilityError = '请输入施工能力';
    } else {
      if (!this.checkNumber(this.teamInfo['ability'])) {
        this.abilityError = '请输入正确的施工能力';
      } else {
        if (this.teamInfo['ability'].length) {
          this.abilityError = '最多3个字符';
        }
        this.abilityError = '';
      }
    }
  }

  cashDepositStatusCk() {
    if (!(this.selectedCashDepositStatus && this.selectedCashDepositStatus['code'])) {
      this.cashDepositStatusError = '保证金状态不能为空';
    } else {
      this.cashDepositStatusError = '';
      this.cashAmountError = '';
      if (this.selectedCashDepositStatus.code === 'cashdepositstatus_staypaid') {
        this.teamInfo['cashAmount'] = 0;
      }
    }
  }

  cashAmountCk() {
    if (this.selectedCashDepositStatus.code && this.selectedCashDepositStatus.code === 'cashdepositstatus_havepaid') {
      this.teamInfo['cashAmount'] = this.teamInfo['cashAmount'] ? this.teamInfo['cashAmount'].trim() : '';
      if (!this.teamInfo['cashAmount']) {
        this.cashAmountError = '请输入保证金额';
      } else {
        if (!this.checkNumber(this.teamInfo['cashAmount'])) {
          if(this.teamInfo['cashAmount']>999999999){
            this.cashAmountError = '最多小于10亿';
          }
          else{
            this.cashAmountError = '请输入正确的保证金额';
          }

        } else {
          this.cashAmountError = '';
        }
      }
    } else {
      this.teamInfo['cashAmount'] = 0;
      this.cashAmountError = '';
    }
  }

  getWorkingYearsCk() {
    if (!this.foreman['workingStartTime']) {
      this.workingStartTimeError = '请选择从业时间';
    } else {
      this.workingStartTimeError = '';
    }
  }
  fNameCK() {
    this.foreman['name'] = this.foreman['name'] ? this.foreman['name'].trim() : '';
    if (!this.foreman['name']) {
      this.foremanNameError = '请输入姓名';
    } else {
      this.foremanNameError = '';
    }
  }
  selectSexCk() {
    if (!(this.selectSex && this.selectSex['code'])) {
      this.sexError = '请选择性别';
    } else {
      this.sexError = '';
    }
  }
  levelCk() {
    if (!(this.selectLevel && this.selectLevel['code'])) {
      this.levelError = '请选择级别';
    } else {
      this.levelError = '';
    }
  }

  currentAddressCk() {

    this.foreman['addressCurrentDetail'] = this.foreman['addressCurrentDetail'] ?
      this.foreman['addressCurrentDetail'].trim() : '';
    if(!this.selectedCurrentProvince['area_code'] || !this.selectedCurrentCity['area_code'] || !this.selectedCurrentArea['area_code']){
      this.proCityDisError = "请选择省市区";
      return;
    }
    if (!this.foreman['addressCurrentDetail']) {
      this.CurrentProvinceError = '请输入详细地址';
    } else {
      if (this.foreman['addressCurrentDetail'].length > 25) {
        this.CurrentProvinceError = '详细地址25个字内';
      } else {
        this.CurrentProvinceError = '';
      }
    }
    // if (!(this.selectedCurrentProvince && this.selectedCurrentProvince['area_code'])) {
    //   this.CurrentProvinceError = '请选择省市区';
    // } else {
    //   this.CurrentProvinceError = '';
    //   if (!(this.selectedCurrentCity && this.selectedCurrentCity['area_code'])) {
    //     this.CurrentProvinceError = '请选择省市区';
    //   } else {
    //     this.CurrentProvinceError = '';
    //     if (!(this.selectedCurrentArea && this.selectedCurrentArea['area_code'])) {
    //       this.CurrentProvinceError = '请选择省市区';
    //     } else {
    //       this.CurrentProvinceError = '';
    //       this.foreman['addressCurrentDetail'] = this.foreman['addressCurrentDetail'] ?
    //         this.foreman['addressCurrentDetail'].trim() : '';
    //       if (!this.foreman['addressCurrentDetail']) {
    //         this.CurrentProvinceError = '请输入详细地址';
    //       } else {
    //         if (this.foreman['addressCurrentDetail'].length > 25) {
    //           this.CurrentProvinceError = '详细地址25个字内';
    //         } else {
    //           this.CurrentProvinceError = '';
    //         }
    //       }
    //     }
    //   }
    // }
  }

  descriptionCk() {
    this.foreman['description'] = this.foreman['description'] ? this.foreman['description'].trim() : '';
    if (!this.foreman['description']) {
      this.descriptionError = '请输入个人简介';
    } else {
      if (this.foreman['description'].length > 70) {
        this.descriptionError = '70个字符内';
      } else {
        this.descriptionError = '';
      }
    }
  }
  managerPhotoPcCk() {

    if (!this.foreman['managerPhotoPc']) {
      this.managerPhotoPcError = '请上传手持身份证照片';
    } else {
      this.managerPhotoPcError = '';
    }

  }
  idcardPhotoFrontCk() {
    if (!this.foreman['idcardPhotoFront']) {
      this.idcardPhotoFrontError = '请上传身份证正面照片';
    } else {
      this.idcardPhotoFrontError = '';
    }

  }
  idcardPhotoReverseCk() {

    if (!this.foreman['idcardPhotoReverse']) {
      this.idcardPhotoReverseError = '请上传身份证反面照片';
    } else {
      this.idcardPhotoReverseError = '';
    }

  }
  insuranceAttachCk() {
    if (!this.foreman['insuranceAttach']) {
      this.insuranceAttachError = '请上传保险单附件照片';
    } else {
      this.insuranceAttachError = '';
    }
  }
  insuranceTimeCk() {
    if (!this.foreman['insuranceStartTime']) {
      this.insuranceStartTimeError = '请选择保险起止日期';
    } else {
      this.insuranceStartTimeError = '';
    }

    if (!this.foreman['insuranceEndTime']) {
      this.insuranceStartTimeError = '请选择保险起止日期';
    } else {
      this.insuranceStartTimeError = '';
    }
  }
  serviceContractAttachCk() {
    if (!this.foreman['serviceContractAttach']) {
      this.serviceContractAttachError = '请上传合同附件照片';
    } else {
      this.serviceContractAttachError = '';
    }
  }

  contractTimeCk() {

    if (!this.foreman['contractStartTime']) {
      this.contractStartTimeError = '请选择合同起止日期';
    } else {
      this.contractStartTimeError = '';
    }

    if (!this.foreman['contractEndTime']) {
      this.contractStartTimeError = '请选择合同起止日期';
    } else {
      this.contractStartTimeError = '';
    }
  }
  electricAttachCk() {
    if (!this.foreman['electricAttach']) {
      this.electricAttachError = '请上传电工证照片';
    } else {
      this.electricAttachError = '';
    }
  }
  electricAttachTimeCk() {
    if (!this.foreman['electricAttachStartTime']) {
      this.electricAttachStartTimeError = '请选择电工证起止日期';
    } else {
      this.electricAttachStartTimeError = '';
    }
    if (!this.foreman['electricAttachEndTime']) {
      this.electricAttachStartTimeError = '请选择电工证起止日期';
    } else {
      this.electricAttachStartTimeError = '';
    }
  }
  pipeCardAttachCk() {
    if (!this.foreman['pipeCardAttach']) {
      this.pipeCardAttachError = '请上传管道证附件证照片';
    } else {
      this.pipeCardAttachError = '';
    }

  }
  pipeCardTimeCk() {
    // if (!this.foreman['pipeCardStartTime']) {
    //   this.pipeCardStartTimeError = '请选择管道证起止日期';
    // } else {
    //   this.pipeCardStartTimeError = '';
    // }
    // if (!this.foreman['pipeCardEndTime']) {
    //   this.pipeCardStartTimeError = '请选择管道证起止日期';
    // } else {
    //   this.pipeCardStartTimeError = '';
    // }
  }
  constructionManualCk() {
    if (!this.foreman['constructionManual']) {
      this.constructionManualError = '请上传施工手册照片';
    } else {
      this.constructionManualError = '';
    }
  }
  productManualCk() {
    if (!this.foreman['productManual']) {
      this.productManualError = '请上传产品套餐手册';
    } else {
      this.productManualError = '';
    }

  }
  engineerStandardCk() {
    if (!this.foreman['engineerStandard']) {
      this.engineerStandardError = '请上传工程部处罚规范';
    } else {
      this.engineerStandardError = '';
    }
  }
  appStandardCk() {
    if (!this.foreman['appStandard']) {
      this.appStandardError = '请上传APP使用规范';
    } else {
      this.appStandardError = '';
    }
  }
  /**
   * 数据验证
   */
  checkSaveData(): boolean {
    let flag = true;
    if (!(this.selectedRegion && this.selectedRegion.length > 0)) {
      flag = false;
      this.regionError = '请选择服务区域';
    } else {
      this.regionError = '';
    }

    if (!this.teamInfo['name']) {
      flag = false;
    }
    if (!(this.selectedOrganizationArr && this.selectedOrganizationArr['name'])) {
      flag = false;
      this.organizationError = '请选择所属公司';
    } else {
      this.organizationError = '';
    }

    if (!this.teamInfo['labourCompany']) {
      flag = false;
      this.labourCompanyError = '请输入劳务公司';
    } else {
      if (this.teamInfo['labourCompany'].length > 50) {
        this.labourCompanyError = '最多50个字符';
      } else {
        this.labourCompanyError = '';
      }
    }

    if (this.teamInfo['telephone']) {
      let mobileArr = [];

      let temTelephone = '';
      temTelephone = this.teamInfo['telephone'].replace(/，/g, ',');
      mobileArr = temTelephone.split(',');


      for (let i in mobileArr) {
        if (!this.check.phone(mobileArr[i])) {
          flag = false;
          this.tTelephoneError = '请输入正确的联系电话';
          break;
        } else {
          this.tTelephoneError = '';
        }
      }

    }

    // if (!this.teamInfo['workerNum']) {
    //   flag = false;
    //   this.workerNumError = '工人人数不能为空';
    // } else {
    //   if (!this.checkNumber(this.teamInfo['workerNum'])) {
    //     flag = false;
    //     this.workerNumError = '工人人数必须是数字';
    //   } else {
    //     this.workerNumError = '';
    //   }
    // }

    if (!this.teamInfo['ability']) {
      flag = false;
      this.abilityError = '请输入施工能力';
    } else {

      if (!this.checkNumber(this.teamInfo['ability'])) {
        flag = false;
        this.abilityError = '施工能力必须是数字';
      } else {
        this.abilityError = '';
      }
    }

    // if (this.isRecommendErr) {
    //   flag = false;
    //   this.selectRecommendError = '是否推介不能为空';
    // } else {
    //   this.selectRecommendError = '';
    // }

    if (!(this.selectedCashDepositStatus && this.selectedCashDepositStatus['code'])) {
      flag = false;
      this.cashDepositStatusError = '保证金状态不能为空';
    } else {
      this.cashDepositStatusError = '';
    }

    if (this.selectedCashDepositStatus && this.selectedCashDepositStatus.code && this.selectedCashDepositStatus.code === 'cashdepositstatus_havepaid') {
      if (!this.teamInfo['cashAmount']) {
        flag = false;
        this.cashAmountError = '保证金额不能为空';
      } else {
        if (!this.checkNumber(this.teamInfo['cashAmount'])) {
          flag = false;
          if(this.teamInfo['cashAmount']>999999999){
            this.cashAmountError = '最多小于10亿';
          }
          else{
            this.cashAmountError = '请输入正确的保证金额';
          }
        } else {
          this.cashAmountError = '';
        }
      }
    } else {
      this.teamInfo['cashAmount'] = 0;
      this.cashAmountError = '';
    }

    if (this.foremanAccountError) {
      flag = false;
    }

    if (!this.foreman['account']) {
      flag = false;
      this.foremanAccountError = '请输入账号';
    }

    if (!this.foreman['workingStartTime']) {
      flag = false;
      this.workingStartTimeError = '请选择从业时间';
    } else {
      this.workingStartTimeError = '';
    }

    if (this.idNumberError) {
      flag = false;
    }
    if (!this.foreman['idNumber']) {
      this.idNumberError = '请输入身份证号';
    }
    // else {
    //   console.log(this.userIDCard(this.foreman['idNumber']));
    //   this.userIDCard(this.foreman['idNumber']);
    // }

    // if (!this.foreman['workingYears']) {
    //   flag = false;
    //   this.workingYearsError = '从业年限不能为空';
    // } else {
    //   if (!this.checkNumber(this.foreman['workingYears'])) {
    //     flag = false;
    //     this.workingYearsError = '从业年限必须是数字';
    //   } else {
    //     this.workingYearsError = '';
    //   }
    // }

    if (!this.foreman['managerTelephone']) {
      flag = false;
      this.managerTelephoneError = '请输入手机号';
    } else {
      if (!this.check.phone(this.foreman['managerTelephone'])) {
        flag = false;
        this.managerTelephoneError = '请输入正确的手机号';
      } else {
        this.managerTelephoneError = '';
      }
    }

    if (this.foreman['birthPlace'] && this.foreman['birthPlace'].length > 11) {
      flag = false;
      this.birthPlaceError = '10个字符内';
    }

    if (!this.foreman['name']) {
      flag = false;
      this.foremanNameError = '请输入姓名';
    } else {
      this.foremanNameError = '';
    }

    if (!(this.selectSex && this.selectSex['code'])) {
      flag = false;
      this.sexError = '请选择性别';
    } else {
      this.sexError = '';
    }

    if (!(this.selectLevel && this.selectLevel['code'])) {
      flag = false;
      this.levelError = '请选择级别';
    } else {
      this.levelError = '';
    }

    // if (!this.foreman['level']) {
    //   flag = false;
    //   this.levelError = '请选择级别';
    // } else {
    //   this.levelError = '';
    // }

    if (!(this.selectedCurrentProvince && this.selectedCurrentProvince['area_code'])) {
      flag = false;
      this.CurrentProvinceError = '请选择省市区';
    } else {
      this.CurrentProvinceError = '';
      if (!(this.selectedCurrentCity && this.selectedCurrentCity['area_code'])) {
        flag = false;
        this.CurrentProvinceError = '请选择省市区';
      } else {
        this.CurrentProvinceError = '';
        if (!(this.selectedCurrentArea && this.selectedCurrentArea['area_code'])) {
          flag = false;
          this.CurrentProvinceError = '请选择省市区';
        } else {
          this.CurrentProvinceError = '';
          if (!this.foreman['addressCurrentDetail']) {
            flag = false;
            this.CurrentProvinceError = '请输入详细地址';
          } else {
            if (this.foreman['addressCurrentDetail'].length > 25) {
              flag = false;
              this.CurrentProvinceError = '详细地址25个字内';
            } else {
              this.CurrentProvinceError = '';
            }
          }
        }
      }
    }

    // if (!(this.selectedBirthProvinceArr && this.selectedBirthProvinceArr['area_code'])) {
    //   flag = false;
    //   this.BirthProvinceError = '户籍所在地填写错误';
    // } else {
    //   this.BirthProvinceError = '';
    //   if (!(this.selectedBirthCityArr && this.selectedBirthCityArr['area_code'])) {
    //     flag = false;
    //     this.BirthProvinceError = '户籍所在地填写错误';
    //   } else {
    //     this.BirthProvinceError = '';
    //     if (!(this.selectedBirthAreaArr && this.selectedBirthAreaArr['area_code'])) {
    //       flag = false;
    //       this.BirthProvinceError = '户籍所在地填写错误！';
    //     } else {
    //       this.BirthProvinceError = '';
    //       if (!this.foreman['addressBirthDetail']) {
    //         flag = false;
    //         this.BirthProvinceError = '户籍所在地填写错误！';
    //       } else {
    //         this.BirthProvinceError = '';
    //       }
    //     }
    //   }
    // }

    if (!this.foreman['description']) {
      flag = false;
      this.descriptionError = '请输入个人简介';
    } else {
      if (this.foreman['description'].length > 70) {
        flag = false;
        this.descriptionError = '70个字符内';
      } else {
        this.descriptionError = '';
      }
    }


    if (!this.foreman['managerPhotoPc']) {
      flag = false;
      this.managerPhotoPcError = '请上传手持身份证照片';
    } else {
      this.managerPhotoPcError = '';
    }

    if (!this.foreman['idcardPhotoFront']) {
      flag = false;
      this.idcardPhotoFrontError = '请上传身份证正面照片';
    } else {
      this.idcardPhotoFrontError = '';
    }

    if (!this.foreman['idcardPhotoReverse']) {
      flag = false;
      this.idcardPhotoReverseError = '请上传身份证正面照片';
    } else {
      this.idcardPhotoReverseError = '';
    }

    if (!this.foreman['insuranceAttach']) {
      flag = false;
      this.insuranceAttachError = '请上传保险单附件照片';
    } else {
      this.insuranceAttachError = '';
    }


    if (!this.foreman['insuranceStartTime']) {
      flag = false;
      this.insuranceStartTimeError = '请选择保险起止日期';
    } else {
      this.insuranceStartTimeError = '';
    }

    if (!this.foreman['insuranceEndTime']) {
      flag = false;
      this.insuranceStartTimeError = '请选择保险起止日期';
    } else {
      this.insuranceStartTimeError = '';
    }

    if (!this.foreman['serviceContractAttach']) {
      flag = false;
      this.serviceContractAttachError = '请上传合同附件照片';
    } else {
      this.serviceContractAttachError = '';
    }

    if (!this.foreman['contractStartTime']) {
      flag = false;
      this.contractStartTimeError = '请选择合同起止日期';
    } else {
      this.contractStartTimeError = '';
    }

    if (!this.foreman['contractEndTime']) {
      flag = false;
      this.contractStartTimeError = '请选择合同起止日期';
    } else {
      this.contractStartTimeError = '';
    }

    if (!this.foreman['electricAttach']) {
      flag = false;
      this.electricAttachError = '请上传电工证照片';
    } else {
      this.electricAttachError = '';
    }

    if (!this.foreman['electricAttachStartTime']) {
      flag = false;
      this.electricAttachStartTimeError = '请选择电工证起止日期';
    } else {
      this.electricAttachStartTimeError = '';
    }

    if (!this.foreman['electricAttachEndTime']) {
      flag = false;
      this.electricAttachStartTimeError = '请选择电工证起止日期';
    } else {
      this.electricAttachStartTimeError = '';
    }

    // if (!this.foreman['pipeCardAttach']) {
    //   flag = false;
    //   this.pipeCardAttachError = '请上传管道证附件证照片';
    // } else {
    //   this.pipeCardAttachError = '';
    // }

    // if (!this.foreman['pipeCardStartTime']) {
    //   flag = false;
    //   this.pipeCardStartTimeError = '请选择管道证起止日期';
    // } else {
    //   this.pipeCardStartTimeError = '';
    // }

    // if (!this.foreman['pipeCardEndTime']) {
    //   flag = false;
    //   this.pipeCardStartTimeError = '请选择管道证起止日期';
    // } else {
    //   this.pipeCardStartTimeError = '';
    // }

    if (!this.foreman['constructionManual']) {
      flag = false;
      this.constructionManualError = '请上传施工手册照片';
    } else {
      this.constructionManualError = '';
    }

    if (!this.foreman['productManual']) {
      flag = false;
      this.productManualError = '请上传产品套餐手册';
    } else {
      this.productManualError = '';
    }

    if (!this.foreman['engineerStandard']) {
      flag = false;
      this.engineerStandardError = '请上传工程部处罚规范';
    } else {
      this.engineerStandardError = '';
    }
    if (!this.foreman['appStandard']) {
      flag = false;
      this.appStandardError = '请上传APP使用规范';
    } else {
      this.appStandardError = '';
    }
    return flag;
  }
  /**
   * 手机号验证
   * @param data
   */
  checkMobile(data) {
    this.teamInfo['telephone'] = this.teamInfo['telephone'] ? this.teamInfo['telephone'].trim() : '';
    if (!data) {
      this.tTelephoneError = '';
      return;
    }

    let mobileArr = [];
    let temTelephone = '';
    temTelephone = this.teamInfo['telephone'].replace(/，/g, ',');
    mobileArr = temTelephone.split(',');
    // if (mobileArr.includes(',').lengt <= 0) {
    //   mobileArr = this.teamInfo['telephone'].split('，');
    // }
    for (let i in mobileArr) {
      if (!this.check.phone(mobileArr[i])) {
        this.tTelephoneError = '请输入正确的联系电话';
        break;
      } else {
        this.tTelephoneError = '';
      }
    }
  }

  /**
   * 是否推荐选中没选中的判断
   */
  clickIsRecommend() {
    this.isRecommendErr = false;
  }

  /**
   * 判断数据是否加载完整
   */
  loadDicData() {
    if (this.addressCurrentProvinceArr.length <= 0 && this.addressBirthProvinceArr.length <= 0) {
      return;
    }
    if (!this.foreman['id'] && !this.foreman['id']) {
      return;
    }
    if (this.sexArr.length < 0) {
      return;
    }
    if (!(this.cashDepositStatus && this.cashDepositStatus.length > 0)) {
      return;
    }
    if (!(this.organizationArr && this.organizationArr.length > 0)) {
      return;
    }
    if (!(this.region && this.region.length > 0)) {
      return;
    }
    this.selectRecommend = this.teamInfo['isRecommend'] ? '1' : '0';
    this.selectedCashDepositStatus = { code: this.teamInfo['cashDepositStatus'] };
    this.selectSex = { code: this.foreman['sex'] };
    this.selectLevel = { code: this.foreman['level'] };
    this.selectedOrganizationArr = {
      name: this.teamInfo['companyName'], cityCode: this.teamInfo['cityCode']
      , provinceCode: this.teamInfo['provinceCode'], companyCode: this.teamInfo['companyCode'],
      code: this.teamInfo['organizationCode']
    };


    const regions = this.teamInfo['serviceArea'].split(',');
    for (const item of regions) {
      this.selectedRegion = [...this.selectedRegion, { 'area_code': item }];
    }
    // 手机验证
    this.checkMobile(this.teamInfo['telephone']);
    // 单选按钮
    this.clickIsRecommend();
    // 现居住地
    for (const item of this.addressCurrentProvinceArr) {
      if (item.area_code === this.foreman['addressCurrentProvince']) {
        this.selectedCurrentProvince = { area_code: this.foreman['addressCurrentProvince'] };
        this.addressCurrentCityArr = item.city;
      }
    }
    for (const item of this.addressCurrentCityArr) {
      if (item.area_code === this.foreman['addressCurrentCity']) {

        this.addressCurrentAreaArr = item.area;
        this.selectedCurrentCity = { area_code: this.foreman['addressCurrentCity'] };
      }
    }
    this.selectedCurrentArea = { area_code: this.foreman['addressCurrentArea'] };

    // 户口所在地
    for (const item of this.addressBirthProvinceArr) {
      if (item.area_code === this.foreman['addressBirthProvince']) {
        this.selectedBirthProvinceArr = { area_code: this.foreman['addressBirthProvince'] };
        this.addressBirthCityArr = item.city;
      }
    }
    for (const item of this.addressBirthCityArr) {
      if (item.area_code === this.foreman['addressBirthCity']) {
        this.addressBirthAreaArr = item.area;
        this.selectedBirthCityArr = { area_code: this.foreman['addressBirthCity'] };
      }
    }
    this.selectedBirthAreaArr = { area_code: this.foreman['addressBirthArea'] };
  }

  /**
   * 根据用户选中的开始时间。设置结束时间的mixdate
   * @param mixDate
   */
  updateCalendarEndTime(type) {
    if (type === 'insurance') {
      this.insuranceEndTimeMinDate = new Date(this.foreman['insuranceStartTime']);
    } else if (type === 'contract') {
      this.contractStartTimeMinDate = new Date(this.foreman['contractStartTime']);
    } else if (type === 'electric') {
      this.electricAttachEndTimeMinDate = new Date(this.foreman['electricAttachStartTime']);
    } else if (type === 'pipe') {
      this.pipeCardEndTimeMinDate = new Date(this.foreman['pipeCardStartTime']);
    }
  }

  /**
  * 根据用户选中的结束时间。设置开始时间的maxdate
  * @param
  */
  updateCalendarStartTime(type) {
    if (type === 'insurance') {
      this.insuranceEndTimeMaxDate = new Date(this.foreman['insuranceEndTime']);
    } else if (type === 'contract') {
      this.contractStartTimeMaxDate = new Date(this.foreman['contractEndTime']);
    } else if (type === 'electric') {
      this.electricAttachEndTimeMaxDate = new Date(this.foreman['electricAttachEndTime']);
    } else if (type === 'pipe') {
      this.pipeCardEndTimeMaxDate = new Date(this.foreman['pipeCardEndTime']);
    }
  }


  /**
   * 判断是不是正整数
   */
  checkNumber(data) {
    return (/^[1-9][0-9]{0,8}$|(^([1-9][0-9]{0,8})\.([0-9]{1,2})$)|^[0]\.([0-9]{1,2})$/g.test(data));
  }

  /**
   * 上传你成功以后数据会显并更新对应图片的数据
   * @param event
   * @param f
   */
  onUpload(event, f) {
    const result = JSON.parse(event.xhr.responseText);
    if (result && result.result && result.data) {
      this.foreman[f.cancelLabel] = this.foreman[f.cancelLabel] ? this.foreman[f.cancelLabel] + ',' + result.data.fileCode :
        result.data.fileCode;
      for (let items of result.data.filePath.split(',')) {
        this[f.cancelLabel + 'Images'] = [...this[f.cancelLabel + 'Images'], {
          source: items, thumbnail: items,
          width: '100px', maxWidth: '800px', title: ''
        }];
      }
    }
  }

  /**
   * 计算工作年限
   */
  getWorkingYears() {
    this.teaminfoService.getWorkingYears({ startTime: this.foreman['workingStartTime'] }).then(
      data => {
        if (data.data === '0') {
          this.foreman['workingYears'] = '不满一年';
        } else {
          this.foreman['workingYears'] = data.data;
        }
      }
    );
  }
  /**
   *删除图片并更新对应的图片数据
   * @param event
   * @param f
   */
  removeItem(event, f) {
    let codeArray = this.foreman[f].split(',');
    codeArray.splice(event.index, 1);
    this.foreman[f] = codeArray.join(',');
    this[f + 'Images'] = this[f + 'Images'].filter((val, index) => index !== event.index);
  }


  /**
   * 上传单张图片验证
   */
  onSelect(event, id) {
    if (event.files.length <= 1) {
      event.upload();
    } else {
      id.clear();
      this[id.cancelLabel + 'Error'] = '图片最多上传1张';
    }
  }

  /**
   * Description: 图片大小超过指定值
   */
  onSelectError(event,id){
    this.errorFiel = event;
    let errorType:any = event[0].errorType;
    if(errorType == "typeError"){
      this[id.cancelLabel + 'Error'] = "上传的图片类型错误";
    }
    if(errorType == "maxError"){
      this[id.cancelLabel + 'Error'] = "图片大小超过限制";
    }
  }

  /**
   * 上传9张图片验证
   */
  onSelect9(event, id) {
    if(this.errorFiel && this.errorFiel.length!=0){
      id.clear();
      this.errorFiel = undefined;
      return
    }else{
      this.errorFiel = undefined;
    }
    if (event.files.length <= (20 - this[id.cancelLabel + 'Images'].length)) {
      event.upload();
      // this[id.cancelLabel + 'Error'] = '';
    } else {
      id.clear();
      this[id.cancelLabel + 'Error'] = '图片最多上传20张';
    }
  }

  /**
   * 省份证验证
   * @param data
   */
  userIDCard(data) {
    let reg18 = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
    let reg15 = /^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$/;
    this.foreman['idNumber'] = this.foreman['idNumber'] ? this.foreman['idNumber'].trim() : '';
    if (!this.foreman['idNumber']) {
      this.idNumberError = '请输入身份证号';
      return;
    }
    if (this.idNumberOld !== this.foreman['idNumber']) {
      if (reg18.test(data) || reg15.test(data)) {
        this.teaminfoService.uniqueUserIDCard({ idNumber: data, foremanId: this.teamId }).then(
          result => {
            if (result.result) {
              this.idNumberError = '';
            } else {
              this.idNumberError = result.msg;
            }
          }
        );
      } else {
        this.idNumberError = '请输入正确的身份证';
      }
    }

  }

  /**
   * 返回
   */
  returnBtn() {
    if (this.teamId) {
      this.router.navigate(['saas/engineer/constructionteam/teaminfo', this.teamId]);
    } else {
      this.router.navigate(['saas/engineer/constructionteam/teaminfo']);
    }
  }


  /**
   * Description: 验证省市区
   */
  checkProCityDis(){
    this.CurrentProvinceError = "";
    if(!this.selectedCurrentProvince['area_code'] || !this.selectedCurrentCity['area_code'] || !this.selectedCurrentArea['area_code']){
      this.proCityDisError = "请选择省市区";
    }else{
      this.proCityDisError = "";
      if(!this.foreman.addressCurrentDetail){
        this.CurrentProvinceError = '请输入详细地址';
      }
    }
  }


}

