import { Component, OnInit } from '@angular/core';
import { TeaminfoService } from '../teaminfo.service';
import { WorkerService } from '../../worker/worker.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckService } from '../../check/check.service';
import {StorageService} from "../../../../common/storage.service";


@Component({
  selector: 'app-info-teaminfo',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [TeaminfoService, WorkerService, CheckService],
})
export class InfoTeaminfoComponent implements OnInit {
  colsCheck;
  checkData;
  colsWorker;
  workerData;
  workerTotalRecords;
  workerInfo: any = {};
  workerLoading;
  pageSize;
  pageNo;
  teamId = 1;
  teamInfo: any = {};
  foreman: any = {};
  displayWorkInfo = false;
  workInfo: any = {};
  title = '查看项目工队';
  managerPhotoPcImages = []; // 手持身份证照片
  idcardPhotoFrontImages = []; // 身份证正面
  idcardPhotoReverseImages = []; // 身份证反面
  insuranceAttachImages = []; // 保险单附件
  appStandardImages = []; // APP使用规范
  engineerStandardImages = []; // 工程部处罚规范
  productManualImages = []; // 产品套餐手册
  constructionManualImages = []; // 施工手册
  pipeCardAttachImages = []; // 管道证附件
  electricAttachImages = []; // 电工证附件
  serviceContractAttachImages = []; // 合同附件
  editFlag = true;

  constructor(private teaminfoService: TeaminfoService, private workerService: WorkerService,
    private routeInfo: ActivatedRoute, private checkService: CheckService, private router: Router,private storage: StorageService) {
    this.workerLoading = true;
    // 审核表头
    this.colsCheck = [
      { field: 'name', header: '审核人姓名', width: '100px', frozen: false, sortable: true },
      { field: 'telephone', header: '手机号', frozen: false, sortable: true },
      { field: 'approveTime', header: '审核时间', width: '150px', sortable: true, tem: true },
      { field: 'approveResultText', header: '审核结果', sortable: true },
      { field: 'approveNote', header: '原因', width: '80px' },
    ];
    // 工人表头
    this.colsWorker = [
      { field: 'code', header: '账号', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'name', header: '姓名', frozen: false, sortable: true },
      { field: 'address', header: '现居所在地', width: '150px', sortable: true, tem: true },
      { field: 'workType', header: '工种', sortable: true },
      { field: 'telephone', header: '手机号', sortable: true },
      { field: 'inConstruction', header: '在施数量', width: '80px' },
      { field: 'level', header: '级别', },
      { field: 'status', header: '状态', width: '50px', sortable: true },
    ];
  }
  ngOnInit() {
    // 获取路由id
    this.storage.loading();
    this.routeInfo.params.subscribe((params) => {
      this.teamId = params.id;
      // 获取施工对信息
      this.teaminfoService.getTeamInfoById(this.teamId).then(data => {
        this.teamInfo = data.data.teamInfo;
        this.foreman = data.data.foreman;
        this.imgUrlData();
        if (this.teamInfo['billStatus'] === 'billstatus_incommit') {
          this.editFlag = true;
        } else {
          this.editFlag = false;
        }
        // 获取工人信息
        this.workerService.list({ constructionteamTeaminfoId: this.teamId }).then(result => {
          this.storage.loadout();
          if (result.result) {
            this.workerLoading = false;
            this.workerData = result.data.list;
            this.workerTotalRecords = result.data.totalCount;
          } else {
            this.workerLoading = false;
            this.workerData = [];
            this.workerTotalRecords = 0;
            this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
          }
        });
      });
      // 获取审核信息
      this.checkService.approveList({ teamId: this.teamId }).then(data => {
        this.checkData = data.data.list;
      });
      this.storage.loadout();
    });
  }

  /**
   * 查看员工详情
   * @param event 员工详情
   */
  showWorker(event) {
    this.displayWorkInfo = true;
    this.workerService.getWorkerInfoById(event.id).then(data => {
      this.workerInfo = data.data;
    });
  }
  /**
   * dataTable分页查询方法
   * @param event
   */
  onPage(event) {
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.workerLoading = true;
    this.workerService.list({ constructionteamTeaminfoId: this.teamId }, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.workerLoading = false;
          this.workerData = data.data.list;
          this.workerTotalRecords = data.data.totalCount;
        } else {
          this.workerLoading = false;
          this.workerData = [];
          this.workerTotalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  back() {
    this.router.navigate(['saas/engineer/constructionteam/teaminfo']);
  }

  edit() {
    if (this.teamId) {
      this.router.navigate(['saas/engineer/constructionteam/teaminfo/add', this.teamId]);
    }
  }


  /**
 * 上传图片的回显
 */
  imgUrlData() {
    // 手持身份证照片
    for (let item of this.foreman['managerPhotoPcUrls'].split(',')) {
      this['managerPhotoPcImages'] = [...this['managerPhotoPcImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 身份证正面
    for (let item of this.foreman['idcardPhotoFrontUrls'].split(',')) {
      this['idcardPhotoFrontImages'] = [...this['idcardPhotoFrontImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 身份证反面
    for (let item of this.foreman['idcardPhotoReverseUrls'].split(',')) {
      this['idcardPhotoReverseImages'] = [...this['idcardPhotoReverseImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 保险单附件
    for (let item of this.foreman['insuranceAttachUrls'].split(',')) {
      this['insuranceAttachImages'] = [...this['insuranceAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 合同附件
    for (let item of this.foreman['serviceContractAttachUrls'].split(',')) {
      this['serviceContractAttachImages'] = [...this['serviceContractAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 电工证附件
    for (let item of this.foreman['electricAttachUrls'].split(',')) {
      this['electricAttachImages'] = [...this['electricAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 管道证附件
    for (let item of this.foreman['pipeCardAttachUrls'].split(',')) {
      this['pipeCardAttachImages'] = [...this['pipeCardAttachImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 施工手册
    for (let item of this.foreman['constructionManualUrls'].split(',')) {
      this['constructionManualImages'] = [...this['constructionManualImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 产品套餐手册
    for (let item of this.foreman['productManualUrls'].split(',')) {
      this['productManualImages'] = [...this['productManualImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // 工程部处罚规范
    for (let item of this.foreman['engineerStandardUrl'].split(',')) {
      this['engineerStandardImages'] = [...this['engineerStandardImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
    // APP使用规范
    for (let item of this.foreman['appStandardUrls'].split(',')) {
      this['appStandardImages'] = [...this['appStandardImages'], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    }
  }



}
