import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class CheckService {
    private baseUrl = '';
    private serveName = 'com-dyrs-mtsp-constructionteamservice';
    constructor(private checkService: HttpInterceptorService) { }

    /**
     * 获取施工队信息 分页方法
     * @param pageSize 分页大小 默认值10
     * @param pageNo  当前页 默认值是1
     * @param search  搜索条件
     */
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1, ) {
        return this.checkService.post(
            this.serveName + '/constructionteamservice/approveLogs/teamList',
            { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }

    /**
     * 获取施工队审核状态和施工队押金状态
     */
    getStatus() {
        return this.checkService.post(
            this.serveName + '/constructionteamservice/approveLogs/getCashAndApproveStatus', {}, this.baseUrl
        );
    }

    /**
     * 审核日志列表
     */
    approveList(search: any = {}) {
        return this.checkService.post(
            this.serveName + '/constructionteamservice/approveLogs/approveList', { ...search }, this.baseUrl);
    }
    /**
     * 审核确认
     */
    confirm(search: any = {}) {
        return this.checkService.post(
            this.serveName + '/constructionteamservice/approveLogs/approveStatus', { ...search }, this.baseUrl);
    }
    /**
     * 审核校验状态
     */
    confirmStatus(search: any = {}) {
        return this.checkService.post(
            this.serveName + '/constructionteamservice/approveLogs/checkStatus', { ...search }, this.baseUrl);
    }
}
