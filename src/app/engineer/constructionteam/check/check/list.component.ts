import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TimeConfigClass } from '../../../../common/common-config';
import { CheckService } from '../check.service';
import { StorageService } from '../../../../common/storage.service';


declare const window;
@Component({
  selector: 'app-check',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [CheckService],
})
export class CheckComponent implements OnInit {

  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  cols; // 表格头部
  colsSelected; // 表格选中的显示列
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  listData;
  cashDepositStatus; // 施工队保证金状态
  selectedCashDepositStatus; // 选中后的施工队保证金状态
  searchObj: any = {}; // 查询条件
  searFlag = true;
  searResult = '您还未添加任何内容';
  paginator = false;
  selectedCars3 = [];
  constructor(private router: Router, private checkService: CheckService, private storage: StorageService) {
    this.cols = [
      { field: 'foremanAccount', header: '项目经理用户名', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'teamName', header: '工队名称', width: '150px', sortable: true },
      { field: 'companyName', header: '所属公司', sortable: true },
      { field: 'managerTelephone', header: '项目经理手机号', width: '80px' , sortable: true},
      { field: 'serviceAreaName', header: '服务区域', sortable: true },
      { field: 'workerNum', header: '工人数量', width: '50px', sortable: true },
      { field: 'ability', header: '施工能力', width: '200px', sortable: true },
      { field: 'cashAmount', header: '保证金额', sortable: true },
      { field: 'cashDepositStatusText', header: '保证金状态', width: '200px', sortable: true },
      { field: 'froemanLevel', header: '级别', width: '200px', sortable: true, hidden: true },
    ];
    this.colsSelected = this.cols;
    // 获取施工队状态和施工队押金状态并填充到对对应下拉框中
    this.checkService.getStatus().then(
      data => {
        if (data.result) {
          this.cashDepositStatus = data.data.cashDepositStatus;
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '字典服务异常！' });
        }
      }
    );
    // 施工队表格初始化
    this.storage.loading();
    this.checkService.list().then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.listData.length > 0 ? this.paginator = true : this.paginator = false;
          this.searResult = '您还未添加任何内容';
        } else {
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  ngOnInit() {
  }
  /**
    * 搜索方法
    */
  search(event?) {
    this.searchObj['sortF'] = "";
    if (this.selectedCashDepositStatus && this.cashDepositStatus[0] && this.selectedCashDepositStatus.name !== this.cashDepositStatus[0].name) {
      this.searchObj['cashDepositStatus'] = this.selectedCashDepositStatus.code;
    } else {
      delete this.searchObj['cashDepositStatus'];
    }

    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      this.searchObj['searchValue'] = this.searchObj['searchValue'].trim();
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '工队名称/用户名/手机号/姓名 50个字符内' });
        return;
      }
    }
    this.pageSize = 20;
    this.pageNo = 1;
    this.storage.loading();
    this.checkService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.listData.length > 0 ? this.paginator = true : this.paginator = false;
          this.searResult = '未查到想要的数据';
        } else {
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }


  edit(e, row) {
    this.checkService.confirmStatus({ teamId: row.id }).then(
      data => {
        if (data.result) {
          let a = window.document.createElement('a');
          a.href = '/saas/engineer/constructionteam/check/' + row.id;
          a.target = '_blank';
          window.document.body.appendChild(a);
          a.click();
        } else {
          this.storage.makesureService.emit({
            message: data.msg,
            header: '提示',
            rejectVisible: false,
            acceptVisible: false,
            accept: () => {
              this.search(2);
            },
            reject: () => {
              this.search(2);
            }
          });
        }
      }
    );
  }


  /**
 * dataTable分页查询方法
 * @param eventevent
 */
  onPage(event) {
    this.searchObj['sortF'] = event.sortField;
    this.searchObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    if (this.selectedCashDepositStatus && this.selectedCashDepositStatus.name !== this.cashDepositStatus[0].name) {
      this.searchObj['cashDepositStatus'] = this.selectedCashDepositStatus.code;
    } else {
      delete this.searchObj['cashDepositStatus'];
    }

    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      this.searchObj['searchValue'] = this.searchObj['searchValue'].trim();
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '工队名称/用户名/手机号/姓名 50个字符内' });
        return;
      }
    }
    this.searResult = '没有搜到您想要的数据';
    this.storage.loading();
    this.checkService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.listData.length > 0 ? this.paginator = true : this.paginator = false;
        } else {
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

}
