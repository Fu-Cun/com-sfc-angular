import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
// check
import { CheckComponent } from './check/check/list.component';
import { InfoCheckComponent } from './check/check/info.component';
// team
import { TeaminfoComponent } from './team/team/list.component';
import { AddTeaminfoComponent } from './team/team/add.component';
import { InfoTeaminfoComponent } from './team/team/info.component';
// worker
import { AddWorkerComponent } from './worker/worker/add.component';
import { InfoWorkerComponent } from './worker/worker/info.component';
import { WorkerComponent } from './worker/worker/list.component';
/**
 * 施工队管理路由参数配置
 */
const mainRoutes: Routes = [
  {
    path: 'worker',
    component: WorkerComponent,
    data: { title: '工人管理'}
  },
  {
    path: 'worker/add',
    component: AddWorkerComponent,
    data: { title: '编辑工人', 'hasSidebar': false, 'hasHeader': false}
  },
  {
    path: 'worker/:id',
    component: InfoWorkerComponent,
    data: { title: '工人详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'teaminfo',
    component: TeaminfoComponent,
    data: { title: '项目工队信息'}
  },
  {
    path: 'teaminfo/add',
    component: AddTeaminfoComponent,
    data: { title: '新建施工队'}
  },
  {
    path: 'teaminfo/add/:id',
    component: AddTeaminfoComponent,
    data: { title: '编辑施工队', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'teaminfo/:id',
    component: InfoTeaminfoComponent,
    data: { title: '施工队详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'teaminfo/worker/add',
    component: AddWorkerComponent,
    data: { title: '新建工人'}
  },
  {
    path: 'check',
    component: CheckComponent,
    data: { title: '工队准入审核'}
  },
  {
    path: 'check/:id',
    component: InfoCheckComponent,
    data: { title: '审核详情'}
  },


];
/**
 * 施工队队管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ConstructionteamRoutingModule { }
