import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConstructionteamRoutingModule } from './main-routing.module';

// primeNG
import { CheckboxModule } from '../../components/checkbox/checkbox';
import { PanelModule } from '../../components/panel/panel';
import { InputTextModule } from '../../components/inputtext/inputtext';
import { DataTableModule } from '../../components/datatable/datatable';
import { MultiSelectModule } from '../../components/multiselect/multiselect';
import { InputMaskModule } from '../../components/inputmask/inputmask';
import { DropdownModule } from '../../components/dropdown/dropdown';
import { InputSwitchModule } from '../../components/inputswitch/inputswitch';
import { ButtonModule } from '../../components/button/button';
import { RatingModule } from '../../components/rating/rating';
import { CalendarModule } from '../../components/calendar/calendar';
import { RadioButtonModule } from '../../components/radiobutton/radiobutton';
import { InputTextareaModule } from '../../components/inputtextarea/inputtextarea';
import { FileUploadModule } from '../../components/fileupload/fileupload';
import { DialogModule } from '../../components/dialog/dialog';
import { LightboxModule } from '../../components/lightbox/lightbox';
import { FieldsetModule } from '../../components/fieldset/fieldset';
import { SpinnerModule } from '../../components/spinner/spinner';
import { MessageModule } from '../../components/message/message';
// team
import { TeaminfoComponent } from './team/team/list.component';
import { AddTeaminfoComponent } from './team/team/add.component';
import { InfoTeaminfoComponent } from './team/team/info.component';
// check
import { CheckComponent } from './check/check/list.component';
import { InfoCheckComponent } from './check/check/info.component';
// worker
import { InfoWorkerComponent } from './worker/worker/info.component';
import { WorkerComponent } from './worker/worker/list.component';
import { AddWorkerComponent } from './worker/worker/add.component';
import { from } from 'rxjs/observable/from';
/**
 * 施工队模块
 */
@NgModule({
  imports: [
    ConstructionteamRoutingModule,
    CommonModule,
    FormsModule,
    PanelModule,
    InputTextModule,
    DataTableModule,
    MultiSelectModule,
    ButtonModule,
    InputMaskModule,
    DropdownModule,
    InputSwitchModule,
    CalendarModule,
    RatingModule,
    CheckboxModule,
    RadioButtonModule,
    InputTextareaModule,
    FileUploadModule,
    DialogModule,
    ReactiveFormsModule,
    LightboxModule,
    FieldsetModule,
    SpinnerModule,
    MessageModule,
    SpinnerModule
  ],
  declarations: [
    TeaminfoComponent,
    WorkerComponent,
    CheckComponent,
    AddWorkerComponent,
    AddTeaminfoComponent,
    InfoTeaminfoComponent,
    InfoCheckComponent,
    InfoWorkerComponent
  ],
  exports: [],
  providers: []
})
export class ConstructionteamModule {
}
