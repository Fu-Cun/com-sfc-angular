import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

/**
 * 子工程路由参数
 */
const mainRoutes: Routes = [
  {
    path: 'constructionteam',
    loadChildren: 'app/engineer/constructionteam/main.module#ConstructionteamModule'
  },
  {
    path: 'constructionconfig',
    loadChildren: 'app/engineer/constructionconfig/main.module#ConstructionconfigModule'
  },
  {
    path: 'customerself',
    loadChildren: 'app/engineer/customerself/main.module#CustomerselfModule'
  },
  {
    path: 'constructionprocess',
    loadChildren: 'app/engineer/constructionprocess/main.module#ConstructionprocessModule'
  },
  {
    path: 'punishrecordmanage',
    loadChildren: 'app/engineer/punishrecord/main.module#PunishrecordModule'
  },
  {
    path: 'msgTemplate',
    loadChildren: 'app/engineer/msgTemplate/main.module#MsgTemplateManageModule'
  }
];
/**
 * 子工程路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EngineerRoutingModule { }
