import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

// usermanage
import { UsermanageComponent } from './usermanage/usermanage.component';
import { SelfdynamicComponent } from './selfdynamic/selfdynamic.component';
import { DynamicmanageComponent } from './dynamicmanage/dynamicmanage.component';
import { DynamicmanageComponentInfo } from './dynamicmanage/dynamicmanageInfo.component';
import { FeedbackComponent } from './feedback/feedback.component';

/**
 * 客户自助APP后台路由参数配置
 */
const mainRoutes: Routes = [
  {
    path: 'usermanage',
    component: UsermanageComponent,
    data:{title:'用户管理'}
  },
  {
    path: 'selfdynamic',
    component: SelfdynamicComponent,
    data:{title:'自定义动态'}
  },
  {
    path: 'dynamicmanage',
    component: DynamicmanageComponent,
    data:{title:'动态管理'}
  },
  {
    path: 'dynamicmanageInfo/:id',
    component: DynamicmanageComponentInfo,
    data:{title:'动态详情'}
  },
  {
    path: 'feedback',
    component: FeedbackComponent,
    data:{title:'用户反馈'}
  },

];
/**
 * 用户管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CustomerselfRoutingModule { }
