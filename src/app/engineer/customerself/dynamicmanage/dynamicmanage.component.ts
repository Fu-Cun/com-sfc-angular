import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TimeConfigClass } from '../../../common/common-config';
import { StorageService } from "app/common/storage.service";
import { DynamicmanageService } from './dynamicmanage.service';

@Component({
  selector: 'app-dynamicmanage',
  templateUrl: './dynamicmanage.component.html',
  styleUrls: ['./dynamicmanage.component.css'],
  providers: [DynamicmanageService]
})
export class DynamicmanageComponent implements OnInit {

  //列表
  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  loading: boolean = true;
  colsSelected: any;
  cols: any = [];
  selectedItem: any = []; //选中记录
  pageSize: number; // 每页条数
  pageNo: number; // 当前页
  listData: any = []; // 数据
  totalRecords: number; // 数据条数
  searchObj: any = {}; // 搜索条件
  searcSortObj: any = {}; // 排序信息

  //高级搜索
  isShowH: boolean = false; //弹窗H
  searchHighObj: any = {}; // 高级搜索
  selectedHighObj: any = {}; //选中条件

  dynamicTypeArr: any = [];  //类型 列表
  projectStageArr: any = [];  //项目阶段
  sendstatusArr: any = [];  //发送状态



  constructor(private storage: StorageService, private router: Router, private server: DynamicmanageService) {
    this.cols = [
      { field: 'code', header: '编号', frozen: false, sortable: true, tem: true, width: '10%'},
      { field: 'dynamicTypeDic', header: '类型', sortable: true, width: '10%'},
      { field: 'projectName', header: '项目名称', sortable: true, width: '10%'},
      { field: 'projectCode', header: '项目编号', sortable: true, width: '10%'},
      { field: 'projectStageDic', header: '项目阶段', sortable: true, width: '10%'},
      { field: 'receiverCode', header: '接收用户ID', sortable: true, width: '10%'},
      { field: 'userName', header: '发布人', sortable: true, width: '10%'},
      { field: 'sendTimeDic', header: '发送时间', sortable: true , width: '10%'},
      // { field: 'isFavor', header: '点赞', sortable: true,tem: true,  width: '8%' },
      // { field: 'isBoring', header: '踩', sortable: true, tem: true, width: '8%' },
      { field: 'sendstatusDic', header: '状态', sortable: true, width: '10%' }
    ];
    this.colsSelected = this.cols;
  }


  ngOnInit() {
    this.getDictionaries();
  }


  /* 跳转详情页面
  * @param event
  */
  info(code) {
    this.router.navigate(['saas/engineer/customerself/dynamicmanageInfo/', code]);
  }

  //撤回
  withdraw() {
    let list = this.selectedItem;
    if (list.length < 1) {
      this.warning2();
      return false;
    }
    let ids = [];
    for (let index in list) {
      ids.push(list[index].code)
    }
    this.storage.makesureService.emit({
      message: '确定要撤回您选定的' + list.length + '项吗?',
      header: '提示',
      icon: 'fa fa-question-circle',
      accept: () => {
        this.loading = true;
        this.server.withdraw(ids.join(",")).then(
          data => {
            this.loading = false;
            if (data.result) {
              const params = Object.assign(this.searchObj, this.searchHighObj, this.searcSortObj);
              this.getData(params);
              this.success();
              this.selectedItem = [];
            } else {
              this.error();
            }
          }
        );
      },
      reject: () => { }
    });

  }

  //删除
  delete() {
    let list = this.selectedItem;
    if (list.length < 1) {
      this.warning2();
      return false;
    }
    let ids = [];
    for (let index in list) {
      ids.push(list[index].code)
    }
    this.storage.makesureService.emit({
      message: '确定要删除您选定的' + list.length + '项吗?',
      header: '提示',
      icon: 'fa fa-question-circle',
      accept: () => {
        this.loading = true;
        this.server.delete(ids.join(",")).then(
          data => {
            this.loading = false;
            if (data.result) {
              const params = Object.assign(this.searchObj, this.searchHighObj, this.searcSortObj);
              this.getData(params);
              this.success();
              this.selectedItem = [];
            } else {
              this.error();
            }
          }
        );
      },
      reject: () => { }
    });

  }
  /**
   * 获取数据
   */
  getData(params) {
    //let params = this.searchObj;
    this.loading = true;
    this.server.list(params, this.pageSize, this.pageNo).then(
      data => {
        this.loading = false;
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );
  }
  /**
   * 改变模糊搜索框数据值
   */
  searchTextChange(event){
    if(event==undefined)
      return false
    if(event.constructor==String && event!=''){
      this.searchObj['searchValue'] = event;
    }else{
      this.searchObj['searchValue'] = undefined;
    };
  }
  /**
   * 搜索方法
   * @param event
   */
  search(event?) {
    //筛选查询
    this.searchTextChange(event);
    // this.pageNo = 1;
    this.searchHighObj = {};
    const params = Object.assign(this.searchObj);
    this.getData(params);
  }
  /**
   * 搜索方法 高级
   */
  searchHigh() {
    if(this.searchHighObj.sendTimeBegin==undefined&&this.searchHighObj.sendTimeEnd!=undefined){
      this.warning2('请选择开始时间');
      return false;
    } else if(this.searchHighObj.sendTimeBegin!=undefined&&this.searchHighObj.sendTimeEnd==undefined){
      this.warning2('请选择结束时间');
      return false;
    }
    this.searchObj = {};
    const params = Object.assign(this.searchHighObj);
    this.getData(params);
    // this.isShowH = false;
  }
  /**
   * 分页查询
   * @param event
   */
  loadCarsLazy(event) {
    this.pageSize = + event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.searcSortObj['sortF'] = event.sortField;
    this.searcSortObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    const params = Object.assign(this.searchObj, this.searchHighObj, this.searcSortObj);
    this.getData(params);
  }

  /**
  * 加载字典项
  */
  getDictionaries() {
    // 加载工种和工人状态
    this.server.getDictionaries().then(
      data => {
        if (data.result) {
           //类型
           this.dynamicTypeArr = data.data.dynamictype;
           this.dynamicTypeArr.unshift({code: '', name: '全部'});
           //项目阶段
           this.projectStageArr = data.data.technologytype;
           this.projectStageArr.unshift({code: '', name: '全部'});
           //发送状态
           this.sendstatusArr = data.data.sendstatus;
           this.sendstatusArr.unshift({code: '', name: '全部'});
        } else {
        };
      }
    );
  }


  /**
   * 改变时间
   */
  calendarChangeHigh(e){
    this.selectedHighObj[e] = new Date(this.searchHighObj[e]);
  }

  /**
   * 提示框
   */
  success(msg? : string) {
    msg = msg!=undefined ? msg : '操作成功';
    this.storage.messageService.emit({ severity: 'success', detail: msg });
  }
  error(msg? : string) {
    msg = msg!=undefined ? msg : '请选择要操作的数据！';
    this.storage.messageService.emit({ severity: 'error', detail: msg });
  }
  warning(msg? : string) {
    msg = msg!=undefined ? msg : '请选择要操作的数据！';
    this.storage.messageService.emit({ severity: 'warn', detail: msg });
  }
  warning2(msg? : string) {
    msg = msg!=undefined ? msg : '请选择要操作的数据！';
    this.storage.makesureService.emit({
      message: msg,
      header: '提示',
      icon: '',
      rejectVisible:false,
      accept: () => { },
      reject: () => { }
    });
  }

}
