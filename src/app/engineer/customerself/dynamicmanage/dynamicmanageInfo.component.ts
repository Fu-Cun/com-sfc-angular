import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from "app/common/storage.service";
import { DynamicmanageService } from './dynamicmanage.service';


@Component({
  selector: 'app-dynamicmanageInfo',
  templateUrl: './dynamicmanageInfo.component.html',
  styleUrls: ['./dynamicmanageInfo.component.css'],
  providers: [DynamicmanageService]
})
export class DynamicmanageComponentInfo implements OnInit {

  formObj: any = {};//表单数据
  imageArr = [];  //标题照片

  constructor(private storage: StorageService,private routeInfo: ActivatedRoute, private server: DynamicmanageService, private router: Router) { }

  ngOnInit() {
    this.routeInfo.params.subscribe((params) => {
      this.server.get(params.id).then(data => {
        if (data.result) {
          this.formObj = data.data;
          this.imgUrlData(this.formObj.imagePath,'imageArr');
        } else {
          this.error('请求失败!记录不存在或已被删除');
        }
      });
    });
  }

  /**
   * 展示图片数据
   */
  imgUrlData(fileUrl: string,fileArr: any,isClear?: string) {
    if(!fileUrl || !fileArr){
      return false;
    };
    if(isClear==undefined||isClear=='true'){
      this[fileArr] = [];
    };
    for (let item of fileUrl.split(',')) {
      this[fileArr] = [...this[fileArr], {
        source: item, thumbnail: item,
        width: '100px', maxWidth: '800px', title: ''
      }];
    };
  }

  /**
   * 提示框
   */
  success(msg? : string) {
    msg = msg!=undefined ? msg : '操作成功';
    this.storage.messageService.emit({ severity: 'success', detail: msg });
  }
  error(msg? : string) {
    msg = msg!=undefined ? msg : '请选择要操作的数据！';
    this.storage.messageService.emit({ severity: 'error', detail: msg });
  }
  warning(msg? : string) {
    msg = msg!=undefined ? msg : '请选择要操作的数据！';
    this.storage.messageService.emit({ severity: 'warn', detail: msg });
  }
  warning2(msg? : string) {
    msg = msg!=undefined ? msg : '请选择要操作的数据！';
    this.storage.makesureService.emit({
      message: msg,
      header: '提示',
      icon: '',
      rejectVisible:false,
      accept: () => { },
      reject: () => { }
    });
  }

}
