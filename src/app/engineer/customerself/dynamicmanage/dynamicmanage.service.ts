import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class DynamicmanageService {

    // 客户自助服务名称
    private serveName = 'com-dyrs-mtsp-customerselfservice';
    
    constructor(private httpService: HttpInterceptorService) {}

    /**
     * 获取列表 分页方法
     * @param pageSize 分页大小 默认值10
     * @param pageNo  当前页 默认值是1
     * @param search  搜索条件
     */
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
      return  this.httpService.post(this.serveName + '/dynamicManage/list/api', { pageSize: pageSize, pageNo: pageNo, ...search } );
    }
  
     /**
     * 获取单条记录
     * @param code 
     */
    get(code: string){
      return  this.httpService.post(this.serveName + '/dynamicManage/get/api', {code} );
    }
    /**
     * 撤回
     * @param code
     */
    withdraw(code: string){
      return  this.httpService.post(this.serveName + '/dynamicManage/withdraw/api', {code} );
    }
    /**
     * 删除
     * @param code
     */
    delete(code: string){
      return this.httpService.post(this.serveName + '/dynamicManage/delete/api', {code} );
    }
    /**
     * 获取数据字典
     */
    getDictionaries() {
      return  this.httpService.post(this.serveName + '/dynamicManage/getDictionaries/api', {} );
    }

    /**
     * 获取系统时间
     */
    getSystemTime() {
      return  this.httpService.post(this.serveName + '/base/getSystemTime/api', {} );
    }

}
