import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TimeConfigClass } from '../../../common/common-config';
import { StorageService } from "app/common/storage.service";
import { FeedbackService } from './feedback.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css'],
  providers: [FeedbackService]
})
export class FeedbackComponent implements OnInit {
  //列表
  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  loading: boolean = true;
  colsSelected: any;
  cols: any = [];
  selectedItem: any = []; //选中记录
  pageSize: number; // 每页条数
  pageNo: number; // 当前页
  listData: any = []; // 数据
  totalRecords: number; // 数据条数
  searchObj: any = {}; // 搜索条件
  searcSortObj: any = {}; // 排序信息
  submited: boolean ;

  //表单
  isShow: boolean = false; //弹窗a
  showTitle: string = '处理反馈';
  dateKey: string = '';//数据标识
  baseObj: any = {};//原始数据
  formObj: any = {};//表单数据
  formOptObj: any = {};//表单选中数据
  errorObj: any = {};//错误信息

  resouceTypeArr: any = []; //
  statusArr: any = []; //状态
  statusArr2: any = []; //状态2

  constructor(private storage: StorageService, private router: Router, private server: FeedbackService) {

    this.cols = [
      { field: 'userCode', header: '用户ID', frozen: false, sortable: true, width: '8%' },
      { field: 'telphone', header: '手机号', sortable: true, width: '8%' },
      { field: 'userName', header: '姓名', sortable: true, width: '8%' },
      { field: 'feedbackContent', header: '反馈内容', sortable: true, width: '18%' },
      { field: 'resouceTypeDic', header: '来源', sortable: true, width: '6%' },
      { field: 'feedbackTime', header: '反馈时间', sortable: true, width: '8%' },
      { field: 'statusDic', header: '状态', sortable: true, width: '8%' },
      { field: 'handleName', header: '处理人', sortable: true, width: '8%' },
      { field: 'note', header: '备注', sortable: true, width: '8%' },
      { field: 'handleTime', header: '处理时间', sortable: true, width: '8%' }
    ];

    this.colsSelected = this.cols;

  }

  ngOnInit() {

    this.getDictionaries();
  }


  //显示处理页面
  disposeShow() {
    this.resetFormDate()
    let list = this.selectedItem;
    if (list.length < 1) {
      this.warning2();
      return false;
    }
    if (list.length > 1) {
      this.warning2('只能勾选一条数据！');
      return false;
    }
    this.dateKey = list[0].code;
    this.isShow = true;
  }


  /**
   * 获取数据
   */
  getData(params) {
    //let params = this.searchObj;
    this.loading = true;
    this.server.list(params, this.pageSize, this.pageNo).then(
      data => {
        this.loading = false;
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );
  }
  /**
   * 改变模糊搜索框数据值
   */
  searchTextChange(event) {
    if (event == undefined)
      return false
    if (event.constructor == String && event != '') {
      this.searchObj['searchValue'] = event;
    } else {
      this.searchObj['searchValue'] = undefined;
    };
  }
  /**
   * 搜索方法
   * @param event
   */
  search(event?) {
    //筛选查询
    this.searchTextChange(event);
    // this.pageNo = 1;
    const params = Object.assign(this.searchObj);
    this.getData(params);
  }
  /**
   * 分页查询
   * @param event
   */
  loadCarsLazy(event) {
    this.pageSize = + event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.searcSortObj['sortF'] = event.sortField;
    this.searcSortObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    const params = Object.assign(this.searchObj, this.searcSortObj);
    this.getData(params);
  }

  /**
  * 加载字典项
  */
  getDictionaries() {
    // 加载工种和工人状态
    this.server.getDictionaries().then(
      data => {
        if (data.result) {
          //发送对象
          this.resouceTypeArr = data.data.source;
          this.resouceTypeArr.unshift({ code: '', name: '全部' });
          //状态
          this.statusArr = [].concat(data.data.status);
          this.statusArr.unshift({ code: '', name: '全部' });
          //状态
          this.statusArr2 = [].concat(data.data.status);
          this.statusArr2.unshift({ code: '', name: '请选择' });
        } else {
        };
      }
    );
  }

  /*****表单 */
  /**
   * 重置表单数据
   */
  resetFormDate() {
    this.isShow = false; //弹窗a
    this.dateKey = '';
    this.baseObj = {};//原始数据
    this.formObj = {};
    this.formOptObj = {};
    this.errorObj = {};//错误信息
  }


  //保存处理数据
  disposeSave() {
    //自定义验证
    if (!this.verifyCustom()) {
      return false;
    }
    this.formObj['code'] = this.dateKey
    this.loading = true;
    this.server.dispose(this.formObj).then(
      data => {
        this.loading = false;
        if (data.result) {
          const params = Object.assign(this.searchObj, this.searcSortObj);
          this.getData(params);
          this.isShow = false;
          this.success();
          this.selectedItem = [];
        } else {
          this.error();
        }
      }
    );

  }

  /**
   * 自定义验证
   */
  verifyCustom() {
    const a = this.changeStatus();
    if (a || !this.formObj.note) {
      this.errorObj['e'] = true;
      return false;
    }
    if (this.errorObj['e']) {
      delete this.errorObj['e'];
    }
    return true;
  }

  /**
   * 状态 验证
   */
  changeStatus() {
    if (this.formObj.status == undefined || this.formObj.status == '') {
      this.errorObj['status'] = { show: true };
      return true;
    }
    if (this.errorObj['status']) {
      delete this.errorObj['status'];
    }
    return false;
  }


  /**
   * 验证字段信息显示
   */
  verifyFields(e, obj) {
    if (e == undefined || e['name'] == undefined || obj == undefined) {
      return false;
    };
    let err = { show: true };
    if (e['value'] == undefined || e.value.trim() == '') {
      err['blank'] = true;
    };
    this.errorObj[e.name] = err;
    obj[e.name] = e['value'] == undefined ? '' : e.value.trim();
  }
  /**
   * 清除验证信息提示框
   */
  clearVerify(e) {
    if (e == undefined) {
      return false;
    }
    this.errorObj[e.name] = { show: false };
  }

  /**
   * 提示框
   */
  success(msg?: string) {
    msg = msg != undefined ? msg : '操作成功';
    this.storage.messageService.emit({ severity: 'success', detail: msg });
  }
  error(msg?: string) {
    msg = msg != undefined ? msg : '请选择要操作的数据！';
    this.storage.messageService.emit({ severity: 'error', detail: msg });
  }
  warning(msg?: string) {
    msg = msg != undefined ? msg : '请选择要操作的数据！';
    this.storage.messageService.emit({ severity: 'warn', detail: msg });
  }
  warning2(msg?: string) {
    msg = msg != undefined ? msg : '请选择要操作的数据！';
    this.storage.makesureService.emit({
      message: msg,
      header: '提示',
      icon: '',
      rejectVisible: false,
      accept: () => { },
      reject: () => { }
    });
  }

}
