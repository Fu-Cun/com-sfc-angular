import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class UsermanageService {

  // 客户自助服务名称
  private serveName = 'com-dyrs-mtsp-customerselfservice';

  constructor(private httpService: HttpInterceptorService) {}


  /**
   * 获取列表 分页方法
   * @param pageSize 分页大小 默认值10
   * @param pageNo  当前页 默认值是1
   * @param search  搜索条件
   */
  list(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
    return  this.httpService.post(this.serveName + '/user/list/api', { pageSize: pageSize, pageNo: pageNo, ...search } );
  }

   /**
   * 获取单条记录
   * @param code 
   */
  get(code: string){
    return  this.httpService.post(this.serveName + '/user/get/api', {code} );
  }
  /**
   * 禁用
   * @param code
   */
  stopUsing(code: string){
    return  this.httpService.post(this.serveName + '/user/stopUsing/api', {code} );
  }
  /**
   * 启用
   * @param code
   */
  startUsing(code: string){
    return this.httpService.post(this.serveName + '/user/startUsing/api', {code} );
  }
  /**
   * 保存数据
   * @param params 
   */
  save(params: any){
    return  this.httpService.post(this.serveName + '/user/edit/api', {...params} );
  }
  /**
   * 支付记录
   * @param code 
   */
  paymentListByCode(code: string){
    return  this.httpService.post(this.serveName + '/user/paymentListByCode/api', {code} );
  }
  /**
   * 获取数据字典
   */
  getDictionaries() {
    return  this.httpService.post(this.serveName + '/user/getDictionaries/api', {} );
  }
  /**
   * 验证手机号
   * @param phoneNumber 
   */
  checkUserIsExistByPhoneNumber(phoneNumber: string){
    return  this.httpService.post(this.serveName + '/user/checkUserIsExistByPhoneNumber/api', {phoneNumber} );
  }

  


}
