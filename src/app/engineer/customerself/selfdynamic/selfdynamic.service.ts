import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';
@Injectable()
export class SelfdynamicService {

    // 客户自助服务名称
    private serveName = 'com-dyrs-mtsp-customerselfservice';
  
    constructor(public httpService: HttpInterceptorService) {}
  
  
    /**
     * 获取列表 分页方法
     * @param pageSize 分页大小 默认值10
     * @param pageNo  当前页 默认值是1
     * @param search  搜索条件
     */
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
      return  this.httpService.post(this.serveName + '/selfDynamic/list/api', { pageSize: pageSize, pageNo: pageNo, ...search } );
    }
  
     /**
     * 获取单条记录
     * @param code 
     */
    get(code: string){
      return  this.httpService.post(this.serveName + '/selfDynamic/get/api', {code} );
    }
    /**
     * 撤回
     * @param code
     */
    withdraw(code: string){
      return  this.httpService.post(this.serveName + '/selfDynamic/withdraw/api', {code} );
    }
    /**
     * 删除
     * @param code
     */
    delete(code: string){
      return this.httpService.post(this.serveName + '/selfDynamic/delete/api', {code} );
    }
    /**
     * 保存数据
     * @param params 
     */
    save(params: any){
      return  this.httpService.post(this.serveName + '/selfDynamic/save/api', {...params} );
    }

    /**
     * 获取数据字典
     */
    getDictionaries() {
      return  this.httpService.post(this.serveName + '/selfDynamic/getDictionaries/api', {} );
    }

    /**
     * 获取系统时间
     */
    getSystemTime() {
      return  this.httpService.post(this.serveName + '/base/getSystemTime/api', {} );
    }
    

}
