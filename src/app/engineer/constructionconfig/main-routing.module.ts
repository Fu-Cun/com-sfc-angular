import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
// technology
import { TechnologyComponent } from './technology/technology/list.component';
import { ExportComponent } from './technology/technology/export.component';
import { InfoTechnologyComponent } from './technology/technology/info.component';

// template
import { TemplateEditComponent } from './template/template/edit.component';
import { ListTemplateComponent } from './template/template/list.component';
import { AddTemplateComponent } from './template/template/add.component';
// 验收管理
import { AddCheckComponent } from './check/check/add.component';
import { InfoCheckComponent } from './check/check/info.component';
import { ListCheckComponent } from './check/check/list.component';
// 节点管理
import { AddNodeComponent } from './node/node/add.component';
import { InfoNodeComponent } from './node/node/info.component';
import { ListNodeComponent } from './node/node/list.component';
// 派单规则设置
import { InfoSendorderruleComponent } from './sendorderrule/sendorderrule/info.component';
import { AddSendorderruleComponent } from './sendorderrule/sendorderrule/add.component';
// 巡检规则设置
import { InfoInspectionComponent } from './inspectionmanage/inspection/info.component';


/**
 * 施工队管理路由参数配置
 */
const mainRoutes: Routes = [
  {
    path: 'technology',
    component: TechnologyComponent,
    data: { title: '基础工艺库' }
  },
  {
    path: 'technology/export',
    component: ExportComponent,
    data: { title: '导出工艺' }
  },
  {
    path: 'technology/:id',
    component: InfoTechnologyComponent,
    data: { title: '工艺详情' , 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'template',
    component: ListTemplateComponent,
    data: { title: '模板管理' }
  },
  {
    path: 'template/edit/:id',
    component: TemplateEditComponent,
    data: { title: '模板详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'template/add',
    component: AddTemplateComponent,
    data: { title: '新建模板' }
  },
  {
    path: 'check',
    component: ListCheckComponent,
    data: { title: '验收管理' }
  },
  {
    path: 'check/add',
    component: AddCheckComponent,
    data: { title: '新建验收' }
  },
  {
    path: 'check/:id',
    component: InfoCheckComponent,
    data: { title: '验收详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'check/add/:id',
    component: AddCheckComponent,
    data: { title: '编辑验收', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'node',
    component: ListNodeComponent,
    data: { title: '节点管理' }
  },
  {
    path: 'node/add',
    component: AddNodeComponent,
    data: { title: '新建节点' }
  },
  {
    path: 'node/add/:id',
    component: AddNodeComponent,
    data: { title: '编辑节点', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'node/:id',
    component: InfoNodeComponent,
    data: { title: '节点详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'sendorderrule',
    component: InfoSendorderruleComponent,
    data: { title: '派单规则设置' }
  },
  {
    path: 'sendorderrule/:type',
    component: InfoSendorderruleComponent,
    data: { title: '编辑派单规则设置' }
  },
  {
    path: 'sendorderrule/add/:type',
    component: AddSendorderruleComponent,
    data: { title: '编辑派单规则设置' }
  },
  {
    path: 'sendorderrule/add',
    component: AddSendorderruleComponent,
    data: { title: '新建派单规则设置' }
  },
  {
    path: 'inspection',
    component: InfoInspectionComponent,
    data: { title: '巡检规则设置' }
  }

];
/**
 * 施工队队管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ConstructionconfigRoutingModule { }
