import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class CheckService {
  private baseUrl = '';
  // 施工配置服务名称
  private serveName = 'com-dyrs-mtsp-constructionconfigservice/constructionconfigservice/';

  constructor(private checkService: HttpInterceptorService) { }

  /**
   * 获取施工队信息 分页方法
   * @param pageSize 分页大小 默认值10
   * @param pageNo  当前页 默认值是1
   * @param search  搜索条件
   */
  list(search: any = {}, pageSize: number = 10, pageNo: number = 1, ) {
    return this.checkService.post(this.serveName + 'checkManageBase/list/api',
      { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }

  /**
   * Description: 启用、禁用
   */
  updateStatus(search) {
    return this.checkService.post(this.serveName + 'checkManageBase/updateStatus/api', search, this.baseUrl);
  }

  /**
   * 通过id获取验收项信息
   * @param id 工队id
   */
  getCheckManageById(id) {
    return this.checkService.post(this.serveName + 'checkManageBase/detailCheckManage/api', { checkId: id }, this.baseUrl);
  }

  /**
   * 添加方法
   * @param param
   */
  save(param: any = {}) {
    return this.checkService.post(this.serveName + 'checkManageBase/save/api',param, this.baseUrl);
  }
  /**
   * 修改方法
   * @param param
   */
  updata(param: any = {}) {
    return this.checkService.post(this.serveName + 'checkManageBase/updateOr/api',param, this.baseUrl);
  }

  /**
   * 验收项编号唯一性验证
   * @param param
   */
  uniqueCode(param: any = {}) {
    return this.checkService.post(this.serveName + 'checkManageBase/checkCheckCodeIsUsable/api',param, this.baseUrl);
  }

  /**
   * 验收项名称唯一性验证
   * @param param
   */
  uniqueName(param: any = {}) {
    return this.checkService.post(this.serveName + 'checkManageBase/checkNameIsUsable/api',param, this.baseUrl);
  }
}
