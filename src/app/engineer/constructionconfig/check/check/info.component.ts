import { Component, OnInit } from '@angular/core';
import { CheckService } from "../check.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-info-check',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [CheckService],
})
export class InfoCheckComponent implements OnInit {

  checkId;  //验收项id
  checkManage: any = {};  //验收项
  conStandardList;  //施工标准
  checkStandardList;   //验收标准
  colsConStandard; //施工标准列
  colsCheckStandard;  //验收标准列

  constructor(private router: Router, private routeInfo: ActivatedRoute, private checkService: CheckService) {
    //验收-施工标准
    this.colsConStandard = [
      { field: 'sortNum', header: '序号', width: '83px', frozen: false, sortable: true },
      { field: 'content', header: '施工标准', width: '500px', frozen: false, sortable: true }
    ];
    //验收-验收标准
    this.colsCheckStandard = [
      { field: 'sortNum', header: '序号', width: '100px', frozen: false, sortable: true },
      { field: 'checkStandard', width: '335px', header: '验收标准', frozen: false, sortable: true },
      { field: 'checkItem', width: '175px', header: '检查项目', sortable: true },
      { field: 'phoneRequire', width: '100px', header: '照片/张', sortable: true },
      { field: 'tackPhoneRequire', width: '200px', header: '拍摄要求', sortable: true },
      { field: 'toolConfig', width: '120px', header: '基本配备工具', sortable: true },
      { field: 'isRequestPassItem', width: '170px', header: '是否为必过项', sortable: true, tem: true }
    ];
  }

  ngOnInit() {
    // 获取路由id
    this.routeInfo.params.subscribe((params) => {
      this.checkId = params["id"];
      // 获取施工对信息
      this.checkService.getCheckManageById(this.checkId).then(data => {
        this.checkManage = data.data.checkManage;
        this.conStandardList = data.data.conStandardList;
        this.checkStandardList = data.data.checkStandardList;
      });
    });
  }

  edit() {
    if (this.checkId) {
      this.router.navigate(['saas/engineer/constructionconfig/check/add/', this.checkId]);
    }
  }

  // back() {
  //   if (this.checkId) {
  //     this.router.navigate(['saas/engineer/constructionconfig/check']);
  //   }
  // }

}
