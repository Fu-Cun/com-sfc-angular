import { Component, OnInit } from '@angular/core';
import { CheckService } from "../check.service";
import { StorageService } from "../../../../common/storage.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-list-check',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [CheckService]
})
export class ListCheckComponent implements OnInit {

  cols; // 表格头部
  colsSelected; // 表格选中的显示列
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  loading; // 表格数据是否加载中
  listData; // 数据
  searchObj: any = {}; // 搜索条件

  isWarningStatus = [];   //是否预警
  selectedWarningStatus;  //选择的项

  isEffects = [];  //启用、禁用
  selectedisEffect;  //选择的选项

  isCustomConfirm = [];  //是否需要客户确认
  selectedConfirm;

  isCustomDiscussStatus = [];  //是否需要客户评价
  selectedCustomDiscuss;  //选择的选项

  isCustomBackStatus = []; //是否客户回访
  selectedIsCustomBack;

  selectedRowData = []; // 选中行数据

  constructor(private router: Router, private checkService: CheckService, private storage: StorageService) {
    this.loading = true;
    // 表头
    this.cols = [
      { field: 'code', header: '编号', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'name', header: '名称', frozen: false, sortable: true },
      { field: 'isWarningText', header: '是否预警', sortable: true },
      { field: 'isCustomDiscussText', header: '客户评价', sortable: true },
      { field: 'isCustomBackText', header: '客户回访', sortable: true },
      { field: 'isCustomConfirmText', header: '客户确认', sortable: true },
      { field: 'isEffectText', header: '状态', sortable: true },
    ];
    this.colsSelected = this.cols;
    //初始化验收项列表数据
    this.checkService.list().then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
    let status = [{ 'name': '全部', 'code': '0' }, { 'name': '是', 'code': 'true' }, { 'name': '否', 'code': 'false' }]
    this.isWarningStatus = status
    this.isCustomConfirm = status
    this.isCustomDiscussStatus = status
    this.isCustomBackStatus = status
    this.isEffects = [{ 'name': '全部', 'code': '0' }, { 'name': '启用', 'code': 'true' }, { 'name': '禁用', 'code': 'false' }]
  }


  ngOnInit() {
  }


  /**
   * dataTable分页查询方法
   * @param event
   */
  onPage(event) {
    this.searchObj['sortF'] = event.sortField;
    this.searchObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.loading = true;
    this.checkService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  /**
   * 搜索方法
   */
  search() {
    if (this.selectedWarningStatus && this.selectedWarningStatus.code !== this.isWarningStatus[0].code) {
      this.searchObj['isWarning'] = this.selectedWarningStatus.code;
    } else {
      delete this.searchObj['isWarning'];
    }
    if (this.selectedisEffect && this.selectedisEffect.code !== this.isEffects[0].code) {
      this.searchObj['isEffect'] = this.selectedisEffect.code;
    } else {
      delete this.searchObj['isEffect'];
    }
    if (this.selectedConfirm && this.selectedConfirm.code !== this.isCustomConfirm[0].code) {
      this.searchObj['isCustomConfirm'] = this.selectedConfirm.code;
    } else {
      delete this.searchObj['isCustomConfirm'];
    }
    if (this.selectedCustomDiscuss && this.selectedCustomDiscuss.code !== this.isCustomDiscussStatus[0].code) {
      this.searchObj['isCustomDiscuss'] = this.selectedCustomDiscuss.code;
    } else {
      delete this.searchObj['isCustomDiscuss'];
    }
    if (this.selectedIsCustomBack && this.selectedIsCustomBack.code !== this.isCustomBackStatus[0].code) {
      this.searchObj['isCustomBack'] = this.selectedIsCustomBack.code;
    } else {
      delete this.searchObj['isCustomBack'];
    }
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.pageSize = 10;
    this.pageNo = 1;
    this.loading = true;
    this.checkService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.selectedRowData = [];
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.selectedRowData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  /**
   * Description: 启用、禁用
   */
  toggleEffect(param, isEffect) {
    if (this.selectedRowData) {
      let list = this.selectedRowData;
      let ids = new Array();
      for (let index of list) {
        ids.push(index.id);
      }
      if (ids.length === 0) {
        this.nodata();
        return;
      }
      this.enableOrDisable(ids, param, isEffect)
    }
  }

  // 启用、禁用修改提示
  enableOrDisable(ids, _param, isEffect) {
    this.storage.makesureService.emit({
      message: '是否确认' + _param + ids.length + '个验收？',
      header: '提示',
      rejectLabel: '取消',
      acceptLabel: '确定',
      acceptVisible: true,
      rejectVisible: true,
      accept: () => {
        this.loading = true;
        this.checkService.updateStatus({ ids: ids.toString(), isEffect: isEffect }).then(
          data => {
            if (data.result) {
              this.loading = false;
              this.search();
              this.storage.messageService.emit({ severity: 'success', detail: data.msg });
            } else {
              this.loading = false;
              this.storage.messageService.emit({ severity: 'error', detail: data.msg });
            }
            this.selectedRowData = [];
          }
        );
      },
      reject: () => {
        this.loading = false;
        this.selectedRowData = [];
      }
    });
  }

  /**
   * 验收详情
   * @param data 验收详情
   */
  // info(data) {
  //   console.log('info', data);
  //   this.router.navigate(['saas/engineer/constructionconfig/check/', data.id]);
  // }

  addCheck() {
    this.loading = true;
    this.router.navigate(['saas/engineer/constructionconfig/check/add']);
  }

  /**
   * Description: 未选择提示
   */
  nodata() {
    this.storage.makesureService.emit({
      message: '请选择要操作的数据！',
      header: '提示',
      rejectVisible: false,
      acceptVisible: false,
      accept: () => {
      }
    });
  }


}
