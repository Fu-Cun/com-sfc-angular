import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class TemplateService {
    private baseUrl = '';
    private serveName = 'com-dyrs-mtsp-constructionconfigservice';
    // 适用组织 刘会起接口
    private orgServeName = 'com-dyrs-mtsp-organizationservice/organizationservice/treeMenuList/api';
    // 报价数据 王志磊接口
    private  quoteServeName = 'com-dyrs-mtsp-quoteservice/quoteservice/getQuoteData/api';
    // 价款模板 刘超接口
    private payServeName = 'com-dyrs-mtsp-financeservice/financeservice/findPayTemNoDelApi/api';
    // 物料树 王飞接口
    private  treeServeName = 'saas-productservice/category/getTreeByExcept';
    constructor(private templateService: HttpInterceptorService) { }
    // 模板列表
    list(search: any = {}, pageSize: number = 20, pageNo: number = 1, ) {
        return this.templateService.post(
            this.serveName + '/templateConfig/templateList', { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }
    // 启用禁用
    updateStatus(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateConfig/updateStatus', param, this.baseUrl);
    }
    /**
     * 添加方法
     * @param param
     */
    save(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateConfig/saveTemplate',
            param, this.baseUrl);
    }
    /**
     * 修改模板
     * @param param
     */
    updateTemplate(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateConfig/updateTemplate',
            param, this.baseUrl);
    }
    /**
     * 名称校验
     * @param param
     */
    checkNameUnique(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateConfig/checkNameUnique',
            param, this.baseUrl);
    }
    /**
     * 名称校验
     * @param param
     */
    checkNameUniqueForUpdata(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateConfig/checkNameUniqueForUpdata',
            param, this.baseUrl);
    }
    /**
     * 获取详情
     * @param param
     */
    templateConfigDetail(id) {
        return this.templateService.post(this.serveName + '/templateConfig/templateConfigDetail', { id: id }, this.baseUrl);
    }

    // base里的所有施工阶段
    stageList(baseId) {
        return this.templateService.post(this.serveName + '/templateStage/stageList', { baseId: baseId }, this.baseUrl);
    }
    // 保存base里的所有施工阶段
    saveStage(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateStage/saveStage',
            param, this.baseUrl);
    }
    // 该base里的所有阶段
    findTempChechList(baseId) {
        return this.templateService.post(this.serveName + '/templateCheck/findTempChechList', { baseId: baseId }, this.baseUrl);
    }
    // 该base里的验收项
    findCheckList(search: any = {}, pageSize: number = 20, pageNo: number = 1, ) {
        return this.templateService.post(this.serveName + '/templateCheck/findCheckList',
            { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }
    // 保存base里的所有验收项
    saveCheck(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateCheck/saveCheck',
            param, this.baseUrl);
    }
    // 施工阶段设置列表
    stageAndNodeList(baseId) {
        return this.templateService.post(this.serveName + '/templateStage/stageAndNodeList', { baseId: baseId }, this.baseUrl);
    }
    // 施工阶段设置节点弹框
    findNodeList(search: any = {}, pageSize: number = 20, pageNo: number = 1, ) {
        return this.templateService.post(this.serveName + '/templateStage/findNodeList',
            { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }
    // 保存施工阶段的节点
    saveStageNode(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateStage/saveStageNode',
            param, this.baseUrl);
    }
    // 物料配置列表
    findProductList(baseId) {
        return this.templateService.post(this.serveName + '/templateProduct/findProductList', { baseId: baseId }, this.baseUrl);
    }
    // 物料批次
    selectProductNum(baseId) {
        return this.templateService.post(this.serveName + '/templateProduct/selectProductNum', { baseId: baseId }, this.baseUrl);
    }
    // 修改物料配送数据
    updateProductList(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateProduct/updateProductList',
            param, this.baseUrl);
    }
    // 施工排期
    findScheduleList(baseId) {
        return this.templateService.post(this.serveName + '/templateCheck/findScheduleList', { baseId: baseId }, this.baseUrl);
    }
    // 保存施工排期
    saveSchedule(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateCheck/saveSchedule',
            param, this.baseUrl);
    }
    // 物料树
    getTreeByExcept(ids) {
        return this.templateService.post(this.treeServeName, { code: 'SM', nonIds: ids }, this.baseUrl);
    }
    // 交款数据
    findTemplateList() {
        return this.templateService.post(this.serveName + '/templateConfig/getTemplateStatus', {}, this.baseUrl);
    }
    // 必交款
    findPayList() {
        return this.templateService.post(this.serveName + '/templateConfig/getPayStatus', {}, this.baseUrl);
    }
    // 报价数据
    findPriceTemplate(param: any = {}) {
        return this.templateService.post(this.serveName + '/templateConfig/getTemplatePriceStatus', {param}, this.baseUrl);
    }
    // 适用组织
    loadOrganization(codes) {
        if (codes.length > 0) {
            return this.templateService.post(this.orgServeName , { exceptCodes: codes });
        } else {
            return this.templateService.post(this.orgServeName , {});
        }

    }
}
