import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TimeConfigClass } from '../../../../common/common-config';
import { TemplateService } from '../template.service';
import { StorageService } from '../../../../common/storage.service';

@Component({
  selector: 'app-template',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [TemplateService],
})
export class ListTemplateComponent implements OnInit {

  cols; // 表格头部
  colsSelected; // 表格选中的显示列
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  loading; // 表格数据是否加载中
  listData;
  type; // 类型
  selectedType; // 选中后的类型
  isEffect; // 状态
  selectedIsEffect; // 选中后的状态
  selectedRowData = []; // 选中行数据
  searchObj: any = {}; // 查询条件
  payTemplateName; // 交款模板名称
  constructor(private router: Router, private templateService: TemplateService, private storage: StorageService) {
    this.loading = true;
    this.cols = [
      { field: 'name', header: '名称', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'constructionLimitDays', header: '标准施工周期/天', frozen: false, sortable: true },
      { field: 'constructionLimit', header: '施工周期范围/天', sortable: true, tem: true },
      { field: 'useAbleAreaFrom', header: '适用面积/m', frozen: false, sortable: true, tem: true },
      { field: 'priceTemplateName', header: '报价模板', frozen: false, sortable: true },
      { field: 'payTemplateName', header: '交款模板', frozen: false, sortable: true },
      { field: 'isEffect', header: '状态', sortable: true, tem: true },
    ];
    this.colsSelected = this.cols;
    this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
    // 模板表格初始化
    this.templateService.list().then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  ngOnInit() {
  }
  /**
    * 搜索方法
    */
  search() {
    // if (this.selectedType && this.selectedType.code) {
    //   this.searchObj['type'] = this.selectedType.code;
    // } else {
    //   delete this.searchObj['type'];
    // }
    if (this.selectedIsEffect && this.selectedIsEffect.code !== this.isEffect[0].code) {
      this.searchObj['isEffect'] = this.selectedIsEffect.code;
    } else {
      delete this.searchObj['isEffect'];
    }
    this.searchObj['searchValue'] = this.searchObj['searchValue'];
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    }
    this.templateService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  /**
 * dataTable分页查询方法
 * @param event
 */
  onPage(event) {
    this.searchObj['sortF'] = event.sortField;
    this.searchObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.loading = true;
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.templateService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.selectedRowData = [];
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.selectedRowData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );

  }

  /**
   * dataTable单行选中方法
   * @param event
   */
  onRowSelect(event) {
    if (event.type === 'row') {
      this.router.navigate(['saas/engineer/constructionteam/teaminfo/add']);
    }
  }

  /**
 * 模板详情
 * @param data 模板数据
 */
  info(data) {
    // this.router.navigate(['/saas/engineer/constructionconfig/technology/', data.id]);
  }
  // 添加模板
  addTemplate() {
    this.router.navigate(['/saas/engineer/constructionconfig/template/add']);
  }
  // 启用禁用修改数据
  confirmUpdate(ids) {
    this.loading = true;
    this.templateService.updateStatus({ ids: ids.toString(), effect: this.isEffect }).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.storage.messageService.emit({ severity: 'success', detail: data.msg });
          this.search();
        } else {
          this.loading = false;
          this.retdata(data.msg);
          // this.storage.messageService.emit({ severity: 'error', detail: '操作失败' });
        }
      }
    );
    this.selectedRowData = [];
    this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
  }
  // 判断启用禁用
  updateStatus(param) {
    let list = this.selectedRowData;
    let ids = new Array();
    for (let index of list) {
      ids.push(index.id);
    }
    if (ids.length === 0) {
      this.nodata();
      return;
    }
    if (param == 'start') {
      if (ids.length === 1) {
        this.isEffect = true;
        this.enable(ids);
      } else {
        this.manydata();
      }
    } else {
      this.disable(ids);
      this.isEffect = false;
    }
  }
  // 启用修改提示
  enable(ids) {
    this.storage.makesureService.emit({
      message: '是否确认启用模板？',
      header: '提示',
      rejectVisible: true,
      acceptVisible: true,
      accept: () => {
        this.confirmUpdate(ids);
      },
      reject: () => {
        this.selectedRowData = [];
        this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
      }
    });
  }
  // 禁用修改提示
  disable(ids) {
    this.storage.makesureService.emit({
      message: '是否确认禁用' + ids.length + '条模板？',
      header: '提示',
      rejectVisible: true,
      acceptVisible: true,
      accept: () => {
        this.confirmUpdate(ids);
      },
      reject: () => {
        this.selectedRowData = [];
        this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
      }
    });
  }
  // 未选择
  nodata() {
    this.storage.makesureService.emit({
      message: '请选择要操作的数据！',
      header: '提示',
      rejectVisible: false,
      acceptVisible: false,
      accept: () => {
      }
    });
  }
  // 多选
  manydata() {
    this.storage.makesureService.emit({
      message: '只能勾选一条数据！',
      header: '提示',
      rejectVisible: false,
      accept: () => {
      }
    });
  }
  // 返回值
  retdata(msg) {
    this.storage.makesureService.emit({
      message: msg,
      header: '提示',
      rejectVisible: false,
      accept: () => {
      }
    });
  }
  crateTemplate(data) {
    this.router.navigate(['/saas/engineer/constructionconfig/template/edit', data.id]);
  }
}
