import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {SendorderruleService} from "../sendorderrule.service";
import {StorageService} from "../../../../common/storage.service";
@Component({
  selector: 'app-info-sendorderrule',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [SendorderruleService],
})
export class InfoSendorderruleComponent implements OnInit {
  sendOrderRule: any = {}; // 规则基本数据
  tabViewIndex:number=0;// tab标签页
  dataList; // 数据
  punishDataList:any[]=[]; //处罚 数据
  type;
  cols; // 表格头部
  punishCols; // 处罚表格头部
  loading; // 表格数据是否加载中
  constructor(private router: Router,private routeInfo: ActivatedRoute,private storage:StorageService, private service:SendorderruleService) { }

  ngOnInit() {

    this.cols = [
      { field: 'sort', header: '排序', width: '100px', frozen: false, sortable: true},
      { field: 'content', header: '条件',  width: '420px', frozen: false, sortable: true}
    ];
    this.punishCols = [
      { field: 'check', header: '', width: '50px', frozen: false, temread:true},
      { field: 'type', header: '处罚类型', width: '240px', frozen: false},
      { field: 'num', header: '达到次数X/个',  width: '160px', frozen: false},
      { field: 'score', header: '扣分Y/分',  width: '160px', frozen: false}
    ];

    this.routeInfo.params.subscribe((params)=>{
      this.type = params.type;
      if(!this.type) this.type = "team";
      if(this.type == 'team') {
        this.tabViewIndex = 0;
        this.loadDetail(this.type);
      } else if(this.type == 'supervisor') {
        this.tabViewIndex = 1;
        this.loadDetail(this.type);
      }
    });
  }

  /**
   * 编辑
   */
  edit(){
    this.router.navigate(['saas/engineer/constructionconfig/sendorderrule/add', this.type]);
  }

  /**
   * 页面tab切换事件
   * @param event
   */
  onTabChange(event) {
    if(event.index == 0) {
      this.type = 'team';
    } else if (event.index == 1) {
      this.type = 'supervisor';
    }
    this.loadDetail(this.type);
  }

  /**
   * 加载详情
   * @param type
   */
  loadDetail(type) {
    this.punishDataList = [];
    this.dataList = [];

    this.service.findDetail(type).then(data=>{
      if(data.result) {
        this.sendOrderRule = data.data;
        //  三级指标列表
        this.dataList = [];
        this.dataList.push({content:this.sendOrderRule['thirdLevelPassRateTopToBottomContent'],sort:this.sendOrderRule['thirdLevelPassRateTopToBottomSort']});
        this.dataList.push({content:this.sendOrderRule['thirdLevelReceiveBottomToTopContent'],sort:this.sendOrderRule['thirdLevelReceiveBottomToTopSort']});
        this.dataList.push({content:this.sendOrderRule['thirdLevelCreateTimeBottomToTopContent'],sort:this.sendOrderRule['thirdLevelCreateTimeBottomToTopSort']});
        this.dataList.sort((o1, o2)=>{
          return o1.sort - o2.sort;
        });
        // 处罚列表
        this.punishDataList = [];
        this.punishDataList.push({check:this.sendOrderRule['secondLevelMinusRateGeneralPunish'],type:"一般处罚",num:this.sendOrderRule['secondLevelMinusRateGeneralPunishNum'],score:this.sendOrderRule['secondLevelMinusRateGeneralPunishScore']});
        this.punishDataList.push({check:this.sendOrderRule['secondLevelMinusRateServicePunish'],type:"服务类处罚",num:this.sendOrderRule['secondLevelMinusRateServicePunishNum'],score:this.sendOrderRule['secondLevelMinusRateServicePunishScore']});
        this.punishDataList.push({check:this.sendOrderRule['secondLevelMinusRateAccidentPunish'],type:"安全事故处罚",num:this.sendOrderRule['secondLevelMinusRateAccidentPunishNum'],score:this.sendOrderRule['secondLevelMinusRateAccidentPunishScore']});
      } else {
        this.sendOrderRule = {};
        this.storage.makesureService.emit({
          message: '数据加载失败,请重新加载!',
          header: '提示',
          icon: '',
          reject: () => {
            this.loadDetail(this.type);
          },
          acceptVisible:false,
          rejectLabel:'重新加载',
          rejectVisible:true});
      }
    });
  }

  // 错误提示
  error(msg){
    this.storage.messageService.emit({severity:'error', detail:msg})
  }

}
