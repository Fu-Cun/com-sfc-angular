import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class SendorderruleService {
    private baseUrl = '';
    private severName = 'com-dyrs-mtsp-constructionconfigservice';
    constructor(private service: HttpInterceptorService) { }

  // 查询详情
  findDetail(param: any = {}) {
    return this.service.post(this.severName + '/sendorderRule/detail/api', {type:param}, this.baseUrl);
  }

  save(param: any = {}) {
    return this.service.post(this.severName + '/sendorderRule/saveOrUpdate/api', param, this.baseUrl);
  }
}
