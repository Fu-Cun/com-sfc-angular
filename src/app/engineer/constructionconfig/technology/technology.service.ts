import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class TechnologyService {
    private baseUrl = '';
    private serveName = 'com-dyrs-mtsp-constructionconfigservice';
    constructor(private technologyService: HttpInterceptorService) { }

    // 工艺列表
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1, ) {
        return this.technologyService.post(
            this.serveName + '/technology/technologyList', { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }
    // 工艺详情
    getTechnologyDetail(id) {
        return this.technologyService.post( this.serveName + '/technology/technologyDetail', { id: id }, this.baseUrl);
    }
    // 工艺类型
    getTechnologyStatus() {
        return this.technologyService.post( this.serveName + '/technology/getTechnologyStatus', {}, this.baseUrl);
    }
    updateStatus(param: any = {}) {
        return this.technologyService.post( this.serveName + '/technology/updateStatus', param, this.baseUrl);
    }
}
