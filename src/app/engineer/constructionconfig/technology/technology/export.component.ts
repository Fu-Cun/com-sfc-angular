import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TimeConfigClass } from '../../../../common/common-config';
import { TechnologyService } from '../technology.service';
import { StorageService } from '../../../../common/storage.service';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  providers: [TechnologyService],
})
export class ExportComponent implements OnInit {

  constructor(private storage: StorageService, private technologyService: TechnologyService) {
  }

  ngOnInit() {
   
  }

  showTrue: boolean = false;
  showFalse: boolean = false;
  subList: any;
  quotaList: any;
  errorList: any;
  savaDate: any = {};
  isRight: boolean = false;  //数据是否正确
  data: any;
  url: string;
  display: boolean;
  importFlag:any;
  files: any = [];
  files2: any = [];

  /* 共用方法 */
  onBeforeUpload(event) {
    event.formData.append('fileFid', '10086')
    event.formData.append('fileFrom', 'dynamic')
  }

  /**
   * 上传之后返回数据
   * @param event
   */
  onUpload(event) {
    let data = eval('(' + event.xhr.responseText + ')');
    console.log(data)
    if (data.result) {
      this.isRight = true
      this.quotaList = data.data.quotaList;
      this.subList = data.data.subList;
      this.savaDate = data.data;
      this.showTrue = true;
      this.showFalse = false;
    } else {
      this.errorList = data.data
      this.errorList[0].bookmark? this.importFlag={'text-align':'left'}: this.importFlag={'text-align':'center'};
      this.showFalse = true;
      this.showTrue = false;
      this.isRight = false;
    }
  }


  //保存数据
  save() {
    // this.isRight = false
    // this.quotasService.saveExcleData(this.savaDate).then(data => {
    //   if (data.result) {
    //     this.storage.messageService.emit({severity: 'success', detail: data.msg})
    //   } else {
    //     this.storage.messageService.emit({severity: 'error', detail: data.msg})
    //   }
    // })
  }

  onSelect(event) {
    this.files = event.files;
    for (let val of event.files) {
      this.files2 = [...this.files2, val]
    }
  }

  /* 文件上传 */
  cancle(index) {
    this.files2 = this.files2.filter((_, i) => i != index)
  }

}
