import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TimeConfigClass } from '../../../../common/common-config';
import { Check } from '../../../../common/check';
import { TechnologyService } from '../technology.service';

@Component({
  selector: 'app-info-technology',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [TechnologyService]
})
export class InfoTechnologyComponent implements OnInit {
  id;
  colsCheck;
  technologyData: any = {}; // 工艺信息
  dataList; // 施工步骤
  uploadedFiles: any[] = [];
  isLoadDic: Boolean = false;
  constructor(private routeInfo: ActivatedRoute, private technologyService: TechnologyService, private router: Router) {
    // 施工标准
    this.colsCheck = [
      { field: 'sortNum', header: '序号', width: '50px', frozen: false, sortable: true },
      { field: 'content', header: '标准', width: '500px', frozen: false, sortable: true },
    ];
  }

  ngOnInit() {
    // 获取路由id
    this.routeInfo.params.subscribe((params) => {
      this.id = params.id;
      // 获取施工对信息
      this.technologyService.getTechnologyDetail(this.id).then(data => {
        this.technologyData = data.data.technologyData;
        this.dataList = data.data.dataList;
      });
    });
  }
}
