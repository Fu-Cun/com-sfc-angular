import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TimeConfigClass } from '../../../../common/common-config';
import { TechnologyService } from '../technology.service';
import { StorageService } from '../../../../common/storage.service';

@Component({
  selector: 'app-technology',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [TechnologyService],
})
export class TechnologyComponent implements OnInit {

  timeConfig: any = new TimeConfigClass(); // 时间空间初始化
  cols; // 表格头部
  colsSelected; // 表格选中的显示列
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  loading; // 表格数据是否加载中
  listData;
  type; // 类型
  selectedType; // 选中后的类型
  isEffect; // 状态
  selectedIsEffect; // 选中后的状态
  selectedRowData = []; // 选中行数据
  searchObj: any = {}; // 查询条件
  constructor(private router: Router, private technologyService: TechnologyService, private storage: StorageService) {
    this.loading = true;
    this.cols = [
      { field: 'code', header: '编号', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'name', header: '名称', frozen: false, sortable: true },
      { field: 'technologyText', header: '类型', width: '150px', sortable: true },
      { field: 'isEffect', header: '状态', sortable: true, tem: true },
    ];
    this.colsSelected = this.cols;
    this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
    // 工艺状态
    this.technologyService.getTechnologyStatus().then(
      data => {
        if (data.result) {
          this.type = [{ 'code': '', name: '全部' }, ...data.data.technologyStatus];
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '字典服务异常！' });
        }
      }
    );
    // 工艺表格初始化
    this.technologyService.list().then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  ngOnInit() {
  }
  getImport(){
    this.router.navigate(['saas/engineer/constructionconfig/technology/export']);
  }
  /**
    * 搜索方法
    */
  search() {
    if (this.selectedType && this.selectedType.code) {
      this.searchObj['type'] = this.selectedType.code;
    } else {
      delete this.searchObj['type'];
    }
    if (this.selectedIsEffect && this.selectedIsEffect.code !== this.isEffect[0].code) {
      this.searchObj['isEffect'] = this.selectedIsEffect.code;
    } else {
      delete this.searchObj['isEffect'];
    }
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.technologyService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  /**
 * dataTable分页查询方法
 * @param event
 */
  onPage(event) {
    this.searchObj['sortF'] = event.sortField;
    this.searchObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.loading = true;
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.technologyService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.selectedRowData = [];
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.selectedRowData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败' });
        }
      }
    );

  }

  /**
   * dataTable单行选中方法
   * @param event
   */
  onRowSelect(event) {
    if (event.type === 'row') {
      this.router.navigate(['saas/engineer/constructionteam/teaminfo/add']);
    }
  }

  /**
 * 工艺详情
 * @param data 工艺数据
 */
  info(data) {
    this.router.navigate(['/saas/engineer/constructionconfig/technology/', data.id]);
  }
  // 启用禁用修改数据
  confirmUpdate(codes) {
    this.loading = true;
    this.technologyService.updateStatus({ codes: codes.toString(), effect: this.isEffect }).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.storage.messageService.emit({ severity: 'success', detail: data.msg });
          this.search();
        } else {
          this.loading = false;
          this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          this.errordata(data.msg);
        }
      }
    );
    this.selectedRowData = [];
    this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
  }
  // 判断启用禁用
  updateStatus(param) {
    let list = this.selectedRowData;
    let codes = new Array();
    for (let index of list) {
      codes.push(index.code);
    }
    if (codes.length === 0) {
      this.nodata();
      return;
    }
    if (param == 'start') {
      this.isEffect = true;
      this.enable(codes);
    } else {
      this.disable(codes);
      this.isEffect = false;
    }
  }
  // 启用提示
  enable(codes) {
    this.storage.makesureService.emit({
      message: '是否确认启用' + codes.length + '条工艺？',
      header: '提示',
      rejectVisible: true,
      acceptVisible: true,
      accept: () => {
        this.confirmUpdate(codes);
      },
      reject: () => {
        this.selectedRowData = [];
        this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
      }
    });
  }
  // 禁用提示
  disable(codes) {
    this.storage.makesureService.emit({
      message: '是否确认禁用' + codes.length + '条工艺？',
      header: '提示',
      rejectVisible: true,
      acceptVisible: true,
      accept: () => {
        this.confirmUpdate(codes);
      },
      reject: () => {
        this.selectedRowData = [];
        this.isEffect = [{ name: '全部' }, { name: '启用', code: true }, { name: '禁用', code: false }];
      }
    });
  }
  // 未选择
  nodata() {
    this.storage.makesureService.emit({
      message: '请选择要操作的数据！',
      header: '提示',
      rejectVisible: false,
      acceptVisible: false,
      accept: () => {
      }
    });
  }

  // 修改失败返回提示
  errordata(msg) {
    this.storage.makesureService.emit({
      message: '工艺' + msg + '修改失败',
      header: '提示',
      icon: 'fa fa-question-circle',
      rejectVisible: false,
      accept: () => {
      }
    });
  }
}
