import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConstructionconfigRoutingModule } from './main-routing.module';

// primeNG
import { CheckboxModule } from '../../components/checkbox/checkbox';
import { PanelModule } from '../../components/panel/panel';
import { InputTextModule } from '../../components/inputtext/inputtext';
import { DataTableModule } from '../../components/datatable/datatable';
import { MultiSelectModule } from '../../components/multiselect/multiselect';
import { InputMaskModule } from '../../components/inputmask/inputmask';
import { DropdownModule } from '../../components/dropdown/dropdown';
import { InputSwitchModule } from '../../components/inputswitch/inputswitch';
import { ButtonModule } from '../../components/button/button';
import { RatingModule } from '../../components/rating/rating';
import { CalendarModule } from '../../components/calendar/calendar';
import { RadioButtonModule } from '../../components/radiobutton/radiobutton';
import { InputTextareaModule } from '../../components/inputtextarea/inputtextarea';
import { FileUploadModule } from '../../components/fileupload/fileupload';
import { DialogModule } from '../../components/dialog/dialog';
import { TabViewModule } from '../../components/tabview/tabview';
import { AdvanceModule } from '../../components/advancesearch/advancesearch';
import { FieldsetModule } from '../../components/fieldset/fieldset';
import { SpinnerModule } from '../../components/spinner/spinner';
import { MessageModule } from '../../components/message/message';
import { PickListModule } from '../../components/picklist/picklist';
import { LabelModule } from '../../components/label/label';
import { TreeModule } from '../../components/tree/tree';
import { TooltipModule } from '../../components/tooltip/tooltip';

// technology(工艺)
import { TechnologyComponent } from './technology/technology/list.component';
import { ExportComponent } from './technology/technology/export.component';
import { InfoTechnologyComponent } from './technology/technology/info.component';

// template
import { TemplateEditComponent } from './template/template/edit.component';

import { ListTemplateComponent } from './template/template/list.component';
import { AddTemplateComponent } from './template/template/add.component';
// 验收管理
import { AddCheckComponent } from './check/check/add.component';
import { InfoCheckComponent } from './check/check/info.component';
import { ListCheckComponent } from './check/check/list.component';
// 节点管理
import { AddNodeComponent } from './node/node/add.component';
import { InfoNodeComponent } from './node/node/info.component';
import { ListNodeComponent } from './node/node/list.component';
// 派单规则设置
import { InfoSendorderruleComponent } from './sendorderrule/sendorderrule/info.component';
import { AddSendorderruleComponent } from './sendorderrule/sendorderrule/add.component';
// 巡检规则设置
import { InfoInspectionComponent } from './inspectionmanage/inspection/info.component';

/**
 * 施工配置模块
 */
@NgModule({
  imports: [
    ConstructionconfigRoutingModule,
    TooltipModule,
    CommonModule,
    FormsModule,
    PanelModule,
    InputTextModule,
    DataTableModule,
    MultiSelectModule,
    ButtonModule,
    InputMaskModule,
    DropdownModule,
    InputSwitchModule,
    CalendarModule,
    RatingModule,
    CheckboxModule,
    RadioButtonModule,
    InputTextareaModule,
    FileUploadModule,
    DialogModule,
    ReactiveFormsModule,
    TabViewModule,
    AdvanceModule,
    FieldsetModule,
    SpinnerModule,
    MessageModule,
    PickListModule,
    LabelModule,
    TreeModule
  ],
  declarations: [
    TechnologyComponent,
    ExportComponent,
    InfoTechnologyComponent,
    ListTemplateComponent,
    TemplateEditComponent,
    AddTemplateComponent,
    AddCheckComponent,
    InfoCheckComponent,
    ListCheckComponent,
    AddNodeComponent,
    InfoNodeComponent,
    ListNodeComponent,
    InfoSendorderruleComponent,
    AddSendorderruleComponent,
    InfoInspectionComponent
  ],
  exports: [],
  providers: []
})
export class ConstructionconfigModule {
}
