import { Component, OnInit } from "@angular/core";
import { NodeService } from "../node.service";
import { Router } from "@angular/router";
import { StorageService } from "../../../../common/storage.service";

@Component({
  selector: 'app-list-node',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [NodeService]
})
export class ListNodeComponent implements OnInit {

  cols; // 表格头部
  colsSelected; // 表格选中的显示列
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  loading; // 表格数据是否加载中
  listData; // 数据
  searchObj: any = {}; // 搜索条件

  isWarningStatus = [];   //是否预警
  selectedWarningStatus;  //选择的项

  isEffects = [];  //启用、禁用
  selectedisEffect;  //选择的选项

  isSupervisorConfirm = [];  //是否需要监理确认
  selectedConfirm;

  selectedRowData = []; // 选中行数据

  constructor(private router: Router, private nodeService: NodeService, private storage: StorageService) {
    this.loading = true;
    // 表头
    this.cols = [
      { field: 'code', header: '编号', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'name', header: '名称', frozen: false, sortable: true },
      { field: 'shortName', header: '简称', frozen: false, sortable: true },
      { field: 'limitCodes', header: '定额号', width: '150px', sortable: true },
      { field: 'isWarningText', header: '是否预警', sortable: true },
      { field: 'isSupervisorConfirmText', header: '是否监理确认', sortable: true },
      { field: 'isEffectText', header: '状态', sortable: true },
      { field: 'sortNum', header: '排序', width: '80px', sortable: true }
    ];
    this.colsSelected = this.cols;
    //初始化节点列表数据
    this.nodeService.list().then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
    let status = [{ 'name': '全部', 'code': '0' }, { 'name': '是', 'code': 'true' }, { 'name': '否', 'code': 'false' }]
    this.isEffects = [{ 'name': '全部', 'code': '0' }, { 'name': '启用', 'code': 'true' }, { 'name': '禁用', 'code': 'false' }]
    this.isWarningStatus = status
    this.isSupervisorConfirm = status
  }

  ngOnInit() {
  }

  /**
   * dataTable分页查询方法
   * @param event
   */
  onPage(event) {
    this.searchObj['sortF'] = event.sortField;
    this.searchObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.loading = true;
    this.nodeService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.selectedRowData = [];
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.selectedRowData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  /**
   * 搜索方法
   */
  search() {
    if (this.selectedWarningStatus && this.selectedWarningStatus.code !== this.isWarningStatus[0].code) {
      this.searchObj['isWarning'] = this.selectedWarningStatus.code;
    } else {
      delete this.searchObj['isWarning'];
    }
    if (this.selectedisEffect && this.selectedisEffect.code !== this.isEffects[0].code) {
      this.searchObj['isEffect'] = this.selectedisEffect.code;
    } else {
      delete this.searchObj['isEffect'];
    }
    if (this.selectedConfirm && this.selectedConfirm.code !== this.isSupervisorConfirm[0].code) {
      this.searchObj['isSupervisorConfirm'] = this.selectedConfirm.code;
    } else {
      delete this.searchObj['isSupervisorConfirm'];
    }
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号/定额号 50个字符内' });
        return;
      }
    }
    this.pageSize = 10;
    this.pageNo = 1;
    this.loading = true;
    this.nodeService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  /**
   * Description: 启用、禁用
   */
  toggleEffect(param, isEffect) {
    if (this.selectedRowData) {
      let list = this.selectedRowData;
      let ids = new Array();
      for (let index of list) {
        ids.push(index.id);
      }
      if (ids.length === 0) {
        this.nodata();
        return;
      }
      this.enableOrDisable(ids, param, isEffect)
    }
  }

  // 启用、禁用修改提示
  enableOrDisable(ids, _param, isEffect) {
    this.storage.makesureService.emit({
      message: '是否确认' + _param + ids.length + '个节点？',
      header: '提示',
      rejectLabel: '取消',
      acceptLabel: '确定',
      acceptVisible: true,
      rejectVisible: true,
      accept: () => {
        this.nodeService.updateStatus({ ids: ids.toString(), isEffect: isEffect }).then(
          data => {
            if (data.result) {
              this.loading = false;
              this.search();
              this.storage.messageService.emit({ severity: 'success', detail: data.msg });
            } else {
              this.loading = false;
              this.storage.messageService.emit({ severity: 'error', detail: data.msg });
            }
            this.selectedRowData = [];
          }
        );
      },
      reject: () => {
        this.loading = false;
        this.selectedRowData = [];
      }
    });
  }


  /**
   * 节点详情
   * @param data 节点详情
   */
  // info(data) {
  //   console.log('info', data);
  //   this.router.navigate(['saas/engineer/constructionconfig/node/', data.id]);
  // }


  /**
   * Description: 新增节点
   */
  addNode() {
    this.router.navigate(['saas/engineer/constructionconfig/node/add']);
  }

  /**
   * Description: 未选择提示
   */
  nodata() {
    this.storage.makesureService.emit({
      message: '请选择要操作的数据！',
      header: '提示',
      rejectVisible: false,
      acceptVisible: false,
      accept: () => {
      }
    });
  }


}
