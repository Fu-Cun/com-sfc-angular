import { Component, OnInit } from '@angular/core';
import { NodeService } from "../node.service";
import { ActivatedRoute, Router } from "@angular/router";
import { StorageService } from "../../../../common/storage.service";

@Component({
  selector: 'app-info-node',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [NodeService],
})
export class InfoNodeComponent implements OnInit {

  nodeId;   //节点编号
  nodeManage: any = {};  //节点
  technologyList = [];   //节点工艺
  standardList = [];   //节点验收标准
  colsConStandard = []; //施工标准列
  colsCheckStandard = [];  //验收标准列

  quotaCol = [];   //关联定额
  quotaList = [];   //定额列表

  constructor(private router: Router, private nodeService: NodeService, private routeInfo: ActivatedRoute, private storage: StorageService) {
    //节点-工艺表头
    this.colsConStandard = [
      { field: 'sortNum', header: '序号', width: '55px', frozen: false, sortable: true },
      { field: 'nodeCode', header: '工艺编号', width: '135px', frozen: false, sortable: true },
      { field: 'name', header: '名称', width: '165px', frozen: false, sortable: true },
      { field: 'technologyDetails', header: '施工标准', width: '400px', frozen: false, sortable: true, tem: true }
    ];
    //节点-验收标准表头
    this.colsCheckStandard = [
      { field: 'sortNum', header: '序号', width: '100px', frozen: false, sortable: true },
      { field: 'checkStandard', width: '335px', header: '验收标准', frozen: false, sortable: true },
      { field: 'checkItem', width: '175px', header: '检查项目', sortable: true },
      { field: 'phoneRequire', width: '100px', header: '照片/张', sortable: true },
      { field: 'tackPhoneRequire', width: '200px', header: '拍摄要求', sortable: true },
      { field: 'toolConfig', width: '120px', header: '基本配备工具', sortable: true },
      { field: 'isReferenceCheckItem', width: '170px', header: '是否关联验收项', sortable: true, tem: true },
      { field: 'isRequestPassItem', width: '170px', header: '是否为必过项', sortable: true, tem: true }
    ];
    //节点-关联定额
    this.quotaCol = [
      { field: 'quotaNum', header: '定额号', width: '150px', frozen: false, sortable: true, editable: false, tem: false, },
      { field: 'quotaName', width: '350px', header: '名称', frozen: false, sortable: true, editable: false, tem: false },
      { field: 'category', width: '100px', header: '类别', sortable: true, editable: false, tem: false, dic: 'quoCategory' },
      { field: 'dateCreated', width: '100px', header: '启用日期', sortable: true, editable: false, tem: false },
      { field: 'expirationTime', width: '100px', header: '失效日期', sortable: true, editable: false, tem: false },
    ];
  }

  ngOnInit() {
    // 获取路由id
    this.routeInfo.params.subscribe((params) => {
      this.nodeId = params["id"];
      // 获取节点信息
      this.nodeService.getNodeById(this.nodeId).then(data => {
        this.nodeManage = data.data.nodeManage;
        this.technologyList = data.data.technologyList;
        this.standardList = data.data.standardList;
        this.quotaList = data.data.quota.data == undefined ? [] : data.data.quota.data;
      });
    });
  }
  //
  // back(){
  //   this.router.navigate(['saas/engineer/constructionconfig/node']);
  // }

  edit() {
    if (this.nodeId) {
      this.router.navigate(['saas/engineer/constructionconfig/node/add/', this.nodeId]);
    }
  }

}
