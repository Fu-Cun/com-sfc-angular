import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NodeService } from "../node.service";
import { StorageService } from "../../../../common/storage.service";

@Component({
  selector: 'app-add-node',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [NodeService],
})
export class AddNodeComponent implements OnInit {

  /**================节点======================*/
  title;  //页签标题
  loading; // 表格数据是否加载中
  nodeId;  //节点id
  nodeManage: any = {};  //节点
  isWarningStatus = [];   //是否预警
  selectedWarningStatus;  //选择的项
  isEffects = [];  //启用、禁用
  selectedisEffect;  //选择的选项
  isSupervisorConfirm = [];  //是否需要监理确认
  selectedConfirm;  //是否监理确认选中项


  /**================节点-工艺-施工标准=============*/
  technologyList = [];   //节点工艺-施工标准
  colsConStandard; //施工标准列
  W001 = false; //工艺弹框显隐控制
  teccols;  //选择工艺弹出框列表表头
  technologyStatus = [];  //工艺类型
  selectedRowData = [];  //工艺弹出框选中行数据
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  listData;  //工艺弹出框数据
  selectedType; // 选中后的类型
  searchObj: any = {}; // 弹出框工艺查询条件
  technologyListForDel = [];  //删除的工艺


  /**================节点-验收标准=================*/
  standardList = [];   //节点验收标准
  colsCheckStandard;  //验收标准列
  isReferenceCheckItemArr = [];  //是否关联验收项  新增字段
  isRequestPassItemArr = [];  //是否为必过项  新增字段
  standardListForDel = [];  //删除的验收

  /**================关联定额====================*/
  W002 = false;  //定额弹框显隐控制
  quotaList = [];   //定额列表
  quotaCol;   //定额列表表头
  quotaListForDialog;   //弹框定额列表
  quotaColForDialog;   //弹框定额列表表头
  selectedQuotaRowData = []; //弹出框定额选中的行
  quotaTotalRecords; // 数据条数
  quotaCode = [];  //定额编号
  quotaIds = [];  //关联定额id
  pageSizeTwo; // 定额分页大小
  pageNoTwo; // 定额当前页
  searchQuotaObj: any = {};  //定额弹框查询条件
  selectQuotaType;   //定额弹框查询选中的类型
  quotaType = [];  //定额类型

  /**================验证========================*/
  nodeCodeError;   //节点编号验证提示
  nodeNameError;   //节点名称验证提示
  isWarningError;  //是否预警验证提示
  warningDaysError;  //预警天数验证提示
  isSupervisorConfirmError;  //是否监理确认验证提示
  confirmDaysError;  //监理确认自动确认天数验证提示
  isEffectError;   //状态不能为空
  checkSortNumError;  //需要唯一性验证提示
  technologyError; // 工艺错误提示
  openDialogForQuotaError; // 关联定额错误提示
  standardError; // 验收标准错误提示
  shortNameError;  //节点简称错误提示

  quotaNum: any = [];  //定额编号


  constructor(private router: Router, private nodeService: NodeService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
    //节点-工艺-施工标准表头
    this.colsConStandard = [
      { field: 'sortNum', header: ' 序号 ', width: '60px', frozen: false, sortable: true, tem: true },
      { field: 'nodeCode', header: '工艺编号', width: '135px', frozen: false, sortable: true, editable: false, tem: false },
      { field: 'name', header: '名称', width: '165px', frozen: false, sortable: true, editable: false, tem: false },
      { field: 'technologyDetails', header: '施工标准', width: '400px', frozen: false, sortable: true, tem: true, editable: false },
      { field: 'operation', header: '操作', width: '180px', frozen: false, sortable: true, tem: true }
    ];
    //节点-验收标准
    this.colsCheckStandard = [
      { field: 'sortNum', header: ' 序号 ', width: '60px', frozen: false, sortable: true, editable: false, tem: true },
      { field: 'checkStandard', width: '315px', header: '验收标准', frozen: false, sortable: true, editable: true, tem: true, required: true },
      { field: 'checkItem', width: '175px', header: '检查项目', sortable: true, editable: true, tem: true },
      { field: 'phoneRequire', width: '110px', header: '照片/张', sortable: true, editable: true, tem: true, required: true },
      { field: 'tackPhoneRequire', width: '160px', header: '拍摄要求', sortable: true, editable: true, tem: true },
      { field: 'toolConfig', width: '120px', header: '基本配备工具', sortable: true, editable: true, tem: true },
      { field: 'isReferenceCheckItem', width: '130px', header: '是否关联验收项', sortable: true, editable: true, tem: true },
      { field: 'isRequestPassItem', width: '120px', header: '是否为必过项', sortable: true, editable: true, tem: true },
      { field: 'operation', header: '操作', width: '180px', frozen: false, sortable: true, tem: true }
    ];
    //工艺弹出框
    this.teccols = [
      { field: 'nodeCode', header: '编号', width: '100px', frozen: false, sortable: true, tem: false },
      { field: 'name', header: '名称', frozen: false, sortable: true },
      { field: 'technologyText', header: '类型', width: '150px', sortable: true }
    ];
    //节点-关联定额
    this.quotaCol = [
      { field: 'quotaNum', header: '定额号', width: '150px', frozen: false, sortable: true, editable: false, tem: false, },
      { field: 'quotaName', width: '350px', header: '名称', frozen: false, sortable: true, editable: false, tem: false },
      { field: 'category', width: '100px', header: '类别', sortable: true, editable: false, tem: false, dic: 'quoCategory' },
      { field: 'dateCreated', width: '100px', header: '启用日期', sortable: true, editable: false, tem: false },
      { field: 'expirationTime', width: '100px', header: '失效日期', sortable: true, editable: false, tem: false },
      { field: 'operation', header: '操作', width: '100px', frozen: false, sortable: true, tem: true }
    ];
    //节点-关联定额弹框
    this.quotaColForDialog = [
      { field: 'quotaNum', header: '定额号', width: '150px', frozen: false, sortable: true, editable: false, tem: false },
      { field: 'quotaName', width: '250px', header: '名称', frozen: false, sortable: true, editable: false, tem: false },
      { field: 'category', width: '100px', header: '类别', sortable: true, editable: false, tem: false, dic: 'quoCategory' },
      { field: 'dateCreated', width: '100px', header: '启用日期', sortable: true, editable: false, tem: false },
      { field: 'expirationTime', width: '100px', header: '失效日期', sortable: true, editable: false, tem: false }
    ];
    //下拉框初始化
    let status = [{ 'name': '是', 'code': true }, { 'name': '否', 'code': false }];
    this.isEffects = [{ 'name': '启用', 'code': true }, { 'name': '禁用', 'code': false }];
    this.isWarningStatus = status;
    this.isSupervisorConfirm = status;
    this.isReferenceCheckItemArr = status;
    this.isRequestPassItemArr = status;
  }

  ngOnInit() {
    this.storage.loading();
    // 获取路由id
    this.routeInfo.params.subscribe((params) => {
      this.nodeId = params["id"];
      // 获取节点信息
      if (this.nodeId) {
        this.title = "编辑节点";
        this.nodeService.getNodeById(this.nodeId).then(data => {
          this.nodeManage = data.data.nodeManage;
          this.selectedWarningStatus = this.nodeManage['isWarning'] ? { 'name': '是', 'code': true } : { 'name': '否', 'code': false };
          this.selectedConfirm = this.nodeManage['isSupervisorConfirm'] ? { 'name': '是', 'code': true } : { 'name': '否', 'code': false };
          this.selectedisEffect = this.nodeManage['isEffect'] ? { 'name': '启用', 'code': true } : { 'name': '禁用', 'code': false };
          this.technologyList = data.data.technologyList;
          this.standardList = data.data.standardList;
          this.standardList.filter((item) => {
            item.isReferenceCheckItem = item['isRequestPassItem'] ? { 'name': '是', 'code': true } : { 'name': '否', 'code': false };
            item.isRequestPassItem = item['isRequestPassItem'] ? { 'name': '是', 'code': true } : { 'name': '否', 'code': false };
            return false;
          });
          this.technologyStatus = data.data.technologyStatus['technologyStatus'];
          this.technologyStatus = [{ code: '', name: '全部' }, ...this.technologyStatus]
          this.quotaList = data.data.quota.data == undefined ? [] : data.data.quota.data;
          let quotaNums = data.data.nodeManage['quotaNums'];
          if (quotaNums) {
            this.quotaCode = this.quotaCode.concat(quotaNums.split(','));
          }
          let referenceMoneyLimitCodes = data.data.nodeManage['referenceMoneyLimitCodes'];
          if(referenceMoneyLimitCodes){
            this.quotaNum = this.quotaNum.concat(referenceMoneyLimitCodes.split(','));
          }
          let referenceMoneyLimitIds = data.data.nodeManage['referenceMoneyLimitIds'];
          if (referenceMoneyLimitIds) {
            this.quotaIds = this.quotaIds.concat(referenceMoneyLimitIds.split(','))
          }
          this.storage.loadout();
        });
      } else {
        this.title = "创建节点";
        this.selectedWarningStatus = { 'name': '是', 'code': true };
        this.selectedConfirm = { 'name': '否', 'code': false };
        this.storage.loadout();
      }
    });
  }


  //======================================================工艺列表开始==============================================================

  //工艺上移
  technologyMoveUp(index) {
    if (index == 0) {
      // this.error('已经是第一行了！')
      return;
    }
    let doc = this.technologyList.splice(index, 1);
    this.technologyList.splice(index - 1, 0, doc[0]);
    this.technologyList = this.technologyList.concat([]);
  }

  //工艺下移
  technologyMoveDown(index) {
    if (index == this.technologyList.length - 1) {
      // this.error('已经是最后一行了！')
      return;
    }
    let doc = this.technologyList.splice(index, 1);
    this.technologyList.splice(index + 1, 0, doc[0]);
    this.technologyList = this.technologyList.concat([])
  }

  /**
   * Description: 删除工艺信息
   */
  removeTechnology(i) {
    let tec = this.technologyList[i]
    if (tec.id) {
      tec.isDeleted = true;
      this.technologyListForDel.push(tec)
    }
    this.technologyList = this.technologyList.filter((item, index) => {
      if (index === i) {
        return false;
      } else {
        return true;
      }
    })
  }
  //======================================================工艺列表结束==============================================================

  //======================================================验收标准列表开始==============================================================

  //验收标准上移
  standardMoveUp(index) {
    if (index == 0) {
      // this.error('已经是第一行了！')
      return;
    }
    let doc = this.standardList.splice(index, 1);
    this.standardList.splice(index - 1, 0, doc[0]);
    this.standardList = this.standardList.concat([])
  }

  //验收标准下移
  standardMoveDown(index) {
    if (index == this.standardList.length - 1) {
      // this.error('已经是最后一行了！')
      return;
    }
    let doc = this.standardList.splice(index, 1);
    this.standardList.splice(index + 1, 0, doc[0]);
    this.standardList = this.standardList.concat([])
  }

  /**
   * Description: 验收标准表格添加行
   */
  addStandard() {
    let standardListForSave = JSON.parse(JSON.stringify(this.standardList.concat(this.standardListForDel)));
    for (let item of standardListForSave) {
      if (!item['checkStandard']) {
        return;
      }
      if (!item['phoneRequire']) {
        return;
      }
    }
    let standard = {
      checkItem: "", checkStandard: "", isReferenceCheckItem: { 'name': '否', 'code': false },
      isRequestPassItem: { 'name': '否', 'code': false }, phoneRequire: "", showTitle: false, sortNum: 1,
      tackPhoneRequire: "", toolConfig: ""
    };
    this.standardError = '';
    this.standardList = [...this.standardList, standard];
  }


  /**
   *Description: 删除验收标准
   */
  removeStandrad(i) {
    let standrad = this.standardList[i];
    if (standrad.id) {
      standrad.isDeleted = true;
      this.standardListForDel.push(standrad)
    }
    this.standardList = this.standardList.filter((item, index) => {
      if (index === i) {
        return false;
      } else {
        return true;
      }
    })
  }
  //======================================================验收标准列表结束==============================================================


  //=================================================弹出框工艺开始==================================================================
  /**
   * Description: 打开工艺弹框
   */
  openDialogForTec() {
    this.selectedType = { code: false, name: '全部' };
    this.searchObj['searchValue'] = "";
    this.selectedRowData = [];
    let codes = [];
    for (let index of this.technologyList) {
      codes.push(index.nodeCode)
    }
    // 工艺表格初始化
    delete this.searchObj['type'];
    delete this.searchObj['searchValue'];
    this.searchObj['codes'] = codes.toString();
    this.storage.loading();
    this.technologyListFn(this.searchObj);
  }

  /**
   * Description: 弹出框工艺列表
   */
  technologyListFn(param) {
    this.nodeService.technologyList(param, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data['totalCount'];
          this.technologyStatus = data.data.technologyStatus['technologyStatus'];
          this.technologyStatus = [{ code: '', name: '全部' }, ...this.technologyStatus];
          this.W001 = true;
        } else {
          this.listData = [];
          this.totalRecords = 0;
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败' });
        }
        this.storage.loadout();
      }
    );
  }

  /**
   * 弹出框工艺搜索方法
   */
  search() {
    if (this.selectedType && this.selectedType.code) {
      this.searchObj['type'] = this.selectedType.code;
    } else {
      delete this.searchObj['type'];
    }
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      } else {
        this.searchObj['searchValue'] = this.searchObj['searchValue'].trim();
      }
    }
    let codes = [];
    for (let index of this.technologyList) {
      codes.push(index.nodeCode);
    }
    this.searchObj['codes'] = codes.toString();
    this.technologyListFn(this.searchObj);
  }


  /**
   * 工艺弹出框分页查询方法
   * @param event
   */
  onPage(event) {
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.loading = true;
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      } else {
        this.searchObj['searchValue'] = this.searchObj['searchValue'].trim();
      }
    }
    let codes = [];
    for (let index of this.technologyList) {
      codes.push(index.nodeCode);
    }
    this.searchObj['codes'] = codes.toString();
    this.technologyListFn(this.searchObj);
  }


  /**
   * Description: 弹出框保存工艺信息
   */
  saveStandrad() {
    let standrad = this.selectedRowData;
    if (standrad.length == 0) {
      this.W001 = false;
      return;
    } else {
      this.technologyError = '';
    }
    for (let index of standrad) {
      this.technologyList = [...this.technologyList, index];
    }
    this.selectedRowData = [];
    this.W001 = false;

  }

  //=================================================弹出框工艺结束==================================================================



  //错误提示
  error(msg) {
    this.storage.messageService.emit({ severity: 'error', detail: msg })
  }

  //取消
  back() {
    if (this.nodeId) {
      this.router.navigate(['saas/engineer/constructionconfig/node/', this.nodeId]);
    } else {
      this.router.navigate(['saas/engineer/constructionconfig/node']);
    }
  }


  /**
   * Description: 保存节点信息
   */
  save() {
    if (!this.nodeId) {
      this.nodeManage['isSupervisorConfirm'] = this.selectedConfirm.code
      this.nodeManage['isWarning'] = this.selectedWarningStatus.code
    }
    if (!this.checkSaveData()) {
      return;
    }
    //节点-工艺保存数据
    let technologyListForSave = JSON.parse(JSON.stringify(this.technologyList.concat(this.technologyListForDel)));
    //节点-验收保存数据
    let standardListForSave = JSON.parse(JSON.stringify(this.standardList.concat(this.standardListForDel)));
    for (let item of standardListForSave) {
      if (!item['checkStandard']) {
        return;
      }
      if (!item['phoneRequire']) {
        return;
      }
    }
    // if (this.checkSortNumError) {
    //   return;
    // }
    //关联定额编号
    this.nodeManage['referenceMoneyLimitCodes'] = this.quotaNum.toString();
    this.nodeManage['quotaNums'] = this.quotaCode.toString();
    //关联定额id
    this.nodeManage['referenceMoneyLimitIds'] = this.quotaIds.toString();
    standardListForSave.filter((item) => {
      item.isReferenceCheckItem = item.isReferenceCheckItem['code'];
      item.isRequestPassItem = item.isRequestPassItem['code'];
      return false;
    });
    this.storage.loading();
    if (this.nodeId) {
      if (!this.selectedisEffect && this.nodeManage['isEffect'] == undefined) {
        this.storage.loadout();
        return false;
      }
      this.nodeService.updata({ nodeManage: this.nodeManage, technologyList: technologyListForSave, standardList: standardListForSave }).then(
        data => {
          this.storage.loadout();
          if (data.result) {
            this.storage.messageService.emit({ severity: 'success', detail: data.msg });
            this.router.navigate(['saas/engineer/constructionconfig/node/', this.nodeId]);
          } else {
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
    } else {
      this.nodeService.save({
        nodeManage: this.nodeManage, technologyList: technologyListForSave, standardList: standardListForSave
      }).then(
        data => {
          this.storage.loadout();
          if (data.result) {
            this.storage.messageService.emit({ severity: 'success', detail: data.msg });
            this.router.navigate(['saas/engineer/constructionconfig/node']);
          } else {
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
        );
    }
  }

  //=================================================弹出框关联定额开始=======================================================================

  /**
   * Description: 打开关联定额弹框
   */
  openDialogForQuota() {
    this.selectedQuotaRowData = [];
    this.selectQuotaType = { code: false, name: '全部' };
    this.searchQuotaObj['keyword'] = "";
    let codes = this.quotaCode == null ? "" : this.quotaCode;
    // 关联定额初始化
    this.searchQuotaObj['codeList'] = codes.toString();
    delete this.searchQuotaObj['type'];
    delete this.searchQuotaObj['keyword'];
    this.getQuotaListFn(this.searchQuotaObj);
  }


  /**
   * Description: 获取定额弹出框列表
   */
  getQuotaListFn(param) {
    this.nodeService.getQuota(param, this.pageSizeTwo, this.pageNoTwo).then(
      data => {
        if (data.result) {
          this.loading = false;
          this.quotaListForDialog = data.data.data.list;
          this.quotaType = data.data['quotaType'];
          this.quotaType = [{ code: '', name: '全部' }, ...this.quotaType];
          this.quotaTotalRecords = data.data.data['totalCount'];
          this.W002 = true;
        } else {
          this.loading = false;
          this.quotaListForDialog = [];
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }


  /**
   * 弹出框定额搜索方法
   */
  searchQuota() {
    if (this.selectQuotaType && this.selectQuotaType.code) {
      this.searchQuotaObj['type'] = this.selectQuotaType.code;
    } else {
      delete this.searchQuotaObj['type'];
    }
    if (!this.searchQuotaObj['keyword']) {
      delete this.searchQuotaObj['keyword'];
    } else {
      if (this.searchQuotaObj['keyword'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      } else {
        this.searchQuotaObj['keyword'] = this.searchQuotaObj['keyword'].trim();
      }
    }
    let codes = this.quotaCode == null ? "" : this.quotaCode;
    // 关联定额初始化
    this.searchQuotaObj['codeList'] = codes.toString();
    this.getQuotaListFn(this.searchQuotaObj);
  }

  /**
   * Description: 定额弹出框分页查询
   */
  onPageQuota(event) {
    this.pageSizeTwo = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNoTwo = pageTem + 1;
    this.loading = true;
    if (!this.searchQuotaObj['keyword']) {
      delete this.searchQuotaObj['keyword'];
    } else {
      if (this.searchQuotaObj['keyword'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      } else {
        this.searchQuotaObj['keyword'] = this.searchQuotaObj['keyword'].trim();
      }
    }
    let codes = this.quotaCode == null ? "" : this.quotaCode;
    // 关联定额初始化
    this.searchQuotaObj['codeList'] = codes.toString();
    this.getQuotaListFn(this.searchQuotaObj);
  }


  /**
   * Description: 弹出框保存定额信息
   */
  saveQuota() {
    debugger
    let quotas = this.selectedQuotaRowData;
    this.quotaCode = [];
    this.quotaIds = [];
    this.quotaNum = [];
    if (quotas.length == 0) {
      this.W002 = false;
      return;
    } else {
      this.openDialogForQuotaError = '';
    }
    for (let index of quotas) {
      this.quotaList = [...this.quotaList, index];
    }
    for (let index of this.quotaList) {
      this.quotaCode.push(index.code);
      this.quotaIds.push(index.id);
      this.quotaNum.push(index.quotaNum)
    }
    this.selectedQuotaRowData = [];
    this.W002 = false
  }

  //=================================================弹出框关联定额结束=======================================================================

  /**
   * Description: 删除关联定额
   */
  removeQuota(i) {
    let quota = this.quotaList[i];
    this.quotaCode = this.quotaCode.filter((item, index) => {
      if (item === quota.code) {
        return false;
      } else {
        return true;
      }
    });
    this.quotaNum = this.quotaNum.filter((item, index) => {
      if (item === quota.quotaNum) {
        return false;
      } else {
        return true;
      }
    });
    debugger
    this.quotaIds = this.quotaIds.filter((item, index) => {
      if (item == quota.id) {
        return false;
      } else {
        return true;
      }
    });
    this.quotaList = this.quotaList.filter((item, index) => {
      if (index === i) {
        return false;
      } else {
        return true;
      }
    })
  }

  //=================================================数据验证========================================================================

  /**
   * Description: 用户编号唯一性验证
   */
  nodeCodeCk() {
    this.nodeManage['code'] = this.nodeManage['code'] ? this.nodeManage['code'].trim() : '';
    if (!this.nodeManage['code']) {
      this.nodeCodeError = '请输入编号';
    } else {
      if (this.nodeManage['code'].length > 20) {
        this.nodeCodeError = '最多20个字符!';
      } else {
        this.nodeService.uniqueCode({ 'code': this.nodeManage['code'], "nodeId": this.nodeId }).then(data => {
          if (data.result) {
            this.nodeCodeError = '';
          } else {
            this.nodeCodeError = data.msg;
          }
        });
        this.nodeNameError = '';
      }
    }
  }


  /**
   * Description: 节点名称验证
   */
  nodeNameCk() {
    this.nodeManage['name'] = this.nodeManage['name'] ? this.nodeManage['name'].trim() : '';
    if (!this.nodeManage['name']) {
      this.nodeNameError = '请输入名称';
    } else {
      if (this.nodeManage['name'].length > 25) {
        this.nodeNameError = '最多25个字符';
      } else {
        this.nodeService.uniqueNodeName({ 'name': this.nodeManage['name'], "nodeId": this.nodeId }).then(data => {
          if (data.result) {
            this.nodeNameError = '';
          } else {
            this.nodeNameError = data.msg;
          }
        });
        this.nodeNameError = '';
      }
    }
  }

  shortNameCk() {
    this.nodeManage['shortName'] = this.nodeManage['shortName'] ? this.nodeManage['shortName'].trim() : '';
    if (!this.nodeManage['shortName']) {
      this.shortNameError = '请输入节点简称';
    } else {
      if (this.nodeManage['shortName'].length > 6) {
        this.shortNameError = '最多6个字符';
      }
    }
  }

  // /**
  //  * Description: 节点序号唯一性检验
  //  */
  // checkSortNumCk() {
  //   if (this.nodeManage['sortNum'] != null && Number(this.nodeManage['sortNum']) > 0) {
  //     this.nodeService.uniqueSortNum({ 'sortNum': this.nodeManage['sortNum'], "nodeId": this.nodeId }).then(data => {
  //       if (data.result) {
  //         this.checkSortNumError = '';
  //       } else {
  //         this.checkSortNumError = data.msg;
  //       }
  //     });
  //     this.checkSortNumError = '';
  //   }
  // }

  /**
   * Description: 是否预警验证
   */
  isWarningStatusCk() {
    if (!this.selectedWarningStatus) {
      this.isWarningError = '请选择是否预警';
    } else {
      this.nodeManage['isWarning'] = this.selectedWarningStatus.code;
      this.isWarningError = '';
      if (!this.selectedWarningStatus.code) {
        this.nodeManage['warningDays'] = 0;
        this.warningDaysError = '';
      }
    }
  }

  /**
   * Description: 预警天数不能为空
   */
  warningDaysCk() {
    if (this.selectedWarningStatus && this.selectedWarningStatus.code) {
      let warningDays = this.nodeManage['warningDays'];
      if (warningDays == null || Number(warningDays) < 0) {
        this.warningDaysError = '请输入预警天数';
      }
    } else {
      this.nodeManage['warningDays'] = 0;
      this.warningDaysError = '';
    }
  }

  /**
   * Description: 是否监理确认验证
   */
  isSupervisorConfirmCk() {
    if (!this.selectedConfirm) {
      this.isSupervisorConfirmError = '请选择是否监理确认';
    } else {
      this.nodeManage['isSupervisorConfirm'] = this.selectedConfirm.code;
      this.isSupervisorConfirmError = '';
      if (!this.selectedConfirm.code) {
        this.nodeManage['confirmDays'] = 0;
        this.confirmDaysError = '';
      }
    }
  }

  /**
   * Description: 自动确认天数验证
   */
  confirmDaysCk() {
    if (this.selectedConfirm && this.selectedConfirm.code) {
      let confirmDays = this.nodeManage['confirmDays'];
      if (confirmDays == null || confirmDays < 0) {
        this.confirmDaysError = '请输入自动确认天数';
      }
    } else {
      this.nodeManage['confirmDays'] = 0;
      this.confirmDaysError = '';
    }
  }

  /**
   * Description: 状态验证
   */
  isEffectCk() {
    if (!this.selectedisEffect) {
      this.isEffectError = '请选择状态';
    } else {
      this.nodeManage['isEffect'] = this.selectedisEffect.code;
      this.isEffectError = '';
    }
  }

  /**
   * Description: 验收标准必填项验证
   */
  checkStandrad() {
    let checkStandards = this.standardList;
    if (checkStandards.length == 0) {
      return false
    }
    for (let index of this.standardList) {
      if (!index.checkStandard || !index.phoneRequire || index.isReferenceCheckItem == undefined || index.isRequestPassItem == undefined) {
        return false;
      }
    }
    return true;
  }
  // 保存时校验
  checkSaveData(): boolean {
    let flag = true;
    if (!this.nodeManage['code']) {
      flag = false;
      this.nodeCodeError = '请输入节点编号';
    } else {
      this.nodeCodeError = '';
    }
    if (!this.nodeManage['name']) {
      flag = false;
      this.nodeNameError = '请输入节点名称';
    } else {
      this.nodeNameError = '';
    }
    if (!this.nodeManage['shortName']) {
      flag = false;
      this.shortNameError = '请输入节点简称';
    } else {
      this.shortNameError = '';
    }
    if (this.nodeManage['isWarning'] == undefined) {
      flag = false;
      this.isWarningError = '请选择是否预警';
    } else {
      this.isWarningError = '';
    }
    if (this.nodeManage['isSupervisorConfirm'] == undefined) {
      flag = false;
      this.isSupervisorConfirmError = '请选择是否监理确认';
    } else {
      this.isSupervisorConfirmError = '';
    }
    if (this.technologyList.length == 0) {
      flag = false;
      this.technologyError = '请选择工艺';
    } else {
      this.technologyError = '';
    }
    if (this.standardList.length == 0) {
      flag = false;
      this.standardError = '请添加验收标准';
    } else {
      this.standardError = '';
    }
    if (this.quotaList.length == 0) {
      flag = false;
      this.openDialogForQuotaError = '请选择关联定额';
    } else {
      this.openDialogForQuotaError = '';
    }
    if (this.nodeManage['isWarning'] && (this.nodeManage['warningDays'] == null || Number(this.nodeManage['warningDays']) < 0)) {
      flag = false;
      this.warningDaysError = '请输入预警天数';
    } else {
      this.warningDaysError = '';
    }
    if (this.nodeManage['isSupervisorConfirm'] && (this.nodeManage['confirmDays'] == null || Number(this.nodeManage['confirmDays']) < 0)) {
      flag = false;
      this.confirmDaysError = '请输入自动确认天数';
    } else {
      this.confirmDaysError = '';
    }
    return flag;
  }
  // 验收标准照片/张提示
  checkPgotosIsNull(photos) {
    if (!photos) {
      return '请输入照片/张';
    } else {
      if (photos.length > 5) {
        return '最大长度5';
      } else {
        if (isNaN(photos)) {
          return '请输入正确的照片/张';
        }
      }
    }

  }
  // 验收标准照片/张 红框显示
  checkPhotos(photos) {
    if (!photos) {
      return true;
    } else {
      if (photos.length > 5) {
        return true;
      } else {
        if (isNaN(photos)) {
          return true;
        }
      }
    }
    return false;
  }
  // 验收标准提示
  checkStandardIsNull(photos) {
    if (!photos) {
      return '请输入验收标准';
    } else {
      if (photos.length > 100) {
        return '最大长度100';
      }
    }

  }
  // 验收标准红框显示
  checkStandard(photos) {
    if (!photos) {
      return true;
    } else {
      if (photos.length > 100) {
        return true;
      }
    }
    return false;
  }

  selectedCk(event) {
    if (event.originalEvent.checked && (this.quotaList.length + this.selectedQuotaRowData.length) > 100) {
      this.storage.makesureService.emit({
        message: '关联定额不能大于100！',
        header: '提示',
        rejectLabel: '取消',
        acceptLabel: '确定',
        acceptVisible: false,
        rejectVisible: false,
        accept: () => {
        },
        reject: () => {
          this.selectedQuotaRowData = this.selectedQuotaRowData.slice(0, (100 - this.quotaList.length));
        }
      });
    }
  }

  selectedAllCk(event) {
    if (event.checked) {
      if ((this.quotaList.length + this.selectedQuotaRowData.length) > 100) {
        this.storage.makesureService.emit({
          message: '关联定额不能大于100！',
          header: '提示',
          rejectLabel: '取消',
          acceptLabel: '确定',
          acceptVisible: false,
          rejectVisible: false,
          accept: () => {
          },
          reject: () => {
            this.selectedQuotaRowData = this.selectedQuotaRowData.slice(0, (100 - this.quotaList.length));
          }
        });
      }
    }
  }
}
