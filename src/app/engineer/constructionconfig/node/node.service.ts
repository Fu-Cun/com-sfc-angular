import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class NodeService {
  private baseUrl = '';
  // 施工配置服务名称
  private serveName = 'com-dyrs-mtsp-constructionconfigservice/constructionconfigservice/';

    constructor(private nodeService: HttpInterceptorService) { }

    /**
     * 获取节点列表信息 分页方法
     * @param pageSize 分页大小 默认值10
     * @param pageNo  当前页 默认值是1
     * @param search  搜索条件
     */
    list(search: any = {}, pageSize: number = 10, pageNo: number = 1, ) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/list/api',
        { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }

    /**
     * Description: 启用、禁用
     */
    updateStatus(search) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/updateStatus/api', search, this.baseUrl);
    }

    /**
     * 通过id获取节点信息
     * @param id 工队id
     */
    getNodeById(id) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/detailNodeManage/api', { nodeId: id }, this.baseUrl);
    }

    technologyList(search: any = {}, pageSize: number = 10, pageNo: number = 1, ) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/technologyList/api', { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }

    /**
     * 添加方法
     * @param param
     */
    save(param: any = {}) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/save/api',param, this.baseUrl);
    }
    /**
     * 修改方法
     * @param param
     */
    updata(param: any = {}) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/update/api',param, this.baseUrl);
    }

    /**
     * 获取关联定额
     * @param param
     */
    getQuota(search: any = {},pageSize: number = 10, pageNo: number = 1,) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/getQuota/api',
        { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
    }

    /**
     * 节点名称唯一性验证
     * @param param
     */
    uniqueNodeName(param: any = {}) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/checkNodeNameIsUsable/api',param, this.baseUrl);
    }
    /**
     * 节点编号唯一性验证
     * @param param
     */
    uniqueCode(param: any = {}) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/checkNodeCodeIsUsable/api',param, this.baseUrl);
    }

    /**
     * 节点序号唯一性验证
     * @param param
     */
    uniqueSortNum(param: any = {}) {
      return this.nodeService.post(this.serveName + 'nodeManageBase/checkSortNumIsUsable/api',param, this.baseUrl);
    }
}
