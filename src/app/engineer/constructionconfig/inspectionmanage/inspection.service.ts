import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class InspectionService {
  private baseUrl = '';
  // 施工配置服务名称
  private serveName = 'com-dyrs-mtsp-constructionconfigservice/constructionconfigservice/';

  constructor(private httpService: HttpInterceptorService) { }

  /**
   * 通过id获取节点信息
   * @param id 工队id
   */
  getInspectionById(id) {
    return this.httpService.post(this.serveName + 'inspectionRule/detailInspectionRule/api', { inspectionId: id }, this.baseUrl);
  }

  /**
   * Description: 保存或修改巡检规则
   * @param  formObj 规则对象
   */
  saveOrUpdate(formObj: any = {}){
    return this.httpService.post(this.serveName + 'inspectionRule/saveOrUpdate/api', formObj, this.baseUrl);
  }
}
