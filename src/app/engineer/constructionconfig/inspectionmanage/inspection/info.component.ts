import { Component, OnInit } from '@angular/core';
import { InspectionService } from '../inspection.service';
import { Router, ActivatedRoute } from '@angular/router';
import {StorageService} from "../../../../common/storage.service";

@Component({
  selector: 'app-inspectionmanage-inspection',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [InspectionService],
})
export class InfoInspectionComponent implements OnInit {

  id: any; //巡检规则id
  inspectionForm: any = {};  //巡检提交表单
  inspectionDaysError: any;  //平均巡检天数错误提示
  selectedisConsiderCheckError: any;  //平均巡检天数错误提示

  isConsiderChecks: any;  //是否考虑验收
  selectedisConsiderCheck: any;  //是否考虑验收选中项

  isEdit: boolean = false;  //是否编辑状态

  constructor(private router: Router, private routeInfo: ActivatedRoute, private inspectionService: InspectionService,private storage: StorageService) {
  }

  ngOnInit() {
    this.isConsiderChecks = [{'name': "是",code: 'true'},{'name': "否",code: 'false'}];
    this.selectedisConsiderCheck = {'name': "是",code: 'true'};
    this.getData();
  }


  /**
   * Description:
   */
  edit(){
      this.isEdit = true;
      this.getData();
  }

  /**
   * Description: 获取巡检规则信息
   */
  getData(){
      this.storage.loading();
      this.inspectionService.getInspectionById(this.id).then(
        data=>{
          this.storage.loadout();
          if(data.result){
            this.inspectionForm = data.data;
            this.id = this.inspectionForm.id;
          }else{
            // this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
  }

  /**
   * Description: 取消
   */
  cancel(){
      this.isEdit = false;
      this.inspectionDaysError = "";
      this.selectedisConsiderCheckError = "";
      this.getData();
  }

  /**
   * Description: 保存
   */
  save(){
    let flag: boolean = true;
    if(!this.inspectionForm['avgInspectionDays']){
        this.inspectionDaysError = "请输入平均巡检天数";
        flag = false;
    }
    if(this.selectedisConsiderCheck && !this.selectedisConsiderCheck.code){
        this.selectedisConsiderCheckError = "请选择是否考虑验收";
        flag = false;
    }
    if(!flag){
        return false;
    }
    this.storage.loading();
    this.inspectionForm['id'] = this.id ? this.id : 0;
    this.inspectionForm['isThinkCheck'] = this.selectedisConsiderCheck.code;
    this.inspectionService.saveOrUpdate(this.inspectionForm).then(
        data=>{
          this.storage.loadout();
          if(data.result){
              this.isEdit = false;
              this.inspectionForm = data.data;
              this.id = this.inspectionForm.id;
          }else{
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
    );

  }
}
