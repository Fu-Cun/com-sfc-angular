import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConstructionprocessRoutingModule } from './main-routing.module';

// primeNG
import { CheckboxModule } from '../../components/checkbox/checkbox';
import { PanelModule } from '../../components/panel/panel';
import { InputTextModule } from '../../components/inputtext/inputtext';
import { DataTableModule } from '../../components/datatable/datatable';
import { MultiSelectModule } from '../../components/multiselect/multiselect';
import { InputMaskModule } from '../../components/inputmask/inputmask';
import { DropdownModule } from '../../components/dropdown/dropdown';
import { InputSwitchModule } from '../../components/inputswitch/inputswitch';
import { ButtonModule } from '../../components/button/button';
import { RatingModule } from '../../components/rating/rating';
import { CalendarModule } from '../../components/calendar/calendar';
import { RadioButtonModule } from '../../components/radiobutton/radiobutton';
import { InputTextareaModule } from '../../components/inputtextarea/inputtextarea';
import { FileUploadModule } from '../../components/fileupload/fileupload';
import { DialogModule } from '../../components/dialog/dialog';
import { TabViewModule } from '../../components/tabview/tabview';
import { AdvanceModule } from '../../components/advancesearch/advancesearch';
import { FieldsetModule } from '../../components/fieldset/fieldset';
import { SpinnerModule } from '../../components/spinner/spinner';
import { MessageModule } from '../../components/message/message';
import { PickListModule } from '../../components/picklist/picklist';
import { LabelModule } from '../../components/label/label';
import { TreeModule } from '../../components/tree/tree';
import { TooltipModule } from '../../components/tooltip/tooltip';
import { LightboxModule } from '../../components/lightbox/lightbox';
import { SelectButtonModule } from '../../components/selectbutton/selectbutton';
import { DoubleTableModule } from '../../components/doublegrid/doublegrid';
import { TabMenuModule } from '../../components/tabmenu/tabmenu';
import { GalleriaModule } from '../../components/galleria/galleria';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete';
// 整改管理
import { ListModifymanageComponent } from './modifymanage/modifymanage/list.component';

// 工程详情
import { ListConstructioninfoComponent } from './constructionmanage/constructionmanage/list.component';
import { InfoConstructioninfoComponent } from './constructionmanage/constructionmanage/info.component';
import { ListConstructioninfoPrepareComponent } from './constructionmanage/constructionprepare/list.component';
import { ListConstructioninfoScheduleComponent } from './constructionmanage/constructionprogress/list.component';
import { ListConstructioninfoServiceteamComponent } from './constructionmanage/serviceteam/list.component';
import { InfoConstructioninfoModifyrecordComponent } from './constructionmanage/modifyrecord/info.component';
import { ListConstructioninfoModifyrecordComponent } from './constructionmanage/modifyrecord/list.component';
import { ListConstructioninfoDeliveryplanComponent } from './constructionmanage/deliveryplan/list.component';
import { ListConstructioninfoContractinfoComponent } from './constructionmanage/contractinfo/list.component';
import { ListDesigndrawingComponent } from './constructionmanage/designdrawing/list.component';
import { ListConstructioninfoOrderinfoComponent } from './constructionmanage/orderinfo/list.component';
import { InfoConstructioninfoStageComponent } from './constructionmanage/constructionprogress/stage.component';
import { InfoConstructioninfoCheckComponent } from './constructionmanage/constructionprogress/check.component';
import { InfoConstructioninfoNodeComponent } from './constructionmanage/constructionprogress/node.component';
import { DivTableComponent } from './constructionmanage/divtable/divtable.component';

import { ListConstructioninfoEstimateComponent } from './constructionmanage/estimate/list.component';
import { ListConstructioninfoInspectionComponent } from './constructionmanage/inspection/list.component';
import { ListConstructioninfoPunishComponent } from './constructionmanage/punish/list.component';
import { ServiceteamInfoComponent } from './constructionmanage/serviceteam/info.component';
import { PunishDialogComponent } from './constructionmanage/punishdialog/dialog.component';
// 延迟审核
import { InfoDelaymanageComponent } from './delaymanage/delaymanage/info.component';
import { ListDelaymanageComponent } from './delaymanage/delaymanage/list.component';



/**
 * 施工配置模块
 */
@NgModule({
  imports: [
    ConstructionprocessRoutingModule,
    TooltipModule,
    CommonModule,
    FormsModule,
    PanelModule,
    InputTextModule,
    DataTableModule,
    MultiSelectModule,
    ButtonModule,
    InputMaskModule,
    DropdownModule,
    InputSwitchModule,
    CalendarModule,
    RatingModule,
    CheckboxModule,
    RadioButtonModule,
    InputTextareaModule,
    FileUploadModule,
    DialogModule,
    ReactiveFormsModule,
    TabViewModule,
    AdvanceModule,
    FieldsetModule,
    SpinnerModule,
    MessageModule,
    PickListModule,
    LabelModule,
    LightboxModule,
    SelectButtonModule,
    TreeModule,
    DoubleTableModule,
    TabMenuModule,
    GalleriaModule,
    AutoCompleteModule
  ],
  declarations: [
    ListModifymanageComponent,
    ListConstructioninfoComponent,
    InfoConstructioninfoComponent,
    ListConstructioninfoPrepareComponent,
    ListConstructioninfoScheduleComponent,
    InfoConstructioninfoStageComponent,
    InfoConstructioninfoCheckComponent,
    InfoConstructioninfoNodeComponent,
    ListConstructioninfoServiceteamComponent,
    InfoConstructioninfoModifyrecordComponent,
    ListConstructioninfoModifyrecordComponent,
    ListConstructioninfoDeliveryplanComponent,
    ListConstructioninfoContractinfoComponent,
    ListDesigndrawingComponent,
    ListConstructioninfoOrderinfoComponent,
    DivTableComponent,
    ListConstructioninfoEstimateComponent,
    ListConstructioninfoInspectionComponent,
    ListConstructioninfoPunishComponent,
    PunishDialogComponent,
    ServiceteamInfoComponent,
    InfoDelaymanageComponent,
    ListDelaymanageComponent
  ],
  exports: [],
  providers: []
})
export class ConstructionprocessModule {
}
