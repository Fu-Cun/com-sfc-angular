import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class ModifymanageService {
  private baseUrl = '';

  // 工程管理服务名称
  private serveName = 'com-dyrs-mtsp-constructionprocessservice';

  constructor(private modifymanageService: HttpInterceptorService) { }
  // 整改列表
  list(search: any = {}, pageSize: number = 10, pageNo: number = 1, ) {
    return this.modifymanageService.post(
      this.serveName + '/constructionInfoModify/modifyList', { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }
  // 整改类型
  getModifyStatus() {
    return this.modifymanageService.post(this.serveName + '/constructionInfoModify/getModifyStatus', {}, this.baseUrl);
  }
  // 整改详情
  modifyDetail(id) {
    return this.modifymanageService.post(this.serveName + '/constructionInfoModify/modifyDetail', { id: id }, this.baseUrl);
  }
}
