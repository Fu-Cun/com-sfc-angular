import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModifymanageService } from '../modifymanage.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-modifymanage',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ModifymanageService]
})
export class ListModifymanageComponent implements OnInit {
  cols; // 表格头部
  colsSelected; // 表格选中的显示列
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  loading; // 表格数据是否加载中
  listData;
  type; // 类型
  isCustomScan; // 是否客户可见
  isDelay; // 是否延期
  selectedType; // 选中后的类型
  selectedIsDelay; // 选择后的时候延期
  selectedIsCustomScan; // 选中后是否客户可见
  status; // 状态
  selectedIsEffect; // 选中后的状态
  selectedRowData = []; // 选中行数据
  searchObj: any = {}; // 查询条件

  constructor(private router: Router, private modifymanageService: ModifymanageService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
    this.loading = true;
    this.cols = [
      { field: 'sortNum', header: '序号', width: '80px', frozen: false, sortable: true, tem: true, hidden: false },
      { field: 'modifyRecord', header: '整改项', frozen: false, sortable: true, tem: true, hidden: false },
      { field: 'businessOpportunityCode', header: '项目编号', frozen: false, sortable: true, hidden: false },
      { field: 'isCustomScan', header: '客户是否可见', sortable: true, tem: true, hidden: false },
      { field: 'isDelay', header: '是否延期', frozen: false, sortable: true, tem: true, hidden: false },
      { field: 'description', header: '说明', frozen: false, sortable: true, hidden: false },
      { field: 'deadline', header: '最后期限', frozen: false, sortable: true, hidden: false },
      { field: 'factFinishTime', header: '实际完成时间', frozen: false, sortable: true, hidden: false },
      { field: 'modifyStatus', header: '状态', frozen: false, sortable: true, tem: true, hidden: false },
      { field: 'creater', header: '创建人', frozen: false, sortable: true, dic: 'user_type', hidden: false },
      { field: 'createTime', header: '创建时间', frozen: false, sortable: true, hidden: false },
    ];
    this.colsSelected = this.cols;
    this.isCustomScan = [{ name: '全部' }, { name: '客户可见', code: true }, { name: '客户不可见', code: false }];
    this.isDelay = [{ name: '全部' }, { name: '是', code: true }, { name: '否', code: false }];
    // this.loading = false;
    // this.listData = [{ sortNum: '1', modifyRecord: '整改1', createTime: 'sadsadsa' }];

    // 整改状态
    this.modifymanageService.getModifyStatus().then(
      data => {
        if (data.result) {
          this.status = [{ 'code': '', name: '全部' }, ...data.data.modifyStatus];
        } else {
        }
      }
    );

    // 整改表格初始化
    this.storage.loading();
    this.modifymanageService.list().then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );
  }
  ngOnInit() {

  }

  /**
  * 搜索方法
  */
  search() {
    if (this.selectedType && this.selectedType.code) {
      this.searchObj['status'] = this.selectedType.code;
    } else {
      delete this.searchObj['status'];
    }
    if (this.selectedIsDelay && this.selectedIsDelay.code !== this.isDelay[0].code) {
      this.searchObj['isDelay'] = this.selectedIsDelay.code;
    } else {
      delete this.searchObj['isDelay'];
    }
    if (this.selectedIsCustomScan && this.selectedIsCustomScan.code !== this.isCustomScan[0].code) {
      this.searchObj['isCustomScan'] = this.selectedIsCustomScan.code;
    } else {
      delete this.searchObj['isCustomScan'];
    }
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.storage.loading();
    this.modifymanageService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );

  }
  /**
  * dataTable分页查询方法
  * @param event
  */
  onPage(event) {
    this.searchObj['sortF'] = event.sortField;
    this.searchObj['sortO'] = event.sortOrder == 1 ? 'asc' : 'desc';
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.loading = true;
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.storage.loading();
    this.modifymanageService.list(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.loading = false;
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.loading = false;
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );

  }
  /**
  * 整改详情
  * @param data 整改数据
  */
  info(data) {
    this.router.navigate(['/saas/engineer/constructionprocess/modifymanage/', data.id]);
  }
}
