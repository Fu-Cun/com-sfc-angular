import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

// 整改管理
import { ListModifymanageComponent } from './modifymanage/modifymanage/list.component';

// 工程详情
import { ListConstructioninfoComponent } from './constructionmanage/constructionmanage/list.component';
import { InfoConstructioninfoComponent } from './constructionmanage/constructionmanage/info.component';
import { InfoConstructioninfoModifyrecordComponent } from './constructionmanage/modifyrecord/info.component';
import { InfoConstructioninfoStageComponent } from './constructionmanage/constructionprogress/stage.component';
import { InfoConstructioninfoCheckComponent } from './constructionmanage/constructionprogress/check.component';
import { InfoConstructioninfoNodeComponent } from './constructionmanage/constructionprogress/node.component';
import { ServiceteamInfoComponent } from './constructionmanage/serviceteam/info.component';

// 延迟审核
import { InfoDelaymanageComponent } from './delaymanage/delaymanage/info.component';
import { ListDelaymanageComponent } from './delaymanage/delaymanage/list.component';
/**
 * 施工队管理路由参数配置
 */
const mainRoutes: Routes = [
  {
    path: 'delaymanage',
    component: ListDelaymanageComponent,
    data: { title: '延期审核' }
  },
  {
    path: 'delaymanage/:id',
    component: InfoDelaymanageComponent,
    data: { title: '延期详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'modifymanage',
    component: ListModifymanageComponent,
    data: { title: '整改管理' }
  },
  {
    path: 'constructionmanage',
    component: ListConstructioninfoComponent,
    data: { title: '工程管理' }
  },
  {
    path: 'constructionmanage/modifyrecord/:id/:infoId',
    component: InfoConstructioninfoModifyrecordComponent,
    data: { title: '整改详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'constructionmanage/:id/:code',
    component: InfoConstructioninfoComponent,
    data: { title: '工程详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'constructionmanage/constructionprogress/check/:id/:infoId',
    component: InfoConstructioninfoCheckComponent,
    data: { title: '验收项详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'constructionmanage/constructionprogress/stage/:id/:infoId',
    component: InfoConstructioninfoStageComponent,
    data: { title: '施工阶段详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'constructionmanage/constructionprogress/node/:id/:infoId',
    component: InfoConstructioninfoNodeComponent,
    data: { title: '节点详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'serviceteam/punish/:id/:code/:type/:userCode',
    component: ServiceteamInfoComponent,
    data: { title: '查看评价', 'hasSidebar': false, 'hasHeader': false }
  }
];
/**
 * 施工队队管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ConstructionprocessRoutingModule { }
