import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DelaymanageService } from '../delaymanage.service';
import { StorageService } from '../../../../common/storage.service';
import { TimeConfigClass } from '../../../../common/common-config';

@Component({
  selector: 'app-info-delaymanage',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [DelaymanageService ],
})
export class InfoDelaymanageComponent implements OnInit {

  delayId: any;  //延期信息id

  item: any = {};  //延期信息
  itemCol: any = [];  //延期表头

  operations: any = [];  //操作人
  operationsCol: any = [];  //操作人表头

  formObj: any = {}; //申诉

  isShow: boolean = false;  //延期审核弹框是否显示
  assistIsShow: boolean = false;  //协助处理是否显示

  assist: any = {};  //协助处理

  approveReasultError: any;  //审核错误提示
  approveError: any;  //审核错误提示

  assistReasonError: any;  //协助错误提示
  assistError: any;  //协助错误提示

  approveBtnIsShow: boolean = false;  //延期审核按钮显示隐藏
  assistBtnIsShow: boolean = false;  //延期审核按钮显示隐藏


  constructor(private router: Router, private delaymanageService: DelaymanageService, private routeInfo: ActivatedRoute,
    private storage: StorageService, ) {

  }
  ngOnInit() {
    this.routeInfo.params.subscribe((params) => {
        this.delayId = params['id']
    });
    this.itemCol = [
      { field: 'code', title: '延期单号' },
      { field: 'delayDays', title: '延期天数/天',fun:(field, data)=>{if(data && data['delayDays'] && data['delayDaysHint']){
       return data['delayDays'] + " " + "(" + data['delayDaysHint'] +")"}
      }},
      { field: 'businessOpportunityCode', title: '项目编号' },
      { field: 'delayResponsibilityType', title: '责任类型',fun: (field, data) => this.storage.dicFilter('responsibilitytype', data[field])  },
      { field: 'organizationCode', title: '所属组织',fun: (field, data) => this.storage.orgFilter(data[field])},
      { field: 'delayReason', title: '延期理由',fun: (field, data) => this.storage.dicFilter('customdelayreason', data[field]) },
      { field: 'customerName', title: '客户' },
      { field: 'descirption', title: '详细描述' },
      { field: 'fitmentArea', title: '装修地址' },
      { field: 'isNeedAssist', title: '是否需要协助' ,fun: (field, data) => data[field] != undefined ? (data[field] == true ? '是' : '否') : ''},
      { field: 'foremanName', title: '项目经理',fun: (field, data) => (data['creater'] && data['userTelePhone']) ? data['creater'] + " " +  data['userTelePhone'] : '' },
      { field: 'applyPerson', title: '申请人',fun: (field, data) => (data['applyPessonName'] && data['applyPessonType']) ? data['applyPessonName'] + " " + this.storage.dicFilter('user_type', data['applyPessonType']) : '' },
      { field: 'approveReasult', title: '审核结果',fun: (field, data) => this.storage.dicFilter('approveResult', data[field]) },
      { field: 'applyTime', title: '申请时间' },
      { field: 'approveReason', title: '审核原因/说明' },
      { field: 'customAdvice', title: '客户意见' },
      { field: 'approveTime', title: '审核时间' },
      { field: 'customReason', title: '客户理由' },
      { field: 'assistReason', title: '协助结果' ,fun: (field, data) => this.storage.dicFilter('assistresult', data[field])},
      { field: 'confirmTime', title: '确认时间' },
      { field: 'reason', title: '原因/备注' },
      { field: 'assistHandleTime', title: '协助处理时间' },
    ];
    //操作人信息表头
    this.operationsCol = [
      { field: 'sortNum', header: '序号', width: '100px',tem: true },
      { field: 'operationContent', header: '操作', width: '100px' },
      { field: 'afterSubmitStatus', header: '提交后状态', width: '100px',dic: 'delaystatus' },
      { field: 'creater', header: '提交人', width: '100px',dic: 'user_type' },
      { field: 'telephone', header: '手机号', width: '100px' },
      { field: 'submitTime', header: '提交时间', width: '100px' },
      { field: 'isAutoConfirmText', header: '是否是自动确认', width: '100px' },
    ];
    //初始化数据
    this.storage.loading();
    this.delaymanageService.detailDelay(this.delayId).then(
      data=>{
        this.storage.loadout();
        if(data.result){
          this.item = data.data['delayInfo'];
          if(this.item.status == 'delaystatus_180208000018'){
            this.approveBtnIsShow = true;
          }
          if(this.item.status == 'delaystatus_180208000022'){
            this.assistBtnIsShow = true;
          }
          this.operations = data.data['operations'];
        }else{
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );

  }

  //延期审核弹框
  openWindow(){
    this.resetFormData();
    this.isShow = true;
    this.formObj.approveReasult = 'approveresult_approve'
  }

  //协助处理弹框
  openAssistWindow(){
    this.resetFormData();
    this.assistIsShow = true;
    this.assist.assistReason = 'assistresult_180208000011';
  }


  /*****表单 */
  /**
   * 重置表单数据
   */
  resetFormData() {
    this.isShow = false; // 弹窗a
    this.assist = {}; // 原始数据
    this.formObj = {};
    this.approveReasultError = '';
    this.approveError = '';

    this.assistReasonError = '';
    this.assistError = '';
  }




  //保存延期审核
  saveApprove(){
    let flag: boolean = true;
    if(!this.formObj['approveReasult']){
      this.approveReasultError = "请选择审核结果";
      flag = false
    }
    if(!this.formObj['approveReason']){
      this.approveError = "请输入原因/备注";
      flag = false
    }
    if(!flag){
      return
    }
    this.formObj.delayId = this.delayId;
    this.storage.loading();
    this.delaymanageService.saveApprove(this.formObj).then(
      data=>{
        this.storage.loadout();
        if(data.result){
          this.storage.messageService.emit({ severity: 'success', detail: '操作成功！' });
          this.item = data.data['delayInfo'];
          if(this.item.status == 'delaystatus_180208000018'){
            this.approveBtnIsShow = true;
          }else{
            this.approveBtnIsShow = false;
          }
          if(this.item.status == 'delaystatus_180208000022'){
            this.assistBtnIsShow = true;
          }else{
            this.assistBtnIsShow = false;
          }
          this.operations = data.data['operations'];
          this.isShow = false;
        }else{
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }

  //保存协助处理
  saveAssist(){
    let flag: boolean = true;
    if(!this.assist['assistReason']){
      this.assistReasonError = "请选择协助结果";
      flag = false
    }
    if(!this.assist['reason']){
      this.assistError = "请输入原因/备注";
      flag = false
    }
    if(!flag){
      return
    }
    this.assist.delayId = this.delayId;
    this.storage.loading();
    this.delaymanageService.saveAssist(this.assist).then(
      data=>{
        this.storage.loadout();
        if(data.result){
          this.storage.messageService.emit({ severity: 'success', detail: '操作成功！' });
          this.item = data.data['delayInfo'];
          if(this.item.status == 'delaystatus_180208000018'){
            this.approveBtnIsShow = true;
          }else{
            this.approveBtnIsShow = false;
          }
          if(this.item.status == 'delaystatus_180208000022'){
            this.assistBtnIsShow = true;
          }else{
            this.assistBtnIsShow = false;
          }
          this.operations = data.data['operations'];
          this.assistIsShow = false;
        }else{
          this.storage.messageService.emit({ severity: 'error', detail: '操作失败！' });
        }
      }
    );
  }


  //检验协助原因
  checkAssistReason(){
    if(!this.assist.reason){
      this.assistError = "请输入备注";
    }else{
      this.assistError = "";
    }
  }

  //检验延期审核备注
  checkApproveReason(){
    if(!this.formObj['approveReason']){
      this.approveError = '请输入备注';
    }else{
      this.approveError = "";
    }
  }


}
