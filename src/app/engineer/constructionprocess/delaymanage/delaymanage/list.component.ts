import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DelaymanageService } from '../delaymanage.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-delaymanage',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [DelaymanageService]
})
export class ListDelaymanageComponent implements OnInit {

  searchObj: any = {}; // 简易查询条件
  totalRecords: number = 0; // 数据条数据
  listData: any[] = []; // 表格数据
  dataHead: any[] = []; // 表格表头
  statusDefault: any = { code: '', name: '全部' }; // 状态默认值
  status: any[] = []; // 状态
  selectStatus: any = {}; // 选中的状态

  organizationArray: any[] = []; // 组织数据
  seletcedOrganization: any; // 选中的组织数据

  pageSize: number; // 分页大小
  pageNo: number; // 当前页

  dutyType: any = []; //责任类型
  selectDutyType: any = {};  //选择的责任类型

  delayReason: any = []; //延期理由
  selectDelayReason: any = {};  //选择的延期理由

  loading: boolean = true;

  constructor(private router: Router, private delaymanageService: DelaymanageService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
  }

  ngOnInit() {

    // 获取组织机构
    this.getOrganization();
    this.status = [...this.storage.getDic('delaystatus')];
    this.dutyType = [this.statusDefault, ...this.storage.getDic('responsibilitytype')];
    this.selectStatus = this.statusDefault;
    this.selectDelayReason = this.statusDefault;
    this.dataHead = [
      { field: 'code', header: '延期单号', width: '200px', tem: true, hidden: false },
      { field: 'businessOpportunityCode', header: '项目编号', width: '80px', tem: false, hidden: false },
      { field: 'organizationCode', header: '所属组织', width: '150px',tem: true, hidden: false },
      { field: 'customerName', header: '客户', width: '150px', hidden: false },
      { field: 'fitmentArea', header: '装修地址', width: '150px',tem: false, hidden: false },
      { field: 'foreman', header: '项目经理', width: '150px',tem: true, hidden: false },
      { field: 'delayDays', header: '延期天数/天', width: '150px', hidden: false },
      { field: 'delayResponsibilityType', header: '责任类型', tem: true,width: '150px', hidden: false },
      { field: 'delayReason', header: '延期理由', width: '150px',tem: true, hidden: false },
      { field: 'applyTime', header: '申请时间', width: '150px', hidden: false },
      { field: 'status', header: '状态', width: '300px',tem: true, hidden: false },
    ];
  }

  //责任类型与延期理由联动
  changeDutyType(){
    if (this.selectDutyType && this.selectDutyType.code && this.selectDutyType.code != this.dutyType[0].code) {
      this.searchObj['delayResponsibilityType'] = this.selectDutyType.code;
      if(this.selectDutyType.code == 'responsibilitytype_180208000001'){
        this.delayReason = [this.statusDefault, ...this.storage.getDic('customdelayreason')];
      }else if(this.selectDutyType.code == 'responsibilitytype_180208000002'){
        this.delayReason = [this.statusDefault, ...this.storage.getDic('notcustomdelayreason')];
      }
    } else {
      delete this.searchObj['delayResponsibilityType'];
      delete this.searchObj['delayReason'];
      this.delayReason = [this.statusDefault];
    }
    this.selectDelayReason = this.statusDefault;
  }

  /**
   * 简易查询条件整合
   */
  normalSearchFun() {
    // 状态
    if (this.selectStatus && this.selectStatus.code) {
      this.searchObj['status'] = this.selectStatus.code;
    } else {
      delete this.searchObj['status'];
    }
    // 责任类型
    if (this.selectDutyType && this.selectDutyType.code && this.selectDutyType.code != this.dutyType[0].code) {
      this.searchObj['delayResponsibilityType'] = this.selectDutyType.code;
    } else {
      delete this.searchObj['delayResponsibilityType'];
    }
    // 延期理由
    if (this.selectDelayReason && this.selectDelayReason.code && this.selectDelayReason.code != this.delayReason[0].code) {
      this.searchObj['delayReason'] = this.selectDelayReason.code;
    } else {
      delete this.searchObj['delayReason'];
    }
    // 组织机构
    if (this.seletcedOrganization && this.seletcedOrganization['code']) {
      this.searchObj['organizationCode'] = this.seletcedOrganization['code'];
    } else {
      delete this.searchObj['organizationCode'];
    }
    // 搜索文本框
    this.searchObj['searchValue'] = this.searchObj['searchValue'] ? this.searchObj['searchValue'].trim() : '';
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    }
    this.search();
  }



  /**
   * 查询按钮
   */
  search() {
    this.storage.loading();
    this.delaymanageService.delayList(this.searchObj,this.pageSize,this.pageNo).then((data) => {
      this.loading = false;
      if (data.result) {
        this.listData = data.data.list;
        this.totalRecords = data.data.totalCount;
        this.storage.loadout();
      } else {
        this.storage.loadout();
        this.storage.messageService.emit({ severity: 'error', detail: data.msg });
      }
    });
  }

  /**
   * 分页参数整理
   * @param event
   */
  onPage(event?) {
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.search();
  }


  /**
   * 获取公司数据
   */
  getOrganization() {
    this.delaymanageService.getTree().then(
      data => {
        if (data.result) {
          this.organizationArray = [{
            label: '全部',
            leaf: false,
            orgType: '',
            parentCode: '0',
            info: { companyName: '全部', companyCode: -1 },
            code: '',
            children: data.data
          }];
          this.expandAll(this.organizationArray);
        }
      }
    );
  }

  /**
   * 下拉树数据全部展开
   * @param data
   */
  expandAll(data) {
    data.forEach(node => {
      this.expandRecursive(node, true);
    });
  }
  /**
   * 设置树的展开收缩
   * @param data
   */
  private expandRecursive(node, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }
}
