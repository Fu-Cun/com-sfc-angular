import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class DelaymanageService {
  private baseUrl = '';

  // 工程管理服务名称
  private serveName = 'com-dyrs-mtsp-constructionprocessservice/constructionprocessservice/';

  // 组织管理微服务名称
  private organization = 'com-dyrs-mtsp-organizationservice';

  constructor(private modifymanageService: HttpInterceptorService) {

  }

  /**
   * 获取所属组织下拉树
   */
  getTree() {
    return this.modifymanageService.post(this.organization + '/organizationservice/treeMenuList/api', {}, '');
  }

  /**
   * Description: 整改记录列表
   * @Note:
   */
  delayList(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
    return this.modifymanageService.post(this.serveName + 'constructionInfoDelay/list/api',
      { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }

  /**
   * Description: 延期详情
   * @Note:
   */
  detailDelay(id){
    return this.modifymanageService.post(this.serveName + 'constructionInfoDelay/detailDelay/api', { id: id }, this.baseUrl);
  }

  /**
   * Description: 保存延期审核
   * @Note:
   */
  saveApprove(approve: any = {}){
    return this.modifymanageService.post(this.serveName + 'constructionInfoDelay/saveApprove/api', approve, this.baseUrl);
  }


  /**
   * Description: 保存协助处理
   * @Note:
   */
  saveAssist(assist: any = {}){
    return this.modifymanageService.post(this.serveName + 'constructionInfoDelay/saveAssist/api', assist, this.baseUrl);
  }
}
