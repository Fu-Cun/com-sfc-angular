import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../../common/http-interceptor.service';

@Injectable()
export class ConstructioninfoService {
  private baseUrl = '';
  // 工程管理服务名称
  private serveName = 'com-dyrs-mtsp-constructionprocessservice/constructionprocessservice/';
  // 服务模板
  private templateServeName = 'com-dyrs-mtsp-constructionconfigservice/';
  // 工程管理服务名称 魏纵横
  private serveNameWeiZH = 'com-dyrs-mtsp-constructionprocessservice/';
  // 工程管理服务名称 魏纵横
  private serveNamelhw = 'com-dyrs-mtsp-constructionprocessservicelhw/';
  // 订单微服务名称
  private orderServeName = 'saas-order-service/';
  // 组织管理微服务名称
  private organization = 'com-dyrs-mtsp-organizationservice';
  // 物料服务名称--王志磊
  private schemequotaServerName = 'com-dyrs-mtsp-schemequotaservice/';
   // 设计图服务名称--王志磊
   private clouddesignServerName = 'com-dyrs-mtsp-clouddesignservice/';
  //物料品类 -- 王飞
  private productTypeServerName = 'saas-productservice/';

  constructor(public constructioninfoService: HttpInterceptorService) {
  }


  /**
   * 获取所属组织下拉树
   */
  getTree() {
    return this.constructioninfoService.post(this.organization + '/organizationservice/treeMenuList/api', {}, '');
  }

  /**
   * 获取模板数据
   */
  getTemplate() {
    return this.constructioninfoService.post(this.templateServeName + 'templateConfig/templateListForName', {}, '');
  }

  /**
   * 获取工程管理列表
   * @param search 查询条件
   */
  getConstructionmanageList(search: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/page', search, '');
  }

  /**
   * 匹配模板
   * @param search
   */
  rematchTemplate(search: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/rematchTemplate', search, '');
  }
  /**
  * 查询项目经理
  * @param search
  */
  getManager(search: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/dispatchedUserPage', search, '');
  }
  /**
   * 重新派单
   */
  redispatch(search: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/redispatch', search, '');
  }

  /**
   * 自动补全
   */
  autoCurrentCity(search: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/searchProvinceAndCityAndArea', search, '');
  }

  /**
   * Description: 整改记录列表
   * @Note:
   */
  modifyRecordList(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoModifyTab/modifyRecordList/api',
      { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }


  /**
   * Description: 整改记录列表页面获取下拉选项
   * @Note:
   */
  getSelection() {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoModifyTab/getSelection/api', {}, this.baseUrl);
  }

  /**
   * 通过id获取整改记录
   * @param id 整改记录id
   */
  modifyRecordDetail(id) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoModifyTab/modifyRecordDetail/api', { id: id }, this.baseUrl);
  }

  /**
   * 整改驳回
   * @param id 整改记录id
   */
  modifyReject(param: any = {}) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoModifyTab/modifyReject/api', param, this.baseUrl);
  }


  /**
   * 配送计划列表
   * @param id
   */
  deliveryPlanList(search: any = {}) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoDeliveryTab/deliveryPlanList/api', search, this.baseUrl);
  }

  /**
   * Description: 订单列表
   * @Note:
   */
  getOrderList(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
    return this.constructioninfoService.post(this.orderServeName + 'ddOrder/getOrderInfoList',
      { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }


  /**
   * 配送计划列表
   * @param id
   */
  getSelectionOrder(bussinessId: any) {
    return this.constructioninfoService.post(this.serveName + 'order/getSelection/api', { projectId: bussinessId }, this.baseUrl);
  }

  /**
   * 施工进度列表页
   * @param search
   * @returns {promise.Promise<R>|Promise<R>|Maybe<T>|Observable<R|T>|Promise<{data, msg: string, errcode: string, status, result: boolean}>}
   */
  getSchedulePage(search: any = {}) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/schedulePage/api', search, this.baseUrl);
  }

  /**
   * 施工进度数量按钮
   * @param search
   * @returns {promise.Promise<R>|Promise<R>|Maybe<T>|Observable<R|T>|Promise<{data, msg: string, errcode: string, status, result: boolean}>}
   */
  getScheduleStatusCount(search: any = {}) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/scheduleStatusCount/api', search, this.baseUrl);
  }

  /**
   * 施工进度类型
   * @param search
   * @returns
   */
  getScheduleTypeSelection() {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfo/scheduleTypeSelection/api', this.baseUrl);
  }

  /**
   * 施工阶段详情
   * @param stateId
   * @returns
   */
  getStageDetail(stateId: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoProcess/detail/api', { id: stateId }, this.baseUrl);
  }

  /**
   * 施工节点详情
   * @param stateId
   * @returns
   */
  getNodeDetail(nodeId: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoProcessNode/detail/api', { id: nodeId }, this.baseUrl);
  }
  /**
   * 施工验收项详情
   * @param stateId
   * @returns
   */
  getCheckDetail(checkId: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoProcessCheck/detail/api',
      { id: checkId }, this.baseUrl);
  }

  /**
   * Description: 配送计划查看物料
   * @Note:
   */
  getSchemequota(search: any = {}, pageSize: number = 10, pageNo: number = 1) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoDeliveryTab/getProDataPager/api',
      { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }

  /**
   * Description:获取合同信息
   * @param:
   */
  getContractInfo(businessCode: any) {
    return this.constructioninfoService.post(this.serveName + 'contract/getContractInfo/api', { businessCode: businessCode }, this.baseUrl);
  }

  /**
   * Description: 获取设计图信息
   * @Note:
   */
  getDesignCharts(businessCode: any) {
    return this.constructioninfoService.post(this.schemequotaServerName + 'schemequotaservice/getDesignCharts/api',
      { businessCode: businessCode }, this.baseUrl);
  }
  /**
   * Description: 获取效果图信息
   * @Note:
   */
  getEffectPicture(businessCode: any) {
    return this.constructioninfoService.post(this.clouddesignServerName + 'designservice/getFileInfo/api',
      { businessCode: businessCode }, this.baseUrl);
  }
  /**
   * Description: 获取全景信息
   * @Note:
   */
  getPanoramaCharts(businessCode: any) {
    return this.constructioninfoService.post(this.clouddesignServerName + 'designservice/find360EffectDiagram/api',
      { businessCode: businessCode }, this.baseUrl);
  }  
  
  /**
   * 服务团队
   * @param id
   */
  getServiceteam(params: any = {}) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoServiceteam/detail/api', { ...params });
  }

  /**
   * 施工节点发起整改初始化
   * @param stateId
   * @returns
   */
  getNodeModifyInit(nodeCheckId: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoProcessNode/modifyInit/api',
      { nodeCheckId: nodeCheckId }, this.baseUrl);
  }

  /**
   * 施工验收项发起整改初始化
   * @param stateId
   * @returns
   */
  getCheckModifyInit(checkCheckId: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoProcessCheck/modifyInit/api',
      { checkCheckId: checkCheckId }, this.baseUrl);
  }

  /**
   * 施工验收项发起整改初始化
   * @param stateId
   * @returns
   */
  getSaveCheckModify(param: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoProcessCheck/saveModify/api', param, this.baseUrl);
  }

  /**
   * 施工节点发起整改初始化
   * @param stateId
   * @returns
   */
  getSaveNodeModify(param: any) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoProcessNode/saveModify/api', param, this.baseUrl);
  }
  /**
   * 施工准备
   * @param id 施工准备
   */
  prepareDetail(infoId) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoPrepare/detail', { infoId: infoId }, this.baseUrl);
  }
  /**
   * 罚款清单:
   * @param id 罚款清单
   */
  punishList(infoId) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoPunishFine/punishList',
      { infoId: infoId }, this.baseUrl);
  }

  /**
   * 施工准备
   * @param id 施工准备
   */
  getSelectionInspection() {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoInspection/getSelection/api', {}, this.baseUrl);
  }

  /**
   * Description: 巡检记录
   * @param 商机id
   */
  inspectionList(infoId: any) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoInspection/inspectionList/api', { infoId: infoId }, this.baseUrl);
  }

  /**
   * 通过项目id 获取项目编号已经经理和监理数据
   */
  getProjectNameByInfoId(data: any) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoPunishrecord/getProjectNameByInfoId/api'
    , data, this.baseUrl);
  }

  /**
   * 处罚信息保存
   */
  savePunish(data: any) {
    return this.constructioninfoService.post(this.serveName + 'constructionInfoPunishrecord/add/api'
    , data, this.baseUrl);
  }
  /**
   * 获取服务器当前数据
   */
  getCurrentDate() {
    return this.constructioninfoService.post(this.serveNameWeiZH + '/constructionInfo/currDate', {}, '');
  }

  /**
 * 评价信息
 */
  estimateData(constructionCode) {
    return this.constructioninfoService.post('com-dyrs-mtsp-estimateservice/findEstimate/api'
      , { constructionCode: constructionCode, isPage: 0 }, this.baseUrl);
  }
  /**
* 查看评价
*/
  viewEstimateData(constructionCode, code, pageSize, pageNo,userCode) {
    if (code === 'supervisor') {
      return this.constructioninfoService.post('com-dyrs-mtsp-estimateservice/findEstimate/api'
        , { constructionCode: constructionCode, supervisorCode: userCode, pageSize: pageSize, pageNo: pageNo }, this.baseUrl); // 监理
    } else if (code === 'team') {
      return this.constructioninfoService.post('com-dyrs-mtsp-estimateservice/findEstimate/api'
        , { constructionCode: constructionCode, foremanCode: userCode, pageSize: pageSize, pageNo: pageNo }, this.baseUrl); // 项目经理
    } else {
    }
  }

  /**
   * 查看用户
   */
  viewServiceUserData(infoId, userType) {
    return this.constructioninfoService.post(this.serveNameWeiZH + 'constructionInfoServiceteam/userDetail/api', {infoId:infoId, userType:userType}, this.baseUrl);
  }

  /**
   * Description: 获取物料品类
   */
  getProductType(){
    return this.constructioninfoService.post(this.productTypeServerName + '/category/getTreeByCode', {}, '');
  }

  /**
   * Description: 停工
   */
  lockout(param :any = {}){
    return this.constructioninfoService.post(this.serveName + 'constructionInfo/lockout/api', param, this.baseUrl);
  }

  /**
   * Description: 复工
   * @param 工程id
   */
  recover(infoId: any){
    return this.constructioninfoService.post(this.serveName + 'constructionInfo/recover/api', {id: infoId}, this.baseUrl);
  }


  /**
   * Description: 获取项目状态
   * @param 工程id
   */
  getInfoStatus(infoId: any){
    return this.constructioninfoService.post(this.serveName + 'constructionInfo/getInfoStatus/api', {infoId: infoId}, this.baseUrl);
  }

}
