import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-deliveryplan',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListConstructioninfoDeliveryplanComponent implements OnInit {
  @Input() data; // 商机id和商机编号
  businessId: any;  //商机id
  businessCode: any;  //商机code
  deliveryplan: any = [];  //配送计划

  supplementList: any = [];  //补货列表

  reportResultHead: any[] = []; // 提报表格表头
  materielHead: any[] = []; // 物料表格表头
  materielData: any[] = []; // 物料表格数据
  materielTypes: any[] = []; // 物料类型
  selectedMaterielTypes: any = {}; // 选中的物料类型
  totalRecords; // 数据条数
  pageSize: number;  //每页记录数
  pageNo: number;  //页数

  materielFlag: boolean = false; // 物料查看弹出是否显示
  searchObj: any = {};  //物料查询条件
  conentFlag: boolean = false; // 有模板内容区域显示标记
  conentNoFlag: boolean = false; // 没有模板内容区域显示标记

  isPunishShow: boolean = false; // 发起处罚弹出框
  punishmentsData: any = {}; // 传给处罚弹窗的数据

  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
    //提报结果表头
    this.reportResultHead = [
      { field: 'sortNum', header: '序号', width: '60px', sort: true },
      { field: 'reportResult', header: '提报结果', width: '150px', fun: (data, field) => this.storage.dicFilter('reportresult', data[field]) },
      { field: 'shouldArriveTime', header: '应到场日期', width: '150px' },
      { field: 'deliveryToTime', header: '延后到', width: '150px' },
      { field: 'deliveryReason', header: '延后理由', width: '150px' },
      {
        field: 'creater', header: '提交人', width: '150px',
        fun: (data, field) => data[field] ? data[field] + ' ' + storage.dicFilter('user_type', data['userType']) : ''
      },
      { field: 'submitTime', header: '提交时间' , width: '150px'},
    ];
    //弹出框物料表头
    this.materielHead = [
      { field: 'sortNum', header: '序号', width: '60px', tem: true },
      { field: 'productCode', header: '编号', width: '200px' },
      { field: 'productName', header: '名称', width: '200px' },
      { field: 'unitText', header: '单位', width: '60px' },
      { field: 'amount', header: '数量', width: '70px' },
      { field: 'useLocation', header: '使用区域', width: '120px' },
      { field: 'stockCycle', header: '备货周期/天', width: '125px' },
      { field: 'deliveryCycle', header: '配送周期/天' ,width: '125px'},
    ];

  }
  ngOnInit() {
    // 商机id 和商机编号
    this.storage.loading();
    this.businessId = this.data.id;
    this.businessCode = this.data.code;
    this.constructioninfoService.deliveryPlanList({ projectId: this.businessId }).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.deliveryplan = data.data.list;
          this.supplementList = data.data.supplementList;
          this.conentFlag = !data.data.templateError;
          this.conentNoFlag = data.data.templateError;
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: data.msg });
        }
      }
    );
    // 设置默认选中项
    this.selectedMaterielTypes = { 'name': '全部', 'code': 0 };
    this.materielTypes = [{ 'name': '全部', 'code': 0 }];
    // 获取物料品类
    this.constructioninfoService.getProductType().then(
      data => {
        if (data.result) {
          this.materielTypes = data.data;
          this.materielTypes = [{ 'name': '全部', 'code': 0, label: '全部', children: this.materielTypes }];
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: '物料品类加载失败！' });
        }
      }
    );
  }

  /**
   * Description: 弹出框物料信息查询
   * @Note:
   */
  searchCheck() {
    if (this.selectedMaterielTypes && this.selectedMaterielTypes.code !== this.materielTypes[0].code) {
      this.searchObj['categoryCode'] = this.selectedMaterielTypes.code;
    } else {
      delete this.searchObj['categoryCode'];
    }
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.pageSize = 20;
    this.pageNo = 1;
    this.searchMaterialInfoData(this.searchObj);
  }

  /**
   * Description: 分页查询
   * @param
   */
  onPage(event) {
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.searchMaterialInfoData(this.searchObj);
  }
  /**
   * Description: 根据物料组标号查看物料信息
   * @Note:
   */
  materialInfo(infoId: any,productCode: any) {
    this.searchObj.searchValue = "";
    this.searchObj['categoryCode'] = "";
    this.searchObj['businessCode'] = this.businessCode;
    this.searchObj.infoId = infoId;
    this.searchObj.productIds = productCode;
    this.pageSize = 20;
    this.pageNo = 1;
    this.searchMaterialInfoData(this.searchObj)
  }

  /**
   * Description: 查询获取物料信息
   */
  searchMaterialInfoData(search: any) {
    this.pageSize = this.pageSize ? this.pageSize : 20;
    this.pageNo = this.pageNo ? this.pageNo : 1;
    this.constructioninfoService.getSchemequota(search, this.pageSize, this.pageNo).then(
      data => {
        if (data.result) {
          this.materielData = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.materielFlag = true;
        } else {
          this.storage.messageService.emit({ severity: 'error', detail: data.msg });
        }
      }
    );
  }

  /**
   * Description: 处罚
   * @param
   */
  punishments(id,status) {
    this.constructioninfoService.getInfoStatus(this.businessId).then(
      data =>{
        if(data.result){
          if(data.data == 'constructionstatus_180108000006'){
            this.storage.messageService.emit({ severity: 'error', detail: '该项目已中止，无法进行此操作！' });
            return false
          }else if(data.data == 'constructionstatus_180108000004'){
            this.storage.messageService.emit({ severity: 'error', detail: '该项目已竣工，无法进行此操作！' });
            return false;
          }else if(status == "deliveryproductstatus_180131000004"){
            this.storage.messageService.emit({ severity: 'error', detail: '该任务还未启动，无法进行此操作！' });
            return false;
          }else{
            this.punishmentsData.infoId = this.data.id;
            this.punishmentsData.triggerType = 'deliveryId';
            this.punishmentsData.triggerId = id;
            this.isPunishShow = true;
          }
        }
      }
    );
  }

  /**
   * 修改发起处罚
   */
  changePunishShow(event) {
    this.isPunishShow = event;
  }

}
