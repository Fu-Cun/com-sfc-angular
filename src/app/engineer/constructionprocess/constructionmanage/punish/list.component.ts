import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { TimeConfigClass } from '../../../../common/common-config';
import { Check } from '../../../../common/check';

@Component({
  selector: 'app-list-constructioninfo-punish',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListConstructioninfoPunishComponent implements OnInit {
  @Input() data; // 商机id和商机编号
  sumData: any = {}; // 金额数据
  listData;
  cols; // 表格头部
  infoId;
  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
    this.cols = [
      { field: 'code', header: '处罚单号', width: '120px', href: '/saas/engineer/punishrecordmanage/punishrecord/' },
      {
        field: 'responsibilitier', width: '120px', header: '责任人', frozen: false, sortable: true,
        fun: (data, field) => data[field] + ' ' + storage.dicFilter('user_type', data['responsibilitierType'])
      },
      { field: 'punishTypeText', width: '100px', header: '处罚类型', sortable: true, tem: true },
      { field: 'punishReason', width: '150px', header: '罚款原因', frozen: false, sortable: true, tem: true },
      { field: 'pubishAmout', width: '100px', header: '罚款金额/元', frozen: false, sortable: true },
      { field: 'freeAmount', width: '100px', header: '免额/元', frozen: false, sortable: true },
      { field: 'factAmount', width: '100px', header: '实际金额/元', sortable: true, tem: true },
      {
        field: 'creater', width: '120px', header: '创建人', frozen: false, sortable: true,
        fun: (data, field) => data[field] ? data[field] + ' ' + storage.dicFilter('user_type', data['userType']) : ''
      },
      { field: 'statusText', width: '90px', header: '状态', sortable: true, tem: true },
    ];
  }
  ngOnInit() {
    this.storage.loading();
    // 商机id 和商机编号
    // 整改表格初始化

    // 获取路由id
    this.routeInfo.params.subscribe((params) => {
      this.infoId = params['id'];
    });
    this.constructioninfoService.punishList(this.infoId).then(
      data => {
        if (data.result) {
          this.storage.loadout();
          this.listData = data.data.list;
          this.sumData = data.data.sumAmout;
        } else {
          this.storage.loadout();
          this.listData = [];
        }
      }
    );
  }

}
