import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-orderinfo',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListConstructioninfoOrderinfoComponent implements OnInit {
  @Input() data; // 商机id和商机编号
  businessOpportunityId: any;  //商机id
  businessOpportunityCode: any;  //商机code

  cols: any = []; // 表格头部
  colsSelected: any = {}; // 表格选中的显示列
  pageSize: any; // 分页大小
  pageNo: any; // 当前页
  totalRecords: any; // 数据条数
  loading: boolean; // 表格数据是否加载中
  listData: any = []; // 数据
  searchObj: any = {}; // 搜索条件
  selectedRowData: any = []; // 选中行数据

  orderStatus: any = [];  //状态
  selectStatus: any = {}; //选中的状态

  materialBatch: any = [];  //物料批次
  selectBatch: any = {};  //选中的批次

  orderProductTypes: any = []; // 订单商品类型
  selectType: any = {};  // 选择的类型

  orderDefaultSelect: any = {code: '', name: '全部'};
  orderTypes: any = [];  //订单类型
  selectOrderType: any = {};  //选择的订单类型

  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
    // 表头
    this.cols = [
      { field: 'goodsCode', header: '商品编号', width: '100px', frozen: false, sortable: true, tem: true },
      { field: 'goodsName', header: '名称', frozen: false, sortable: true },
      { field: 'goodsQuantity', header: '数量', sortable: true, tem: true },
      { field: 'usePosition', header: '使用位置', sortable: true },
      { field: 'goodsType', header: '订单商品分类', sortable: true, dic: 'orderGoodsType' },
      { field: 'batchNum', header: '所属配送批次', sortable: true, tem: true },
      { field: 'orderCode', header: '订单编号', sortable: true, tem: true },
      { field: 'status', header: '状态', sortable: true, dic: 'orderStatus' },
      { field: 'orderType', header: '订单类型', sortable: true, dic: 'orderType' },
      { field: 'goodReceiptTime', header: '收货时间', sortable: true },
      { field: 'newestLogistics', header: '最新物流', sortable: true },
    ];
    this.colsSelected = this.cols;
  }
  ngOnInit() {
    // 商机id 和商机编号
    this.orderTypes = [this.orderDefaultSelect, ...this.storage.getDic('orderType')];
    this.selectOrderType = this.orderDefaultSelect;
    this.storage.loading();
    this.businessOpportunityId = this.data.id;
    this.businessOpportunityCode = this.data.code;
    this.searchObj['businessOpportunityCode'] = this.businessOpportunityCode;
    // this.searchObj['businessOpportunityCode'] = '50702140400083000';
    //初始化订单列表数据
    this.constructioninfoService.getSelectionOrder(this.businessOpportunityId).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.orderStatus = data.data.orderStatus;
          this.orderStatus = [{ code: '', name: '全部' }, ...this.orderStatus];
          this.orderProductTypes = data.data['orderGoodsType'];
          this.orderProductTypes = [{ code: '', name: '全部' }, ...this.orderProductTypes];
          this.materialBatch = [{ 'name': '全部', 'code': '' }];
          let batchList = data.data['batchList'];
          if (batchList) {
            for (let index of batchList) {
              this['materialBatch'] = [...this['materialBatch'], { 'name': '第' + index.sortNum + '批', 'code': index.sortNum }];
            }
          }
          let replenishmentBatchList = data.data['replenishmentBatchList'];
          if (replenishmentBatchList) {
            for (let index of replenishmentBatchList) {
              this['materialBatch'] = [...this['materialBatch'], { 'name': '第' + index.sortNum + '批补货', 'code': index.sortNum }];
            }
          }
          // 设置默认选中项
          let select = { 'name': '全部', 'code': '' };
          this.selectBatch = select;
          this.selectType = select;
          this.selectStatus = select;
        } else {
        }
      }
    );
  }

  /**
   * Description: 获取订单列表
   * @Note:
   */
  getOrderList() {
    this.constructioninfoService.getOrderList(this.searchObj, this.pageSize, this.pageNo).then(
      data => {
        this.loading = false;
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );
  }

  /**
   * dataTable分页查询方法
   * @param event
   */
  onPage(event) {
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.getOrderList()
  }

  /**
   * 搜索方法
   */
  search() {
    if (this.selectStatus && this.selectStatus.code !== this.orderStatus[0].code) {
      this.searchObj['status'] = this.selectStatus.code;
    } else {
      delete this.searchObj['status'];
    }
    if (this.selectBatch && this.selectBatch.code !== this.materialBatch[0].code) {
      if(this.selectBatch.name.indexOf("补货") > 0){
        this.searchObj['orderType'] = 'orderType_append'
      }else{
        this.searchObj['orderType'] = 'orderType_normal'
      }
      this.searchObj['batchNum'] = this.selectBatch.code;
    } else {
      delete this.searchObj['batchNum'];
    }
    if (this.selectType && this.selectType.code !== this.orderProductTypes[0].code) {
      this.searchObj['goodsType'] = this.selectType.code;
    } else {
      delete this.searchObj['goodsType'];
    }
    if (this.selectOrderType && this.selectOrderType.code !== this.orderTypes[0].code) {
      this.searchObj['orderType'] = this.selectOrderType.code;
    } else {
      delete this.searchObj['orderType'];
    }
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    } else {
      if (this.searchObj['searchValue'].length > 50) {
        this.storage.messageService.emit({ severity: 'warn', detail: '名称/编号 50个字符内' });
        return;
      }
    }
    this.pageSize = 10;
    this.pageNo = 1;
    this.getOrderList();
  }

  ngOnDestroy() {
    this.storage.loadout();
  }

}
