import { Component, OnInit, Input, Output } from '@angular/core';



@Component({
  selector: 'app-div-table',
  templateUrl: './divtable.component.html',
  styleUrls: ['./divtable.component.css'],
})

export class DivTableComponent implements OnInit {
  @Input() values: any[] = []; // 数据
  @Input() tableHead: any[] = []; // 表格头部
  @Input() searchData: string // 查询条件
  constructor() {
  }

  getResolveData(data, item, searchName?) {
    if (item.fun) {
      if (searchName) {
        return item.fun(data, searchName);
      } else {
        return item.fun(data, item.field);
      }

    } else {
      return data[item.field];
    }
  }

  ngOnInit() {
  }
}
