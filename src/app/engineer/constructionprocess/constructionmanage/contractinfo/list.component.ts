import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-contractinfo',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListConstructioninfoContractinfoComponent implements OnInit {
  @Input() data; // 商机id和商机编号
  businessId: any;  // 商机id
  businessCode: any;  //商机code

  item: any = {};  // 项目信息
  itemCol: any = []; // 项目表头

  contractCol: any = []; // 合同信息表头
  contract: any = {};  // 合同信息
  customer: any = {};  // 客户信息
  customerCol: any = []; // 客户信息表头

  payDataCol: any = []; // 支付清单表头
  listData; // 支付清单支付清单

  constructor(private router: Router, private routeInfo: ActivatedRoute, private storage: StorageService,
    private constructioninfoService: ConstructioninfoService) {
    // 项目表头
    this.itemCol = [
      { field: 'code', title: '项目编号' },
      { field: 'designStyle', title: '设计风格',fun: (field) => this.storage.dicFilter('favorstyle', this.item[field])},
      { field: 'name', title: '项目名称' },
      { field: 'organization', title: '所属组织' },
      { field: 'decorationAddress', title: '装修地址' },
      { field: 'priceTemplateName', title: '报价套餐' },
      { field: 'decorationArea', title: '小区' },
      { field: 'constructionTemplateName', title: '施工模板' },
      { field: 'houseArea', title: '房屋面积',fun: (field) => this.item[field] ? this.item[field] + " m²" : '' },
      { field: 'factStartTime', title: '实际开工日期' },
      { field: 'houseLayout', title: '户型' },
      { field: 'factEndTime', title: '实际竣工日期' },
    ];
    // 客户表头
    this.customerCol = [
      { field: 'name', title: '客户姓名' },
      { field: 'phone', title: '手机号' },
      { field: 'sex', title: '性别' ,fun: (field) => this.storage.dicFilter('sex', this.customer[field])},
      { field: 'address', title: '住址' },
      { field: 'lable', title: '标签' },
      { field: 'g', title: '' },
    ];
    // 合同表头
    this.contractCol = [
      { field: 'contractCode', title: '合同编号' },
      { field: 'contractStartTime', title: '合同开工日期' },
      { field: 'contractMoney', title: '合同金额' ,fun: (field) => this.contract[field] ? this.contract[field] + "元" : ''},
      { field: 'contractEndTime', title: '合同竣工日期' },
    ];
    // 支付清单表头
    this.payDataCol = [
      { field: 'fund', header: '款项', width: '92px' },
      { field: 'amount', header: '金额/元', width: '100px' },
      { field: 'payStatus', header: '状态', width: '100px' },
    ];

  }

  ngOnInit() {
    this.storage.loading();
    // 商机id 和商机编号
    // 获取路由id
    this.businessId = this.data.id;
    this.businessCode = this.data.code;
    this.constructioninfoService.getContractInfo(this.businessCode).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.item = data.data.item;
          this.contract = data.data.contract;
          this.listData = data.data.contract['payList'];
          this.customer = data.data.customer;
        } else {
        }
      }
    );
  }

  ngOnDestroy() {
    this.storage.loadout();
  }
}
