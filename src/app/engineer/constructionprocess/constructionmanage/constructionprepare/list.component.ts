import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-constructionprepare',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})
export class ListConstructioninfoPrepareComponent implements OnInit {
  @Input() data; // 商机id和商机编号
  personCols: any[] = []; // 人员到场情况列
  installationCols: any[] = []; // 水电设施情况列
  houseOriginalCols: any[] = []; // 房屋原始情况列
  definitudeCols: any[] = []; // 交底结果列

  infoId;
  prepareData: any= {}; // 施工准备信息
  houseData = []; // 房屋原始信息

  itemCol: any = []; // 施工时间
  durationPer: any = {};  // 施工时间数据

  personList: any[] = []; // 人员到场情况数据
  installationList: any[] = []; // 水电设施情况数据
  houseOriginalList: any[] = []; // 房屋原始情况数据
  definitudeList: any[] = []; // 交底结果数据

  decorationPermitImages = []; // 装修许可证
  elevatorPermitImages = []; // 电梯使用许可证
  constructionPermitImages = []; // 施工许可证
  personalAttendImages = []; // 施工许可证

  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    public storage: StorageService) {
    this.personCols = [
      { field: 'sortNum', header: '序号', width: '100px', sort: true },
      { field: 'name', header: '姓名', width: '150px', },
      { field: 'userTypeText', header: '用户类型', width: '150px', },
      { field: 'isArrived', header: '是否到场', width: '150px', fun: (data, field) => data[field] == null ? '' : (data[field] ? '已到场' : '未到场') }
    ];

    this.itemCol = [
      { field: 'constructionStartTimeText', title: '开工日期' },
      { field: 'isWeekendWorking', title: '周末', fun: (field) => this.durationPer[field] == null ? '' : (this.durationPer[field] ? '允许施工' : '不允许施工') },
      { field: 'isHolidayWorking', title: '节假日', fun: (field) => this.durationPer[field] == null ? '' : (this.durationPer[field] ? '允许施工' : '不允许施工') },
      { field: 'durationPerDay', title: '每天施工时长' },
    ];

    // this.personDataList =[];
    this.installationCols = [
      { field: 'sortNum', header: '序号', width: '100px', sort: true },
      { field: 'equipmentText', header: '水电设施', width: '150px' },
      { field: 'isPass', header: '是否通畅', width: '150px', fun: (data, field) => data[field] == null ? '' : (data[field] ? '通' : '不通') }
    ];

    this.houseOriginalCols = [
      { field: 'sortNum', header: '序号', width: '100px', sort: true },
      { field: 'areaText', header: '位置', width: '150px', frozen: false, sortable: true },
      { field: 'isFlawText', header: '是否缺陷', width: '150px', frozen: false, sortable: true },
      { field: 'areaNumDescription', header: '部位数量说明', width: '300px', frozen: false, sortable: true }
    ];

    this.definitudeCols = [
      { field: 'sortNum', header: '序号', width: '100px', sort: true },
      { field: 'result', header: '交底结果', width: '100px', fun: (data, field) => data[field] ? '成功' : '失败' },
      { field: 'description', header: '备注说明/失败原因', width: '200px', frozen: false, sortable: true },
      { field: 'resultStatusText', header: '提交后状态', width: '100px', frozen: false, sortable: true },
      { field: 'submitUser', header: '提交人', width: '150px', frozen: false, sortable: true },
      { field: 'nextDisclosureTimeText', header: '下次交底时间', width: '180px', frozen: false, sortable: true },
      { field: 'submitTimeText', header: '提交时间', width: '180px', frozen: false, sortable: true }
    ];

  }
  ngOnInit() {
    this.storage.loading();
    // 商机id 和商机编号
    this.routeInfo.params.subscribe((params) => {
      this.infoId = params.id;
      this.constructioninfoService.prepareDetail(this.infoId).then(
        data => {
          if (data.result) {
            this.storage.loadout();
            this.prepareData = data.data.detail;
            this.durationPer = data.data.detail;
            this.personList = data.data.people;
            this.installationList = data.data.equipment;
            this.houseOriginalList = data.data.house;
            this.definitudeList = data.data.result;
            if (this.prepareData['decorateAttach']) {
              for (let item of this.prepareData['decorateAttach'].split(',')) {
                this['decorationPermitImages'] = [...this['decorationPermitImages'], {
                  source: item, thumbnail: item,
                  width: '100px', maxWidth: '800px', title: ''
                }];
              }
            }
            if (this.prepareData['elevatorAttach']) {
              for (let item of this.prepareData['elevatorAttach'].split(',')) {
                this['elevatorPermitImages'] = [...this['elevatorPermitImages'], {
                  source: item, thumbnail: item,
                  width: '100px', maxWidth: '800px', title: ''
                }];
              }
            }
            if (this.prepareData['constructAttach']) {
              for (let item of this.prepareData['constructAttach'].split(',')) {
                this['constructionPermitImages'] = [...this['constructionPermitImages'], {
                  source: item, thumbnail: item,
                  width: '100px', maxWidth: '800px', title: ''
                }];
              }
            }
            if (this.prepareData['localePictures']) {
              for (let item of this.prepareData['localePictures'].split(',')) {
                this['personalAttendImages'] = [...this['personalAttendImages'], {
                  source: item, thumbnail: item,
                  width: '100px', maxWidth: '800px', title: ''
                }];
              }
            }
          } else {
            this.storage.loadout();
          }
        }
      );
    });

  }

}
