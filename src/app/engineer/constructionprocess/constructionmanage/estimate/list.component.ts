import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-estimate',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListConstructioninfoEstimateComponent implements OnInit {
  listData: any[] = [];
  totalRecords: number; // 数据条数
  @Input() data; // 商机id和商机编号
  conentFlag: boolean = false;
  conentNoFlag: boolean = false;
  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
  }
  ngOnInit() {
    // 商机id 和商机编号
    this.storage.loading();
    // 模板表格初始化
    this.constructioninfoService.estimateData(this.data.code).then(
      data => {
        if (data.result) {
          this.storage.loadout();
          this.listData = data.data.list;
        } else {
          this.storage.loadout();
          this.listData = [];
        }
        this.conentFlag = this.listData.length > 0;
        this.conentNoFlag = this.listData.length <= 0;
      }
    );
  }

}
