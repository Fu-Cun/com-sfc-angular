import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TimeConfigClass } from '../../../../common/common-config';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';

@Component({
  selector: 'app-info-constructioninfo-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.css'],
  providers: [ConstructioninfoService],
})

export class InfoConstructioninfoStageComponent implements OnInit {

  businessId:any;  //商机id

  cars: any = [];
  adviserHead: any[] = []; // 顾问标题头
  adviserData: any = {}; // 顾问数据
  stageId;// 施工阶段id

  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    public storage: StorageService) {

  }
  ngOnInit() {
    this.storage.loading()
      this.adviserHead = [
        { title: '预计开始日期', field: 'planStartTimeText' },
        { title: '实际开始日期', field: 'factStartTimeText'},
        { title: '预计完成日期', field: 'planEndTimeText' },
        { title: '实际完成时间', field: 'factEndTimeText' },
        { title: '预计施工时长/天', field: 'planConstructDays' },
        { title: '实际施工时长/天', field: 'factConstructDays' },
        { title: '是否已延期', field: 'isDelayText' },
        { title: '延期天数/天', field: 'delayDays' }
      ];

    // 获取路由id
    this.routeInfo.params.subscribe((params) => {
      this.stageId = params.id;
      this.businessId = params.infoId;
      // 施工阶段初始化
      this.constructioninfoService.getStageDetail(this.stageId).then(
        data => {
          this.storage.loadout();
          if (data.result) {
            this.adviserData = data.data.detail
            if(this.adviserData) {
              this.adviserData.isDelayText = this.adviserData.isDelay ? '是' : '否'
            }
            this.cars = data.data.node
          } else {
            this.storage.messageService.emit({severity:'error', detail:data.msg})
          }
        }
      );
    });
  }

  /**
   * tabview能否进行切换
   */
  onChange(event) {
      event.open();
  }


  /**
   * 编辑
   */
  info(code) {
    if(code==undefined){
      return false;
    };
    this.router.navigate(['saas/engineer/constructionprocess/constructionmanage/constructionprogress/node/'+'code']);
  }

}
