import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-schedule',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})
export class ListConstructioninfoScheduleComponent implements OnInit {
  @Input() data; // 商机id和商机编号

  businessId:any;  //商机id
  searchObj: any = {}; // 搜索条件
  progressType: any[] = [];//类型【字典】
  progressStatus: any[] = [];//状态【字典】
  progressIsDeplay: any[] = [];//【字典】

  selectedType: any; // 选中的组织数据
  selectedStatus: any; // 选中的组织数据

  loading: boolean; // 表格数据是否加载中
  pageSize: number; // 分页大小
  pageNo: number; // 当前页
  totalRecords: number;
  cols; // 表格头部
  dataList: any[] = [];// 列表
  statusDefault: any = { code: '', name: '全部' }; // 状态默认值
  selectedRowData = []; // 选中行数据
  processInfoId;// 施工信息id

  decorationStartDateMax: Date; // 合同开始最大的时间
  decorationEndDateMin: Date; // 合同结束最小的时间
  emptyMessage: string = ''; // 没数据的提示语

  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
  }
  ngOnInit() {
    // 表头
    this.cols = [
      { field: 'sortNum', header: '序号', width: '60px', tem: true, frozen: false, sortable: true },
      { field: 'name', header: '名称', width: '190px', frozen: false, sortable: true, tem: true },
      { field: 'selectTypeText', header: '类型', width: '80px', sortable: true },
      { field: 'statusText', header: '状态', width: '200px', sortable: true },
      { field: 'planStartTimeText', header: '预计开始日期', width: '150px', sortable: true },
      { field: 'planEndTimeText', header: '预计完成日期', width: '150px', sortable: true },
      { field: 'factStartTimeText', header: '实际开始日期', width: '150px', sortable: true },
      { field: 'factEndTimeText', header: '实际完成日期', width: '150px', sortable: true },
      { field: 'isDelay', header: '是否已延期', width: '100px', sortable: true, tem: true },
      { field: 'isModify', header: '是否被整改', width: '100px', sortable: true, tem: true },
    ];

    this.progressIsDeplay = [{ label: '已完成', value: 1 },
    { label: '延期未处理', value: 2 },
    { label: '被整改', value: 3 }]

    this.constructioninfoService.getScheduleTypeSelection().then(
      data => {
        if (data.result) {
          let scheduleTypeCode = data.data
          this.progressType = [this.statusDefault, ...scheduleTypeCode]
        }
      }
    );
    this.progressStatus = [this.statusDefault];

    // 获取路由id
    this.routeInfo.params.subscribe((params) => {
      this.processInfoId = params.id;
      this.search();
    });
    this.businessId = this.data.id;


  }

  /**
   * 加载列表
   * @param event
   */
  loadData(event) {
    this.pageSize = + event.rows;
    this.pageNo = event.first / event.rows + 1;
    this.search();
  }
  /**
   * 搜索方法
   */
  search() {
    this.storage.loading();

    // 搜索框
    //类型
    if (this.selectedType && this.selectedType.code) {
      this.searchObj['selectType'] = this.selectedType.code;
      //状态
      if (this.selectedStatus) {
        this.searchObj['status'] = this.selectedStatus;
      } else {
        delete this.searchObj['status']
      }
    } else {
      delete this.searchObj['selectType']
    }

    this.constructioninfoService.getSchedulePage({ infoId: this.processInfoId, pageSize: this.pageSize, pageNo: this.pageNo, ...this.searchObj }).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.loading = false;
          this.dataList = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.emptyMessage = '没有搜到您想要的数据';
          if (data.data.templateError) {
            this.emptyMessage = '该项目施工模板匹配异常，请先去“模板管理”中维护适用的施工模板，再重新匹配后即可正常派单';
          } else {
            if (data.data.stageError) {
              this.emptyMessage = '还未提交排期计划';
            }
          }
        } else {
          this.loading = false;
          this.dataList = [];
          this.totalRecords = 0;
        }
      }
    );

    // 按钮数量初始化
    this.constructioninfoService.getScheduleStatusCount({ infoId: this.processInfoId }).then(
      data => {
        if (data.result) {
          let countData = data.data
          this.progressIsDeplay = [{ label: '已完成', value: 1, account: countData.completeCount, showCount: true },
          { label: '延期未处理', value: 2, account: countData.delayCount, showCount: true },
          { label: '被整改', value: 3, account: countData.modifyCount, showCount: true }]
        } else {
        }
      }
    );
  }

  /**
   * 修改施工进度类型
   */
  changeProgressType() {
    let type = this.selectedType.code
    this.progressStatus = [this.statusDefault];
    if (type == 'process') {
      this.progressStatus = [this.statusDefault, ...this.storage.getDic('constructionstagestatus')];
    } else if (type == 'node') {
      this.progressStatus = [this.statusDefault, ...this.storage.getDic('nodestatus')];
    } else if (type == 'check') {
      this.progressStatus = [this.statusDefault, ...this.storage.getDic('checkstatus')];
    } else {
      this.progressStatus = [this.statusDefault];
    }
  }

  /**
   * 修改日期控件结束时间
   * @param type
   */
  updateCalendarStartTime(type?) {
    this.decorationEndDateMin = new Date(this.searchObj.planEndTimeStart);
  }

  /**
   *  修改日期控件开始时间
   * @param type
   */
  updateCalendarEndTime(type?) {
    this.decorationStartDateMax = new Date(this.searchObj.planEndTimeEnd);
  }

  /**
   * 时间控件清空，处理最大值
   * @param type
   */
  clearStart(type?) {
    this.decorationEndDateMin = undefined;
  }
  /**
   * 时间控件清空，处理最小值
   * @param type
   */
  clearEnd(type?) {
    this.decorationStartDateMax = undefined;
  }

}
