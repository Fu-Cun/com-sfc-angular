import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
import {error} from "util";
@Component({
  selector: 'app-list-constructioninfo-designdrawing',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListDesigndrawingComponent implements OnInit {
  @Input() data; // 商机id和商机编号
  businessCode: any;  //项目code
  effectPicture: any = [];  //效果图
  panorama: any = [];  //全景图
  workingDrawing: any = [];  //施工图
  drawingofconcealedwo: any = [];  //隐蔽工程图
  showPicDialog: boolean = false; // 图片详情
  images: any[] = []; // 图片集合
  activeIndex: number = 0;
  constructor(private router: Router, private routeInfo: ActivatedRoute,
    private storage: StorageService, private constructioninfoService: ConstructioninfoService) {
  }
  ngOnInit() {
    // 商机id 和商机编号
    this.storage.loading();
    this.businessCode = this.data.code;
    this.constructioninfoService.getEffectPicture(this.businessCode).then(
      data=>{
        this.storage.loadout();
        if (data.result) {
          for (let item of data.data) {
            this['effectPicture'] = [...this['effectPicture'], {
              source: item.img
            }];
          }
        }
      }
    )
    this.constructioninfoService.getPanoramaCharts(this.businessCode).then(
      data=>{
        this.storage.loadout();
        if (data.result) {
          for (let item of [data.data]) {
            this['drawingofconcealedwo'] = [...this['drawingofconcealedwo'], {
              source: item
            }];
          }
        }
      }
    )
    this.constructioninfoService.getDesignCharts(this.businessCode).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          for (let item of data.data.panorama) {
            this['panorama'] = [...this['panorama'], {
              source: item
            }];
          }
          for (let item of data.data.workingDrawing) {
            this['workingDrawing'] = [...this['workingDrawing'], {
              source: item
            }];
          }
         
        }
      }
    );
  }
  // 效果图显示点击切换
  showPic(i) {
    this.images = [];
    this.showPicDialog = true;
    this.activeIndex = 0;
    let img = [];
    switch (i){
      case 1:
        img = this.effectPicture;
        break;
      case 2:
        img = this.panorama;
        break;
      case 3:
        img = this.workingDrawing;
        break;
      case 4:
        img = this.drawingofconcealedwo;
        break;
    }
    img.map(val => this.images = [...this.images, { source: val.source, title: '' }]);
  }

}
