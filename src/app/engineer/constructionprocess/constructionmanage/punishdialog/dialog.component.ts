import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { TimeConfigClass } from '../../../../common/common-config';
import { Check } from '../../../../common/check';

@Component({
  selector: 'app-list-punish-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  providers: [ConstructioninfoService]
})

export class PunishDialogComponent implements OnInit {
  @Input() isShow: boolean = false; // 弹窗是否显示
  @Output() changeIsShow = new EventEmitter(); // 更新父组件弹窗开启状态的事件
  @Input() data: any = {}; // 父组件传过来的处罚数据
  submited: boolean = false; // 提交按钮启用状态
  personLiableOption: any[] = []; // 责任人数据
  formObj: any = {}; // 添加条件
  punishTypeOption: any[] = []; // 处罚类型数据
  personLiableSeleted: any; // 选中的责任人
  objcetCodeAndName: string = ''; // 项目边和和项目名称
  fineRight: boolean = true; // 处罚金额错误是否显示
  punishRemarkRight: boolean = true; // 处罚原因错误是否显示

  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
  }
  ngOnInit() {

    // 处罚类型
    this.punishTypeOption = [...this.storage.getDic('punishtype')];
    // 获取项目名称
    if (this.data.infoId) {
      this.storage.loading();
      this.constructioninfoService.getProjectNameByInfoId({ infoId: this.data.infoId }).then((result) => {
        if (result.result) {
          this.objcetCodeAndName = result.data.businessOpportunityName;
          this.personLiableOption = result.data.responsibilitierList;
          if (this.personLiableOption.length > 0) {
            this.personLiableSeleted = this.personLiableOption[0];
          }

        } else {
          this.storage.messageService.emit({ severity: 'error', detail: result.msg });
        }
        this.storage.loadout();
      });
    } else {
      this.storage.messageService.emit({ severity: 'error', detail: '项目id获取失败' });
    }


  }
  /**
   * 关闭弹出窗口
   */
  close() {
    this.changeIsShow.emit(false);
  }
  /**
   * 保存处罚信息
   */
  savePunish() {
    this.constructioninfoService.getInfoStatus(this.data.infoId).then(
      data =>{
        if(data.result){
          if(data.data == 'constructionstatus_180108000006'){
            this.storage.messageService.emit({ severity: 'error', detail: '该项目已中止，无法进行此操作！' });
            return false
          }else if(data.data == 'constructionstatus_180108000004'){
            this.storage.messageService.emit({ severity: 'error', detail: '该项目已竣工，无法进行此操作！' });
            return false;
          }else{
            let param = {};
            // 负责人工信息
            if (this.personLiableSeleted && this.personLiableSeleted.responsibilitierId) {
              param = this.personLiableSeleted;
            }
            Object.assign(param, this.formObj, this.data);
            this.storage.loading();
            this.constructioninfoService.savePunish(param).then((result) => {
              this.storage.loadout();
              if (result.result) {
                this.objcetCodeAndName = result.data.businessOpportunityName;
                this.personLiableOption = result.data.responsibilitierList;
                this.storage.messageService.emit({ severity: 'success', detail: result.msg });
                this.changeIsShow.emit(false);
              } else {
                  this.storage.messageService.emit({ severity: 'error', detail: result.msg });
              }
            });
          }
        }
      }
    );
  }

}
