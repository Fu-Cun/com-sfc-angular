import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-inspection',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListConstructioninfoInspectionComponent implements OnInit {
  @Input() data; // 商机id和商机编号

  businessOpportunityId: any;  //商机id

  inspectionList: any = [];  //巡检记录

  workerRealNameCol: any = [];  //工人实名制表头

  carpentryInstallReCheckCol: any = [];  //木作安装复核

  modifyRecordCol: any = [];  //整改记录表头
  modifyRecord: any = [];  //整改记录

  punishCol: any = [];  //处罚记录表头

  civilizationConstruction_redis: any = [];  //文明施工
  civilizationConstruction: any = [];  //文明施工

  productProtectTag_redis: any = [];  //成品保护标识
  productProtectTag: any = [];  //成品保护标识

  constructionSafety_redis: any = [];  //施工安全
  constructionSafety: any = [];  //施工安全

  waterElectricityStandard_redis: any = [];  // 水电工艺规范
  waterElectricityStandard: any = [];  // 水电工艺规范

  bricklayersStandard_redis: any = [];  // 瓦工工艺规范
  bricklayersStandard: any = [];  // 瓦工工艺规范

  carpentersStandard_redis: any = [];  // 木工工艺规范
  carpentersStandard: any = [];  // 木工工艺规范

  oilworkerStandard_redis: any = [];  // 油工工艺规范
  oilworkerStandard: any = [];  // 油工工艺规范

  isPunishShow: boolean = false; // 发起处罚弹出框
  punishmentsData: any = {}; // 传给处罚弹窗的数据
  conentFlag: boolean = false;
  conentNoFlag: boolean = false;


  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {

    // 工人实名制表头
    this.workerRealNameCol = [
      { field: 'waterElectricity', title: '水电', fun: (field, data) => this.dicDataFilter('realName', data[field]) },
      { field: 'carpenters', title: '木工', fun: (field, data) => this.dicDataFilter('workerRealName', data[field]) },
      { field: 'bricklayers', title: '瓦工', fun: (field, data) => this.dicDataFilter('workerRealName', data[field]) },
      { field: 'oilworker', title: '油工', fun: (field, data) => this.dicDataFilter('workerRealName', data[field]) },
      { field: 'elseType', title: '其他', fun: (field, data) => this.dicDataFilter('workerRealName', data[field]) },
      { field: 'g', title: '' },
    ];
    // 木作安装复核表头
    this.carpentryInstallReCheckCol = [
      { field: 'houseDoor', title: '入户门洞/mm' },
      { field: 'kitchenDoor', title: '厨卫门洞/mm' },
      { field: 'bedroomDoor', title: '主次卧门洞/mm' },
      { field: 'cabinetArea', title: '橱柜区域/mm' },
    ];
    // 整改记录表头
    this.modifyRecordCol = [
      {
        field: 'modifyRecord', header: '整改项', width: '111px', href: '/saas/engineer/constructionprocess/constructionmanage/modifyrecord/',
        search: 'id', fun: (data, field) =>   data[field]
      },
      { field: 'isCustomScan', width: '100px', header: '客户是否可见', fun: (data, field) => data[field] ? '是' : '否' },
      { field: 'isDelay', width: '80px', header: '是否延期', fun: (data, field) => data[field] ? '是' : '否' },
      { field: 'description', width: '200px', header: '说明' },
      { field: 'deadline', width: '100px', header: '最后期限' },
      { field: 'factFinishTime', width: '100px', header: '实际完成时间' },
      { field: 'status', header: '状态', width: '100px', fun: (data, field) => this.storage.dicFilter('modifystatus', data[field]) },
    ];
    // 处罚记录表头
    this.punishCol = [
      { field: 'code', header: '处罚单号', width: '111px', href: '/saas/engineer/punishrecordmanage/punishrecord/' },
      {
        field: 'creater', header: '责任人', width: '130px',
        fun: (data, field) => data[field] ? data[field] + ' ' + this.storage.dicFilter('user_type', data['userType']) : ''
      },
      { field: 'punishType', header: '罚款类型', width: '130px', fun: (data, field) => this.storage.dicFilter('punishtype', data[field]) },
      { field: 'punishReason', header: '罚款原因', width: '200px' },
      { field: 'pubishAmout', header: '罚款金额/元', width: '130px', },
      { field: 'freeAmount', header: '免额/元', width: '100px', },
      { field: 'factAmount', header: '实际金额/元', width: '130px', },
      { field: 'status', header: '状态', width: '94px', fun: (data, field) => this.storage.dicFilter('penaltyStatus', data[field]) },
    ];

  }
  ngOnInit() {
    this.businessOpportunityId = this.data.id;
    this.storage.loading();
    this.constructioninfoService.getSelectionInspection().then(
      data => {
        this.storage.loadout();
        if (data.result) {
          //redis缓存数据
          this.civilizationConstruction_redis = data.data.civilizationConstruction;
          this.productProtectTag_redis = data.data.productProtectTag;
          this.constructionSafety_redis = data.data.constructionSafety;
          this.waterElectricityStandard_redis = data.data.waterElectricityStandard;
          this.bricklayersStandard_redis = data.data.bricklayersStandard;
          this.carpentersStandard_redis = data.data.carpentersStandard;
          this.oilworkerStandard_redis = data.data.oilworkerStandard;
          this.constructioninfoService.inspectionList(this.businessOpportunityId).then(
            data => {
              if (data.result) {
                let listData = data.data.list;
                for (let index of listData) {
                  let civilizationConstruction_redis: any = JSON.parse(JSON.stringify(this.civilizationConstruction_redis));
                  let productProtectTag_redis: any = JSON.parse(JSON.stringify(this.productProtectTag_redis));
                  let constructionSafety_redis: any = JSON.parse(JSON.stringify(this.constructionSafety_redis));
                  let waterElectricityStandard_redis: any = JSON.parse(JSON.stringify(this.waterElectricityStandard_redis));
                  let bricklayersStandard_redis: any = JSON.parse(JSON.stringify(this.bricklayersStandard_redis));
                  let carpentersStandard_redis: any = JSON.parse(JSON.stringify(this.carpentersStandard_redis));
                  let oilworkerStandard_redis: any = JSON.parse(JSON.stringify(this.oilworkerStandard_redis));

                  index.civilizationConstruction_redis = this.dataFilter(civilizationConstruction_redis, index['civilizationConstruction']);
                  index.productProtectTag_redis = this.dataFilter(productProtectTag_redis, index['productProtectTag']);
                  index.constructionSafety_redis = this.dataFilter(constructionSafety_redis, index['constructionSafety']);
                  if (index['technologystandard']) {
                    index['technologystandard'].waterElectricityStandard_redis = this.dataFilter(waterElectricityStandard_redis, index['technologystandard']['waterElectricity']);
                    index['technologystandard'].bricklayersStandard_redis = this.dataFilter(bricklayersStandard_redis, index['technologystandard']['bricklayers']);
                    index['technologystandard'].carpentersStandard_redis = this.dataFilter(carpentersStandard_redis, index['technologystandard']['carpenters']);
                    index['technologystandard'].oilworkerStandard_redis = this.dataFilter(oilworkerStandard_redis, index['technologystandard']['oilworker']);
                  }
                }
                this.inspectionList = listData;
                this.conentFlag = this.inspectionList.length > 0;
                this.conentNoFlag = this.inspectionList.length <= 0;
              } else {
              }
            }
          );
        } else {
        }
      }
    );
  }

  /**
   * Description: 数据过滤处理
   */
  dataFilter(redisData, data) {
    if (data) {
      let dbData: any = data.toString().split(",");
      redisData.map((item) => {
        if (dbData.some(i => item['code'] == i)) {
          item['flag'] = true;
        }
      });
      redisData.sort(function (item1, item2) {
        if (item1.flag && !item2.flag) {
          return -1;
        } else {
          return 1;
        }
      });
      return redisData
    }
  }

  /**
   * Description: 字典数据查询
   * @param redisKey 字典库的key  data 数据
   */
  dicDataFilter(redisKey, data) {
    if (data) {
      let dbData: any = data.toString().split(",");
      let result: any = [];
      for (let index of dbData) {
        result.push(this.storage.dicFilter(redisKey, index));
      }
      return result.toString()
    }
    return "";
  }

  /**
   * Description: 处罚
   * @param
   */
  punishments(id) {
    this.punishmentsData.infoId = this.data.id;
    this.punishmentsData.triggerType = 'inspectionId';
    this.punishmentsData.triggerId = id;
    this.isPunishShow = true;
  }

  /**
   * 修改发起处罚
   */
  changePunishShow(event) {
    this.isPunishShow = event;
  }

  /**
   * 上传图片的回显
   */
  imgUrlData(url) {
    let imageArr: any = [];
    if (url) {
      for (let item of url.split(',')) {
        imageArr = [...imageArr, {
          source: item, thumbnail: item,
          width: '100px', maxWidth: '800px', title: ''
        }];
      }
    }
    return imageArr;
  }
}
