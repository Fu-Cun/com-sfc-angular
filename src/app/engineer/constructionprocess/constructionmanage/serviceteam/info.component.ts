import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../../constructionmanage/constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
import { TimeConfigClass } from '../../../../common/common-config';

@Component({
  selector: 'app-serviceteam-punish',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [ConstructioninfoService],
})
export class ServiceteamInfoComponent implements OnInit {

  operationsCol: any = [];  // 操作人记录表头
  listData;
  dataPro;
  pageSize; // 分页大小
  pageNo; // 当前页
  totalRecords; // 数据条数
  paramsData: any = [];

  constructor(private router: Router, private routeInfo: ActivatedRoute,
    private storage: StorageService, private constructioninfoService: ConstructioninfoService) {

    this.operationsCol = [
      { field: 'constructionCode', header: '项目编号', width: '100px' },
      { field: 'estimateUserName', header: '客户', width: '80px' },
      { field: 'decorationDetailAddress', header: '装修地址', width: '200px', dic: 'modifystatus' },
      { field: 'contructionStatusName', header: '工程进度', width: '80px', dic: 'user_type' },
      { field: 'supervisorScore', header: '监理评分', width: '100px', tem: true },
      { field: 'constructionScore', header: '工程评分', width: '100px', tem: true },
      { field: 'estimateText', header: '评语', width: '100px' },
      { field: 'estimateTime', header: '评价时间', width: '120px', tem: true },
      { field: 'estimateManType', header: '评价人', width: '100px' },
    ];
  }
  ngOnInit() {
    this.routeInfo.params.subscribe((params) => {
      this.paramsData = params;
      if (params.type === 'team') {
        this.operationsCol = [
          { field: 'constructionCode', header: '项目编号', width: '100px' },
          { field: 'estimateUserName', header: '客户', width: '80px' },
          { field: 'decorationDetailAddress', header: '装修地址', width: '200px', dic: 'modifystatus' },
          { field: 'contructionStatusName', header: '工程进度', width: '80px', dic: 'user_type' },
          { field: 'foremanScore', header: '项目经理评分', width: '100px', tem: true },
          { field: 'constructionScore', header: '工程评分', width: '100px', tem: true },
          { field: 'estimateText', header: '评语', width: '100px' },
          { field: 'estimateTime', header: '评价时间', width: '120px', tem: true },
          { field: 'estimateManType', header: '评价人', width: '100px', tem: true },
        ];
      }
      this.storage.loading();
      this.constructioninfoService.viewServiceUserData(params.id, params.type).then(
        data => {
          if (data.result) {
            let userData = data.data
            this.dataPro = userData
            this.constructioninfoService.viewEstimateData(userData.businessOpportunityCode, params.type, 20, 1,params.userCode).then(
              data => {
                if (data.result) {
                  this.storage.loadout();
                  this.listData = data.data.list;
                  this.totalRecords = data.data.totalCount;
                } else {
                  this.storage.loadout();
                  this.listData = [];
                  this.totalRecords = 0;
                }
              }
            );

          } else {
          }
        }
      );

    });

  }

  /**
* dataTable分页查询方法
* @param event
*/
  onPage(event) {

    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.storage.loading();
    this.constructioninfoService.viewEstimateData(this.paramsData.code, this.paramsData.type, this.pageSize, this.pageNo,this.paramsData.userCode).then(
      data => {
        if (data.result) {
          this.storage.loadout();
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.storage.loadout();
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );

  }

}
