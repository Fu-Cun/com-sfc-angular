import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-serviceteam',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})
export class ListConstructioninfoServiceteamComponent implements OnInit {
  @Input() data; // 商机id和商机编号
  workerListHead = []; // 工人列表表头
  workerListData = []; // 工人数据
  penaltyManagerListData = []; // 经理处罚列表数据
  penaltySupervisorListData = []; // 监理处罚列表数据
  penaltyListHead = []; // 处罚列表表头
  adviserHead: any[] = []; // 顾问标题头
  adviserData: any = {}; // 顾问数据

  loading: boolean = true;

  sendOrderStatus: string = '';
  content: string = '';
  // 家居顾问
  houseAdviser: any = {};
  // 监理
  supervisor: any = {};
  // 工长
  foreman: any = {};
  // 工人
  worker: any = [];
  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService, private route: ActivatedRoute) {
    this.penaltyListHead = [
      { field: 'sort', header: '处罚类型', width: '150px' },
      { field: 'name', header: '处罚次数' }
    ];
    this.workerListHead = [
      { field: 'sort', header: '姓名', width: '100px' },
      { field: 'name1', header: '性别', width: '80px' },
      { field: 'sort2', header: '手机号', width: '150px' },
      { field: 'name2', header: '工种', width: '150px' },
      { field: 'sort3', header: '在施工地数量', width: '150px' },
      { field: 'name3', header: '总接单量' },
    ];

    this.adviserHead = [
      { title: '姓名', field: 'name' },
      { title: '性别', field: 'sex', fun: (field) => (this.adviserData[field] ? '男' : '女') },
      { title: '手机号', field: 'telephone' },
      { title: '所属组织', field: 'organization' }
    ];
    this.adviserData = {
      name: '小明', sex: 1, telephone: '15950155481', organization: '所属组织'
    };
    this.route.params.subscribe(params => {
      let id = params['id'];
      this.getData({ infoId: id });
    });
  }
  ngOnInit() {
    // 商机id 和商机编号
    this.penaltySupervisorListData = [
      { sort: '一般处罚', name: 1 },
      { sort: '服务类处罚', name: 5 },
      { sort: '安全事故处罚', name: 7 }
    ];
    this.workerListData = [
      { sort: '某某某', name1: '男', sort2: '13333333333', name2: '水工', sort3: 4, name3: 12 },
      { sort: '某某某', name1: '男', sort2: '13333333333', name2: '水工', sort3: 4, name3: 12 },
      { sort: '某某某', name1: '男', sort2: '13333333333', name2: '水工', sort3: 4, name3: 12 }
    ];
  }


  /**
   * 获取数据
   */
  getData(params) {
    //let params = this.searchObj;
    this.storage.loading();
    this.constructioninfoService.getServiceteam(params).then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.sendOrderStatus = data.data.sendOrderStatus;
          this.content = data.data.content;
          this.houseAdviser = data.data.houseAdviser;
          this.supervisor = data.data.supervisor;
          this.foreman = data.data.foreman;
          this.worker = data.data.worker;
        } else {
        }
      }
    );
  }
  // 查看评价
  viewEstimateData(param) {
    this.router.navigate(['/saas/engineer/constructionprocess/serviceteam/punish', this.data.code, param]);
  }

}
