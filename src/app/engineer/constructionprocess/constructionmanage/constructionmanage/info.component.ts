import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';

@Component({
  selector: 'app-info-constructioninfo',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [ConstructioninfoService],
})
export class InfoConstructioninfoComponent implements OnInit {
  currentTableIndex: number = 0; // 当前选中tab的index
  tabItems: any[] = [];
  paramOjb: any = {}; // 商机id和商机code
  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {

  }
  ngOnInit() {
    this.routeInfo.params.subscribe((params) => {
      this.paramOjb.id = params['id'];
      this.paramOjb.code = params['code'];
    });
    this.tabItems = [
      {
        label: '施工准备', command: () => {
          this.currentTableIndex = 0;
        }
      },
      {
        label: '工程进度', command: () => {
          this.currentTableIndex = 1;
        }
      },
      {
        label: '服务团队', command: () => {
          this.currentTableIndex = 2;
        }
      },
      {
        label: '整改记录', command: () => {
          this.currentTableIndex = 3;
        }
      },
      {
        label: '配送计划', command: () => {
          this.currentTableIndex = 4;
        }
      },
      {
        label: '订单信息', command: () => {
          this.currentTableIndex = 5;
        }
      },
      {
        label: '合同信息', command: () => {
          this.currentTableIndex = 6;
        }
      },
      {
        label: '设计图', command: () => {
          this.currentTableIndex = 7;
        }
      },
      {
        label: '违规罚款', command: () => {
          this.currentTableIndex = 8;
        }
      },
      {
        label: '评价', command: () => {
          this.currentTableIndex = 9;
        }
      },
      {
        label: '巡检记录', command: () => {
          this.currentTableIndex = 10;
        }
      },
    ];
  }


  /**
   * tabview能否进行切换
   */
  onChange(event) {
    this.currentTableIndex = event.index;
    event.open();
  }


}
