import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstructioninfoService } from '../constructioninfo.service';
import { StorageService } from '../../../../common/storage.service';
@Component({
  selector: 'app-list-constructioninfo-modifyrecord',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConstructioninfoService]
})

export class ListConstructioninfoModifyrecordComponent implements OnInit, OnDestroy {
  @Input() data; // 商机id和商机编号
  businessId: any; //商机id

  selectedStatus: any = {}; // 选中状态
  status: any[] = []; // 状态
  selectedIsDelay: any = {}; // 选中是否延期
  isDelay: any[] = []; // 是否延期
  selectedIsCustomScan: any = {}; // 选中用户是否可见
  isCustomScan: any[] = []; // 用户是否可见
  searchObj: any = {}; // 查询条件
  colsSelected: any[] = []; // 表格表头
  listData: any[] = []; // 表格数据
  pageSize: number; // 分页大小
  pageNo: number; // 当前页
  totalRecords: number; // 数据条数
  loading;  //表格数据加载
  constructor(private router: Router, private constructioninfoService: ConstructioninfoService, private routeInfo: ActivatedRoute,
    private storage: StorageService) {
    //整改记录表头
    this.colsSelected = [
      { field: 'sortNum', header: '序号', width: '80px', tem: true },
      { field: 'modifyRecord', header: '整改项', width: '120px', tem: true },
      { field: 'isCustomScanText', header: '客户是否可见', width: '100px' },
      { field: 'isDelayText', header: '是否延期', width: '100px' },
      { field: 'description', header: '说明', width: '120px' },
      { field: 'deadline', header: '最后期限', width: '100px' },
      { field: 'factFinishTime', header: '实际完成日期', width: '100px' },
      { field: 'status', header: '状态', dic: 'modifystatus', width: '100px' },
      { field: 'creater', header: '创建人', dic: 'user_type', width: '100px' },
      { field: 'dateCreated', header: '创建时间', width: '150px' },
    ];
  }
  ngOnInit() {
    // 商机id 和商机编号
    this.storage.loading();
    this.businessId = this.data.id;
    // 初始化整改记录列表
    this.getData();
    // 获取整改记录状态
    this.constructioninfoService.getSelection().then(
      data => {
        this.storage.loadout();
        if (data.result) {
          this.status = data.data['modifystatus'];
          this.status = [{ code: '', name: '全部' }, ...this.status];
          this.selectedStatus = { code: '', name: '全部' };
        } else {
        }
      }
    );
    // 是否延期
    this.isDelay = [
      { code: '', name: '全部' },
      { code: 'true', name: '是' },
      { code: 'false', name: '否' }
    ];
    this.selectedIsDelay = { code: '', name: '全部' };
    // 是否客户可见
    this.isCustomScan = [
      { code: '', name: '全部' },
      { code: 'true', name: '客户可见' },
      { code: 'false', name: '客户不可见' }
    ];
    this.selectedIsCustomScan = { code: '', name: '全部' };
  }

  ngOnDestroy() {
    this.storage.loadout();
  }
  /**
   * 查询方法
   */
  search() {
    if (this.selectedIsCustomScan.code && this.selectedIsCustomScan.code !== this.isCustomScan[0].code) {
      this.searchObj['isCustomScan'] = this.selectedIsCustomScan.code;
    } else {
      delete this.searchObj['isCustomScan'];
    }

    if (this.selectedIsDelay.code && this.selectedIsDelay.code !== this.isDelay[0].code) {
      this.searchObj['isDelay'] = this.selectedIsDelay.code;
    } else {
      delete this.searchObj['isDelay'];
    }

    if (this.selectedStatus.code && this.selectedStatus.code !== this.status[0].code) {
      this.searchObj['status'] = this.selectedStatus.code;
    } else {
      delete this.searchObj['status'];
    }
    this.searchObj['searchValue'] = this.searchObj['searchValue'] ? this.searchObj['searchValue'].trim() : '';
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    }
    this.getData(this.searchObj, this.pageSize, this.pageNo)
  }


  /**
   * 表格分页方法
   * @param event 分页数据
   */
  onPage(event) {
    // this.searchObj['sortF'] = event.sortField;
    // this.searchObj['sortO'] = event.sortOrder === 1 ? 'asc' : 'desc';
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;

    if (this.selectedIsCustomScan.code && this.selectedIsCustomScan.code !== this.isCustomScan[0].code) {
      this.searchObj['isCustomScan'] = this.selectedIsCustomScan.code;
    } else {
      delete this.searchObj['isCustomScan'];
    }

    if (this.selectedIsDelay.code && this.selectedIsDelay.code !== this.isDelay[0].code) {
      this.searchObj['isDelay'] = this.selectedIsDelay.code;
    } else {
      delete this.searchObj['isDelay'];
    }

    if (this.selectedStatus.code && this.selectedStatus.code !== this.status[0].code) {
      this.searchObj['status'] = this.selectedStatus.code;
    } else {
      delete this.searchObj['status'];
    }
    this.searchObj['searchValue'] = this.searchObj['searchValue'] ? this.searchObj['searchValue'].trim() : '';
    if (!this.searchObj['searchValue']) {
      delete this.searchObj['searchValue'];
    }
    this.getData(this.searchObj, this.pageSize, this.pageNo)
  }

  /**
   * Description: 获取列表数据
   */
  getData(search: any = {}, pageSize?, pageNo?) {
    search['projectId'] = this.businessId;
    this.constructioninfoService.modifyRecordList(search, pageSize, pageNo).then(
      data => {
        this.loading = false;
        if (data.result) {
          this.listData = data.data.list;
          this.totalRecords = data.data.totalCount;
        } else {
          this.listData = [];
          this.totalRecords = 0;
        }
      }
    );
  }

}
