import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { EngineerRoutingModule } from './engineer-routing.module';
/**
 * 子系统模块
 */
@NgModule({
  imports: [
    EngineerRoutingModule,
    CommonModule,
    FormsModule,
  ],
  declarations: [
  ],
  exports: [],
  providers: []
})
export class EngineerModule {
}
