import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {MsgTemplateManageService} from "./msgTemplate.service";
import {StorageService} from "../../common/storage.service";


@Component({
  selector: 'app-msgTemplateManage-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [MsgTemplateManageService]
})
export class MsgTemplateManageListComponent implements OnInit {

  dataHead: any = [];  //模板表头
  templates: any = [];  //模板数据
  totalRecords: number = 0;  //总记录数

  pageSize: number;  //每页显示条数
  pageNo: number;  //页数

  searchObj: any = {};  //查询参数

  adviceType: any = [];  //通知类型
  selectAdviceType: any = {};  //选择的类型

  msgType: any = []; //消息类型
  selectMsgType: any = {};  //选择的消息类型

  titles: any = [];  //消息标题
  selectTitle: any = {};  //选择的标题

  statusDefault: any = { code: '', name: '全部' }; // 状态默认值
  statusTitleDefault: any = {title: '全部' }; // 状态默认值

  status: any = [];  //状态
  selectStatus: any = {};  //选择的状态

  selectedRowData: any = [];  //选择的数据

  constructor(private router: Router, private storage: StorageService,private templateService: MsgTemplateManageService) {
    this.dataHead = [
      { field: 'code', header: '模板编号', width: '200px', tem: true, hidden: false },
      { field: 'adviceType', header: '通知类型', width: '80px', tem: true, hidden: false },
      { field: 'msgType', header: '消息类型', width: '150px',tem: true, hidden: false },
      { field: 'title', header: '消息标题', width: '150px',tem: false, hidden: false },
      { field: 'content', header: '消息内容', width: '150px', hidden: false },
      { field: 'isEffect', header: '状态', width: '150px', hidden: false ,tem: true},
      { field: 'triggerEvent', header: '触发事件', width: '150px', hidden: false ,tem: false}
    ];
    this.status = [{code: '',name: '全部'},{code: 'false',name: '禁用'},{code: 'true',name: '启用'}]
  }

  ngOnInit() {
    this.adviceType = [this.statusDefault, ...this.storage.getDic('advicetype')];
    this.templateService.templateTitleList().then(
      data=>{
        if(data.result){
          this.titles = [this.statusTitleDefault, ...data.data];
        }else {
          this.storage.messageService.emit({ severity: 'error', detail: data.msg });
        }
    });
  }

  /**
   * Description: 查询方法
   */
  search(){
    this.storage.loading();
    this.templateService.getTemplateInfos(this.searchObj,this.pageSize,this.pageNo).then(
      data=>{
        this.storage.loadout();
        if(data.result){
          this.templates = data.data.list;
          this.totalRecords = data.data.totalCount;
          this.searchObj.searchValue = '';
        }else{
          this.storage.messageService.emit({ severity: 'error', detail: data.msg });
        }
      });
  }


  /**
   * 分页参数整理
   * @param event
   */
  onPage(event?) {
    this.pageSize = event.rows;
    const pageTem = parseInt((event.first / event.rows).toString(), 0);
    this.pageNo = pageTem + 1;
    this.search();
  }


  /**
   * 简易查询条件整合
   */
  normalSearchFun() {
    // 通知类型
    if (this.selectAdviceType && this.selectAdviceType.code && this.selectAdviceType.code != this.adviceType[0].code) {
      this.searchObj['adviceType'] = this.selectAdviceType.code;
    } else {
      delete this.searchObj['adviceType'];
    }
    // 消息类型
    if (this.selectMsgType && this.selectMsgType.code && this.selectMsgType.code != this.msgType[0].code) {
      this.searchObj['msgType'] = this.selectMsgType.code;
    } else {
      delete this.searchObj['msgType'];
    }
    // 模板标题
    if (this.selectTitle && this.selectTitle.title && this.selectTitle.title != this.titles[0].title) {
      this.searchObj['title'] = this.selectTitle.title;
    } else {
      delete this.searchObj['title'];
    }
    // 模板状态
    if (this.selectStatus && this.selectStatus.code && this.selectStatus.code != this.status[0].code) {
      this.searchObj['isEffect'] = this.selectStatus.code;
    } else {
      delete this.searchObj['isEffect'];
    }
    if(this.searchObj.searchValue){
      this.searchObj.searchValue = this.searchObj.searchValue.trim();
    }
    this.search();
  }


  /**
   * Description: 通知类型和消息类型联动
   */
  changeAdviceType(){
    if(this.selectAdviceType.code == 'advicetype_180309000011'){
        this.msgType = [this.statusDefault, ...this.storage.getDic('msgtype_order')];
    }else if(this.selectAdviceType.code == 'advicetype_180309000012'){
        this.msgType = [this.statusDefault, ...this.storage.getDic('msgtype_sys')];
    }else if(this.selectAdviceType.code == 'advicetype_180309000013'){
        this.msgType = [this.statusDefault, ...this.storage.getDic('msgtype_engineering')];
    }
    this.selectMsgType = this.statusDefault;
  }

  /**
   * Description: 新建模板
   */
  newTemplate(){
    this.router.navigateByUrl('/saas/engineer/msgTemplate/template/add');
  }


  /**
   * Description: 启用、禁用
   */
  toggleEffect(param, isEffect) {
    if (this.selectedRowData) {
      let list = this.selectedRowData;
      let ids = new Array();
      for (let index of list) {
        ids.push(index.id);
      }
      if (ids.length === 0) {
        this.nodata();
        return;
      }
      this.enableOrDisable(ids, param, isEffect)
    }
  }

  // 启用、禁用修改提示
  enableOrDisable(ids, _param, isEffect) {
    this.storage.makesureService.emit({
      message: '是否确认' + _param + ids.length + '个模板？',
      header: '提示',
      rejectLabel: '取消',
      acceptLabel: '确定',
      acceptVisible: true,
      rejectVisible: true,
      accept: () => {
        this.templateService.updateStatus({ ids: ids.toString(), isEffect: isEffect }).then(
          data => {
            if (data.result) {
              this.search();
              this.storage.messageService.emit({ severity: 'success', detail: data.msg });
            } else {
              this.storage.messageService.emit({ severity: 'error', detail: data.msg });
            }
            this.selectedRowData = [];
          }
        );
      },
      reject: () => {
        this.selectedRowData = [];
      }
    });
  }

  /**
   * Description: 未选择提示
   */
  nodata() {
    this.storage.makesureService.emit({
      message: '请选择要操作的数据！',
      header: '提示',
      rejectVisible: false,
      acceptVisible: false,
      accept: () => {
      }
    });
  }
}
