import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MsgTemplateManageService } from './msgTemplate.service';
import {StorageService} from "../../common/storage.service";
@Component({
  selector: 'app-msg-template-info',
  templateUrl: './info.component.html',
  providers: [MsgTemplateManageService],
})
export class MsgTemplateManageInfoComponent implements OnInit {

  templateId: any;  //模板id

  templateObj: any = {};  //模板

  constructor(public router: Router, public routeInfo: ActivatedRoute,public storage: StorageService, public templateService: MsgTemplateManageService) {
  }

  ngOnInit() {
    this.routeInfo.params.subscribe((params) => {
      this.templateId = params['id'];
    });
    this.storage.loading();
    this.templateService.getTemplateDetail(this.templateId).then(
      data=>{
        this.storage.loadout();
        if(data.result){
          this.templateObj = data.data;
        }else{
          this.storage.messageService.emit({ severity: 'error', detail: data.msg });
        }
      }
    );
  }

  /**
   * Description: 编辑模板信息
   */
  edit(){
    this.router.navigate(['/saas/engineer/msgTemplate/template/add',this.templateId]);
  }

}
