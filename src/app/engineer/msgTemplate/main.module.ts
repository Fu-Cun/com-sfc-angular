import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MsgTemplateManageRoutingModule } from './main-routing.module';
import {MsgTemplateManageListComponent} from './list.component';
import {MsgTemplateManageInfoComponent} from './info.component';
import {MsgTemplateManageAddComponent} from './add.component';
import {TooltipModule} from '../../components/tooltip/tooltip';
import {PanelModule} from '../../components/panel/panel';
import {InputTextModule} from '../../components/inputtext/inputtext';
import {DataTableModule} from '../../components/datatable/datatable';
import {MultiSelectModule} from '../../components/multiselect/multiselect';
import {ButtonModule} from '../../components/button/button';
import {InputMaskModule} from '../../components/inputmask/inputmask';
import {DropdownModule} from '../../components/dropdown/dropdown';
import {InputSwitchModule} from '../../components/inputswitch/inputswitch';
import {CalendarModule} from '../../components/calendar/calendar';
import {RatingModule} from '../../components/rating/rating';
import {CheckboxModule} from '../../components/checkbox/checkbox';
import {RadioButtonModule} from '../../components/radiobutton/radiobutton';
import {InputTextareaModule} from '../../components/inputtextarea/inputtextarea';
import {FileUploadModule} from '../../components/fileupload/fileupload';
import {DialogModule} from '../../components/dialog/dialog';
import {TabViewModule} from '../../components/tabview/tabview';
import {AdvanceModule} from '../../components/advancesearch/advancesearch';
import {FieldsetModule} from '../../components/fieldset/fieldset';
import {SpinnerModule} from '../../components/spinner/spinner';
import {MessageModule} from '../../components/message/message';
import {PickListModule} from '../../components/picklist/picklist';
import {LabelModule} from '../../components/label/label';
import {LightboxModule} from '../../components/lightbox/lightbox';
import {SelectButtonModule} from '../../components/selectbutton/selectbutton';
import {TreeModule} from '../../components/tree/tree';
import {DoubleTableModule} from '../../components/doublegrid/doublegrid';
import {TabMenuModule} from '../../components/tabmenu/tabmenu';
import {GalleriaModule} from '../../components/galleria/galleria';
import {AutoCompleteModule} from '../../components/autocomplete/autocomplete';

/**
 * 施工配置模块
 */
@NgModule({
  imports: [
    MsgTemplateManageRoutingModule,
    TooltipModule,
    CommonModule,
    FormsModule,
    PanelModule,
    InputTextModule,
    DataTableModule,
    MultiSelectModule,
    ButtonModule,
    InputMaskModule,
    DropdownModule,
    InputSwitchModule,
    CalendarModule,
    RatingModule,
    CheckboxModule,
    RadioButtonModule,
    InputTextareaModule,
    FileUploadModule,
    DialogModule,
    ReactiveFormsModule,
    TabViewModule,
    AdvanceModule,
    FieldsetModule,
    SpinnerModule,
    MessageModule,
    PickListModule,
    LabelModule,
    LightboxModule,
    SelectButtonModule,
    TreeModule,
    DoubleTableModule,
    TabMenuModule,
    GalleriaModule,
    AutoCompleteModule
  ],
  declarations: [
    MsgTemplateManageListComponent,
    MsgTemplateManageInfoComponent,
    MsgTemplateManageAddComponent
  ],
  exports: [],
  providers: []
})
export class MsgTemplateManageModule {
}
