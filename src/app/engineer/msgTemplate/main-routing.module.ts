import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { MsgTemplateManageInfoComponent } from './info.component';
import { MsgTemplateManageListComponent } from './list.component';
import { MsgTemplateManageAddComponent } from './add.component';


/**
 * 施工队管理路由参数配置
 */
const mainRoutes: Routes = [
  {
    path: 'template',
    component: MsgTemplateManageListComponent,
    data: { title: '消息模板管理' }
  },
  {
    path: 'template/add',
    component: MsgTemplateManageAddComponent,
    data: { title: '消息模板新建' }
  },
  {
    path: 'template/:id',
    component: MsgTemplateManageInfoComponent,
    data: { title: '消息模板详情', 'hasSidebar': false, 'hasHeader': false }
  },
  {
    path: 'template/add/:id',
    component: MsgTemplateManageAddComponent,
    data: { title: '消息模板编辑', 'hasSidebar': false, 'hasHeader': false }
  },

];
/**
 * 施工队队管理路由模块
 */
@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MsgTemplateManageRoutingModule { }
