import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {MsgTemplateManageService} from "./msgTemplate.service";
import {StorageService} from "../../common/storage.service";

declare const window;
@Component({
  selector: 'app-add-template',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [MsgTemplateManageService]
})


export class MsgTemplateManageAddComponent implements OnInit {

  templateId: any;  //模板id

  templateObj: any = {};  //模板

  adviceType: any = [];  //通知类型
  selectAdviceType: any = {};  //选择的类型

  msgType: any = []; //消息类型
  selectMsgType: any = {};  //选择的消息类型

  statusDefault: any = { code: '', name: '请选择' }; // 状态默认值


  titleError: any;  //标题错误提示
  adviceError : any;  //通知类型错误提示
  msgTypeError: any;  //消息类型错误提示
  contentError: any; //消息内容错误提示
  triggerEventError: any;  //触发事件错误提示



  constructor(private templateService: MsgTemplateManageService,
    private routeInfo: ActivatedRoute, private router: Router, private storage: StorageService ) {

  }

  ngOnInit() {
    this.routeInfo.params.subscribe((params) => {
      this.templateId = params['id'];
    });
    this.adviceType = [this.statusDefault, ...this.storage.getDic('advicetype')];
    if(this.templateId){
      this.storage.loading();
      this.templateService.getTemplateDetail(this.templateId).then(
        data=>{
          this.storage.loadout();
          if(data.result){
            this.templateObj = data.data;
            this.adviceType.filter((item) => {
              if(data.data.adviceType == item.code){
                this.selectAdviceType = item;
              }
            });
            if(data.data.adviceType == 'advicetype_180309000011'){
              this.msgType = this.storage.getDic('msgtype_order');
              this.msgType.filter((item)=>{
                if(data.data.msgType == item.code){
                  this.selectMsgType = item;
                }
              });
            }
            if(data.data.adviceType == 'advicetype_180309000012'){
              this.msgType = this.storage.getDic('msgtype_sys');
              this.msgType.filter((item)=>{
                if(data.data.msgType == item.code){
                  this.selectMsgType = item;
                }
              });
            }
            if(data.data.adviceType == 'advicetype_180309000013'){
              this.msgType = this.storage.getDic('msgtype_engineering');
              this.msgType.filter((item)=>{
                if(data.data.msgType == item.code){
                  this.selectMsgType = item;
                }
              });
            }
          }else{
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
    }

  }

  /**
   * Description: 校验消息标题
   */
  titleCk(){
    if(!this.templateObj.title){
      this.titleError = '请输入标题';
      return false;
    }else{
      this.templateObj.title = this.templateObj.title.trim();
      this.titleError = '';
      return true
    }
  }

  /**
   * Description:
   */
  adviceTypeCk(){
    if(!this.selectAdviceType || !this.selectAdviceType.code){
      this.adviceError = '请选择通知类型';
      return false;
    }else{
      if(this.selectAdviceType.code == 'advicetype_180309000011'){
        this.msgType = [this.statusDefault, ...this.storage.getDic('msgtype_order')];
        this.selectMsgType = this.statusDefault;
      }else if(this.selectAdviceType.code == 'advicetype_180309000012'){
        this.msgType = [this.statusDefault, ...this.storage.getDic('msgtype_sys')];
        this.selectMsgType = this.statusDefault;
      }else if(this.selectAdviceType.code == 'advicetype_180309000013'){
        this.msgType = [this.statusDefault, ...this.storage.getDic('msgtype_engineering')];
        this.selectMsgType = this.statusDefault;
      }
      this.adviceError = '';
    }
    return true;
  }

  /**
   * Description: 校验消息类型
   */
  msgTypeCk(){
    if(!this.selectMsgType || !this.selectMsgType.code){
      this.msgTypeError = '请选择消息类型';
      return false;
    }else{
      this.msgTypeError = '';
    }
    return true;
  }

  /**
   * Description: 触发事件校验
   */
  triggerEventCk(){
    if(!this.templateObj['triggerEvent']){
      this.triggerEventError = '请输入触发事件';
      return false;
    }else{
      this.templateObj.triggerEvent = this.templateObj['triggerEvent'].trim();
    }
    return true;
  }

  /**
   * Description: 消息内容检验
   */
  contentCk(){
    if(!this.templateObj.content){
      this.contentError = '请输入消息内容';
      return false;
    }else{
      this.contentError = '';
    }
    return true;
  }


  /**
   * Description: 保存模板信息
   */
  save(){
    if(!this.selectAdviceType || !this.selectAdviceType.code){
      this.adviceError = '请选择通知类型';
      return false;
    }
    if(!this.titleCk() || !this.msgTypeCk() || !this.contentCk() || !this.triggerEventCk()){
      return false;
    }
    this.templateObj.adviceType = this.selectAdviceType.code;
    this.templateObj.msgType = this.selectMsgType.code;
    this.storage.loading();
    if(!this.templateId){
      this.templateService.saveTemplate(this.templateObj).then(
        data=>{
          this.storage.loadout();
          if(data.result){
            this.storage.messageService.emit({ severity: 'success', detail: '操作成功！' });
            this.router.navigateByUrl('/saas/engineer/msgTemplate/template');
          }else{
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
    }else{
      this.templateObj.id = this.templateId;
      this.templateService.updateTemplate(this.templateObj).then(
        data=>{
          this.storage.loadout();
          if(data.result){
            this.storage.messageService.emit({ severity: 'success', detail: '操作成功！' });
            this.router.navigate(['/saas/engineer/msgTemplate/template',this.templateId]);
          }else{
            this.storage.messageService.emit({ severity: 'error', detail: data.msg });
          }
        }
      );
    }

  }

  /**
   * Description: 取消
   */
  back(){
    if(this.templateId){
      this.router.navigate(['/saas/engineer/msgTemplate/template',this.templateId]);
    }else{
      this.router.navigateByUrl('/saas/engineer/msgTemplate/template');
    }
  }
}

