import { Injectable } from '@angular/core';
import { HttpInterceptorService } from '../../common/http-interceptor.service';

@Injectable()
export class MsgTemplateManageService {

  private baseUrl = '';

  // 工程管理服务名称
  private serveName = 'com-dyrs-mtsp-customerselfservice/';

  constructor(private httpService: HttpInterceptorService) {}

  /**
   * Description: 获取模板数据
   */
  getTemplateInfos(search: any = {}, pageSize: number = 10, pageNo: number = 1){
    return this.httpService.post(this.serveName + 'msgTemplateManage/templateList/api',
      { pageSize: pageSize, pageNo: pageNo, ...search }, this.baseUrl);
  }

  /**
   * Description: 查询所有标题
   */
  templateTitleList(){
    return this.httpService.post(this.serveName + 'msgTemplateManage/templateTitleList/api',{}, this.baseUrl);
  }

  /**
   * Description: 保存模板信息
   */
  saveTemplate(templateObj: any = {}){
    return this.httpService.post(this.serveName + 'msgTemplateManage/saveTemplateInfo/api',templateObj, this.baseUrl);
  }

  /**
   * Description: 模板详情
   */
  getTemplateDetail(templateId: any){
    return this.httpService.post(this.serveName + 'msgTemplateManage/getTemplateDetail/api',{id: templateId}, this.baseUrl);
  }

  /**
   * Description: 修改模板信息
   */
  updateTemplate(templateObj: any = {}){
    return this.httpService.post(this.serveName + 'msgTemplateManage/updateTemplateInfo/api',templateObj, this.baseUrl);
  }

  /**
   * Description: 启用或禁用
   */
  updateStatus(param: any = {}){
    return this.httpService.post(this.serveName + 'msgTemplateManage/updateTemplateStatus/api',param, this.baseUrl);
  }
}
