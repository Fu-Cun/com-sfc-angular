import { Injectable }    from '@angular/core';
import { HttpInterceptorService } from "../../common/http-interceptor.service";

@Injectable()
export class AccountService {
    constructor (public spaceService: HttpInterceptorService){}

    public redirectUrl:string;
    getPhone(params:any) {
        
    }

    /*************************个人资料****************************/
    // 用户名
    nameValidate(params:any) {
        return this.spaceService.post('com-dyrs-mtsp-authorityservice/user/checkUniqueName/api', params)
    }
    /*************************上传头像****************************/
    updateIconUrl(params:any) {
        return this.spaceService.post('com-dyrs-mtsp-authorityservice/user/updateUserInfo/api',params);
    }
    /*************************修改密码****************************/
    updatePwd(params:any) {
        return this.spaceService.post('com-dyrs-mtsp-authorityservice/account/updatePwd/api', params)
    }

    // 详情
    detailById(params:any){
        return this.spaceService.post('com-dyrs-mtsp-employeeservice/employee/detail/api', params)
    }

    // 省
    province(){
        return this.spaceService.post('com-dyrs-mtsp-employeeservice/threelevel/province/api', {})

    }

    // 市
    city(params: any){
        return this.spaceService.post('com-dyrs-mtsp-employeeservice/threelevel/city/api', params)

    }
    // 区
    district(params: any){
        return this.spaceService.post('com-dyrs-mtsp-employeeservice/threelevel/district/api', params)
    }

    // 编辑提交
    updateSaveData(params: any){
        return this.spaceService.post('com-dyrs-mtsp-employeeservice/employee/updateEmpByCode/api', params)
    }

    /*************************绑定手机****************************/

    validInputMobile(params:any) {
        return this.spaceService.post('com-dyrs-mtsp-authorityservice/user/validInputMobile/api',params);
    }
    userIsExistsByMobile(params:any) {
        return this.spaceService.post('com-dyrs-mtsp-authorityservice/user/userIsExistsByMobile/api',params);
    }
    updateMobile(params:any) {
        return this.spaceService.post('com-dyrs-mtsp-authorityservice/user/updateUserInfo/api',params);
    }
    
    sendMsg(params:any) {
        return this.spaceService.post('com-dyrs-mtsp-authorityservice/user/sendSms/api',params);
    }
}