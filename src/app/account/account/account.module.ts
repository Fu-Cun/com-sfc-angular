import { ImageCropperModule } from 'ng2-img-cropper';
import { NgModule }       from '@angular/core';
import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { TabViewModule } from '../../components/tabview/tabview';
import { DialogModule } from '../../components/dialog/dialog';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from '../../components/dropdown/dropdown';
import { ButtonModule } from '../../components/button/button';
import { CommonModule } from '@angular/common/';
import { MessageModule } from '../../components/message/message';
import {RadioButtonModule} from '../../components/radiobutton/radiobutton';
import { AccountService } from './account.service';
import { FieldsetModule } from '../../components/fieldset/fieldset';

@NgModule({
    imports: [
     CommonModule,
     AccountRoutingModule,
     TabViewModule,
     DialogModule,
     FormsModule,
     DropdownModule,
     ButtonModule,
     MessageModule,
     RadioButtonModule,
     FieldsetModule,
     ImageCropperModule
    ],
    declarations: [
        AccountComponent
    ],
    providers: [AccountService]
  })

export class AccountModule{}