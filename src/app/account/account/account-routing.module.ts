import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account.component';
import { AccountService } from './account.service';
import { StorageService } from "../../../app/common/storage.service"

const AccountRoutes: Routes = [
  {
    path: '',
    component: AccountComponent,
  //   children:[
  //       { path: '', redirectTo: 'phone', pathMatch: 'full' },
  //       { path: 'phone', component:PhoneComponent, data: { title: '账号设置' } },
  //  ]    
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(AccountRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [AccountService,StorageService]
})
export class AccountRoutingModule { }
