import {Component, Input} from '@angular/core';
import { fadeIn, fadeOut} from "./animate.common";
@Component({
    animations:[fadeIn, fadeOut],
    selector:'error-bar',
    template: `
      <p class="error-msg" *ngIf="msg" @fadeIn @fadeOut>
          <i class="fa fa-minus-circle"></i>
          <span>{{msg}}</span>   
      </p>
    `,
    styles:[`
    .error-msg{border: 1px solid #ffb4a8;background-color: #fef2f2;color: #e4393c;
               line-height: 22px;font-size: 14px;padding: 5px;padding-left:15px}
    `]
})
export class ErrorBarComponent {
  @Input() msg:string;
}