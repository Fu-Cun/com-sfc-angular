import { FormOption} from './check';

export class ReactiveFormatData{

    getLocalData(localData:FormOption[],httpData:any,format?,repeatParam?){
        localData.map(val=>{
            this.dataToValue(format,httpData,val);
            if(val.repeatUrl && repeatParam){val.repeatParam = repeatParam}
            if(val.group){
                val.group.map(value=>{
                    if(value.repeatUrl && repeatParam){value.repeatParam = repeatParam}
                    this.dataToValue(format,httpData,value);
                })
            } 
        }) 
     }
     
     dataToValue(format,httpData,value){
        value.defaultIndex = undefined;
        if(value.name && httpData[value.name]){value.defaultValue = httpData[value.name]}
        if(value.name && format[value.name]){
            let resultFlag:boolean = true;
            let obj = format[value.name];
            value.defaultValue = {};
            for(let v in obj){
                if(httpData[obj[v]]){resultFlag = false}
                value.defaultValue[v] = httpData[obj[v]]
            }
            if(resultFlag){value.defaultValue = undefined}
        }
     }

     getPostData(formValue,format){
        for(let k in format){
            const v = format[k];
            if(formValue[k]){
                for(let key in v){
                    const value = v[key];
                    formValue[value] = formValue[k][key]
                }
            }
        }
     }

}