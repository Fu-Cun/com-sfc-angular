export class TableConfigClass {
  readonly pageLinks: number = 5; //页数显示个数
  readonly rowsPerPageOptions: number[] = [20, 50, 100, 300]; //可选每页显示行数
  readonly rows: number = this.rowsPerPageOptions[0]; //每页显示行数
  paginator: boolean = true; //是否分页
  editableL: boolean = false; //是否可编辑
  resizableColumns: boolean = true; //是否可拖动
  totalRecords: number;//总行数
  scrollable: boolean = true;//是否滚动
  scrollHeight: string = "100%";//滚动触发高度
  frozenWidth: string;//冻结列宽度
  unfrozenWidth: string;//没有冻结列部分宽度
  frozenValue: any[];
  emptyMessage: string = '没有搜到您想要的数据'
  selectionMode: string = "single";//选中   "single" and "multiple".
}


//时间组件 配置参数
export class TimeConfigClass {
  readonly dateFormat: string = 'yy-mm-dd'; //时间格式默认  年-月-日
  readonly dataType: string = 'string'; //ngModel 时间格式默认  date string
  readonly monthNavigator: boolean = true; //年份下拉框
  readonly showButtonBar: boolean = false; //显示 按钮
  readonly selectOtherMonths: boolean = true; //显示 按钮
  readonly readonlyInput: boolean = true; //显示 按钮
  readonly showIcon: boolean = true; //显示 按钮
  readonly inputStyle: object = { "width": "calc(100% - 2em)", "padding": ".3em" }; //显示 按钮
  readonly locale: object = {
    firstDayOfWeek: 1,
    dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
    dayNamesShort: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
    dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
    monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
    monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    today: '今天',
    clear: '清除'
  }; //显示 按钮

}

export const SIDE_BAR_BIG = '210px';

export const SIDE_BARS_MALL = '60px';

export const NAV_HEIGHT = '50px';

export const COMPANY_NAME = '家装综合运营管理平台';


