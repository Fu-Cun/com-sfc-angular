import { Component, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule } from '../components/panel/panel';
import { HttpInterceptorService } from './http-interceptor.service';
@Component({
  selector: 'app-not-find',
  template: `<div class="bg-img"> </div>`,
  styles: [`.bg-img{width:calc(100% - 60px);height:calc(100% - 50px); background:url(/assets/imgs/404.png) no-repeat center center; `]
})
export class NotFoundComponent { }

@Component({
  selector: 'app-error',
  template: `<div class="bg-img"> </div>`,
  styles: [`.bg-img{width:calc(100% - 60px);height:calc(100% - 50px); background:url(/assets/imgs/500.png) no-repeat center center;`]
})
export class ErrorComponent { }


@Component({
  selector: 'app-home-page',
  template: `<div class="home-page">
  <div class="home-page-conent-no" style="display:none" >
    <div class="conent-bg-imge"></div>
    <p class="conent-font">hi~ &nbsp;&nbsp;小明&nbsp;&nbsp;你好! &nbsp;&nbsp;欢迎使用综合管理系统</p>
  </div>
  <div class="home-page-conent" style="display:block">

    <p-panel headerClass="bg-none" styleClass="m-t-20">
      <p-header>
        <div class="text-right p-6-0" style="height: 48px;">
          <span class="border-l-1a8fe8 p-l-8 pull-left m-t-8">系统公告（1）</span>
          <div class="pull-right flex message-paginator">
            <a class="bg-d4d1d1 ui-state-disabled message-paginator-element ui-corner-all ui-state-default" >
              <span class="fa fa-play fa-rotate-180"></span>
            </a>
            <a class="ui-paginator-last message-paginator-element ui-state-default ui-corner-all bg-d4d1d1"  style="margin-left:10px">
              <span class="fa fa-play "></span>
            </a>
          </div>
        </div>
      </p-header>
      <div class="m-l-20 m-r-20">
        <div class="message-item message-border">
          <span>【公告】欢迎使用系统</span>
          <span class="pull-right message-time">2018-08-23 16:00:01</span>
        </div>
      </div>
    </p-panel>

  </div>
</div>
`,
  styles: [
    `.home-page{width: 100% ;height: 100%;background: #fff; position: relative;}
     .home-page-conent-no{width: 600px;height: 226px;position: absolute;top: 50%;left: 50%;margin-top: -160px;margin-left: -300px;}
     .conent-bg-imge{width:180px;height:176px;margin:auto;background:url(/assets/imgs/home_page_noMessage.png) no-repeat center center;}
     .conent-font{text-align: center;width: 100%;font-size:18px; color:#333;padding-top:30px}
     .home-page-conent{padding:20px;}
     :host /deep/ .p-20{ padding:0px !important}
     .message-item{height: 50px;line-height: 50px;}
     .message-border{border-bottom: 1px solid #d9d9d9; }
     .message-time{color:#a3a3a3}
     .message-title{overflow: hidden;text-overflow: ellipsis;white-space: nowrap;display: inline-block;width: calc( 100% - 160px);  }
     :host /deep/ .message-paginator-element{ padding: .375em .625em !important;border: none !important;outline: none;color: #1b1d1f;}
     :host /deep/ .message-paginator{margin-left: 10px;margin-top: 4px;height: 27px;}
     :host /deep/ .fa-caret-up {position: absolute;top: 3px;left: 15px;}
     :host /deep/ .fa-caret-down {position: absolute;top: 12px;left: 15px;}
     :host /deep/ .message-sort{margin-left: 10px;margin-top: 3px;height: 27px;}
     :host /deep/ .parent {position: relative;height: 16px;display: inline-block;line-height: 30px;padding-left: 32px;cursor: pointer;}
     :host /deep/ .box {color: #9b9b9b;margin-right: 10px; border: 1px solid #dddddd;width: 80px;height: 30px;text-align: center;}
     :host /deep/ .ui-widget-content {border: none !important;box-shadow: 0 4px 16px 1px rgba(0, 0, 0, 0.05);border-radius: 6px !important;}
    `
  ]
})
export class HomePageComponent implements OnInit {
  riskManagementList: any[] = []; // 分控数据
  systemBulletinList: any[] = []; // 系统公告
  ngOnInit() {

  }
}


@NgModule({
  imports: [CommonModule, PanelModule],
  exports: [NotFoundComponent, ErrorComponent, HomePageComponent],
  declarations: [NotFoundComponent, ErrorComponent, HomePageComponent]

})
export class PromptPageModule { }