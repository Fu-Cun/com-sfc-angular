import {Injectable, EventEmitter} from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { environment } from '../../environments/environment';

interface Message {
  sendto: string[];
  type: number;
  data: any;
}

@Injectable()

export class SocketmessageService {

  public onMessage: EventEmitter<any>; //监听消息

  public onOpen: EventEmitter<any>; //连接成功socket

  public onClose: EventEmitter<any>; //关闭socket；

  public onError: EventEmitter<any>; //socket错误

  private headers = new Headers({ 'Content-Type': 'application/json' });

  private options = new RequestOptions({ headers: this.headers});

  public baseUrl:string = 'http://' + environment.wsUrl;

  constructor(private http: Http,) {

    this.onMessage = new EventEmitter();

    this.onOpen = new EventEmitter();

    this.onClose = new EventEmitter();

    this.onError = new EventEmitter();

  }

/** 
* @author  发送消息
* @author  最后修改者:liutg 
* @version 最后修改版本:v-1.0
* @description ：
  1、url ： /sendto
  2、method：Post
  3、参数类型：json
  4、参数说明：'sendto':['code','code2'] 【用户code】
              'type': 0 【类型：(0：表示只给一个人发，此时sendto只能有一个人，1：给多个人发，2：给所有在线的人发，此时sendto失效)】  
              'data':{'type':'',data:''} 【json格式=》type:0表示为营销服务的消息】  
  5、例：{
      "sendto": ["8888"], 
      "type": 0, 
      "data": "{"type":"0","data":{"clueCode":"1234","ClueName":"线索一"}}"
    }
*/

  send(data:Message){
    let post = data;
    post.data = JSON.stringify(post.data);
    return this.post('sendto',data) 
  }

/** 
* @author  获取在线人数
* @author  最后修改者:liutg 
* @version 最后修改版本:v-1.0
* @description ：
  1、url ： /onlinecount
  2、method：get
  3、返回参数：数值
*/ 

  getOnlineNum(){
    return this.get('onlinecount')
  }

/** 
* @author  获取在线人员
* @author  最后修改者:liutg 
* @version 最后修改版本:v-1.0
* @description ：
  1、url ： /onlineusers
  2、method：get
  3、返回参数：数组
  4、返回示例：[{"code":"123"},{"code":"8888"},{"code":"wangcl"}]
*/  
  getOnlineUsers(){
    return this.get('onlineusers')
  }


/* http 请求****** */

  public post(url: string, params: any) {
    return this.http.post(this.baseUrl + url, params, this.options)
      .toPromise()
      .then(res=>res)
      .catch(res=>res);
  }
  
  public get(url: string): any {
    return this.http.get(this.baseUrl + url,this.options)
      .toPromise()
      .then(res=>JSON.parse(res['_body']))
      .catch(res=>res);
  }
  


}

