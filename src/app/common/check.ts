
import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export class Check{

    phone(data){
       return (/^(1[3|4|5|7|8][\d]{9}|0[\d]{2,3}-[\d]{7,8}|400[-]?[\d]{3}[-]?[\d]{4})$/.test(data))
    }

    password(data){
        // return (/(?!^[0-9]+$)(?!^[A-z]+$)(?!^[^A-z0-9]+$)^.{6,12}$/.test(data))
        return (/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/.test(data))
    }
}

//身份证
const idCard18 = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
//手机号
const phone = /^(1[3|4|5|7|8][\d]{9}|0[\d]{2,3}-[\d]{7,8}|400[-]?[\d]{3}[-]?[\d]{4})$/;
//密码
const password = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/;
//身份证15
const idCard15 = /^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$/;
//小数保留两位
const float2 = /^[1-9][0-9]{0,8}$|(^([1-9][0-9]{0,8})\.([0-9]{1,2})$)|^[0]\.([0-9]{1,2})$/g;
//5位正整数
const init5 = /^[1-9][0-9]{0,4}$/;
//5位正整数+2位小数
const init5float2 = /^[1-9][0-9]{0,1}$|(^([1-9][0-9]{0,1})\.([0-9]{1,2})$)|^[0]\.([0-9]{1,2})$/;
//5位正整数+2位小数
const email = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
//只能汉字和字母
const cnAnden = /^[A-Za-z\u4e00-\u9fa5]+$/;
//只能字母数字下划线,不能纯数字，不能下划线在首尾 
const a_1 = /^(?!_)(?!.*?_$)(?!^\d+$)(\w+)$/;
//只能字母数字汉字下划线cna_1
const cna_1 = /^(?!_)(?!.*?_$)(?!^\d+$)([a-zA-Z0-9_\u4e00-\u9fa5]+)$/;



/**
 * @author ltg
 * @version 修改记录[(2018-01-17:ltg)]
 * @method  导出正则集合:PatternMap|导出验证集合:ValidatorsMap|导出响应式表单接口:FormOption|导出form类型枚举
 * 
 */

export const FormComponent = {
    'input': 'input',
    'date':'date',
    'textarea':'textarea',
    'checkbox':'checkbox',
    'radiobutton':'radiobutton',
    'multiselect':'multiselect',
    'tree':'tree',
    'dropdown': 'dropdown'
}

export const PatternMap = {
    idCard18,
    phone,
    password,
    idCard15,
    float2,
    init5,
    init5float2,
    cnAnden,
    cna_1,
    a_1,
    email
}

export const ValidatorsMap = {
    required:Validators.required,
    phone:Validators.pattern(phone),
    password:Validators.pattern(password),
    idCard18:Validators.pattern(idCard18),
    idCard15:Validators.pattern(idCard15),
    cnAnden:Validators.pattern(cnAnden),
    cna_1:Validators.pattern(cna_1),
    a_1:Validators.pattern(a_1),
    email:Validators.pattern(email),
    maxlength:Validators.maxLength,
    minlength:Validators.minLength,
    max:Validators.max,
    min:Validators.min,
    customerValidator:customerValidator,
}

export interface FormOption {
    runOutputWhenValue?:boolean;//当有值时就触发 output 方法
    formArray?:any[];//数据组  即 数组
    hidden?:boolean;//是否隐藏
    validators?:any[];//验证
    defaultValue?:any;//默认值
    defaultIndex?:number;//默认值选中
    selectionMode?:string;//默认值为'single'
    title?:string | string[];//label
    component?:string;//组件
    numberType?:string;//数字输入框
    name?: string;//字段名称
    justValue?:boolean;//下拉只绑定某一个 属性
    dataKey?:string;//身份证
    optionLabel?:string;//下拉绑定显示属性
    class?:string;//组件样式
    options?:any[];//组件需要的 数据
    placeholder?:string;//组件展位符
    keyWord?:string;//搜索属性
    dataType?:string;//日期格式
    group?:any[];//是否分组即 一行显示 如 省市区三级联动
    maxDate?:any;//最大选择日期
    minDate?:any;//最小日期
    maxlength?:number;//文本最大长度
    focusStatus?:any;//readOnly 获取焦点失去焦点状态 
    output?:any;//组件输出 用户数据关联
    function?:Function;//
    url?:string;//需要后台数据的api路径
    dataPath?:string;//获取数据路径 如 后台返回 {data:{data:{list:[]}}则为 data.list
    residue?:boolean;//是否显示 剩余多少字 for textarea
    rows?:number;//文本域高度默认 5行
    disabled?:boolean;//是否禁用
    tabindex?:number;//tab键索引
    repeatUrl?:string;//验证重复api
    repeatPath?:string;//验证重复数据路径
    repeatParam?:any;//验证重复数据路径
    componentClass?:any;//组件外层 div 样式类，用于限制宽度
    componentStyle?:any;//组件外层 div 样式，用于限制宽度
    titleClass?:any;//label外层 div 样式类，用于限制宽度
    titleStyle?:any;//label外层 div 样式，用于限制宽度
    select?:string;//选择器
    unit?:string;//单位
    template?:boolean;//
    errorType?:string;//错误提示样式 默认为 error
    cancleLabel?:string;//是否显示 全部或请选择 
    isDic?:boolean;//是否字典  用于 字典数据 下拉选项
  }

/**
 * @author ltg
 * @version 修改记录[(2018-01-17:ltg)]
 * @augments name:字段名称；nameRe:正则表达式 
 * @function 验证通过返回null，验证失败返回对象{name:true}
 */

export function customerValidator(name,nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const value = control.value;
    const no = nameRe.test(value);
    return no ? {[name]: true} : null;
  };
}














































// @Injectable()
// export class Customer {

//     constructor(private httpService:HttpInterceptorService){}
    
//     customerValidatorAsync(name:string,url:string,keyWord:string): AsyncValidatorFn {
        
//         return (control: AbstractControl):Promise<{[key: string]: any}> =>{
//             return new Promise(resolve =>
//                     this.httpService.obsPost(url,{[keyWord]:control.value}).subscribe(res => {
//                         if (!res.result) {
//                             resolve({[name]: res.msg})
//                         }
//                         else {
//                             resolve(null)
//                         }
//                     })) 
//             }

//     }        
// }



