//sim-anim.ts

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnimationTriggerMetadata,trigger, state, style,stagger, transition, animate, keyframes, group, query } from '@angular/animations';


/* export const routeAnimation = trigger('routeAnimation', [
  transition('* <=> *', [
    query(':enter, :leave', style({ opacity: 1 })
      , { optional: true }),
    group([
      query(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.2s ease-in-out', style({ transform: 'translateX(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateX(0%)' }),
        animate('0.2s ease-in-out', style({ transform: 'translateX(-100%)'}))
      ], { optional: true }),
    ])
  ])
]) */
// 动画时间线
var time = '300ms'
var styles = {
  ease: time + ' ease ',
  linear: time + ' linear ',
  easeIn: time + ' ease-in',
  easeOut: time + ' ease-out',
  stepStart: time + ' step-start',
  stepEnd: time + ' step-end',
  easeInOut: time + ' ease-in-out',
  faseOutSlowIn: time + ' cubic-bezier(0.4, 0, 0.2, 1)',
  inOutBack: time + ' cubic-bezier(0.68, -0.55, 0.27, 1.55)',
  inOutCubic: time + ' cubic-bezier(0.65, 0.05, 0.36, 1)',
  inOutQuadratic: time + ' cubic-bezier(0.46, 0.03, 0.52, 0.96)',
  inOutSine: time + ' cubic-bezier(0.45, 0.05, 0.55, 0.95)'
}

// 动画配置

var opts = {
  fadeIn: [
    style({ opacity: 0 }),
    animate(styles.inOutBack, style({ opacity: 1 })),
  ],
  fadeOut: [
    style({ opacity: 1 }),
    animate(styles.inOutBack, style({ opacity: 0 }))
  ],
  shrink: [
    style({ height: '*' }),
    animate(styles.inOutBack, style({ height: 0 }))
  ],
  stretch: [
    style({ height: '0' }),
    animate(styles.inOutBack, style({ height: '*' }))
  ],
  expand: [
    style({ width: '0' }),
    animate('0.1s linear', style({ width: '*' }))
  ],
  collapsed: [
    style({ width: '*' }),
    animate('0.1s linear', style({ width: 0 }))
  ],
  flyIn: [
    style({ transform: 'translateX(30px)' }),
    animate(styles.easeOut, style({ transform: '*' }))
  ],
  flyOut: [
    style({ transform: '*' }),
    animate(styles.easeOut, style({ transform: 'translateX(30px)' }))
  ],
  zoomIn: [
    style({ transform: 'scale(.5)' }),
    animate(styles.inOutBack, style({ transform: '*' }))
  ],
  zoomOut: [
    style({ transform: '*' }),
    animate(styles.inOutBack, style({ transform: 'scale(.5)' }))
  ]
}

// 导出动画时间线定义,供自定义动画的时候使用
export const animStyle = styles

// 导出动画
export const fadeIn = [trigger('fadeIn', [transition('void => *', opts.fadeIn)])]
export const fadeOut = [trigger('fadeOut', [transition('* => void', opts.fadeOut)])]
export const stretch = [trigger('stretch', [transition('void => *', opts.stretch)])]
export const shrink = [trigger('shrink', [transition('* => void', opts.shrink)])]
export const expand = [trigger('expand', [transition('void => *', opts.expand)])]
export const collapsed = [trigger('collapsed', [transition('* => void', opts.collapsed)])]
export const flyIn = [trigger('flyIn', [transition('void => *', opts.flyIn)])]
export const flyOut = [trigger('flyOut', [transition('* => void', opts.flyOut)])]
export const zoomIn = [trigger('zoomIn', [transition('void => *', opts.zoomIn)])]
export const zoomOut = [trigger('zoomOut', [transition('* => void', opts.zoomOut)])]

// export const listAnimation = [trigger('listAnimation', [
//   transition('void => *', [ 
//     query('.item-all', [
//       style({ transform: 'translateX(30px)' }),
//       stagger(100, [
//         animate('0.5s', style({ transform: '*' }))
//       ])
//     ])
//   ])])]

export const simAnim = [
  trigger('simAnim', [
    transition('* => fadeIn', opts.fadeIn),
    transition('* => fadeIn', opts.fadeOut),
    transition('* => shrink', opts.shrink),
    transition('* => stretch', opts.stretch),
    transition('* => expand', opts.expand),
    transition('* => collapsed', opts.collapsed),
    transition('* => flyIn', opts.flyIn),
    transition('* => flyOut', opts.flyOut),
    transition('* => zoomIn', opts.zoomIn),
    transition('* => zoomOut', opts.zoomOut)
  ])
]

export const notifyAnimation: AnimationTriggerMetadata = trigger(
  'notifyAnimation', [
    state('*', style({
      opacity: 0,
      visibility: 'hidden',
      transform: 'translate3D(0, 0, 0)',
    })),
    state('false', style({
      opacity: 0,
      visibility: 'hidden',
      transform: 'translate3D(0, -10px, 0)',
    })),
    state('true', style({
      opacity: 1,
      transform: 'translate3D(0, 0, 0)',
      visibility: 'inherit',
    })),
    transition('0 => 1', [
      style({
        opacity: 0,
        visibility: 'inherit',
        transform: 'translate3D(50px, 0, 0)',
      }),
      animate('250ms linear')
    ]),
    transition('1 => 0', [
      style({
        opacity: 1,
        visibility: 'inherit',
        transform: 'translate3D(0px, 0, 0)',
      }),
      animate('250ms linear')
    ]),
  ])


