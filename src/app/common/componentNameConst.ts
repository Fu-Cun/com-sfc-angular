import { Calendar } from "../components/calendar/calendar";//日期控件
import { Dropdown } from "../components/dropdown/dropdown";//单选下拉框控件
import { MultiSelect } from "../components/multiselect/multiselect";//多选下拉
import { RadioButton } from "../components/radiobutton/radiobutton";//单选 按钮
import { Checkbox } from "../components/checkbox/checkbox";//复选选 按钮


export const ComponentMap = {
    Calendar,Dropdown,MultiSelect,RadioButton,Checkbox
}


export const ComponentArr = [
    Calendar,Dropdown,MultiSelect,RadioButton,Checkbox
]

export interface TableConfig {
    editableL?: boolean; //是否可编辑
    scrollable?: boolean;//是否滚动
    showToggle?:boolean;//是否显示 切换列
    dataKey?:string;//数据唯一属性
    disabledKey?:string;//是否禁用选择
    marginHeight?:number;//margin 或padding 引起的高度差
}

export interface ColsConfig {
    field?: string;
    header?: string;
    sortable?: any;
    editable?: boolean;
    rowspan?: number;
    colspan?: number;
    style?: any;
    required?: any;
    hidden?: boolean;
    frozen?: boolean;
    width?: string;
    placeholder?: string;
    dic?:string;
    status?:any[];
    number?:string;
    date?:string;
    selectionMode?:string;
}

export interface ToolBarConfig {
    label?:string;//按钮文本
    icon?:string;//按钮图标
    permission?:string;//按钮权限
    class?:string;//按钮样式
    callBack?:Function;//按钮点击事件
}

export interface ContentSearch {
    title?:string;//label
    component?:string;//组件
    justValue?:boolean;//下拉只绑定某一个 属性
    dataKey?:string;//身份证
    optionLabel?:string;//下拉绑定显示属性
    class?:string;//组件样式
    options?:any[];//组件需要的 数据
    placeholder?:string;//组件展位符
    keyWord?:string;//搜索属性
    dataType?:string;
    maxKey?:string;
    minKey?:string;
    maxlength?:number;
}



export interface Search {
    headerName?:string;
    headerKeyword?:string;
    contents?:ContentSearch[];
    hasSearch?:boolean;//是否显示搜索
    hasAdvanceSearch?:boolean;//是否有高级搜索
}

export interface SelectButton {
    canDelete?:boolean;
    keyWord?:string;
}
