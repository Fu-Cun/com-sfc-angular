export const environment = {
  production: true,
  envName:'production',
  baseUrl:'https://cms.sunf.top/',
  wsUrl:'https://cms.sunf.top/'
};
